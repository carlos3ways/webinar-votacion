/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-web
 */

$(document).ready(function() {
    if (!isBrowserCompatible()) {
        window.location.href = '/streaming-error'
        return;
    }

    checkSource(function(id) {
        var data = { };

        if (id) {
            data = {
                user_agent: window.navigator.userAgent,
                fuente_id: id
            };
        }
        
        $.ajax({
            type: 'POST',
            url: '/api/users_tiempos',
            data: data,
            dataType: 'json',
            success: function(data) {
                if (!data.success)
                    return;
                
                changeIframeSource('unregister-time', ('/storage/unregister.html?payload=' + data.payload));
            }
        });
    });

    // if (($('#video-area').data('id') == 2) && isIE()) {
    //     $('#change_source-modal').modal('show');
    //     $('#change_source-modal').on('shown.bs.modal', function() {
    //         requestFuente(1);
    //     });
    // }
});

var delegatedVote = false;
var voted = false;
var userId = $('#user-id').val();
var currentQuestion = null;
var isVotingActive = false;
var sourceInterval = setInterval(checkSource, 10000);
window.onunload = unregisterTime;

function unregisterTime() {
    changeIframeSource('unregister-time', '');
    return null;
}

function changeIframeSource(id, source) {
    var iframe = document.getElementById(id);
    if (!iframe)
        return;

    iframe.src = source;
}

function checkSource(callback) {
    var id = null;

    $.ajax('/storage/fuentes.html?' + Date.now(), {
        success: /* async */ function (data) {
            data = JSON.parse(data);

            var disableInterval = data.find(function(source) {
                return (source.idControlFuente == 6 && source.activo == 1);
            });

            if (disableInterval) {
                clearInterval(sourceInterval);
                sourceInterval = null;

                return;
            }
            
            var payload = data.find(function(source) {
                return (source.idControlFuente != 6 && source.tipo != 'voting' && source.activo == 1);
            });

            var activeVoting = (delegatedVote ? null : data.find(function(source) {
                return (source.tipo == 'voting' && source.activo == 1);
            }));

            // if (payload.tipo != 'vimeo' && !isIE())
            //     changeSource(payload);

            var alternativePayload = data.find(function(source) {
                return source.idControlFuente == payload.alternative_id;
            });

            var source = ((alternativePayload && !isBrowserCompatible()) ? alternativePayload : payload);
            changeSource(source);
            id = source.idControlFuente;

            if (activeVoting) {
                if (currentQuestion && currentQuestion.slug != activeVoting.slug) {
                    isVotingActive = false;
                    voted = false;
                }
                
                /* await */ $.ajax({
                    url: ('/evotacion/api/pregunta.php?uid=' + userId),
                    dataType: 'json',
                    headers: {
                        'Cache-Control': 'no-store, no-cache, must-revalidate',
                        Pragma: 'no-cache'
                    },
                    success: function(data) {
                        if (!data || !data.success) {
                            isVotingActive = false;
                            activeVoting = null;
                            hideVoting();
                            return;
                        }

                        if (data.data.delegado) {
                            delegatedVote = true;
                            return;
                        }

                        if (currentQuestion && currentQuestion.pregunta.id != data.data.pregunta.id) {
                            isVotingActive = false;
                            voted = false;
                        }
                        
                        currentQuestion = data.data;
                        currentQuestion.slug = activeVoting.slug;

                        // if (!currentQuestion.answered && voted) {
                        //     voted = false;
                        //     isVotingActive = false;
                        // }
                        
                        // if (activeVoting.slug == 'poll' && (currentQuestion.answered && !voted)) {
                        //     isVotingActive = false;
                        //     activeVoting = null;
                        //     hideVoting();
                        // } else
                        
                        showVoting(activeVoting);
                        changeChatStatus(payload, (activeVoting ? true : false));
                    }
                });
            } else {
                isVotingActive = false;
                changeChatStatus(payload, false);
            }
        },
        complete: function() {
            if (callback) {
                callback(id);
            }
        }
    });
}

function changeSource(payload) {
    if (!payload)
        return;
    
    if (payload.idControlFuente != (document.getElementById('video-area').dataset ? document.getElementById('video-area').dataset.id : $('#video-area').data('id'))) {
        try {
            document.getElementById('video-area').dataset.id = payload.idControlFuente;
        } catch (e) {
            $('#video-area').data('id', payload.idControlFuente);
        }

        if (payload.tipo == 'image') {
            $('#video').attr('src', '');
            
            var content = payload.source;
            if(payload.audio)
                content += '<audio autoplay><source src="' + payload.audio + '" type="audio/mpeg"></audio>';
                
            $('#video').addClass('d-none');
            $('.content-imagen').removeClass('d-none');
            $('#source-image').attr('src', payload.source);
        } else {
            $('#video').removeClass('d-none');
            $('.content-imagen').addClass('d-none');
            $('#source-image').attr('src', '');
            
            var content = payload.source;
            if(payload.audio)
                content += '<audio autoplay><source src="' + payload.audio + '" type="audio/mpeg"></audio>';
            
            $('#video').attr('src', payload.source);
        }
    }
}

function changeChatStatus(payload, isVoting/*  = false */) {
    if (!payload)
        return;
    
    var isChatEnabled = (isVoting || (((payload.chat && payload.chat == 1) && payload.chat_source)) || false);
    if (isChatEnabled != $('#chat-area').data('enabled')) {
        $('#chat-area').data('enabled', isChatEnabled);
        $('#video-area').attr('class', ('source-area ' + (isChatEnabled ? 'col-12 col-lg-8' : 'col-12 no-chat')));
        
        if (isChatEnabled) {
            $('#chat').attr('src', (isVoting ? '' : payload.chat_source));
            $('#chat-area').removeClass('d-none');
            
            if (isVoting) {
                $('#chat-area').removeClass('source-area');
                $('#chat').addClass('d-none');
            }
            else if (!$('#chat-area').hasClass('source-area')) {
                $('#chat-area').addClass('source-area');
                $('#chat').removeClass('d-none');
            }
        } else {
            $('#chat').attr('src', '');
            $('#chat-area').addClass('d-none');
        }
    }
}

// function requestFuente(id) {
//     if (!id)
//         return;
    
//     $.ajax({
//         url: '/api/control_fuentes/' + id,
//         dataType: 'json',
//         success: function (data) {
//             if (!data.success)
//                 return;
            
//             changeSource(data.payload);
//             changeChatStatus(data.payload, false);
//         }
//     });
// }

function hideVoting() {
    $('#chat-area').data('enabled', false);
    $('#video-area').attr('class', 'source-area col-12 no-chat');
    
    $('#chat').attr('src', '');
    $('#chat-area').addClass('d-none');
}

function showVoting(voting) {
    if (!voting || isVotingActive)
        return;
    
    isVotingActive = true;
    document.getElementById('voting').dataset.id = 0;
    $('.select-vote.not-selected', document).removeClass('not-selected');
    $('.select-vote.selected', document).removeClass('selected');
    $('.select-vote.disabled', document).removeClass('disabled');
    $('#vote-title').html('');
    $('#vote-result-title').html('');
    $('#poll').addClass('d-none');
    $('#poll-results').addClass('d-none');
    $('.loading-area').fadeIn();
    $('input[name="respuesta"]').prop('disabled', false);
    $('#submit-vote').fadeIn();

    if (voting.slug == 'poll') {
        $('#poll').removeClass('d-none');

        if (!currentQuestion || !currentQuestion.pregunta)
            return;
        
        document.getElementById('voting').dataset.id = currentQuestion.pregunta.id;
        $('#vote-title').html(currentQuestion.pregunta.pregunta);
        $('#submit-vote').prop('disabled', true);
        $('.loading-area').fadeOut();

        if (currentQuestion.answered !== false && currentQuestion.answered >= 0) {
            $('input[name="respuesta"]:checked').prop('checked', false);
            $('.select-vote[data-id="' + currentQuestion.answered + '"]').addClass('selected');
            $('.select-vote:not(.selected)', document).addClass('disabled');
            $('input[name="respuesta"]').prop('disabled', true);
            $('#submit-vote').fadeOut();
        }
    } else {
        $('#poll-results').removeClass('d-none');

        $.ajax({
            url: '/evotacion/api/resultados.php',
            dataType: 'json',
            success: function(data) {
                if (!data || !data.success)
                    return;
                
                var pregunta = data.data.pregunta;
                var votos = data.data.votos;

                var aprobar = votos.filter(function(voto) { return voto.id_respuesta == 0; });
                var desestimar = votos.filter(function(voto) { return voto.id_respuesta == 1; });
                var abstencion = votos.filter(function(voto) { return voto.id_respuesta == 2; });
                var userVote = votos.find(function(voto) { return voto.id_usuario == userId; });

                document.getElementById('voting').dataset.id = pregunta.id;
                $('#vote-result-title').html(pregunta.pregunta);
                $('.total-answers').text(votos.length);
                $('#aprobar-count').text(aprobar.length);
                $('#desestimar-count').text(desestimar.length);
                $('#abstencion-count').text(abstencion.length);
                $('.progress-bar').removeClass('bg-transparent bg-success bg-dismiss bg-abstinence text-muted');
                $('.loading-area').fadeOut(function() {
                    var aprobarPecentage = (((aprobar.length * 100) / votos.length) || 0).toFixed(2);
                    $('#aprobar-progress').addClass((aprobarPecentage > 0) ? 'bg-success' : 'bg-transparent text-muted');
                    $('#aprobar-progress').text(aprobarPecentage + '%');
                    $('#aprobar-progress').css('width', ((aprobarPecentage > 0) ? aprobarPecentage : 15) + '%');

                    var desestimarPecentage = (((desestimar.length * 100) / votos.length) || 0).toFixed(2);
                    $('#desestimar-progress').addClass((desestimarPecentage > 0) ? 'bg-dismiss' : 'bg-transparent text-muted');
                    $('#desestimar-progress').text(desestimarPecentage + '%');
                    $('#desestimar-progress').css('width', ((desestimarPecentage > 0) ? desestimarPecentage : 15) + '%');

                    var abstencionPecentage = (((abstencion.length * 100) / votos.length) || 0).toFixed(2);
                    $('#abstencion-progress').addClass((abstencionPecentage > 0) ? 'bg-abstinence' : 'bg-transparent text-muted');
                    $('#abstencion-progress').text(abstencionPecentage + '%');
                    $('#abstencion-progress').css('width', ((abstencionPecentage > 0) ? abstencionPecentage : 15) + '%');
                });
            }
        });
    }
}

$('.select-vote', document).on('click', function() {
    if ($(this).hasClass('selected') || $(this).hasClass('disabled'))
        return;
    
    $('.select-vote.not-selected', document).removeClass('not-selected');
    $('.select-vote.selected', document).removeClass('selected');
    $(this).addClass('selected');
    $('.select-vote:not(.selected)', document).addClass('not-selected');
    $('#submit-vote').prop('disabled', false);
});

$('#submit-vote', document).on('click', function() {
    var submit = confirm('Va a proceder a votar, una vez enviado no podrá modificarlo');
    if (!submit)
        return;
    
    var questionId = document.getElementById('voting').dataset.id;
    if (!questionId)
        return;
    
    var response = $('input[name="respuesta"]:checked').val();
    if (!response)
        return;
    
    $('#submit-vote').prop('disabled', true);
    voted = true;
    $.ajax({
        url: '/evotacion/api/respuesta.php',
        type: 'POST',
        data: {
            usuario: userId,
            pregunta: questionId,
            respuesta: response
        },
        success: function(data) {
            if (!data || !data.success)
                return;
            
            $('#saved_vote-modal').modal('show');
            $('input[name="respuesta"]:checked').prop('checked', false);
            $('.select-vote:not(.selected)', document).addClass('disabled');
            $('#submit-vote').fadeOut();
            $('input[name="respuesta"]').prop('disabled', true);
        }
    });
});
