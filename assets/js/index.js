/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-web
 */

$(document).ready(function() {
    let submittingLogin = false;

    // Login button action
    if (document.getElementById('login-button'))
        document.getElementById('login-button').addEventListener('click', function() {
            $('#login-modal').modal('show');
        });

    // Login action
    if (document.getElementById('login-form'))
        document.getElementById('login-form').addEventListener('submit', function(e) {
            e.preventDefault();

            if (submittingLogin)
                return;
            
            const email = document.getElementById('input-email');
            const password = document.getElementById('input-password');

            const emailFeedback = email.nextElementSibling;
            const passwordFeedback = password.nextElementSibling;

            if (!email.value || !password.value) {
                if (!email.value) {
                    email.classList.add('is-invalid');
                    emailFeedback.innerText = 'El campo de email no puede estar vacio';
                } else {
                    email.classList.remove('is-invalid');
                    emailFeedback.innerText = '';
                }

                if (!password.value) {
                    password.classList.add('is-invalid');
                    passwordFeedback.innerText = 'El campo de contraseña no puede estar vacio';
                } else {
                    password.classList.remove('is-invalid');
                    passwordFeedback.innerText = '';
                }

                return;
            }

            submittingLogin = true;
            $.ajax({
                type: 'POST',
                url: '/api/auth/signin',
                data: {
                    email: email.value,
                    password: password.value
                },
                dataType: 'json',
                success: function(data) {
                    if (data.success) {
                        window.location.href = data.sendTo;
                        return;
                    }

                    if (data.errors.email) {
                        email.classList.add('is-invalid');
                        data.errors.email.forEach(function(error) {
                            emailFeedback.innerHTML = '<p>' + error + '</p>';
                        })
                    } else {
                        email.classList.remove('is-invalid');
                        emailFeedback.innerText = '';
                    }

                    if (data.errors.password) {
                        password.classList.add('is-invalid');
                        data.errors.password.forEach(function(error) {
                            passwordFeedback.innerHTML = '<p>' + error + '</p>';
                        })
                    } else {
                        password.classList.remove('is-invalid');
                        passwordFeedback.innerText = '';
                    }
                    
                    submittingLogin = false;
                },
                complete: function() {
                    submittingLogin = false;
                }
            });
        });
});
