$(document).ready(function() {
    setInterval(function() {
        var fuenteId = $('#video-area').data('id');
        
        $.ajax({
            type: 'POST',
            url: '/api/users_intervals',
            data: {
              user_agent: window.navigator.userAgent,
              fuente_id: fuenteId
            },
            dataType: 'json'
        });
    }, (1000 * 60 * 5)); // milisec (1000) * sec (60) * min
});