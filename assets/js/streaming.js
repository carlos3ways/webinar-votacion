/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-web
 */

// Reload button action 
document.getElementById('reload-button').addEventListener('click', function() {
    location.reload()
});

// Char counter
var maxChars = 450;
var textareas = document.getElementsByClassName('char-counter');

for (var i = 0; i < textareas.length; i++) {
    var textarea = textareas[i];
    var chars = textarea.nextElementSibling.nextElementSibling;

    chars.getElementsByTagName('span')[1].innerText = maxChars;
    textarea.setAttribute('maxlength', maxChars);
    textarea.addEventListener('keyup', function(e) {
        if (textarea.value.length > maxChars) {
            e.preventDefault();
            chars.style.color = 'red';
        } else {
            chars.style.color = ((textarea.value.length == maxChars) ? 'red' : 'black');
            chars.getElementsByTagName('span')[0].innerText = textarea.value.length;
        }
    })
}

// Edit/Remove questions
var sendQuestionArea = document.getElementById('send-question-area');
var editQuestionArea = document.getElementById('edit-question-area');
var inputEditQuestion = document.getElementById('input-edit-question');

// Edit question
function editQuestionListener() {
    var editButtons = document.getElementsByClassName('edit-question');
    for (var i = 0; i < editButtons.length; i++) {
        var editButton = editButtons[i];
        
        editButton.addEventListener('click', function() {
            sendQuestionArea.style.display = 'none';
            editQuestionArea.style.display = 'block';
            
            inputEditQuestion.value = document.getElementById('question-' + editButton.dataset.id).innerText;
            inputEditQuestion.dataset.id = editButton.dataset.id;
            inputEditQuestion.nextElementSibling.nextElementSibling.getElementsByTagName('span')[0].innerText = inputEditQuestion.value.length;
        });
    }
}

// Remove question
function removeQuestionListener() {
    var removeButtons = document.getElementsByClassName('remove-question');
    for (var i = 0; i < removeButtons.length; i++) {
        var removeButton = removeButtons[i];

        removeButton.addEventListener('click', function() {
            $.ajax({
                type: 'DELETE',
                url: '/api/questions/' + this.dataset.id,
                dataType: 'json',
                success: function(data) {
                    if (!data.success)
                        return;
                    
                    loadQuestions();
                }
            });
        });
    }
}

// Cancel edit question
document.getElementById('cancel-question').addEventListener('click', clearEditArea);

// Edit question action
document.getElementById('send-edit-question').addEventListener('click', function() {
    var inputEditQuestionFeedback = inputEditQuestion.nextElementSibling;

    if (!inputEditQuestion.value || inputEditQuestion.value.length > maxChars) {
        if (!inputEditQuestion.value) {
            inputEditQuestion.classList.add('is-invalid');
            inputEditQuestionFeedback.innerText = 'El campo de pregunta no puede estar vacio';
        } else if (inputEditQuestion.value.length > maxChars) {
            inputEditQuestion.classList.add('is-invalid');
            inputEditQuestionFeedback.innerText = 'El texto es demasiado largo'; 
        }

        return;
    } else {
        inputEditQuestion.classList.remove('is-invalid');
        inputEditQuestionFeedback.innerText = '';
    }

    $.ajax({
        type: 'PUT',
        url: '/api/questions/' + inputEditQuestion.dataset.id,
        data: {
            question: inputEditQuestion.value
        },
        dataType: 'json',
        success: function(data) {
            if (!data.success) {
                if (data.errors.message) {
                    inputEditQuestion.classList.add('is-invalid');
                    data.errors.message.forEach(function(error) {
                        inputEditQuestionFeedback.innerHTML = '<p>' + error + '</p>';
                    })
                } else {
                    inputEditQuestion.classList.remove('is-invalid');
                    inputEditQuestionFeedback.innerText = '';
                }

                return;
            }
            
            document.getElementById('question-' + inputEditQuestion.dataset.id).innerText = inputEditQuestion.value;
            clearEditArea();
            loadQuestions();
        }
    });
});

// Make question
var inputQuestion = document.getElementById('input-question');
document.getElementById('send-question').addEventListener('click', function() {
    var inputQuestionFeedback = inputQuestion.nextElementSibling;
    if (!audioFile) {
        if (!inputQuestion.value || inputQuestion.value.length > maxChars) {
            if (!inputQuestion.value) {
                inputQuestion.classList.add('is-invalid');
                inputQuestionFeedback.innerText = 'El campo de pregunta no puede estar vacio';
            } else if (inputQuestion.value.length > maxChars) {
                inputQuestion.classList.add('is-invalid');
                inputQuestionFeedback.innerText = 'El texto es demasiado largo'; 
            }

            return;
        } else {
            inputQuestion.classList.remove('is-invalid');
            inputQuestionFeedback.innerText = '';
        }
    }

    var fileName = new Date().toISOString();
    var formData = new FormData();
    formData.append('question', (audioFile ? '/preguntas-audios/' + formatDate() + '/' + fileName + '.mp3' : inputQuestion.value));
    if (audioFile) {
        formData.append('audio', audioFile);
        formData.append('audio_name', fileName);
    }

    $.ajax({
        type: 'POST',
        url: '/api/questions',
        data: formData,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function(data) {
            if (!data.success) {
                if (data.errors.message) {
                    inputQuestion.classList.add('is-invalid');
                    data.errors.message.forEach(function(error) {
                        inputQuestionFeedback.innerHTML = '<p>' + error + '</p>';
                    })
                } else {
                    inputQuestion.classList.remove('is-invalid');
                    inputQuestionFeedback.innerText = '';
                }

                if (data.errors.audio)
                    alert(data.errors.audio);

                return;
            }
            
            inputQuestion.value = '';
            inputQuestion.nextElementSibling.nextElementSibling.getElementsByTagName('span')[0].innerText = 0;

            if (audioFile)
                closePlayer();

            loadQuestions();
        }
    });
});

function formatDate() {
    var date = new Date();

    var month = (date.getMonth() + 1).toString();
    var day = date.getDate().toString();
    var year = date.getFullYear().toString();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;
    
    return [ year, month, day ].join('-');
}

function clearEditArea() {
    editQuestionArea.style.display = 'none';
    sendQuestionArea.style.display = 'block';
    inputEditQuestion.value = '';
    inputEditQuestion.dataset.id = 0;

    inputEditQuestion.classList.remove('is-invalid');
    inputEditQuestion.nextElementSibling.innerText = '';
}

function loadQuestions() {
    var questionsArea = document.getElementById('questions-area');
    
    $.ajax({
        type: 'GET',
        url: '/api/questions/',
        dataType: 'json',
        success: function(data) {
            if (!data.success)
                return;
                
            questionsArea.innerHTML = '';
            data.data.forEach(function(question, key) {
                var content = '<div class="col-12 mt-2">';
                content += '<div class="question-box' + (((key + 1) % 2 == 0) ? ' no-bg' : '') + '" id="question-box-' + question.idpregunta + '">';
                content += '<div class="row">';

                if (question.pregunta.includes('preguntas-audios') && question.pregunta.includes('.mp3')) {
                    content += '<div class="' + ((question.estado == 0) ? 'col-10 col-md-11' : 'col-12') + ' d-flex">';
                    content += '<span>' + (key + 1) + '. </span>';
                    content += '<audio controls src="/storage/' + question.pregunta + '" class="w-100 ml-2"></audio>';
                    content += '</div>';

                    if (question.estado == 0) {
                        content += '<div class="col-2 col-md-1 text-right">';
                        content += '<i class="fa fa-times px-2 cursor-pointer remove-question" data-id="' + question.idpregunta + '"></i>';
                        content += '</div>';
                    }
                } else {
                    content += '<div class="' + ((question.estado == 0) ? 'col-8 col-md-10' : 'col-12') + '">';
                    content += '<span>' + (key + 1) + '. </span>';
                    content += '<span id="question-' + question.idpregunta + '">' + question.pregunta + '</span>';
                    content += '</div>';

                    if (question.estado == 0) {
                        content += '<div class="col-4 col-md-2 text-right">';
                        content += '<i class="fa fa-pencil px-2 cursor-pointer edit-question" data-id="' + question.idpregunta + '"></i>';
                        content += '<i class="fa fa-times px-2 cursor-pointer remove-question" data-id="' + question.idpregunta + '"></i>';
                        content += '</div>';
                    }
                }

                content += '</div>';
                content += '</div>';
                content += '</div>';
                
                questionsArea.innerHTML += content;
            });

            editQuestionListener();
            removeQuestionListener();
        }
    });
}

// Record question functions
var recordButton = document.getElementById('record-voice');
var recordVoiceArea = document.getElementById('record-voice-area');
var recordingArea = document.getElementById('recording-area');
var sendButtonArea = document.getElementById('send-button-area');
var questionTextArea = document.getElementById('question-textarea');
var actionsArea = document.getElementById('actions-area');
var recordTimer = document.getElementById('record-timer');
var questionRecord = document.getElementById('question-record');
var recordingList = document.getElementById('recording-list');
var AudioContext = (window.AudioContext || window.webkitAudioContext);
var timeRecord = 20000;

var audioFile;
URL = window.URL || window.webkitURL;

var timer = (timeRecord / 1000);
var gumStream;
var rec;
var input;
var audioContext;
var stopTimeout;
var recordingTimerInterval;

function recordingTimer() {
    recordTimer.innerText = (timer - 1);
    timer--;
}

function closePlayer() {
    audioFile = null;

    questionRecord.classList.add('d-none');
    questionTextArea.className = 'col-12 col-lg-7 col-md-5 mt-2';
    actionsArea.className = 'col-12 col-lg-5 col-md-7 justify-content-center align-self-start justify-content-md-end mt-2 d-flex';
    questionTextArea.classList.remove('d-none');
    inputQuestion.disabled = false;
}

function startRecording() {
    var constraints = {
        audio: true,
        video:false
    };
    
    questionRecord.classList.add('d-none');
    recordVoiceArea.classList.add('d-none');
    recordingArea.classList.remove('d-none');
    sendButtonArea.classList.add('d-none');
    questionTextArea.className = 'col-12 col-lg-10 col-md-9 mt-2';
    actionsArea.className = 'col-12 col-lg-2 col-md-3 justify-content-center align-self-start justify-content-md-end mt-2 d-flex';
    inputQuestion.disabled = true;

    navigator.mediaDevices.getUserMedia(constraints).then(function(stream) {
        recordingTimerInterval = setInterval(recordingTimer, 1000);
        stopTimeout = setTimeout(stopRecording, timeRecord);
        audioContext = new AudioContext();
        gumStream = stream;
        input = audioContext.createMediaStreamSource(stream);
        rec = new Recorder(input, {
            numChannels:1
        });
        
        rec.record()
    }).catch(function(err) {
        recordVoiceArea.classList.remove('d-none');
        recordingArea.classList.add('d-none');
        sendButtonArea.classList.remove('d-none');
        questionTextArea.className = 'col-12 col-lg-7 col-md-5 mt-2';
        actionsArea.className = 'col-12 col-lg-5 col-md-7 justify-content-center align-self-start justify-content-md-end mt-2 d-flex';
        inputQuestion.disabled = false;
    });
}

function stopRecording() {
    clearTimeout(stopTimeout);
    clearInterval(recordingTimerInterval);
    timer = (timeRecord / 1000);
    recordTimer.innerText = timer;

    actionsArea.className = 'col-12 col-lg-5 col-md-7 justify-content-center align-self-start justify-content-md-end mt-2 d-flex';
    recordingArea.classList.add('d-none');
    questionTextArea.classList.add('d-none');
    recordVoiceArea.classList.remove('d-none');
    sendButtonArea.classList.remove('d-none');
    questionRecord.classList.remove('d-none');
    
    rec.stop();
    gumStream.getAudioTracks()[0].stop();
    rec.exportWAV(createDownloadLink, 'audio/mpeg');
}

function createDownloadLink(blob) {
    audioFile = blob;

    var url = URL.createObjectURL(blob);
    var au = document.createElement('audio');

    au.controls = true;
    au.src = url;
    au.style.width = '100%';
    
    recordingList.innerHTML = '';
    recordingList.appendChild(au);
}

var voiceQuestionsInfo = document.getElementById('voice-questions-info');
if (voiceQuestionsInfo) {
    recordTimer.innerText = timer;
    voiceQuestionsInfo.addEventListener('click', function() {
        $('#voice_questions-modal').modal('show');
    });

    document.getElementById('stop-recording').addEventListener('click', stopRecording);
    document.getElementById('close-player').addEventListener('click', closePlayer);

    if (isIE()) {
        recordButton.addEventListener('click', function(){
            $('#compatibility-modal').modal('show');
        });
    } else
        recordButton.addEventListener('click', startRecording);
}

editQuestionListener();
removeQuestionListener();