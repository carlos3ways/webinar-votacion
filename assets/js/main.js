/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-web
 */

$(document).ready(function() {
    var cookieBox = document.getElementById('cookie-box');
    if (cookieBox) {
        document.getElementById('close-cookies').addEventListener('click', function() {
            createCookie('cookie-banner', '1');
            cookieBox.style.display = 'none';
        });

        $.ajax({
            type: 'get',
            url: '/api/cookies/cookie-banner',
            dataType: 'json',
            success: function(data) {
                if (!data.exists || data.value != 1)
                    cookieBox.style.display = 'block';
            }
        });   
    }
    
    var disabledLinks = document.querySelectorAll('a.disabled');
    for (var i = 0; i < disabledLinks.length; i++) {
        var disabledLink = disabledLinks[i];
        var imageElement = disabledLink.querySelector('img');
        
        if (imageElement) {
            imageElement.src = grayScale(imageElement);
            imageElement.classList.remove('button');
        }
        
        disabledLink.href = '';
        disabledLink.addEventListener('click', function(e) {
            e.preventDefault();
        });
    }
});

function createCookie(cname, cvalue) {
    $.ajax({
        type: 'post',
        url: '/api/cookies',
        data: {
            cname: cname,
            cvalue: cvalue
        },
        dataType: 'json',
        success: function(data) {
            if (!data.success)
                console.log('Error al guardar cookie');
            else
                console.log('Cookie guardada correctamente: ' + cname);
        }
    });
}

function isIE() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf('MSIE ');
    
    return ((msie > 0) || !!navigator.userAgent.match(/Trident.*rv\:11\./));
}

function isBrowserCompatible() {
    // Chrome 30 en adelante (tiene actualizaciones automáticas)
    // Firefox 27 en adelante (tiene actualizaciones automáticas)
    // Internet Explorer 11 en adelante *
    // Microsoft Edge
    // Safari 9 en adelante
    // -----------------------------------------------------------
    // *No puedes reproducir videos en Vimeo usando las versiones antiguas de Internet Explorer (IE 10, 9, 8, etc.) ya que no son compatibles con el protocolo TLS. 1.2 ni con HTML5. A partir del 12 de enero de 2016, Microsoft dejará de actualizar estas versiones de Internet Explorer
    
    var isCompatible = false;
    var browser = getBrowser();

    if (!browser) {
        return isCompatible;
    }

    // console.log('Browser: ' + browser.name);
    // console.log('Browser version: ' + browser.version);

    switch (browser.name) {
        case 'chrome': { // Chrome and Microsoft Edge
            isCompatible = (browser.version > 29);
            break;
        }

        case 'firefox': {
            isCompatible = (browser.version > 26);
            break;
        }

        case 'trident': {
            isCompatible = (browser.version > 6);
            break;
        }

        case 'msie': {
            isCompatible = (browser.version > 10);
            break;
        }

        case 'safari': {
            isCompatible = (browser.version > 8);
            break;
        }
    }

    return isCompatible;
}

function getBrowser() {
    var userAgent = window.navigator.userAgent.toLowerCase();
    var browserData = userAgent.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    var browser = [];

    if (browserData[1] === 'chrome') {
        browser = userAgent.match(/\bopr|edge\/(\d+)/)

        if (browser != null) {
            return {
                name: 'opera',
                version: parseInt(browser[1])
            };
        }
    }

    browserData = browserData[2] ? [ browserData[1], browserData[2] ] : [ navigator.appName, navigator.appVersion, '-?' ];

    if ((browser = userAgent.match(/version\/(\d+)/i)) != null) {
        browserData.splice(1, 1, browser[1]);
    }

    return {
        name: browserData[0],
        version: browserData[1]
    };
}

function grayScale(imgObj) {
    var canvas = document.createElement('canvas');
    var canvasContext = canvas.getContext('2d');
    var imgW = imgObj.width;
    var imgH = imgObj.height;

    canvas.width = imgW;
    canvas.height = imgH;
    canvasContext.drawImage(imgObj, 0, 0);
    
    var imgPixels = canvasContext.getImageData(0, 0, imgW, imgH);
     
    for(var y = 0; y < imgPixels.height; y++)
        for(var x = 0; x < imgPixels.width; x++) {
            var i = ((y * 4) * imgPixels.width + x * 4);
            var avg = ((imgPixels.data[i] + imgPixels.data[i + 1] + imgPixels.data[i + 2]) / 3);

            imgPixels.data[i] = avg; 
            imgPixels.data[i + 1] = avg; 
            imgPixels.data[i + 2] = avg;
        }

    canvasContext.putImageData(imgPixels, 0, 0, 0, 0, imgPixels.width, imgPixels.height);
    return canvas.toDataURL();
}

// Polyfills
//-----------------------------------
// includes
if (!String.prototype.includes) {
    String.prototype.includes = function() {
        'use strict';

        return String.prototype.indexOf.apply(this, arguments) !== -1;
    };
}

// find
if (!Array.prototype.find) {
    Object.defineProperty(Array.prototype, 'find', {
        value: function(predicate) {
            if (this == null)
                throw new TypeError('"this" is null or not defined');
  
            var o = Object(this);
            var length = (o.length >>> 0);

            if (typeof predicate !== 'function')
                throw new TypeError('predicate must be a function');
            
            var arg = arguments[1];
            var k = 0;
            
            while (k < length) {
                var kValue = o[k];
                if (predicate.call(arg, kValue, k, o))
                    return kValue;
                
                k++;
            }

            return undefined;
        },
        configurable: true,
        writable: true
    });
}

// reduce
if (!Array.prototype.reduce) {
    Array.prototype.reduce = function(fun /*, inicial*/) {
        if (typeof fun != "function")
            throw new TypeError();

        var length = this.length;
        if (length == 0 && arguments.length == 1)
            throw new TypeError();

        var index = 0;
        var rv = null;
        if (arguments.length >= 2)
            rv = arguments[1];
        else {
            do {
                if (index in this) {
                    rv = this[index++];
                    break;
                }

                if (++index >= length)
                    throw new TypeError();
            } while (true);
        }

        for (; index < length; index++) {
            if (index in this)
                rv = fun.call(null, rv, this[index], index, this);
        }

        return rv;
    };
}

// filter
if (!Array.prototype.filter) {
    Array.prototype.filter = function(func, thisArg) {
        'use strict';

        if (!((typeof func === 'Function') && this))
            throw new TypeError();

        var length = (this.length >>> 0);
        var result = new Array(length);
        var c = 0;
        var i = -1;

        if (thisArg === undefined) {
            while (++i !== length) {
                if (i in this) {
                    if (func(t[i], i, t))
                        result[c++] = t[i];
                }
            }
        } else {
            while (++i !== length) {
                if (i in this) {
                    if (func.call(thisArg, t[i], i, t))
                        result[c++] = t[i];
                }
            }
        }

        result.length = c;
        return result;
    };
}

// map
if (!Array.prototype.map) {
    Array.prototype.map = function(callback, thisArg) {
        if (this == null)
            throw new TypeError('this is null or not defined');
        else if (typeof callback !== 'function')
            throw new TypeError(callback + ' is not a function');
        
        var O = Object(this);
        var length = (O.length >>> 0);
        var T = ((arguments.length > 1) ? thisArg : null);
        
        var A = new Array(length);
        var k = 0;

        while (k < length) {
            if (k in O) {
                A[k] = callback.call(T, O[k], k, O);
            }
            
            k++;
        }
        
        return A;
    };
}