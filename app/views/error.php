{-- @author Alexis Bogado <alexis.bogado@s3w.es> --}
{-- @package webinar-web --}

<?php header("HTTP/1.1 404 Not Found"); ?>

@add('base')

@content('contents')
    @add('includes.header')

    <div class="container my-5">
        <div class="row">
            <div class="col-12">
                <p>
                    <a href="{{ config('app.url') }}">< Volver al inicio</a>
                </p>
                <h3 class="font-weight-bold text-dark-green text-uppercase">Error 404</h3>
            </div>

            <div class="col-12 my-5">
                <h5 class="text-light-green font-weight-bold">¡Página no encontrada!</h5>
                <p>
                    Lo sentimos, la página que estaba buscando no está disponible<br />
                </p>
            </div>
        </div>
    </div>
@endcontent