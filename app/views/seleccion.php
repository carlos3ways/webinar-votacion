{-- @author Alexis Bogado <alexis.bogado@s3w.es> --}
{-- @package webinar-web --}

@add('base')

@content('styles')
    <link rel="stylesheet" href="{{ config('app.url') }}/assets/css/seleccion.css?{{ time() }}">
@endcontent

@content('contents')
    @add('includes.header')

    <div class="container mt-4 mb-5">
        <div class="row">
            <div class="col-12 mb-4">
                <div class="row">
                    <div class="col-12 col-xl-5 pr-0 text-center">
                        <b class="text-light-green fs-17">Créditos ECTS por la Universidad Europea de Madrid:</b>
                    </div>
                    
                    <div class="col-12 col-xl-7 p-xl-0 text-center">
                        <b class="text-dark-green fs-17 bullet-before">
                            <span class="">10 Créditos Fase On-line</span>
                        </b>
                        <br class="d-xl-none" />
                        <b class="text-dark-green fs-17 bullet-before ml-xl-3">
                            <span class="">1 Crédito adicional por asistencia a fase presencial</span>
                        </b>
                    </div>
                </div>
            </div>
            
            <div class="col-12 col-lg-5">
                <img src="{{ config('app.url') }}/assets/images/reunion2.png" alt="Reunion 2" class="img-fluid d-block mx-auto" />
                
                <h6 class="mt-2 text-uppercase font-weight-bold text-dark-green">Más allá de las complicaciones ateroscleróticas en la diabetes mellitus tipo 2: Manejo avanzado de la insuficiencia cardiaca</h6>

                <div class="px-1 mt-4">
                    <div class="form-group line-height-1">
                        <b class="text-light-green">Abordaje de la IC desde las etapas iniciales en el paciente con DM2</b><br />
                        <b class="text-dark-green">Dr. Alberto Calderón Montero.</b><br />
                        <span class="text-dark-green">Médico de Familia. CS Pedro Laín Entralgo, Alcorcón, Madrid.</span>
                    </div>

                    <div class="form-group line-height-1">
                        <b class="text-light-green">Fundamentos prácticos en ecocardiografía</b><br />
                        <b class="text-dark-green">Dr. Marcos García Aguado.</b><br />
                        <span class="text-dark-green">Cardiólogo. Hospital Universitario Puerta de Hierro, Majadahonda, Madrid.</span>
                    </div>

                    <div class="form-group line-height-1">
                        <b class="text-light-green">Sesión de interpretación práctica de la ecocardiografía en Atención Primaria</b><br />
                        <b class="text-dark-green">Dr. Carlos Escobar Cervantes.</b><br />
                        <span class="text-dark-green">Cardiólogo. Hospital Universitario de La Paz, Madrid.</span>
                    </div>

                    <div class="form-group mt-4 mt-lg-0 position-lg-absolute bottom-0 w-100">
                        <a href="{{ route('streaming')->path }}" title="Acceder al Webinar">
                            <img src="{{ config('app.url') }}/assets/images/accederwebinar.png" alt="Acceder al Webinar" class="img-fluid button d-block mx-auto" />
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-lg-2 my-5 my-lg-0 d-flex justify-content-center align-items-center">
                <div class="h-separator v-lg-separator"></div>
            </div>
                        
            <div class="col-12 col-lg-5">
                <img src="{{ config('app.url') }}/assets/images/reunion3.png" alt="Reunion 3" class="img-fluid d-block mx-auto" />
                
                <h6 class="mt-2 text-uppercase font-weight-bold">Situaciones especiales en el paciente con DM2. Futuro inmediato en el tratamiento de la diabetes mellitus tipo 2</h6>

                <div class="px-1 mt-4">
                    <div class="form-group line-height-1">
                        <b class="text-light-green">Sesión 1: La importancia del control metabólico en la prevención de las complicaciones cardiovasculares del diabético</b><br />
                        <b class="text-dark-green">Dra. Alba Galdón Sanz-Pastor.</b><br />
                        <span class="text-dark-green">Endocrino. Hospital Universitario Gregorio Marañón, Madrid.</span>
                    </div>

                    <div class="form-group line-height-1">
                        <b class="text-light-green">Sesión 2: ¿Función renal normal o hiperfiltración?</b><br />
                        <b class="text-dark-green">Dr. José Antonio García Donaire.</b><br />
                        <span class="text-dark-green">Nefrólogo. Hospital Clínico San Carlos, Madrid.</span>
                    </div>

                    <div class="form-group line-height-1">
                        <b class="text-light-green">Sesión 3: Manejo del paciente diabético tras un síndrome coronario agudo</b><br />
                        <b class="text-dark-green">Dr. Carlos Escobar Cervantes.</b><br />
                        <span class="text-dark-green">Cardiólogo. Hospital Universitario de La Paz, Madrid.</span>
                    </div>

                    <div class="form-group line-height-1">
                        <b class="text-light-green">Interpretación avanzada del electrocardiograma en el paciente diabético</b><br />
                        <b class="text-dark-green">Dr. Carlos Escobar Cervantes.</b><br />
                        <span class="text-dark-green">Cardiólogo. Hospital Universitario de La Paz, Madrid.</span><br />
                        <b class="text-dark-green">Dr. Alberto Calderón Montero.</b><br />
                        <span class="text-dark-green">Médico de Familia. CS Pedro Laín Entralgo, Alcorcón, Madrid.</span>
                    </div>

                    <div class="form-group mt-4">
                        <a href="{{ route('streaming')->path }}" title="Acceder al Webinar" class="disabled">
                            <img src="{{ config('app.url') }}/assets/images/accederwebinar.png" alt="Acceder al Webinar" class="img-fluid button d-block mx-auto" />
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endcontent

@content('scripts')
    <script src="{{ config('app.url') }}/assets/js/icons.min.js?{{ time() }}"></script>
@endcontent