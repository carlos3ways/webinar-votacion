{-- @author Alexis Bogado <alexis.bogado@s3w.es> --}
{-- @package webinar-web --}

@add('base')

@content('contents')
    @add('includes.header')

    <div class="container mt-4 mb-5">
        <div class="row">
            <div class="col-12">
                <p>
                    <a href="{{ config('app.url') }}">< Volver al inicio</a>
                </p>
                
                <h2 class="font-weight-bold text-uppercase text-dark-green">Política de cookies</h2>
            </div>

            <div class="col-12 mt-3">
                <h4 class="font-weight-bold text-light-green">1. Utilización de cookies</h4>

                <p>La página Web utiliza cookies y otros mecanismos similares (en adelante, cookies). Las cookies son ficheros enviados a un navegador por medio de un servidor web para registrar las actividades del usuario en una web determinada o en todas las webs, apps y/o servicios de nuestra Web (en adelante, los servicios). La primera finalidad de las cookies es la de facilitar al usuario un acceso más rápido a los servicios seleccionados. Además, las cookies personalizan los servicios que ofrecen la página Web, facilitando y ofreciendo a cada usuario información que es de su interés o que puede ser de su interés, en atención al uso que realiza de los servicios.</p>

                <p>La página Web utiliza cookies para personalizar y facilitar al máximo la navegación del usuario. Las cookies se asocian únicamente a un usuario anónimo y a su ordenador, y no proporcionan referencias que permitan deducir datos personales del usuario. Éste podrá configurar su navegador para que notifique y rechace la instalación de las cookies enviadas por la página Web, sin que ello perjudique la posibilidad del usuario de acceder a los contenidos. Sin embargo, se hace notar que, en todo caso, la calidad de funcionamiento de la página Web puede disminuir.</p>

                <p>El usuario registrado que se registre o que inicie sesión, podrá beneficiarse de unos servicios más personalizados y orientados a su perfil, gracias a la combinación de los datos almacenados en las cookies con los datos personales utilizados en el momento de su registro. Dicho usuario autoriza expresamente el uso de esta información con la finalidad indicada, sin perjuicio de su derecho a rechazar o deshabilitar el uso de cookies.</p>

                <p>Asimismo, la página Web podrá saber todos los servicios solicitados por el usuario de forma que podrán facilitar u ofrecer información adecuada a los gustos y preferencias de cada usuario.</p>
            </div>
            
            <div class="col-12 mt-3">
                <h4 class="font-weight-bold text-light-green">2. Tipología, finalidad y funcionamiento de las Cookies</h4>

                <p>Las cookies, en función de su permanencia, pueden dividirse en cookies de sesión o permanentes. Las primeras expiran cuando el usuario cierra el navegador. Las segundas expiran en función de cuando se cumpla el objetivo para el que sirven (por ejemplo, para que el usuario se mantenga identificado en los servicios) o bien cuando se borran manualmente.</p>

                <p>Adicionalmente, en función de su objetivo, las cookies pueden clasificarse de la siguiente forma:</p>

                <p>COOKIES DE RENDIMIENTO: Este tipo de Cookies recuerda sus preferencias para las herramientas que se encuentran en los servicios, por lo que no tiene que volver a configurar el servicio cada vez que se visita la página. A modo de ejemplo, en esta tipología se incluyen:</p>

                <ul>
                    <li>Ajustes de volumen de reproductores de vídeo o sonido.</li>
                    <li>Las velocidades de transmisión de vídeo que sean compatibles con su navegador.</li>
                    <li>Los objetos guardados en el carrito de la compra en los Servicios de e-commerce tales como tiendas.</li>
                </ul>

                <p>COOKIES DE GEO-LOCALIZACIÓN: Estas cookies se utilizan para averiguar en qué país se encuentra cuando se solicita un servicio. Esta cookie es totalmente anónima y sólo se utiliza para ayudar a orientar el contenido a su ubicación.</p>

                <p>COOKIES DE REGISTRO: Las cookies de registro se generan una vez que el usuario se ha registrado o posteriormente ha abierto su sesión, y se utilizan para identificarle en los servicios con los siguientes objetivos:</p>

                <ul>
                    <li>Mantener al usuario identificado de forma que, si cierra un servicio, el navegador o el ordenador y en otro momento u otro día vuelve a entrar en dicho servicio, seguirá identificado, facilitando así su navegación sin tener que volver a identificarse. Esta funcionalidad se puede suprimir si el usuario pulsa la funcionalidad ‘cerrar sesión’, de forma que esta cookie se elimina y la próxima vez que entre en el servicio, el usuario tendrá que iniciar sesión para estar identificado.</li>
                    <li>Comprobar si el usuario está autorizado para acceder a ciertos servicios, por ejemplo, para participar en un concurso.</li>
                </ul>

                <p>Adicionalmente, algunos servicios pueden utilizar conectores con redes sociales tales como Facebook o Twitter. Cuando el usuario se registra en un servicio con credenciales de una red social, autoriza a la red social a guardar una cookie persistente que recuerda su identidad y le garantiza acceso a los servicios hasta que expira. El usuario puede borrar esta cookie y revocar el acceso a los servicios mediante redes sociales actualizando sus preferencias en la red social que específica.</p>

                <p>COOKIES DE ANALÍTICAS: Cada vez que un usuario visita un servicio, una herramienta de un proveedor externo genera una cookie analítica en el ordenador del usuario. Esta cookie, que sólo se genera en la visita, servirá en próximas visitas a los servicios para identificar de forma anónima al visitante. Los objetivos principales que se persiguen son:</p>

                <ul>
                    <li>Permitir la identificación anónima de los usuarios navegantes a través de la cookie (identifica navegadores y dispositivos, no personas) y por lo tanto la contabilización aproximada del número de visitantes y su tendencia en el tiempo.</li>
                    <li>Identificar de forma anónima los contenidos más visitados y, por lo tanto, más atractivos para los usuarios.</li>
                    <li>Saber si el usuario que está accediendo es nuevo o repite visita.</li>
                </ul>

                <p>Importante: salvo que el usuario decida registrarse en un servicio, la cookie nunca irá asociada a ningún dato de carácter personal que pueda identificarle. Dichas cookies sólo serán utilizadas con propósitos estadísticos que ayuden a la optimización de la experiencia de los usuarios en el sitio.</p>

                <p>COOKIES DE PUBLICIDAD: Este tipo de cookies permiten ampliar la información de los anuncios mostrados a cada usuario anónimo en los servicios. Entre otros, se almacena la duración o frecuencia de visualización de posiciones publicitarias, la interacción con las mismas, o los patrones de navegación y/o comportamientos del usuario ya que ayudan a conformar un perfil de interés publicitario. De este modo, permiten ofrecer publicidad afín a los intereses del usuario.</p>
            </div>
            
            <div class="col-12 mt-3">
                <h4 class="font-weight-bold text-light-green">3. Cómo deshabilitar las cookies y los Web bugs en los principales navegadores</h4>

                <p>Normalmente es posible dejar de aceptar las cookies del navegador, o dejar de aceptar las cookies de un servicio en particular.</p>

                <p>Todos los navegadores modernos permiten cambiar la configuración de cookies. Estos ajustes normalmente se encuentran en las ‘opciones’ o ‘preferencias’ del menú de su navegador. Asimismo, puede configurar su navegador o su gestor de correo electrónico, así como instalar complementos gratuitos para evitar que se descarguen los Web bugs al abrir un email.</p>

                <p>La página Web ofrece orientación al usuario sobre los pasos para acceder al menú de configuración de las cookies y, en su caso, de la navegación privada en cada uno de los navegadores principales:</p>

                <ul>
                    <li>
                        <b>Internet Explorer</b>: Herramientas-> Opciones de Internet-> Privacidad-> Configuración.<br />
                        Para más información, puede consultar el soporte de Microsoft o la Ayuda del navegador.
                    </li>

                    <li>
                        <b>Firefox</b>: Herramientas-> Opciones-> Privacidad-> Historial-> Configuración Personalizada.<br />
                        Para más información, puede consultar el soporte de Mozilla o la Ayuda del navegador.
                    </li>

                    <li>
                        <b>Chrome</b>: Configuración-> Mostrar opciones avanzadas-> Privacidad-> Configuración de contenido.<br />
                        Para más información, puede consultar el soporte de Google o la Ayuda del navegador.
                    </li>

                    <li>
                        <b>Safari</b>: Preferencias-> Seguridad.<br />
                        Para más información, puede consultar el soporte de Apple o la Ayuda del navegador.
                    </li>
                </ul>
            </div>
            
            <div class="col-12 mt-3">
                <h4 class="font-weight-bold text-light-green">4. Qué ocurre si se deshabilitan las cookies</h4>

                <p>Algunas funcionalidades de los servicios quedarán deshabilitados como, por ejemplo, permanecer identificado, mantener las compras en el “carrito de la compra” en un servicio de e-commerce, recibir información dirigida a su localización o la visualización de algunos vídeos.</p>
            </div>

            <div class="col-12 mt-3">
                <h4 class="font-weight-bold text-uppercase text-light-green">Actualizaciones y cambios en la política de privacidad/cookies</h4>

                <p>La página Web puede modificar esta Política de Cookies en función de las exigencias legislativas, reglamentarias, o con la finalidad de adaptar dicha política a las instrucciones dictadas por la Agencia Española de Protección de Datos. Por ello, se aconseja a los usuarios que la visiten periódicamente.</p>

                <p>Cuando se produzcan cambios significativos en esta Política de Cookies, se comunicarán a los usuarios bien mediante la web o bien a través de correo electrónico a los usuarios registrados.</p>
            </div>
        </div>
    </div>
@endcontent