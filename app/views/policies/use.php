{-- @author Alexis Bogado <alexis.bogado@s3w.es> --}
{-- @package webinar-web --}

@add('base')

@content('contents')
    @add('includes.header')

    <div class="container mt-4 mb-5">
        <div class="row">
            <div class="col-12">
                <p>
                    <a href="{{ config('app.url') }}">< Volver al inicio</a>
                </p>
                
                <h2 class="font-weight-bold text-uppercase text-dark-green">Condiciones de Uso</h2>
            </div>

            <div class="col-12 mt-3">
                <p>Sociedad Española de Médicos de Atención Primaria (en adelante, SEMERGEN), es la titular del dominio en Internet "semergen.es" bajo el que figura la página web <a href="www.semergen.es" title="SEMERGEN">www.semergen.es</a></p>
            </div>

            <div class="col-12 mt-3">
                <h4 class="font-weight-bold text-uppercase text-light-green">Información legal</h4>

                <p>Según establece la Ley 34 / 2002, de 11 de julio, de Servicios de la Sociedad de la Información y de Comercio Electrónico, B.O.E. nº 166, se ofrece a todos los usuarios y visitantes la información legal relativa a la sociedad propietaria del portal ubicado en la dirección de internet <a href="www.semergen.es" title="SEMERGEN">www.semergen.es</a></p>

                <p>
                    <span>Razón social Sociedad Española de Médicos de Atención Primaria (SEMERGEN)</span><br />
                    <span>País de residencia España</span>
                </p>

                <p>
                    <span>Domicilio Narváez 15 - 1º izda . 28009- MADRID</span><br />
                    <span>Inscripción en el Registro Mercantil Inscrita en el Registro Nacional de Asociaciones Nº 12321</span><br />
                    <span>C.I.F. G-28628402</span><br />
                    <span>Contacto Tfno (34) 91 500 21 71 Tfno Movil: 606 54 86 89 - Fax (34) 91 431 06 11</span><br />
                    <span>
                        <a href="secretaria@semergen.es" title="Mail secretaría SEMERGEN">secretaria@semergen.es</a>
                    </span>
                </p>
            </div>
            
            <div class="col-12 mt-3">
                <h4 class="font-weight-bold text-light-green">1. Contenido del sitio web <a href="www.semergen.es" title="SEMERGEN" class="text-light-green">www.semergen.es</a></h4>

                <p>Este sitio web, cuyo único propietario y patrocinador es SEMERGEN, tiene por objeto fundamental proporcionar a los médicos generalistas y de familia, las herramientas adecuadas para la mejora continua de su ejercicio profesional. Junto a ello, la promoción de SEMERGEN, los servicios que presta y las actividades que realiza, por lo que la información contenida en él no constituye una oferta vinculante.</p>

                <p>SEMERGEN se reserva el derecho a realizar cambios en el sitio web sin previo aviso, con el objeto de actualizar, corregir, modificar, añadir o eliminar sus contenidos o su diseño. Los contenidos del sitio web se actualizan periódicamente. Debido a que la actualización de la información no es inmediata, le sugerimos que compruebe siempre la vigencia y exactitud de la información contenida en el sitio web.</p>
            </div>
            
            <div class="col-12 mt-3">
                <h4 class="font-weight-bold text-light-green">2. Normas generales de acceso y uso correcto del sitio web</h4>

                <p>El acceso y uso de este sitio web están sujetos a los términos de esta información legal, sin perjuicio de que el acceso a alguno de los servicios pueda precisar la aceptación de unas condiciones particulares.</p>

                <p>Cualquier uso que haga del sitio web o de los servicios en él incluidos implicará la aceptación de los términos legales de este Aviso Legal.</p>

                <p>El acceso al sitio web y el uso correcto de la información contenida en el mismo son responsabilidad de quien realiza estas acciones, no siendo responsable SEMERGEN por el uso incorrecto, ilícito o negligente que del mismo pudiere hacer el usuario.</p>

                <p>SEMERGEN no puede asumir responsabilidad alguna respecto al uso o acceso que realicen los usuarios fuera del ámbito al que se dirige el sitio web, ni de las consecuencias que pudieran traer por causa de la falta de utilidad, adecuación o validez del sitio web y/o de sus servicios o contenidos para satisfacer necesidades, actividades o resultados concretos o expectativas de los usuarios, ni de los resultados que pueda acarrear la aplicación práctica de las opiniones, recomendaciones o estudios a que se pueda acceder a través del sitio web, cuya responsabilidad final recaerá siempre sobre el usuario.</p>

                <p>El usuario se compromete a utilizar el sitio web, los contenidos o servicios de conformidad con la ley, las presentes condiciones generales y las condiciones particulares que puedan mostrarse de cada uno de los servicios. De la misma forma el usuario se obliga a no utilizar el sitio web o los servicios que se ofrecen con fines o efectos ilícitos, contrarios al contenido de estas condiciones generales, lesivos de los derechos e intereses de SEMERGEN o de terceros, o que de cualquier forma puedan dañar, inutilizar, sobrecargar o deteriorar el sitio web o impedir la normal utilización o disfrute del sitio web por otros usuarios.</p>

                <p>El usuario deberá abstenerse de obtener informaciones, mensajes, gráficos, dibujos, archivos de sonido y/o imagen, fotografías, grabaciones, software y, en general, cualquier clase de material accesible a través del sitio web, empleando para ello medios distintos de los que se hayan puesto a su disposición, o en general, de los que se empleen habitualmente en Internet.</p>

                <p>El usuario deberá abstenerse de manipular datos identificativos de SEMERGEN, o servicios del espacio web <a href="https://www.semergen.es/" title="SEMERGEN">https://www.semergen.es/</a></p>
                
                <p>El usuario deberá abstenerse de facilitar datos propios que no permitan una correcta identificación.</p>

                <p>El usuario deberá abstenerse de manipular los dispositivos técnicos de protección de contenidos o de información, y en general de configuración del espacio web <a href="https://www.semergen.es/" title="SEMERGEN">https://www.semergen.es/</a></p>

                <p>SEMERGEN se reserva el derecho a denegar o retirar el acceso a la página web <a href="https://www.semergen.es/" title="SEMERGEN">https://www.semergen.es/</a> en cualquier momento y sin necesidad de preaviso a aquellos usuarios que incumplan estas condiciones generales o las condiciones particulares que resulten de la aplicación a cada servicio en concreto.</p>
            </div>
            
            <div class="col-12 mt-3">
                <h4 class="font-weight-bold text-light-green">3. Derechos de autor, propiedad intelectual e industrial</h4>

                <p>Todos los contenidos del sitio web <a href="https://www.semergen.es/" title="SEMERGEN">https://www.semergen.es/</a>, son de propiedad exclusiva y para todo el mundo, de SEMERGEN, incluyendo, sin limitación, diseño gráfico, logos, imágenes, textos, ilustraciones, fotografías, audiovisuales, marcas registradas y otros signos distintivos, en cualquiera de los lenguajes de programación utilizados o utilizables, así como todo el desarrollo del sitio web. Queda prohibida cualquier modificación, copia, alquiler, préstamo, transmisión y difusión no autorizada. La reproducción, distribución, comunicación al público, cesión y cualquier otro acto o modalidad de difusión y explotación del material de este sitio web que no haya sido expresamente autorizado por SEMERGEN quedan expresamente prohibidos. En cualquier caso, si se ofrece al usuario, la descarga de documentos en formato electrónico para su utilización pública, se ruega el indicar el origen de la fuente y su autoría.</p>

                <p>SEMERGEN no concede ninguna licencia o autorización de uso de ninguna clase sobre sus derechos de propiedad intelectual e industrial o sobre cualquier otra propiedad o derecho relacionado con el sitio web.</p>
            </div>

            <div class="col-12 mt-3">
                <h4 class="font-weight-bold text-light-green">4. Aviso a pacientes y/o familiares</h4>

                <p>La información de contenido médico que pueda estar disponible en este sitio web no debe utilizarse para diagnosticar o tratar problema alguno. SEMERGEN excluye cualquier responsabilidad por los daños y perjuicios de toda naturaleza que puedan deberse a la falta de veracidad, exactitud, exhaustividad y/o actualidad de los contenidos del sitio web <a href="https://www.semergen.es/" title="SEMERGEN">https://www.semergen.es/</a> salvo los que le sean directamente imputables. Si tiene o sospecha la existencia de un problema de salud consulte a su médico de cabecera.</p>
            </div>

            <div class="col-12 mt-3">
                <h4 class="font-weight-bold text-light-green">5. Hipervínculos</h4>

                <p>Los links externos que se muestran en esta web se disponen como cortesía a los usuarios y visitantes de este sitio web, y son meramente informativos. Los enlaces entre el sitio web <a href="https://www.semergen.es/" title="SEMERGEN">https://www.semergen.es/</a> y otros sitios web, no implican relación alguna entre SEMERGEN y la compañía u organización a la que enlazan, ni constituyen ningún tipo de acuerdo, contrato, patrocinio, recomendación, ofrecimiento o invitación de compra a la compañía u organización enlazada, ni un respaldo a dicha compañía u organización.</p>

                <p>Queda fuera de nuestra responsabilidad el contenido, exactitud u objetivo de otros websites; los links son suministrados por SEMERGEN como cortesía a los usuarios - visitantes del sitio web <a href="https://www.semergen.es/" title="SEMERGEN">https://www.semergen.es/</a> y no siendo SEMERGEN responsable de los eventuales cambios en los sitios web con los que podamos establecer un link. No se permitirá el enlace de ninguna página web o de una dirección de correo electrónico al sitio web, salvo con la autorización expresa por escrito de SEMERGEN que podrá ser revocada en cualquier momento.</p>
            </div>
            
            <div class="col-12 mt-3">
                <h4 class="font-weight-bold text-light-green">6. Limitación de garantías y responsabilidades sobre virus y fallos tecnológicos</h4>

                <p>SEMERGEN no controla ni garantiza la ausencia de virus ni de otros elementos en los contenidos que puedan producir alteraciones en el sistema informático del usuario (software y hardware), documentos electrónicos y ficheros almacenados en su sistema informático. Por ello, SEMERGEN no se hace responsable de cualesquiera daños o perjuicios que sufra el usuario por la existencia de virus u otros elementos en el sitio web. SEMERGEN no puede garantizar la ausencia de fallos tecnológicos, ni la permanente disponibilidad del sitio web y de los servicios contenidos en él y, en consecuencia, no asume responsabilidad alguna por los daños y perjuicios que puedan generarse por la falta de disponibilidad y fallos en el acceso. SEMERGEN no puede garantizar que las transmisiones de información a través de Internet sean totalmente seguras, por cuanto intervienen múltiples factores fuera del control de SEMERGEN, entre otros la propia configuración informática del usuario, la seguridad aplicada a las redes públicas de telecomunicaciones, etc. SEMERGEN no se hace responsable de cualesquiera daños o perjuicios que sufra el usuario por posibles contingencias en la transmisión de información, tales como pérdida de datos o accesos no autorizados de terceras personas.</p>
            </div>

            <div class="col-12 mt-3">
                <h4 class="font-weight-bold text-light-green">7. Ley aplicable y sumisión a fuero</h4>

                <p>Cualquier controversia relativa a la condiciones de uso y acceso a este sitio web contenidas en la presente Información Legal, así como cualquier relación entre usted como Usuario - Visitante y SEMERGEN, se regirá por la legislación española, renunciando expresamente las partes al fuero que les corresponda, y sometiéndose a los Juzgados y Tribunales de España.</p>

                <p>
                    <h5 class="font-weight-bold text-light-green">&copy; SEMERGEN. Todos los derechos reservados.</h5>
                </p>
            </div>
        </div>
    </div>
@endcontent