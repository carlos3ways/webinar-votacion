{-- @author Alexis Bogado <alexis.bogado@s3w.es> --}
{-- @package webinar-web --}

@add('base')

@content('contents')
    @add('includes.header')

    <div class="container mt-4 mb-5">
        <div class="row">
            <div class="col-12">
                <p>
                    <a href="{{ config('app.url') }}">< Volver al inicio</a>
                </p>
                
                <h2 class="font-weight-bold text-uppercase text-dark-green">Política de Privacidad y Protección de Datos</h2>
            </div>

            <div class="col-12 mt-3">
                <h4 class="font-weight-bold text-light-green">1. Derecho de información</h4>

                <p>La Política de Privacidad de la Sociedad Española de Médicos de Atención Primaria (SEMERGEN), se basa, entre otras normas, en lo dispuesto por el Reglamento (UE) 2016/679,  relativo a la protección de los datos personales de las personas físicas (RGPD), y regula el uso de los servicios desarrollados por su organización, su oferta formativa y docente, y todas aquellas actividades relacionadas con el cumplimiento sus objetivos estatutarios, focalizados en la promoción de la especialidad de Medicina Familiar y Comunitaria en sus diferentes ámbitos de actuación a partir de la web <a href="https://www.semergen.es/" title="SEMERGEN">https://www.semergen.es/</a>, y sus correspondientes enlaces corporativos, así como a través de otros sistemas de tratamiento alternativos como el soporte papel.</p>
            </div>
            
            <div class="col-12 mt-3">
                <h4 class="font-weight-bold text-light-green">2. ¿Quién es el Responsable del Tratamiento de sus datos personales?</h4>

                <p>De conformidad con lo dispuesto en el RGPD, le informamos que la información personal que facilite quedará debidamente registrada e incorporada a los sistemas de tratamiento de datos responsabilidad de la SEMERGEN, domiciliada provisionalmente en C/ Narváez 15-1º izq, 28009 – Madrid, <a href="https://www.semergen.es/" title="SEMERGEN">https://www.semergen.es/</a>, email de contacto <a href="mailto:secretaria@semergen.es" title="Mail secretaría SEMERGEN">secretaria@semergen.es</a>.</p>
            </div>
            
            <div class="col-12 mt-3">
                <h4 class="font-weight-bold text-light-green">3. ¿Con qué finalidad se tratarán sus datos?</h4>

                <p>La finalidad del tratamiento de los datos personales que haya facilitado a SEMERGEN a través de sus diferentes canales de comunicación u obtención de datos personales, dependerá del motivo por el que nos los haya facilitado:</p>

                <ul>
                    <li>Tratamiento relativo a la gestión de los socios de la SEMERGEN, para las distintas actividades en las que participen como, por ejemplo, la inscripción en los Congresos Nacionales anuales o la participación en cursos organizados por la SEMERGEN.</li>

                    <li>Tratamiento relativo a la gestión de proveedores y colaboradores de la SEMERGEN, los cuales prestan servicios profesionales a la SEMERGEN.</li>

                    <li>Tratamiento relativo al sistema de videovigilancia de las instalaciones de la SEMERGEN, cuya finalidad es la vigilancia, seguridad y control de acceso a la institución.</li>

                    <li>Tratamiento relativo a contactos institucionales, cuya finalidad es la gestión de los datos personales de las personas registradas en los sistemas de información de la SEMERGEN para la gestión de contactos con motivo de convocatorias de eventos, comunicaciones, proyectos y notificaciones relacionadas con la actividad de la SEMERGEN.</li>

                    <li>Tratamiento relativo a la gestión de Recursos Humanos, cuya finalidad es la gestión del personal, nóminas, formación, prevención de riesgos laborales, enfermedades profesionales, accidentes de trabajo.</li>

                    <li>Tratamiento de los datos de los candidatos, con el fin de valorar y gestionar su solicitud de empleo y en su caso, llevar a cabo las actuaciones necesarias para la selección y contratación de personal, a fin de ofertarle puestos que se ajusten a tu perfil.</li>

                    <li>Tratamiento de datos para contactar con el remitente de la información, dar respuesta a su solicitud, petición o consulta y gestionar la publicación de consultas y comentarios y hacer un seguimiento posterior.</li>

                    <li>Remitir periódicamente comunicaciones comerciales sobre servicios, eventos y noticias relacionadas con nuestra actividad profesional (por ejemplo, la newsletter), salvo que se indique lo contrario o se oponga o revoque su consentimiento.</li>
                </ul>

                <p>Los datos personales facilitados se conservarán mientras dure su relación con la SEMERGEN, manteniéndose más allá cuando por imperativo legal se establezca. Igualmente, le comunicamos que sus datos personales no serán utilizados por la SEMERGEN para la elaboración de perfiles comerciales. El tratamiento de su información personal se llevará a cabo de forma lícita, leal, transparente, adecuada, pertinente, limitada, exacta y actualizada. Es por ello que la SEMERGEN se compromete a adoptar todas las medidas razonables para que estos datos se supriman o rectifiquen sin dilación cuando sean inexactos.</p>

                <p>Asimismo, el usuario garantiza que los datos aportados son verdaderos, exactos, completos y actualizados, siendo responsable de cualquier daño o perjuicio, directo o indirecto, que pudiera ocasionarse como consecuencia del incumplimiento de tal obligación. En caso de que el usuario facilite datos de terceros, manifiesta contar con el consentimiento de los mismos y se compromete a trasladarle la información contenida en esta cláusula, eximiendo a la SEMERGEN de cualquier responsabilidad en este sentido. No obstante, la SEMERGEN podrá llevar a cabo las verificaciones para constatar este hecho, adoptando las medidas de diligencia debida que correspondan, conforme a la normativa de protección de datos.</p>
            </div>
            
            <div class="col-12 mt-3">
                <h4 class="font-weight-bold text-light-green">4. Medidas de Seguridad</h4>

                <p>Teniendo en cuenta el estado de la técnica, los costes de aplicación, y la naturaleza, el alcance, el contexto y la finalidad descrita en el punto 3 de las presentes condiciones de privacidad, así como los riesgos de probabilidad y gravedad variables para los derechos y libertades de los usuarios, la SEMERGEN, en cumplimiento con lo dispuesto por el 32.1. del RGPD, ha aplicado medidas técnicas y organizativas apropiadas para garantizar un nivel de seguridad adecuado, que en su caso incluye, entre otros: medidas para garantizar la confidencialidad, integridad, disponibilidad y resiliencia permanentes de los sistemas y servicios de tratamiento, medidas para restaurar la disponibilidad y el acceso a los datos personales de forma rápida en caso de incidente físico o técnico y la implantación de un proceso de verificación, evaluación y valoración regulares de la eficacia de las medidas técnicas y organizativas para garantizar la seguridad del tratamiento.</p>
            </div>

            <div class="col-12 mt-3">
                <h4 class="font-weight-bold text-light-green">5. ¿Cuál es la legitimación para el tratamiento de sus datos?</h4>

                <p>La base legal del tratamiento de sus datos personales reside en la gestión de la solicitud que en cada caso usted haya remitido a la SEMERGEN, la propia relación contractual que mantenga con la institución, su consentimiento para participar en las finalidades de tratamiento recogidas en el punto 3, o aquellas situaciones en las que la legitimación para el tratamiento de sus datos personales por parte de la SEMERGEN venga dispuesta por una ley o concurra un interés legítimo.</p>
            </div>

            <div class="col-12 mt-3">
                <h4 class="font-weight-bold text-light-green">6. ¿A qué destinatarios se comunicarán sus datos?</h4>

                <p>La SEMERGEN no tiene previsto realizar ninguna cesión o transferencia a terceros de la información personal que obre en sus sistemas de tratamiento, salvo en aquellos casos en los usted lo autorice expresamente o venga exigido o permitido por una ley. En tal supuesto, la SEMERGEN se compromete, en su caso, a comunicar previamente a todos los usuarios la identidad de la entidad cesionaria, así como la finalidad que motive la misma para recabar su consentimiento.</p>

                <p>Asimismo, se informa que sus datos personales pueden ser accedidos por entidades o personas ajenas a la SEMERGEN, en aquellos casos en los que sea necesario para la prestación de un servicio profesional a la SEMERGEN. En este último caso, dicho acceso de información se llevará a cabo bajo la firma de un contrato de prestación de servicios en los términos y condiciones recogidos por el artículo 28 del RGPD. A este respecto, la SEMERGEN informa que no realiza trasferencias internacionales de sus datos personales. Sin embargo, para el supuesto de que en aquellos casos en los que la contratación de servicios con entidades ajenas implicara una trasferencia de datos a destinos fuera de la Espacio Económico Europeo, como por ejemplo Estados Unidos, donde la normativa en materia de protección de datos no es tan garantista como la normativa comunitaria, la SEMERGEN adoptará todas las medidas técnicas, organizativas y de seguridad impuestas por el RGPD.</p>
            </div>
            
            <div class="col-12 mt-3">
                <h4 class="font-weight-bold text-light-green">7. ¿Cuáles son sus derechos cuando nos facilita sus datos?</h4>

                <p>Usted tiene derecho a acceder a sus datos personales, así como a solicitar la rectificación de los datos inexactos o, en su caso, solicitar la supresión cuando entre otros motivos, los datos ya no sean necesarios para los fines que fueron recogidos. Usted podrá solicitar la limitación del tratamiento de sus datos, en cuyo caso, la SEMERGEN únicamente los conservará para el ejercicio o defensa de reclamaciones. Igualmente, usted podrá oponerse al tratamiento de sus datos, en cuyo caso, la SEMERGEN dejará de tratar los mismos, salvo que por motivos legítimos o por motivos de defensa ante posibles reclamaciones, no pueda acceder a su petición de oposición. Igualmente, usted podrá solicitar la portabilidad de sus datos, así como que el tratamiento de sus datos no se base en decisiones exclusivamente automatizadas.</p>

                <ul>
                    <li>Ejercicio del derecho de acceso</li>
                    <li>Ejercicio del derecho de rectificación</li>
                    <li>Ejercicio del derecho de supresión</li>
                    <li>Ejercicio del derecho oposición</li>
                    <li>Ejercicio del derecho de limitación del tratamiento</li>
                    <li>Ejercicio del derecho de portabilidad</li>
                    <li>Ejercicio del derecho a la oposición de la toma de decisiones automatizadas</li>
                </ul>

                <p>Para ejercer tales derechos, podrá remitirnos un escrito de solicitud a la dirección postal: “SEMERGEN, C/ Narváez 15-1º izq, 28009, Madrid” o, si así lo desea, podrá remitir un correo electrónico a la dirección <a href="mailto:secretaria@semergen.es" title="Mail secretaría SEMERGEN">secretaria@semergen.es</a> con el título “Ejercicio + el derecho que corresponda”. Recuerde siempre acompañar a su solicitud copia de su DNI.</p>

                <p>Asimismo, en cumplimiento de lo dispuesto por el RGPD, se informa que si así lo desea, podrá acudir a la Agencia Española de Protección de Datos (<a href="http://www.agpd.es/portalwebAGPD/index-ides-idphp.php" title="Agencia Española de Protección de Datos">http://www.agpd.es/portalwebAGPD/index-ides-idphp.php</a>) para obtener información adicional sobre sus derechos y/o para presentar una reclamación cuando no se hayan atendido debidamente los mismos.</p>

                <p>La SEMERGEN se reserva la facultad de modificar la presente Política de Privacidad y Protección de Datos conforme a las modificaciones normativas que vayan aconteciendo, así como a las líneas doctrinales marcadas por la Agencia Española de Protección de Datos. Todo lo cual, será debidamente modificado en la presente website al objeto de informar de lo mismo a todos los usuarios registrados, o interesados en los servicios y actividades de la SEMERGEN.</p>

                <p>Por último, la SEMERGEN tratará su información personal con total confidencialidad, no destinando la misma a finalidades distintas o incompatibles a las descritas en el contenido del presente comunicado sin solicitar previamente su consentimiento.</p>
            </div>

            <div class="col-12 mt-3">
                <h4 class="font-weight-bold text-light-green">8. Consentimiento del usuario</h4>

                <p>El envío de datos personales mediante el uso de los formularios electrónicos o en papel a la SEMERGEN o, en su caso, mensajes de correo electrónico u otro medio análogo, supone el consentimiento expreso del remitente al tratamiento de sus datos incluidos en los medios de comunicación relacionada con SEMERGEN y sus iniciativas (si así lo ha indicado en la solicitud). No obstante, usted podrá revocar el presente consentimiento en cualquier momento. Y ello, a través del ejercicio de los derechos descritos anteriormente en el punto 7 de la presente Política de Privacidad y Protección de Datos.</p>
            </div>
        </div>
    </div>
@endcontent