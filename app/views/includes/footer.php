{-- @author Alexis Bogado <alexis.bogado@s3w.es> --}
{-- @package webinar-web --}

<footer>
    <div class="row justify-content-center align-items-center">
        <div class="col-12 col-lg-2 col-sm-3 mb-4 mb-sm-0">
            <img src="{{ config('app.url') }}/assets/images/semergen_neg.png" alt="3 Ways" class="img-fluid mx-auto d-block" />
        </div>

        <div class="col-12 col-lg-9 col-sm-8">
            <div class="row justify-content-end m-0">
                <div class="col-12 col-lg-2 col-md-3 col-sm-4 px-0 text-center mb-1 mb-sm-0">
                    <a href="{{ route('cookies_policy')->path }}" target="_blank">Política de Cookies</a>
                </div>

                <div class="col-12 col-lg-3 col-md-4 col-sm-4 px-0 text-center mb-1 mb-sm-0">
                    <a href="{{ route('privacy_policy')->path }}" target="_blank">Política de protección de datos</a>
                </div>

                <div class="col-12 col-lg-2 col-md-3 col-sm-4 px-0 text-center mb-1 mb-sm-0">
                    <a href="{{ route('conditions_of_use')->path }}" target="_blank">Condiciones de Uso</a>
                </div>
            </div>
        </div>
    </div>

    <div class="offset-lg-1"></div>
</footer>

<?php if (config('app.cookies_alert')): ?>
<div id="cookie-box" class="text-white">
    <button type="button" id="close-cookies" class="close text-white">×</button>

    Utilizamos cookies propias y de terceros para mejorar nuestros servicios, analizar y personalizar tu navegación, mostrar publicidad y facilitarte publicidad relacionada con tus preferencias. Si sigues navegando por nuestra web, consideramos que aceptas su uso. Puedes cambiar la configuración u obtener más información <a href="{{ route('cookies_policy')->path }}" target="_blank" class="text-white"><u>aquí</u></a>
</div>
<?php endif; ?>