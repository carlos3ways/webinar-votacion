{-- @author Alexis Bogado <alexis.bogado@s3w.es> --}
{-- @package webinar-web --}

<div class="header bg-lightgray">
    <div class="row m-0 w-100 align-items-center p-3 position-absolute">
        <div class="col-12 col-sm-3">
            {--<img src="{{ config('app.url') }}/assets/images/logo-seimc.png" alt="Header" class="img-fluid mt-3 d-block mx-auto" style="height:80px" />--}
        </div>

        <div class="col-12 col-sm-6 my-2 text-center">
            {--<h2 class="font-weight-bold">Asamblea Virtual SEIMC</h2>
            <?php if (auth()->loggedIn()): ?>
                <b>{{ auth()->user()->name }}</b>
            <?php endif; ?>
            --}
        </div>

        <?php if (auth()->loggedIn()): ?>
        <div class="col-12 col-sm-3 text-center text-sm-right align-self-start">
            <a href="{{ route('logout')->path }}" title="Salir">
                <button class="btn btn-danger btn-sm mt-2 px-3" id="logout-button">Salir <i class="fas fa-sign-out ml-2"></i></button>
            </a>
        </div>
        <?php endif; ?>
    </div>
    
    <img src="{{ config('app.url') }}/assets/images/cabecera-webinar.png" alt="Imagen cabecera" class="img-fluid" />
</div>