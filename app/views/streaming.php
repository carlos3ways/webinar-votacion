{-- @author Alexis Bogado <alexis.bogado@s3w.es> --}
{-- @package webinar-web --}

@add('base')

@content('styles')
    <link rel="stylesheet" href="{{ config('app.url') }}/assets/css/streaming.css?{{ time() }}">
@endcontent

@content('contents')
    @add('includes.header')

    <div class="container my-5">
        <iframe id="unregister-time" class="d-none" frameborder="0"></iframe>
        <input type="hidden" id="user-id" value="{{ auth()->user()->id }}" />

        <div class="row">
            <div class="col-12">
                <div class="row mx-auto">
                    <div class="source-area{{ (($source->chat == 1) ? ' col-12 col-lg-8' : ' col-12 no-chat') }}" id="video-area" data-id="{{ $source->idControlFuente }}">
                        <iframe id="video" class="frame{{ (($source->tipo == 'image') ? ' d-none' : '') }}" src="{{ (($source->tipo == 'image') ? '' : $source->source) }}" frameborder="0" allow="autoplay; fullscreen" scrolling="no" allowfullscreen></iframe>
                        <div class="content-imagen{{ (($source->tipo == 'image') ? '' : ' d-none') }}">
                            <img src="{{ (($source->tipo == 'image') ? $source->source : '') }}" alt="Source image" id="source-image">
                        </div>
                        <?php if ($source->audio): ?>
                        <audio autoplay>
                            <source src="{{ $source->audio }}" type="audio/mpeg">
                        </audio>
                        <?php endif; ?>
                    </div>

                    <div class="col-12 col-lg-4 source-area{{ (($source->chat == 1) ? '' : ' d-none') }}" id="chat-area" data-enabled="<?= (($source->chat == 1) ? 'true' : 'false') ?>">
                        <iframe id="chat" class="frame" src="{{ $source->chat_source }}" frameborder="0" scrolling="no"></iframe>
                        <div class="content-imagen text-center my-3 my-lg-0 px-0 px-lg-2" id="voting" data-id="0">
                            <div class="loading-area">
                                <i class="fad fa-spinner-third fa-spin fa-3x text-light-green"></i>
                            </div>

                            <div class="row d-none" id="poll">
                                <div class="col-12 d-flex mb-2">
                                    <span class="text-dark-green" id="vote-title"></span>
                                </div>

                                <div class="col-12 my-2 d-flex align-items-end">
                                    <div class="row w-100">
                                        <div class="col-4 col-lg-12 col-xl-4">
                                            <label for="approve" data-id="0" class="btn btn-success btn-block btn-vote-option text-dark-green select-vote">A favor</label>
                                            <input type="radio" name="respuesta" value="0" id="approve" hidden>
                                        </div>
                                        
                                        <div class="col-4 col-lg-12 col-xl-4">
                                            <label for="dismiss" data-id="1" class="btn btn-danger btn-block btn-vote-option btn-vote-option-dismiss text-dark-green select-vote">En contra</label>
                                            <input type="radio" name="respuesta" value="1" id="dismiss" hidden>
                                        </div>
                                        
                                        <div class="col-4 col-lg-12 col-xl-4">
                                            <label for="abstinence" data-id="2" class="btn btn-secondary btn-block btn-vote-option btn-vote-option-abstinence text-dark-green select-vote">Abstención</label>
                                            <input type="radio" name="respuesta" value="2" id="abstinence" hidden>
                                        </div>

                                        <div class="col-12 mt-4">
                                            <button class="btn btn-success btn-send-vote font-weight-bold" id="submit-vote">Votar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row d-none" id="poll-results">
                                <div class="col-12 d-flex mb-2">
                                    <span class="text-dark-green" id="vote-result-title"></span>
                                </div>

                                <div class="col-12 my-2 d-flex align-items-end">
                                    <div class="row w-100">
                                        <div class="col-12">
                                            <b>A favor</b> <i class="fs-12"><span id="aprobar-count">0</span> de <span class="total-answers">0</span></i><br />

                                            <div class="progress voting-result-progress w-100">
                                                <div class="progress-bar bg-gray text-muted" id="aprobar-progress" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-12 mt-2">
                                            <b>En contra</b> <i class="fs-12"><span id="desestimar-count">0</span> de <span class="total-answers">0</span></i><br />
                                            
                                            <div class="progress voting-result-progress w-100">
                                                <div class="progress-bar bg-gray text-muted" id="desestimar-progress" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-12 mt-2">
                                            <b>Abstención</b> <i class="fs-12"><span id="abstencion-count">0</span> de <span class="total-answers">0</span></i><br />
                                            
                                            <div class="progress voting-result-progress w-100">
                                                <div class="progress-bar bg-gray text-muted" id="abstencion-progress" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 mt-4 mb-2">
                <div class="row align-items-end">
                    <div class="col-8">
                        <h5 class="font-weight-bold">Preguntas de {{ auth()->user()->name }}</h5>
                    </div>

                    <div class="col-4 text-right">
                        <button class="btn btn-primary" id="reload-button">Actualizar <i class="fas fa-sync-alt ml-2"></i></button>
                    </div>

                    <div class="col-12 mt-2 mb-1">
                        <hr>
                    </div>
                    
                    <div id="send-question-area" class="col-12">
                        <div class="row">

                            <div class="col-12 {{ (config('streaming.allow_voice_questions') ? 'col-lg-7 col-md-5' : 'col-lg-10 col-md-9') }} mt-2" id="question-textarea">
                                <textarea class="form-control w-100 char-counter" id="input-question" placeholder="Escribe aquí tu pregunta..."></textarea>
                                <div class="invalid-feedback"></div>

                                <span id="chars">
                                    <span id="char-count">0</span>/<span id="max-chars"></span> máximo de caracteres
                                </span>
                            </div>
                            
                            <?php if (config('streaming.allow_voice_questions')): ?>
                            <div class="col-12 col-lg-7 col-md-5 mt-2 d-none" id="question-record">
                                <div class="row">
                                    <div id="recording-list" class="col-11" style="padding-right: 1px;"></div> <!-- Fix it -->
                                    <div class="col-1" style="padding-left: 1px;"> <!-- Fix it -->
                                        <i id="close-player" class="fa fa-times-circle fa-2x cursor-pointer"></i>
                                    </div>
                                </div>
                            </div>
                            <?php endif; ?>
                            
                            <div class="col-12 {{ (config('streaming.allow_voice_questions') ? 'col-lg-5 col-md-7' : 'col-lg-2 col-md-3') }} justify-content-center align-self-start justify-content-md-end mt-2 d-flex" id="actions-area">
                                <?php if (config('streaming.allow_voice_questions')): ?>
                                <div class="px-2" id="record-voice-area">
                                    <button class="btn btn-danger" id="record-voice">
                                        <i class="fas fa-microphone"></i> Grabar pregunta
                                    </button>

                                    <i class="fas fa-info-circle fa-lg cursor-pointer ml-1 mr-2" id="voice-questions-info"></i>
                                </div>

                                <div class="d-none" id="recording-area">
                                    <button class="btn btn-danger btn-block" id="stop-recording">
                                        <i class="fas fa-stop"></i> Stop (<span id="record-timer">0</span> s)
                                    </button>
                                </div>
                                <?php endif; ?>

                                <div class="pl-2 align-self-end" id="send-button-area">
                                    <button class="btn btn-success px-3" id="send-question">Enviar <i class="fas fa-arrow-right ml-2"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div id="edit-question-area" class="col-12">
                        <div class="row">
                            <div class="col-12 col-lg-8 col-md-6 mt-2">
                                <textarea class="form-control w-100 char-counter" id="input-edit-question" data-id="0" placeholder="Escribe aquí tu pregunta..."></textarea>
                                <div class="invalid-feedback"></div>
    
                                <span id="chars">
                                    <span id="char-count">0</span>/<span id="max-chars"></span> máximo de caracteres
                                </span>
                            </div>
    
                            <div class="col-12 col-lg-4 col-md-6 mt-2 text-center text-md-right align-self-start">
                                <button class="btn btn-success px-3" id="send-edit-question">Enviar <i class="fas fa-arrow-right ml-2"></i></button>
                                <button class="btn btn-danger px-3 ml-3" id="cancel-question">Cancelar <i class="fas fa-times ml-2"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-12">
                <div id="questions-area" class="row">
                    <?php foreach ($preguntas as $key => $pregunta): ?>
                    <div class="col-12 mt-2">
                        <div class="question-box<?= ((($key + 1) % 2 == 0) ? ' no-bg' : '') ?>" id="question-box-<?= $pregunta->idpregunta ?>">
                            <div class="row">
                                <?php if (is_file(__DIR__ . "/../../storage/{$pregunta->pregunta}")): ?>
                                    <div class="<?= (($pregunta->estado == 0) ? 'col-10 col-md-11' : 'col-12') ?> d-flex">
                                        <span><?= ($key + 1) ?>.</span>
                                        <audio controls src="{{ config('app.url') }}/storage/<?= $pregunta->pregunta ?>" class="w-100 ml-2"></audio>
                                    </div>

                                    <?php if ($pregunta->estado == 0): ?>
                                    <div class="col-2 col-md-1 text-right">
                                        <i class="fa fa-times px-2 cursor-pointer remove-question" data-id="<?= $pregunta->idpregunta ?>"></i>
                                    </div>
                                    <?php endif; ?>
                                <?php else: ?>
                                    <div class="<?= (($pregunta->estado == 0) ? 'col-8 col-md-10' : 'col-12')?>">
                                        <span><?= ($key + 1) ?>.</span>
                                        <span id="question-<?= $pregunta->idpregunta ?>"><?= $pregunta->pregunta ?></span>
                                    </div>
                                        
                                    <?php if ($pregunta->estado == 0): ?>
                                    <div class="col-4 col-md-2 text-right">
                                        <i class="fa fa-pencil px-2 cursor-pointer edit-question" data-id="<?= $pregunta->idpregunta ?>"></i>
                                        <i class="fa fa-times px-2 cursor-pointer remove-question" data-id="<?= $pregunta->idpregunta ?>"></i>
                                    </div>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>                                  
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
            
            <div class="col-12 mt-5"></div>
            
            <?php if (config('streaming.support_message')): ?>
            <div class="col-12 text-center fs-17">
                Si tiene algún problema con la visualización de este webinar, por favor contacte con soporte técnico (3WAYS): +34 917 540 900
            </div>
            <?php endif; ?>
        </div>
    </div>
@endcontent

@content('scripts')
    <?php if (config('streaming.enable_intervals')): ?>
        <script src="{{ config('app.url') }}/assets/js/users-intervals.js?{{ time() }}"></script>
    <?php endif; ?>

    <script src="https://cdn.rawgit.com/mattdiamond/Recorderjs/08e7abd9/dist/recorder.js"></script>
    <script src="{{ config('app.url') }}/assets/js/streaming.js?{{ time() }}"></script>
    <script src="{{ config('app.url') }}/assets/js/icons.min.js?{{ time() }}"></script>
    <script src="{{ config('app.url') }}/assets/js/check-fuente.js?{{ time() }}"></script>
@endcontent

@content('modals')
    @add('modals.change_source')
    @add('modals.saved_vote')
    <?php if (config('streaming.allow_voice_questions')): ?>
    @add('modals.voice_questions')
    @add('modals.compatibility')
    <?php endif; ?>
@endcontent