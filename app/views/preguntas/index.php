{-- @author Alexis Bogado <alexis.bogado@s3w.es> --}
{-- @package webinar-web --}

<?php header('Content-Security-Policy:;'); ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('app.name') }}</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.css">
    <link rel="stylesheet" href="{{ config('app.url') }}/assets/preguntas/css/app.min.css">
</head>
<body>
    <div id="app"></div>

    <script src="{{ config('app.url') }}/assets/preguntas/js/vendors.min.js?{{ time() }}"></script>
    <script src="{{ config('app.url') }}/assets/preguntas/js/app.min.js?{{ time() }}"></script>
    <script src="{{ config('app.url') }}/assets/js/icons.min.js?{{ time() }}"></script>
</body>
</html>