{-- @author Alexis Bogado <alexis.bogado@s3w.es> --}
{-- @package webinar-web --}

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ config('app.name') }}</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Muli:wght@200;300;400;500;600;700;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ config('app.url') }}/assets/css/main.css?{{ time() }}">
    @get('styles')
</head>
<body{{ ((route()->name == 'index') ? '' : ' class="no-bg"') }}>
    @get('contents')
    
    <!-- <div class="sponsors">
        <div class="row justify-content-center align-items-center mb-5 w-100">
            <div class="col-12 col-lg-2 col-md-3 col-sm-4">
                <img src="{{ config('app.url') }}/assets/images/sermergen.png" alt="Semergen" class="img-fluid mx-auto d-block" />
            </div>

            <div class="col-12 col-lg-2 col-md-3 col-sm-4 offset-lg-2 offset-md-1">
                <img src="{{ config('app.url') }}/assets/images/universidad_europea.png" alt="Universidad Europea" class="img-fluid mx-auto d-block" />
            </div>

            <div class="col-12 col-lg-2 col-md-3 col-sm-4 offset-lg-2 offset-md-1">
                <img src="{{ config('app.url') }}/assets/images/astraeneca.png" alt="AstraZeneca" class="img-fluid mx-auto d-block" />
            </div>
        </div>
    </div> -->

    <!-- @add('includes.footer') -->
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="{{ config('app.url') }}/assets/js/main.js?{{ time() }}"></script>
    <script src="{{ config('app.url') }}/assets/js/index.js?{{ time() }}"></script>
    @get('scripts')

    <?php if (!auth()->loggedIn()): ?>
    @add('modals.login')
    <?php endif; ?>

    @get('modals')
</body>
</html>
