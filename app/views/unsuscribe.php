{-- @author Alexis Bogado <alexis.bogado@s3w.es> --}
{-- @package webinar-web --}

@add('base')

@content('contents')
    @add('includes.header')

    <div class="container my-5">
        <div class="row">
            <div class="col-12">
                <p>
                    <a href="{{ config('app.url') }}">< Volver al inicio</a>
                </p>
                <h3 class="font-weight-bold text-dark-green text-uppercase">Darse de baja</h3>
            </div>

            <?php if ($success): ?>
                <div class="col-12 my-5">
                    <h5 class="text-light-green font-weight-bold">Las notificaciones se han deshabilitado correctamente</h5>
                    <p>
                        Al haberse dado de baja dejará de recibir todas las comunicaciones electrónicas de marketing a profesionales sanitarios
                    </p>
                </div>
            <?php else: ?>
            <div class="col-12">
                <p class="text-dark-green">
                    Introduce tu email para dejar de recibir todas las comunicaciones electrónicas<br />
                    Al darse de baja cancelará esta dirección de email de todas las comunicaciones electrónicas de marketing a profesionales sanitarios
                </p>
            </div>

            <div class="col-12 mt-3">
                <form id="unsuscribe-form">
                    <div class="form-group">
                        <label for="input-email" class="text-dark-green font-weight-bold">Email</label>
                        <input type="text" id="input-email" name="email" class="form-control{{ ($email ? ' d-none' : '') }}" placeholder="Email" value="{{ $email  }}" />
                        <div class="invalid-feedback"></div>
                        <?php if ($email): ?>
                            <div>{{ $email }}</div>
                        <?php endif; ?>
                    </div>

                    <div class="form-group mt-4">
                        <div class="g-recaptcha" id="input-recaptcha" data-sitekey="{{ config('recaptcha.site_key') }}"></div>
                        <div class="invalid-feedback"></div>
                    </div>

                    <div class="form-group mt-4 text-center text-sm-left">
                        <button type="submit" class="p-0 bg-transparent border-0 w-100 w-sm-auto">
                            <img src="{{ config('app.url') }}/assets/images/continuar.png" alt="Acceder" class="img-fluid button" />
                        </button>
                    </div>
                </form>
            </div>
            <?php endif; ?>
        </div>
    </div>
@endcontent

@content('scripts')
    <script src="{{ config('app.url') }}/assets/js/unsuscribe.js?{{ time() }}"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
@endcontent