{-- @author Alexis Bogado <alexis.bogado@s3w.es> --}
{-- @package webinar-web --}

<div class="modal pr-0 fade" id="login-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body container">
                <form class="row px-3" id="login-form">
                    <div class="col-12">
                        <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="col-12 mt-2 form-group">
                        <label for="input-email" class="control-label font-weight-bold">Email</label>
                        <input type="email" class="form-control" id="input-email" placeholder="Email" data-error="El Email no es correcto">
                        <div class="invalid-feedback"></div>
                    </div>

                    <div class="col-12 form-group">
                        <label for="input-password" class="control-label font-weight-bold">Contraseña</label>
                        <input type="password" class="form-control" id="input-password" placeholder="Contraseña">
                        <div class="invalid-feedback"></div>
                    </div>

                    <div class="col-12 fs-sm-14">
                        <div class="row">
                            <div class="col-12 col-sm-5 form-group text-center text-left-sm">
                                <button type="submit" class="p-0 bg-transparent border-0">
                                    <img src="{{ config('app.url') }}/assets/images/acceder.png" alt="Acceder" class="img-fluid button d-block mx-auto" />
                                </button>
                            </div>
                            
                            <div class="col-12 col-sm-6 offset-sm-1 form-group">
                                <div class="text-center text-sm-right text-muted">
                                    <input type="checkbox" id="input-remember" name="remember_pass" />
                                    <label for="input-remember" class="pl-2 mb-1">Recordarme</label>
                                </div>

                                <div class="text-center text-sm-right">
                                    <a href="{{ route('recover')->path }}" class="text-muted">¿Has olvidado tu contraseña?</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>