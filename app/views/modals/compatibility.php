{-- @author Alexis Bogado <alexis.bogado@s3w.es> --}
{-- @package webinar-web --}

<div class="modal pr-0 fade" id="compatibility-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body container">
                <div class="row px-3">
                    <div class="col-8 align-self-center">
                        <h4 class="font-weight-bold">No se puede grabar preguntas en este navegador</h4>
                    </div>
                    
                    <div class="col-4">
                        <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>

                <div class="row px-3 mt-4">
                    <div class="col-12">
                        <p>Si desea hacer uso de esta característica considere utilizar alguno de los siguientes navegadores:</p>
                        <ul class="list-unstyled pl-5">
                            <li class="my-2">
                                <a href="https://www.google.com/intl/es_es/chrome/"><img src="{{ config('app.url') }}/assets/images/chrome.svg" width="50px" alt="Google Chrome"> Google Chrome</a>
                            </li>

                            <li class="my-2">
                                <a href="https://www.mozilla.org/es-ES/firefox/new/"><img src="{{ config('app.url') }}/assets/images/firefox.svg" width="50px" alt="Mozilla Firefox"> Mozilla Firefox</a>
                            </li>

                            <li class="my-2">
                                <a href="https://www.microsoft.com/es-es/edge"><img src="{{ config('app.url') }}/assets/images/edge.svg" width="50px" alt="Microsoft Edge"> Microsoft Edge</a>
                            </li>
                        </ul>
                    </div>

                    <div class="col-12 mt-4 text-right">
                        <button type="button" class="btn btn-success" data-dismiss="modal" aria-label="Close">
                            Seguir navegando
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>