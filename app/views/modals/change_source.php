{-- @author Alexis Bogado <alexis.bogado@s3w.es> --}
{-- @package webinar-web --}

<div class="modal pr-0 fade" id="change_source-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body container">
                <div class="row px-3">
                    <div class="col-8 align-self-center">
                        <h4 class="font-weight-bold">El reproductor no es compatible con este navegador</h4>
                    </div>
                    
                    <div class="col-4">
                        <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>

                <div class="row px-3 mt-4">
                    <div class="col-12">
                        <p>
                            Internet Explorer no es compatible con todas las funciones del reproductor
                            <img src="{{ config('app.url') }}/assets/images/ie_redcross.png" width="50px" alt="Icono IE no compatible">
                        </p>
                        <p>Si desea tener todas las funciones disponibles, considere utilizar alguno de los siguientes navegadores:</p>

                        <div class="row my-4">
                            <div class="col-4 text-center">
                                <a href="https://www.google.com/intl/es_es/chrome/">
                                    <img src="{{ config('app.url') }}/assets/images/chrome.svg" width="50px" alt="Google Chrome"><br />
                                    Google Chrome
                                </a>
                            </div>
                            
                            <div class="col-4 text-center">
                                <a href="https://www.mozilla.org/es-ES/firefox/new/">
                                    <img src="{{ config('app.url') }}/assets/images/firefox.svg" width="50px" alt="Mozilla Firefox"><br />
                                    Mozilla Firefox
                                </a>
                            </div>
                            
                            <div class="col-4 text-center">
                                <a href="https://www.microsoft.com/es-es/edge">
                                    <img src="{{ config('app.url') }}/assets/images/edge.svg" width="50px" alt="Microsoft Edge"><br />
                                    Microsoft Edge
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 mt-4 text-right">
                        <button type="button" class="btn btn-success" data-dismiss="modal" aria-label="Close" id="continue-with-limited-functions">
                            Continuar con funciones limitadas
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>