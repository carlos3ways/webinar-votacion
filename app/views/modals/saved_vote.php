{-- @author Alexis Bogado <alexis.bogado@s3w.es> --}
{-- @package webinar-web --}

<div class="modal pr-0 fade" id="saved_vote-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body container">
                <div class="row px-3">
                    <div class="col-8 align-self-center">
                        <h4 class="font-weight-bold">Voto registrado</h4>
                    </div>
                    
                    <div class="col-4">
                        <button type="button" class="close close-modal" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>

                <div class="row px-3 mt-4">
                    <div class="col-12">
                        Su voto se ha registrado correctamente
                    </div>

                    <div class="col-12 mt-4 text-right">
                        <button type="button" class="btn btn-success" data-dismiss="modal" aria-label="Close" id="continue-with-limited-functions">
                            Continuar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>