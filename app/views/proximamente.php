{-- @author Alexis Bogado <alexis.bogado@s3w.es> --}
{-- @package webinar-web --}

@add('base')

@content('contents')
    <div class="index-view d-flex flex-column align-items-center justify-content-center">
        <div class="container">
            <img src="{{ config('app.url') }}/assets/images/logo-seimc.png" alt="Logo" class="img-fluid d-block mx-auto" />
			<h1 class="text-center font-weight-bold">Asamblea Virtual SEIMC</h1>
			<div class="text-center">
				Acceso disponible el día 19 de mayo de 2021 (15:30 horas)
			</div>
        </div>
    </div>
@endcontent