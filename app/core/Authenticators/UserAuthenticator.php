<?php

/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-web
 */

namespace App\Core\Authenticators;

use App\Models\User;
use \hash;

class UserAuthenticator
{
    /**
     * Authenticated user variable
     *
     * @var App\Models\User $user
     */
    private $user;

    /**
     * Try to login when class is initialized
     */
    public function __construct()
    {
        if (isset($_SESSION['email']) && isset($_SESSION['password'])):
            $user_data = $this->tryLogin($_SESSION['email'], $_SESSION['password']);
            if ($user_data->validacion != 'ok'):
                $this->logout();
                return;
            endif;

            $this->user = new User($user_data->socio_id, $_SESSION['email'], $user_data->socio_nombre);
        elseif ($this->user):
            $this->logout();
        endif;
    }

    /**
     * Try login through SEIMC api
     *
     * @param string $email
     * @param string $password
     * 
     * @return void
     */
    public function tryLogin($email, $password)
    {
        $headers = [
            'Accept: application/json',
            'Content-type: application/json',
            'Cache-Control: no-cache',
            'Pragma: no-cache'
        ];
    
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://www.seimc.org/aasitio_webservice/ws-datos-seimc-v1.php?servicio=Login&usuario={$email}&contrasena={$password}");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_TIMEOUT, 180);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $output = curl_exec($ch);
        $output = str_replace('(', '', $output);
        $output = str_replace(')', '', $output);
    
        curl_close($ch);
    
        return json_decode($output);
    }

    /**
     * Store current logged user
     *
     * @param App\Models\User $user
     * @param string $password
     * 
     * @return void
     */
    public function setUser($user, $password = '')
    {
        $this->user = $user;
        
        $_SESSION['email'] = $this->user->email;
        $_SESSION['password'] = $password;
    }

    /**
     * Get authenticated user instance
     *
     * @return App\Models\User|null
     */
    public function user()
    {
        return $this->user ?? null;
    }

    /**
     * Check if user is logged in
     *
     * @return bool
     */
    public function loggedIn()
    {
        return !is_null($this->user);
    }

    /**
     * Close current user session
     *
     * @return void
     */
    public function logout()
    {
        $this->user = null;

        unset($_SESSION['email'], $_SESSION['password']);
    }
}
