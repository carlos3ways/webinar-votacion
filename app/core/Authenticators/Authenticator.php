<?php

/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-web
 */

namespace App\Core\Authenticators;

class Authenticator
{
    /**
     * User authenticator instance
     *
     * @var App\Core\Authenticators\UserAuthenticator $user
     */
    private $user;

    /**
     * Admin authenticator instance
     *
     * @var App\Core\Authenticators\AdminAuthenticator $admin
     */
    private $admin;

    /**
     * Initialize available authenticators
     */
    public function __construct()
    {
        $this->user = new UserAuthenticator;
        $this->admin = new AdminAuthenticator;
    }

    /**
     * Get authenticator by type
     *
     * @param string $type
     * 
     * @return App\Core\Authenticators\UserAuthenticator
     * @return App\Core\Authenticators\AdminAuthenticator
     * @return null
     */
    public function get($type)
    {
        return $this->{$type} ?? null;
    }
}
