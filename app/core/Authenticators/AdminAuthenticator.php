<?php

/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-web
 */

namespace App\Core\Authenticators;

use App\Models\Questions\Admin;
use \hash;

class AdminAuthenticator
{
    /**
     * Authenticated user variable
     *
     * @var App\Models\Questions\Admin $user
     */
    private $user;

    /**
     * Try to login when class is initialized
     */
    public function __construct()
    {
        if (isset($_SESSION['admin_username']) && isset($_SESSION['admin_password'])):
            $user = Admin::first('*', [ ['usuario', $_SESSION['admin_username']], ['clave', $_SESSION['admin_password']] ]);

            if (!$user):
                $this->logout();
                return;
            endif;

            $this->user = $user;
        elseif ($this->user):
            $this->logout();
        endif;
    }

    /**
     * Store current logged user
     *
     * @param App\Models\Admin $user
     * 
     * @return void
     */
    public function setUser($user)
    {
        $this->user = $user;
        
        $_SESSION['admin_username'] = $this->user->usuario;
        $_SESSION['admin_password'] = $this->user->clave;
    }

    /**
     * Get authenticated user instance
     *
     * @return App\Models\Admin|null
     */
    public function user()
    {
        return $this->user ?? null;
    }

    /**
     * Check if user is logged in
     *
     * @return bool
     */
    public function loggedIn()
    {
        return !is_null($this->user);
    }

    /**
     * Close current user session
     *
     * @return void
     */
    public function logout()
    {
        $this->user = null;
        
        unset($_SESSION['admin_username'], $_SESSION['admin_password']);
    }
}
