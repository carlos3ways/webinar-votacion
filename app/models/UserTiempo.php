<?php

/**
 * @author Alexis Bogado
 * @package webinar-web
 */

namespace App\Models;

class UserTiempo extends Model
{
    protected $table = 'users_tiempos';
    protected $hidden = [ 'talleres_id' ];

    /**
     * Get user
     *
     * @return \App\Models\User
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'users_id');
    }
}
