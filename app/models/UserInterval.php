<?php

/**
 * @author Alexis Bogado
 * @package webinar-web
 */

namespace App\Models;

class UserInterval extends Model
{
    protected $table = 'users_intervals';
}
