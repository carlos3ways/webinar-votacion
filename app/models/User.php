<?php

/**
 * @author Alexis Bogado
 * @package webinar-web
 */

namespace App\Models;

class User
{
    public $id;
    public $email;
    public $name;

    public function __construct($id, $email, $name)
    {
        $this->id = $id;
        $this->email = $email;
        $this->name = $name;
    }

    /**
     * Get user questions
     *
     * @return \App\Models\Pregunta
     */
    public function questions()
    {
        return Pregunta::all([ ['idusuariogesida', $this->id] ], [ ['idpregunta', 'DESC'] ]);
    }
}
