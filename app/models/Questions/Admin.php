<?php

/**
 * @author Alexis Bogado
 * @package webinar-web
 */

namespace App\Models\Questions;

use App\Models\Model;

class Admin extends Model
{
    protected $table = 'usuarios';
    protected $hidden = [ 'clave', 'token' ];

    /**
     * Get user messages
     *
     * @return App\Models\Questions\Message
     */
    public function messages()
    {
        return Message::select('*', [ ['id_usuario_origen', $this->idusuario, [ 'id_usuario_destino', $this->idusuario ]] ], [ ['id', 'DESC'] ]);
    }
}
