<?php

/**
 * @author Alexis Bogado
 * @package webinar-web
 */

namespace App\Models\Questions;

use App\Models\Model;

class Message extends Model
{
    protected $table = 'mensajes_v2';
    protected $hidden = [ 'id_usuario_destino' ];
    protected $functionsToShow = [ 'sender' ];

    /**
     * Get message sender
     *
     * @return \App\Models\Questions\Admin
     */
    public function sender()
    {
        return $this->belongsTo(Admin::class, 'id_usuario_origen');
    }
}
