<?php

/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-web
 */

namespace App\Models;

class Pregunta extends Model
{
    protected $hidden = [ 'administrador_idusuario', 'administrador_fecha', 'moderador_idusuario', 'moderador_fecha' ];
}
