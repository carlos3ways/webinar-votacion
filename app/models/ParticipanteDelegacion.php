<?php

/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-web
 */

namespace App\Models;

class ParticipanteDelegacion extends Model
{
    protected $table = 'participantes_delegaciones';
    protected $hidden = [ 'socio_delegado_id', 'socio_delegado_nombre' ];
}
