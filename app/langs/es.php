<?php

/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-web
 */

$_lang = [
    'first_key' => 'Valor uno', // __('first_key')
    'test' => [
        'key' => 'Hola' // __('test.key')
    ]
];
