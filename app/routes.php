<?php

/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-web
 */

$routes->get('/', 'IndexController@index')->name('index');
$routes->get('/preview', 'IndexController@preview')->name('preview');
$routes->get('/logout', 'AuthController@logout')->name('logout');
$routes->get('/streaming', 'StreamingController@index')->name('streaming');
$routes->get('/recuperar', 'RecoverPasswordController@index')->name('recover');
$routes->get('/unsuscribe', 'UnsuscribeController@index')->name('unsuscribe');
$routes->view('/politica_cookies', 'policies.cookies')->name('cookies_policy');
$routes->view('/politica_privacidad', 'policies.privacy')->name('privacy_policy');
$routes->view('/condiciones_uso', 'policies.use')->name('conditions_of_use');
$routes->get('/actualizar_fuentes', function() {
    $file_path = __DIR__ . '/../storage/fuentes.html';
    $handle = fopen($file_path, 'w') or die("Cannot open file: {$file_path}");
    fwrite($handle, api_data(App\Models\ControlFuente::all()));
    fclose($handle);

    return 'Los datos fuentes se han actualizado correctamente en el archivo <b>storage/fuentes.html</b>';
});

// API Routes
$routes->api('post', '/auth/signin', 'AuthController@signin');
$routes->api('post', '/users/recover', 'RecoverPasswordController@recover');
$routes->api('post', '/users/unsuscribe', 'UnsuscribeController@unsuscribe');
$routes->api('get', '/cookies/{name}', 'CookiesController@getCookie');
$routes->api('post', '/cookies', 'CookiesController@addCookie');
$routes->api('get', '/questions', 'QuestionsController@getQuestions');
$routes->api('post', '/questions', 'QuestionsController@createQuestion');
$routes->api('put', '/questions/{id}', 'QuestionsController@editQuestion');
$routes->api('delete', '/questions/{id}', 'QuestionsController@deleteQuestion');
$routes->api('post', '/users_tiempos', 'StreamingController@registerUserTiempo');
$routes->api('put', '/users_tiempos/{payload}', 'StreamingController@unregisterUserTiempo');
$routes->api('get', '/control_fuentes/{id}', 'StreamingController@getControlFuente');
$routes->api('post', '/users_intervals', 'StreamingController@saveUserInterval');

// QUESTION ROUTES
$routes->get([ '/preguntas', '/preguntas/questions', '/preguntas/analytics' ], 'Questions\IndexController@index');

// API Routes
$routes->api('get', '/preguntas/settings', 'Questions\IndexController@settings');
$routes->api('get', '/preguntas/auth/user', 'Questions\AuthController@user');
$routes->api('post', '/preguntas/auth/login', 'Questions\AuthController@login');
$routes->api('post', '/preguntas/auth/logout', 'Questions\AuthController@logout');
$routes->api('get', '/preguntas/messages', 'Questions\MessagesController@show');
$routes->api('post', '/preguntas/messages/send', 'Questions\MessagesController@store');
$routes->api('get', '/preguntas/questions', 'Questions\QuestionsController@show');
$routes->api('put', '/preguntas/questions/{id}/update/status', 'Questions\QuestionsController@updateStatus');
$routes->api('put', '/preguntas/questions/{id}/update', 'Questions\QuestionsController@update');
$routes->api('get', '/preguntas/analytics', 'Questions\AnalyticsController@show');
$routes->api('get', '/preguntas/analytics/export/{type}', 'Questions\AnalyticsController@export');
