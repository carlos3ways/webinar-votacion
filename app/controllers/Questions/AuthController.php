<?php

/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-web
 */

namespace App\Controllers\Questions;

use App\Controllers\Controller;
use App\Models\Questions\Admin;

class AuthController extends Controller
{
    public function user()
    {
        return auth('admin')->user();
    }

    public function login($request)
    {
        $this->permissions([ 'admin_guest' ], true);

        $data = [ 'success' => false ];
        $user = null;

        $username = $request->input->username;
        $password = $request->input->password;

        if (!$username || !$password):
            if (!$username)
                $data['errors']['username'][] = 'El campo de usuario no puede estar vacio';
            
            if (!$password)
                $data['errors']['password'][] = 'El campo de contraseña no puede estar vacio';

            goto Result;
        endif;

        // Data validation process
        $user_by_name = Admin::first('idusuario', [ ['usuario', $username] ]);
        if (!$user_by_name):
            $data['errors']['username'][] = 'No hay usuarios registrados con este nombre';
            goto Result;
        endif;

        $user = Admin::first('*', [ ['usuario', $username], ['clave', $password] ]);
        if (!$user):
            $data['errors']['password'][] = 'La contraseña no es correcta';
            goto Result;
        elseif ($user->tipo === 0 && !config('questions.app.questions_approval_account')):
            $data['errors']['username'][] = 'La cuenta no está habilitada';
            goto Result;
        endif;

        $data['success'] = true;
        $data['message'] = 'Authorized login';
        $data['user'] = $user;

        auth('admin')->setUser($user);

        Result:
            return $data;
    }

    public function logout()
    {
        $this->permissions([ 'admin_auth' ], true);

        auth('admin')->logout();
    }
}
