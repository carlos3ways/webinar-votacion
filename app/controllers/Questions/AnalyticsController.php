<?php

/**
 * @author Alexis Bogado
 * @package webinar-preguntas
 */

namespace App\Controllers\Questions;

use App\Controllers\Controller;
use App\Models\Pregunta;
use App\Models\User;
use App\Models\UserTiempo;
use DatePeriod;
use DateTime;
use DateInterval;

class AnalyticsController extends Controller
{
    public function show()
    {
        $this->permissions([ 'admin_auth' ], true);
        if (auth('admin')->user()->tipo != 3) return null;

        $current_range = '';
        $streaming_start = strtotime(config('questions.streaming.date') . ' ' . config('questions.streaming.start'));
        $streaming_end = strtotime(config('questions.streaming.date') . ' ' . config('questions.streaming.end'));
        if ($streaming_start > time()):
            $current_range = 'Sin empezar';
        elseif ($streaming_end < time()):
            $current_range = 'Finalizado';
        else:
            $range = $this->getCurrentRange();
            $current_range = (($range->from ?? '00:00') . ' - ' . ($range->to ?? '00:00'));
        endif;

        $online_users = database()->rowCount('SELECT users_id FROM users_intervals WHERE created_at >= \'' . date('Y-m-d H:i', strtotime('-5 minutes')) . '\'  GROUP BY users_id');
        $total_users = database()->rowCount('SELECT users_id FROM users_tiempos WHERE entrada >= \'' . date('Y-m-d H:i', strtotime('-1 hour', $streaming_start)) . '\' AND entrada <= \'' . date('Y-m-d H:i', strtotime('+1 hour', $streaming_end)) . '\' GROUP BY users_id');
        $question_count = Pregunta::count();
        $screened_questions = Pregunta::count([ ['hora_proyectar', '!=', 'null'] ]);
        $answered_questions = Pregunta::count([ ['estado', 3] ]);
        $edited_questions = Pregunta::count([ ['corregido', '!=', 'null'] ]);
        
        return [
            'date' => config('questions.streaming.date'),
            'start' => config('questions.streaming.start'),
            'end' => config('questions.streaming.end'),
            'current_range' => $current_range,
            'online_users' => $online_users,
            'total_users' => $total_users,
            'question_count' => $question_count,
            'user_count' => 0,
            'new_users' => 0,
            'allow_newsletters' => 0,
            'screened_questions' => $screened_questions,
            'answered_questions' => $answered_questions,
            'edited_questions' => $edited_questions,
            'release_date' => config('questions.app.release_date')
        ];
    }

    /**
     * Undocumented function
     *
     * @param [type] $type
     * @return void
     */
    public function export($type)
    {
        $this->permissions([ 'admin_auth' ], true);
        if (auth('admin')->user()->tipo != 3) return null;

        $columns = null;
        $data = null;

        switch ($type):
            case 'questions':
                $columns = Pregunta::getColumnNames();
                $data = Pregunta::all();
            break;

            case 'users':
                $columns = User::getColumnNames();
                $data = [ ];
            break;

            case 'new_users':
                $columns = User::getColumnNames();
                $data = [ ];
                break;
            
            // case 'users_per_range': // 01-12-2020: No se está usando
            //     $date_start = date('Y-m-d H:i', strtotime(config('streaming.date') . ' ' . config('streaming.start')));
            //     $date_end = date('Y-m-d H:i', strtotime(config('streaming.date') . ' ' . config('streaming.end')));
                
            //     $ranges = $this->getTimeRanges();
            //     $columns = [ 'rango', 'nombre', 'apellidos', 'email', 'especialidad', 'provincia', 'codigo_postal', 'poblacion', 'centro' ];
                
            //     $all_data = [];

            //     foreach ($ranges as $key => $range):
            //         $date_from = date('Y-m-d', strtotime(config('streaming.date'))) . ' ' . $range['from'];
            //         $date_to = date('Y-m-d', strtotime(config('streaming.date'))) . ' ' . $range['to'];
            //         $current_data = null;
            //         $current_range_text = ('De ' . $range['from'] . ' a ' . $range['to']);

            //         if ($key == 0): // First range
            //             $results = database()->fetchAll('SELECT u.name AS nombre, u.surname AS apellidos, u.email, e.especialidad, p.provincia, u.codigo_postal, u.city AS poblacion, u.centro_trabajo AS centro FROM users_tiempos ut, users u, especialidades e, provincias p WHERE ut.users_id=u.id AND e.id=u.especialidad AND p.id=u.id_provincia AND entrada > \'' . $date_from . '\' AND entrada < \'' . $date_to . '\' AND u.email NOT REGEXP \'' . excluded_mail_domains() . '\' GROUP BY users_id');

            //             foreach ($results as $result):
            //                 $result['rango'] = $current_range_text;
            //                 $current_data[] = $result;
            //             endforeach;
            //         elseif ($key == (count($ranges) - 1)): // Last range
            //             $previous_range = $ranges[$key - 1];
                        
            //             $results = database()->fetchAll('SELECT u.name AS nombre, u.surname AS apellidos, u.email, e.especialidad, p.provincia, u.codigo_postal, u.city AS poblacion, u.centro_trabajo AS centro FROM users_tiempos ut, users u, especialidades e, provincias p WHERE ut.users_id=u.id AND e.id=u.especialidad AND p.id=u.id_provincia AND entrada > \'' . $date_start . '\' AND entrada < \'' . $date_end . '\' AND (salida > \'' . $previous_range['from'] . '\' OR salida IS NULL) AND u.email NOT REGEXP \'' . excluded_mail_domains() . '\' GROUP BY users_id');

            //             foreach ($results as $result):
            //                 $result['rango'] = $current_range_text;
            //                 $current_data[] = $result;
            //             endforeach;
            //         else:
            //             $previous_range = $ranges[$key - 1];

            //             $results = database()->fetchAll('SELECT u.name AS nombre, u.surname AS apellidos, u.email, e.especialidad, p.provincia, u.codigo_postal, u.city AS poblacion, u.centro_trabajo AS centro FROM users_tiempos ut, users u, especialidades e, provincias p WHERE ut.users_id=u.id AND e.id=u.especialidad AND p.id=u.id_provincia AND entrada > \'' . $date_start . '\' AND entrada < \'' . $date_to . '\' AND ((salida < \'' . $previous_range['to']. '\' AND salida > \'' . $previous_range['from'] . '\') OR salida > \'' . $previous_range['to'] . '\' OR salida IS NULL) AND u.email NOT REGEXP \'' . excluded_mail_domains() . '\' GROUP BY users_id');

            //             foreach ($results as $result):
            //                 $result['rango'] = $current_range_text;
            //                 $current_data[] = $result;
            //             endforeach;
            //         endif;
                    
            //         $all_data = array_merge($all_data, $current_data);
            //     endforeach;

            //     $data = $all_data;
            // break;
            
            // case 'count_per_range': // 01-12-2020: No se está usando
            //     $date_start = date('Y-m-d H:i', strtotime(config('streaming.date') . ' ' . config('streaming.start')));
            //     $date_end = date('Y-m-d H:i', strtotime(config('streaming.date') . ' ' . config('streaming.end')));

            //     $ranges = $this->getTimeRanges();
            //     $columns = [ 'rango', 'conectados' ];

            //     foreach ($ranges as $key => $range):
            //         $date_from = date('Y-m-d', strtotime(config('streaming.date'))) . ' ' . $range['from'];
            //         $date_to = date('Y-m-d', strtotime(config('streaming.date'))) . ' ' . $range['to'];

            //         $current_data = [
            //             'rango' => ('De ' . $range['from'] . ' a ' . $range['to']),
            //             'conectados' => 0
            //         ];
                    
            //         if ($key == 0): // First range
            //             $current_data['conectados'] = database()->rowCount('SELECT users_id FROM users_tiempos WHERE entrada > \'' . $date_from . '\' AND entrada < \'' . $date_to . '\' AND (users_id IN (SELECT id FROM users WHERE email NOT REGEXP \'' . excluded_mail_domains() . '\') OR users_id IS NULL) GROUP BY users_id');
            //         elseif ($key == (count($ranges) - 1)): // Last range
            //             $previous_range = $ranges[$key - 1];
                        
            //             $current_data['conectados'] = database()->rowCount('SELECT users_id FROM users_tiempos WHERE entrada > \'' . $date_start . '\' AND entrada < \'' . $date_end . '\' AND (salida > \'' . $previous_range['from'] . '\' OR salida IS NULL) AND (users_id IN (SELECT id FROM users WHERE email NOT REGEXP \'' . excluded_mail_domains() . '\') OR users_id IS NULL) GROUP BY users_id');
            //         else:
            //             $previous_range = $ranges[$key - 1];

            //             $current_data['conectados'] = database()->rowCount('SELECT users_id FROM users_tiempos WHERE entrada > \'' . $date_start . '\' AND entrada < \'' . $date_to . '\' AND ((salida < \'' . $previous_range['to']. '\' AND salida > \'' . $previous_range['from'] . '\') OR salida > \'' . $previous_range['to'] . '\' OR salida IS NULL) AND (users_id IN (SELECT id FROM users WHERE email NOT REGEXP \'' . excluded_mail_domains() . '\') OR users_id IS NULL) GROUP BY users_id');
            //         endif;
                    
            //         $data[] = $current_data;
            //     endforeach;
            // break;
            
            case 'total_online_users':
                $columns = [ ];
                $data = [ ];
            break;

            case 'allow_newsletters':
                $columns = User::getColumnNames();
                $data = [ ];
            break;

            default:
                return null;
        endswitch;
        
        return [
            'keys' => $columns,
            'values' => $data
        ];
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    private function getCurrentRange()
    {
        $streaming_start = strtotime(config('questions.streaming.date') . ' ' . config('questions.streaming.start'));
        $streaming_end = strtotime(config('questions.streaming.date') . ' ' . config('questions.streaming.end'));
        if ($streaming_start > time() || $streaming_end < time()) return;

        $result = [ ];

        $now = new DateTime();
        $ranges = new DatePeriod(DateTime::createFromFormat('H:i', date('H:i', $streaming_start)), new DateInterval('PT' . config('questions.streaming.range') . 'M'), DateTime::createFromFormat('H:i', date('H:i', $streaming_end)));
        $result = null;

        foreach ($ranges as $range):
            $from_difference = $range->diff($now);
            if ($from_difference->h > 0 || $from_difference->i > config('questions.streaming.range')) continue;
            if ($result && $result->from >= $from_difference->i) continue;
            
            $to = clone $range;
            $to->modify('+' . config('questions.streaming.range') . ' minute');

            $result = ((object) [ 'from' => $range->format('H:i'), 'to' => $to->format('H:i') ]);
        endforeach;

        return $result;
    }

    /**
     * Undocumented function
     *
     * @return array
     */
    private function getTimeRanges()
    {
        $streaming_start = strtotime(config('questions.streaming.date') . ' ' . config('questions.streaming.start'));
        $streaming_end = strtotime(config('questions.streaming.date') . ' ' . config('questions.streaming.end'));

        $result = [ ];

        $ranges = new DatePeriod(DateTime::createFromFormat('H:i', date('H:i', $streaming_start)), new DateInterval('PT' . config('questions.streaming.range') . 'M'), DateTime::createFromFormat('H:i', date('H:i', $streaming_end)));

        foreach ($ranges as $range):
            $to = clone $range;
            $to->modify('+' . config('questions.streaming.range') . ' minute');

            $result[] = [
                'from' => $range->format('H:i'),
                'to' => $to->format('H:i')
            ];
        endforeach;

        return $result;
    }
}
