<?php

/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-web
 */

namespace App\Controllers\Questions;

use App\Controllers\Controller;

class IndexController extends Controller
{
    public function index()
    {
        return view('preguntas.index');
    }

    public function settings()
    {
        return [
            'max_length' => config('questions.screening.max_length'),
            'color' => config('questions.screening.color'),
        ];
    }
}
