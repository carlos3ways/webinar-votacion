<?php

/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-web
 */

namespace App\Controllers\Questions;

use App\Controllers\Controller;
use App\Models\Questions\Message;

class MessagesController extends Controller
{
    public function show()
    {
        $this->permissions([ 'admin_auth' ], true);

        if (auth('admin')->user()->tipo != 1 && auth('admin')->user()->tipo != 2)
            return null;

        return auth('admin')->user()->messages();
    }

    public function store($request)
    {
        $this->permissions([ 'admin_auth' ], true);

        if (auth('admin')->user()->tipo != 2 || !$request->input->message)
            return null;

        $message = new Message;
        $message->id_usuario_origen = auth('admin')->user()->idusuario;
        $message->id_usuario_destino = 1;
        $message->texto = $request->input->message;
        $message->fecha = date('Y-m-d H:i:s', time());

        $message->save();

        return $message;
    }
}
