<?php

/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-web
 */

namespace App\Controllers\Questions;

use App\Controllers\Controller;
use App\Models\Pregunta;

class QuestionsController extends Controller
{
    public function show()
    {
        $this->permissions([ 'admin_auth' ], true);

        if (!in_array(auth('admin')->user()->tipo, [ 0, 2 ])):
            return Pregunta::all([ ['estado', '>=', 2] ], [ ['orden', 'DESC'] ]);
        elseif (auth('admin')->user()->tipo === 2 && config('questions.app.questions_approval_account')):
            return Pregunta::all([ ['estado', '>=', 1] ], [ ['orden', 'DESC'] ]);
        endif;

        return Pregunta::all([ ['idpregunta', 'DESC'] ]);
    }

    public function updateStatus($id)
    {
        $this->permissions([ 'admin_auth' ], true);

        $question = Pregunta::find($id);
        if (!$question) return null;

        switch (auth('admin')->user()->tipo):
            case 3: // Editor
                $question->hora_proyectar = date('Y-m-d H:i:s', time());
            break;

            case 2: // Administrator
                // $question_ordered = Question::first('orden', [ ['orden', '>', 0] ], [ ['orden', 'DESC'] ]);
                // $question->orden = ($question_ordered ? ($question_ordered->orden + 1) : 1);
                $question->estado = (((!config('questions.app.questions_approval_account') && $question->estado === 0) || $question->estado == 1) ? 2 : (config('questions.app.questions_approval_account') ? 1 : 0));
            break;

            case 1: // Moderator
                $question->estado = (($question->estado === 2) ? 3 : 2);
            break;

            case 0: // Supervisor
                $question->estado = (($question->estado === 0) ? 1 : 0);
            break;
        endswitch;

        $question->save();
        return $question;
    }

    public function update($id, $request)
    {
        $this->permissions([ 'admin_auth' ], true);
        if (auth('admin')->user()->tipo != 3) return null;

        $question = Pregunta::find($id);
        if (!$question) return null;

        $question->corregido = $request->input->message;

        $question->save();
        return $question;
    }
}
