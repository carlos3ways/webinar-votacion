<?php

/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-web
 */

namespace App\Controllers;

class SeleccionController extends Controller
{
    public function index($request)
    {
        $this->permissions([ 'auth' ]);

        return view('seleccion');
    }
}
