<?php

/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-web
 */

namespace App\Controllers;

class Controller
{
    /**
     * Check permissions to show the page
     *
     * @param array $permissions
     * @param bool $api
     * 
     * @return void
     */
    public function permissions($permissions, $api = false)
    {
        $load_view = true;

        foreach ($permissions as $permission):
            switch ($permission):
                case 'auth':
                    $load_view = auth()->loggedIn();
                    
                    if (!$load_view && !$api):
                        header('Location: ' . config('app.url'));
                        exit;
                    endif;
                break;

                case 'guest':
                    $load_view = !auth()->loggedIn();

                    if (!$load_view && !$api):
                        header('Location: ' . route('streaming')->path());
                        exit;
                    endif;
                break;

                case 'admin_auth':
                    $load_view = auth('admin')->loggedIn();
                    
                    if (!$load_view && !$api):
                        header('Location: ' . config('app.url') . '/preguntas');
                        exit;
                    endif;
                break;

                case 'admin_guest':
                    $load_view = !auth('admin')->loggedIn();

                    if (!$load_view && !$api):
                        header('Location: ' . config('app.url') . '/questions');
                        exit;
                    endif;
                break;

                case '3ways': // Check if email is from 3ways, needs always auth permission
                    if (!in_array('auth', $permissions)):
                        $load_view = false;
                        break;
                    endif;

                    $load_view = ($load_view && strpos(auth()->user()->email, 's3w.es'));
                break;
            endswitch;
        endforeach;

        if (!$load_view) die($api ? json_encode([ 'success' => false, 'message' => 'Unauthorized' ]) : view('error'));
    }
}
