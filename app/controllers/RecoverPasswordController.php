<?php

/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-web
 */

namespace App\Controllers;

use App\Models\User;
use \PHPMailer;

class RecoverPasswordController extends Controller
{
    public function index($request)
    {
        $this->permissions([ 'guest' ]);

        return view('recover', [
            'success' => ($request->input->success ?? false)
        ]);
    }

    public function recover($request)
    {
        $this->permissions([ 'guest' ], true);

        $email = $request->input->email;
        $recaptcha = $request->input->{'g-recaptcha-response'};

        $data = [ 'success' => false ];
        
        if (!$email || !$recaptcha):
            if (!$email)
                $data['errors']['email'][] = 'El campo de email no puede estar vacío';

            if (!$recaptcha)
                $data['errors']['recaptcha'][] = 'Debe realizar la comprobación del captcha';

            goto Result;
        endif;

        // API Validation
        $headers = [
            'Accept: application/json',
            'Content-type: application/json',
            'Cache-Control: no-cache',
            'Pragma: no-cache'
        ];
    
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://www.seimc.org/aasitio_webservice/ws-datos-seimc-v1.php?servicio=RecordatorioClaves&email={$email}");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_TIMEOUT, 180);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $output = curl_exec($ch);
        $output = str_replace('(', '', $output);
        $output = str_replace(')', '', $output);
        curl_close($ch);
        $response = json_decode($output);

        if ($response->validacion != 'ok'):
            $data['errors']['email'][] = $response->respuesta;
            goto Result;
        endif;

        // Captcha validation
        $catpcha_response = json_decode(file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . config('recaptcha.secret_key') . '&response=' . $recaptcha . '&remoteip=' . $_SERVER['REMOTE_ADDR']), true);
        if (!$catpcha_response['success']):
            $data['errors']['recaptcha'][] = 'El captcha es incorrecto';
            goto Result;
        endif;
        
        $data['success'] = true;
        $data['message'] = 'Recover password email was sent successfully';
        $data['sendTo'] = route('recover')->path . '?success=true';
        
        Result:
        return $data;
    }
}
