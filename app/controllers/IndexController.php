<?php

/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-web
 */

namespace App\Controllers;

class IndexController extends Controller
{
    public function index($request)
    {
      $this->permissions([ 'guest' ]);

      if (config('app.maintenance'))
        return view('proximamente');

      return view('index');
  }

  public function preview($request)
  {
    $this->permissions([ 'guest' ]);

    return view('index');
  }
}
