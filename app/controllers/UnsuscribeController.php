<?php

/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-web
 */

namespace App\Controllers;

use App\Models\User;

class UnsuscribeController extends Controller
{
    public function index($request)
    {
        if (!config('app.allow_unsuscribe'))
            return view('error');
        
        $email = (auth()->loggedIn() ? auth()->user()->email : ($request->input->email ?? null));

        return view('unsuscribe', [
            'email' => $email,
            'success' => ($request->input->success ?? false)
        ]);
    }

    public function unsuscribe($request)
    {
        $data = [ 'success' => false ];

        if (!config('app.allow_unsuscribe')):
            $data['message'] = 'Unsuscribe is not allowed!';
            goto Result;
        endif;

        $email = (auth()->loggedIn() ? auth()->user()->email : $request->input->email);
        $recaptcha = $request->input->{'g-recaptcha-response'};

        $data = [ 'success' => false ];
        $user_by_email = (auth()->loggedIn() ? auth()->user() : User::first('*', [ ['email', $email] ]));
        
        if ((!$email || !$user_by_email) || !$recaptcha):
            if (!$email)
                $data['errors']['email'][] = 'El campo de email no puede estar vacío';
            elseif (!$user_by_email)
                $data['errors']['email'][] = 'No hay usuarios registrados con ese email';

            if (!$recaptcha)
                $data['errors']['recaptcha'][] = 'Debe realizar la comprobación del captcha';

            goto Result;
        endif;

        // Captcha validation
        $catpcha_response = json_decode(file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . config('recaptcha.secret_key') . '&response=' . $recaptcha . '&remoteip=' . $_SERVER['REMOTE_ADDR']), true);
        if (!$catpcha_response['success']):
            $data['errors']['recaptcha'][] = 'El captcha es incorrecto';
            goto Result;
        endif;

        $user_by_email->acepto_publicar = 0;
        $user_by_email->save();
        
        $data['success'] = true;
        $data['message'] = 'Notifications was disabled successfully';
        $data['sendTo'] = route('unsuscribe')->path . '?success=true';
        
        Result:
        return $data;
    }
}
