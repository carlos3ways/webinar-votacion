<?php

/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-web
 */

namespace App\Controllers;

use App\Models\User;
use App\Models\Evento;
use App\Models\ParticipanteDelegacion;

class AuthController extends Controller
{
    public function signin($request)
    {
        $this->permissions([ 'guest' ], true);

        $email = $request->input->email;
        $password = $request->input->password;
        $data = [ 'success' => false ];

        if (!$email || !$password):
            if (!$email)
                $data['errors']['email'][] = 'El campo de email no puede estar vacio';
            
            if (!$password)
                $data['errors']['password'][] = 'El campo de contraseña no puede estar vacio';
            
            goto Result;
        endif;

        $user_data = auth()->tryLogin($email, $password);
        if ($user_data->validacion != 'ok'):
            $data['errors']['email'][] = $user_data->respuesta;
            goto Result;
        endif;

        $evento = Evento::first('*', [ ['activo', 1] ]);
        if (!$evento):
            $data['errors']['email'][] = 'No hay asamblea en curso';
            $data['message'] = 'No active event found';
            goto Result;
        endif;

        $user = new User($user_data->socio_id, $email, $user_data->socio_nombre);
        $participante_delegacion = ParticipanteDelegacion::first('*', [ ['id_evento', $evento->id], ['socio_id', $user->id] ]);
        if (!$participante_delegacion):
            $participante_delegacion = new ParticipanteDelegacion;
            $participante_delegacion->id_evento = $evento->id;
            $participante_delegacion->socio_id = $user->id;
            $participante_delegacion->socio_nombre = $user->name;
        endif;
        
        $participante_delegacion->asistencia = date('Y-m-d H:i:s');
        $participante_delegacion->save();
        
        $data['success'] = true;
        $data['message'] = 'Authorized login';
        $data['sendTo'] = route('streaming')->path;

        auth()->setUser($user, $password);
        
        Result:
        return $data;
    }

    public function logout()
    {
        $this->permissions([ 'auth' ]);
        
        auth()->logout();
        header('Location: ' . config('app.url'));
    }
}
