# Webinar Web
-----------

1. Renombrar archivo de configuración `config.ini.example` a `config.ini` y realizar los cambios correspondientes en el archivo
2. Renombrar archivo de configuración `evotacion/admin/ajax/db_config.example.php` a `db_config.php` y realizar los cambios correspondientes en el archivo
3. Ejecutar el siguiente comando en la base de datos
```sql
    ALTER TABLE `users` ADD `nacionalidad` VARCHAR(255) NULL AFTER `estado`, ADD `nif` VARCHAR(255) NULL AFTER `nacionalidad`, ADD COLUMN `token` VARCHAR(255) NULL;
    ALTER TABLE `preguntas` ADD COLUMN `corregido` VARCHAR(4500) NULL, ADD COLUMN `hora_proyectar` TIMESTAMP NULL;
    ALTER TABLE `controlfuentes` ADD COLUMN `chat` TINYINT(1) NULL, ADD COLUMN `chat_source` TEXT NULL;
    INSERT INTO `users`(`idusuario`, `usuario`, `clave`, `tipo`) VALUES(3, 'editor', 'editor', 3);
	INSERT INTO `controlfuentes`(`idControlFuente`, `descripcion`, `source`, `activo`, `updateFrequency`, `audio`, `chat`, `chat_source`) VALUES('6', 'Eliminar interval', NULL, '0', NULL, NULL, NULL, NULL);

	--- 11/05/2021

	ALTER TABLE `controlfuentes` CHANGE `idControlFuente` `idControlFuente` INT(11) NOT NULL AUTO_INCREMENT;
	ALTER TABLE `controlfuentes` ADD COLUMN `alternative_id` INT(11) NULL;
	ALTER TABLE `users_tiempos` ADD COLUMN `user_agent` VARCHAR(500) NULL;
	ALTER TABLE `users_tiempos` ADD COLUMN `fuente_id` INT(11) NULL;
	CREATE TABLE `users_intervals` (
	`id` bigint(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
	`users_id` int(11) UNSIGNED DEFAULT NULL,
	`created_at` datetime DEFAULT NULL,
	`user_agent` varchar(500) DEFAULT NULL,
	`fuente_id` int(11) DEFAULT NULL
	) ENGINE=InnoDB DEFAULT CHARSET=latin1;
	UPDATE `usuarios` SET `usuario` = '3ways', `clave` = 'S3ways2012' WHERE `usuarios`.`idusuario` = 0;
```

# TO-DO List
------------
[x] Integrar preguntas con el proyecto principal