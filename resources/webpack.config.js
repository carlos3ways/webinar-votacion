/**
 * @author Alexis Bogado
 * @package webinar-preguntas
 */

const path = require('path')
const { VueLoaderPlugin } = require('vue-loader')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

const jsFiles = {
    devtool: 'inline-source-map', // disable unsafe-eval on development mode
    entry: {
        app: [
            'core-js/stable',
            'regenerator-runtime/runtime',
            path.resolve('./resources/ts/app.ts')
        ]
    },
    output: {
        path: path.resolve('./assets/preguntas/js'),
        filename: '[name].min.js',
    },
    resolve: {
        extensions: ['.js', '.jsx', '.ts', '.tsx', '.vue']
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                exclude: /node_modules/,
                options: {
                    loaders: {
                        js: 'babel-loader',
                        ts: [
                            {
                                loader: 'ts-loader',
                                options: {
                                    appendTsSuffixTo: [/\.vue$/]
                                }                
                            }
                        ]
                    }         
                }
            },
            {
                test: /\.js?$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            [
                                '@babel/preset-env',
                                {
                                    modules: false,
                                    targets: {
                                        browsers: ["> 1%", "last 2 versions", "not ie <= 8", "ie >= 11"]
                                    }
                                }
                            ],
                        ],
                        plugins: [
                            "@babel/plugin-syntax-dynamic-import",
                            ["@babel/plugin-transform-arrow-functions", { "spec": true }]
                        ]
                    }
                }
            },
            {
                test: /\.tsx?$/,
                loader: 'ts-loader',
                exclude: /node_modules/,
                options: {
                    appendTsSuffixTo: [/\.vue$/]
                }
            }
        ]
    },
    plugins: [
        new VueLoaderPlugin(),
    ],
    performance: {
        hints: false,
        maxEntrypointSize: 512000,
        maxAssetSize: 512000
    },
    optimization: {
        splitChunks: {
            chunks: 'all',
            name: true,
            name: 'vendors'
        }
    }
}

const sassFiles = {
    context: path.resolve('./resources/sass'),
    entry: {
        app: './app.sass'
    },
    output: {
        path: path.resolve('./assets/preguntas/css')
    },
    module: {
        rules: [
            {
                test: /\.s[ac]ss$/i,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'sass-loader'
                ]
            }
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: '[name].min.css'
        }),
        {
            apply(compiler) {
                compiler.hooks.shouldEmit.tap('Remove js files', (compilation) => {
                    Object.keys(compilation.assets).filter(asset => asset.substring(asset.length - 3) === '.js').forEach(asset => {
                        delete compilation.assets[asset]
                    })
                
                    return true
                })
            }
        }
    ]
}

module.exports = [ jsFiles, sassFiles ]