/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-preguntas
 */

declare module '*.vue' {
    import Vue from 'vue'
    export default Vue
}