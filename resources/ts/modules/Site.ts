/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-preguntas
 */

import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators'
import Question from '../models/Question'
import Setting from '../models/Setting'

@Module({
    namespaced: true
})
export default class Site extends VuexModule {
    private _isScreeningShown: boolean = false
    private _currentScreeningQuestion: Question | null = null
    private _settings: Setting | null = null

    get isScreeningShown(): boolean {
        return this._isScreeningShown
    }

    get currentScreeningQuestion(): Question | null {
        return this._currentScreeningQuestion
    }

    get settings(): Setting | null {
        return this._settings
    }

    @Mutation
    public setScreeningVisibility(status: boolean): void {
        this._isScreeningShown = status
    }

    @Mutation
    public setCurrentScreeningQuestion(payload: Question | null): void {
        this._currentScreeningQuestion = payload
    }

    @Mutation
    public setSettings(payload: Setting | null): void {
        this._settings = payload
    }

    @Action
    public updateScreeningVisibility(status: boolean): void {
        this.context.commit('setScreeningVisibility', status)
    }

    @Action
    public updateCurrentScreeningQuestion(payload: Question | null): void {
        this.context.commit('setCurrentScreeningQuestion', payload)
    }

    @Action
    public updateSettings(payload: Setting | null): void {
        this.context.commit('setSettings', payload)
    }
}