/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-preguntas
 */

import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators'
import Admin from '../models/Admin'

const storedAdmin: string | null = localStorage.getItem('userData')

@Module({
    namespaced: true
})
export default class User extends VuexModule {
    private _admin: Admin | null = (storedAdmin ? JSON.parse(storedAdmin) : null)

    get admin(): Admin | null {
        return this._admin
    }

    @Mutation
    public setAdmin(payload: Admin | null): void {
        this._admin = payload
    }

    @Action
    public updateAdmin(payload: Admin | null): void {
        if (!payload) {
            localStorage.removeItem('userData')
        } else {
            localStorage.setItem('userData', JSON.stringify(payload))
        }

        this.context.commit('setAdmin', payload)
    }
}