/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-preguntas
 */

import Vue from 'vue'
import VueRouter, { Route, NavigationGuardNext } from 'vue-router'
import LoginView from '../vue/views/Login.vue'
import QuestionsView from '../vue/views/Questions.vue'
import AnalyticsView from '../vue/views/Analytics.vue'
import Roles from './types/Roles'
import axios from 'axios'
import store from './store'

Vue.use(VueRouter)

const router = new VueRouter({
    base: '/preguntas',
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'login',
            component: LoginView,
            meta: {
                requiresAuth: false
            }
        },
        {
            path: '/questions',
            name: 'questions',
            component: QuestionsView,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/analytics',
            name: 'analytics',
            component: AnalyticsView,
            meta: {
                requiresAuth: true,
                requiredRole: Roles.EDITOR
            }
        }
    ]
})

router.beforeEach(async (to: Route, from: Route, next: NavigationGuardNext<Vue>): Promise<void> => {
    let admin = store.getters['User/admin']
    if (admin) {
        await axios.get('/api/preguntas/auth/user')
            .then(({ data }) => (!data || typeof data !== 'object') && store.dispatch('User/updateAdmin', null))
            .catch(() => store.dispatch('User/updateAdmin', null))
            .finally(() => admin = store.getters['User/admin'])
    }

    if (to.meta.hasOwnProperty('requiresAuth')) {
        const isAuthRequired = to.meta.requiresAuth
        const requiredRole = to.meta.requiredRole

        if (isAuthRequired && !admin) {
            next({ name: 'login' })
        } else if (!isAuthRequired && admin) {
            next({ name: 'questions' })
        } else if (requiredRole && admin.role !== requiredRole) {
            next({ name: 'questions' })
        } else {
            next()
        }
    } else {
        next()
    }
})

export default router