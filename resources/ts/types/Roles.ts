/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-preguntas
 */

enum Roles {
    SUPERVISOR,
    MODERATOR,
    ADMINISTRATOR,
    EDITOR,
}

export default Roles