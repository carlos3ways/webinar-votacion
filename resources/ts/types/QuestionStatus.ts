/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-preguntas
 */

enum QuestionStatus {
    NEEDS_APPROVAL,
    RECEIVED,
    SENT,
    ANSWERED,
}
 
export default QuestionStatus