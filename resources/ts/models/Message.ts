/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-preguntas
 */

import Model from './Model'
import Admin from '../models/Admin'

interface Message extends Model {
    text: string,
    created_at: string,
    sender: Admin
}

export default Message