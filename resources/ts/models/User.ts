/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-preguntas
 */

import Model from './Model'

interface User extends Model {
    full_name: string,
    email: string
}

export default User