/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-preguntas
 */

import Model from './Model'

interface Streaming extends Model {
    date: string,
    start: string,
    end: string,
    current_range: string,
    online_users: number,
    total_users: number,
    question_count: number,
    user_count: number,
    new_users: number,
    allow_newsletters: number,
    screened_questions: number,
    answered_questions: number,
    edited_questions: number,
    release_date: string
}

export default Streaming