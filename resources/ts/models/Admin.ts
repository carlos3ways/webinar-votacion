/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-preguntas
 */

import Model from './Model'
import Roles from '../types/Roles'

interface Admin extends Model {
    name: string,
    role: Roles
}

export default Admin