/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-preguntas
 */

import Model from './Model'

interface Setting extends Model {
    max_length: number,
    color: string
}

export default Setting