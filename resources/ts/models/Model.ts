/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-preguntas
 */

interface Model {
    [key: string]: any,
    id?: number
}

export default Model