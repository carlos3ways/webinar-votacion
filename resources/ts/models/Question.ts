/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-preguntas
 */

import Model from './Model'
import User from '../models/User'
import QuestionStatus from '../types/QuestionStatus'

interface Question extends Model {
    user_id: number,
    text: string,
    created_at: string,
    status: QuestionStatus,
    order: number,
    edited: string,
    screening_date: string,
    user: User | null,
    hospitales?: boolean
}

export default Question