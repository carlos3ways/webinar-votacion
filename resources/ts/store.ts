/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-preguntas
 */

import Vue from 'vue'
import Vuex from 'vuex'
import SiteModule from './modules/Site'
import UserModule from './modules/User'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        Site: SiteModule,
        User: UserModule
    }
})