/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-preguntas
 */

import Roles from "./types/Roles"
import store from './store'

const hasRole = (role: string): boolean => {
    const admin = store.getters['User/admin']

    switch (role) {
        case 'editor':
            return admin?.role === Roles.EDITOR

        case 'administrator':
            return admin?.role === Roles.ADMINISTRATOR

        case 'moderator':
            return admin?.role === Roles.MODERATOR

        case 'supervisor':
            return admin?.role === Roles.SUPERVISOR

        default:
            return false
    }
}

export {
    hasRole
}