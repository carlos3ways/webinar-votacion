/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-preguntas
 */

import Vue, { CreateElement } from 'vue'
import Layout from '../vue/Layout.vue'
import store from './store'
import router from './router'

new Vue({
    store,
    router,
    render: (h: CreateElement) => h(Layout)
}).$mount('#app')