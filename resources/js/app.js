/**
 * @author Alexis Bogado
 * @package webinar-preguntas
 */

// Libraries
import Vue from 'vue'
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import Axios from 'axios'

// Vue components
import MainComponent from '../vue/Main.vue'
import LoginComponent from '../vue/Login.vue'
import QuestionsComponent from '../vue/Questions.vue'
import AnalyticsComponent from '../vue/Analytics.vue'

Vue.use(VueRouter)
Vue.use(Vuex)

const router = new VueRouter({
    base: appData.subdir,
    mode: 'history',
    routes: [
        { path: '/', name: 'index', component: LoginComponent },
        { path: '/questions', name: 'questions', component: QuestionsComponent },
        { path: '/analytics', name: 'analytics', component: AnalyticsComponent },
    ],
})

const store = new Vuex.Store({
    state: {
        authData: null,
        showScreening: false
    },
    getters: {
        authData(state) {
            return state.authData
        }
    },
    mutations: {
        setScreeningVisibility(state) {
            state.showScreening = !state.showScreening
        },
        setAuthData(state, payload) {
            state.authData = payload
        },
        logOut(state) {
            state.authData = null
        }
    },
    actions: {
        setScreeningVisibility({ commit }) {
            commit('setScreeningVisibility')
        },
        setAuthData({ commit }, payload) {
            localStorage.setItem('authData', JSON.stringify(payload))
            commit('setAuthData', payload)
        },
        logOut({ commit}) {
            commit('logOut')
        }
    }
})

new Vue({
    router,
    store,
    computed: {
        loggedIn: function() {
            if (!this.$store.getters.authData) {
                const authData = localStorage.getItem('authData')
                if (authData) this.$store.dispatch('setAuthData', JSON.parse(authData))
            }

            return this.$store.getters.authData
        },
        authData: function() {
            return this.$store.getters.authData
        },
        roleId: function() {
            if (!this.authData) return null
            return this.authData.user.tipo
        }
    },
    methods: {
        logOut: function(reload = false) {
            Axios.post(`${appData.url}/api/auth/logout`).then(() => {
                localStorage.removeItem('authData')
                this.$store.dispatch('logOut')
                if (reload) location.reload()
                else this.$router.push({ name: 'index' })
            })
        },
        validJSON: function(data) {
            return (typeof data === 'object')
        }
    },
    mounted() {
        if (this.loggedIn)
            Axios.get(`${appData.url}/api/auth/data`).then(({ data }) => {
                if (!data || !this.validJSON(data)) this.logOut(true)
            }).catch(() => {
                this.logOut(true)
            })
    },
    render: h => h(MainComponent)
}).$mount('#app')
