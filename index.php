<?php

/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 * @package webinar-web
 */

date_default_timezone_set('Europe/Madrid');
error_reporting(E_ALL ^ E_DEPRECATED); // Show all errors except deprecation warnings
// error_reporting(0);

require_once __DIR__ . '/app/helpers.php';

// Set session settings
ini_set('session.cookie_httponly', 1);
ini_set('session.cookie_secure', 1);
ini_set("session.cookie_lifetime", 28800);
ini_set('session.gc_maxlifetime', 28800);

session_start();
load_app();

// Set security settings
header('Cache-Control: no-store, no-cache, must-revalidate, max-age=0');
header('Cache-Control: post-check=0, pre-check=0', false);
header('Pragma: no-cache');
header("Content-Security-Policy: default-src blob:;img-src https: data:;font-src https:;frame-src https:; connect-src https: wss: blob:; script-src 'self' 'unsafe-inline' https: blob: www.google-analytics.com ajax.googleapis.com  www.google.com www.gstatic.com www.googletagmanager.com; style-src https: 'unsafe-inline'; object-src 'none'; frame-ancestors 'none'; base-uri 'none'; form-action 'self'; media-src https: blob:;");
header("Strict-Transport-Security:max-age=63072000; includeSubDomains; preload");
header('X-XSS-Protection: 1; mode=block');
header('X-Content-Type-Options: nosniff');
header('X-Frame-Options: ALLOW-FROM ' . config('app.url'));

// Redirect without www.
if (substr($_SERVER['HTTP_HOST'], 0, 4) === 'www.'):
    header('Location: http' . ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 's' : '') . '://' . (substr($_SERVER['HTTP_HOST'], 4) . $_SERVER['REQUEST_URI']));
    exit;
endif;

echo $routes->render($_SERVER['REQUEST_METHOD'], $_SERVER['REQUEST_URI']);
