<?php
# @author Carlos Colmenarez <carlos.colmenarez@s3w.es>

//datos de conexion
include_once("../config.php");

//validar datos recibidos
if (!isset($_POST["socio_id"]) || !isset($_POST["evento_id"])) {
    echo "No se han recibido los datos correctamente";
    die();
}
$evento_id=$_POST["evento_id"];
//datos de socio que delega su voto
$socio_id=$_POST["socio_id"];

//conexion a base de datos
$pdo=new PDO("mysql:host=$servername;dbname=$database;charset=$charset", $username, $password);

//borrar delegacion
$statement2=$pdo->prepare("UPDATE participantes_delegaciones SET socio_delegado_id = NULL, socio_delegado_nombre = NULL WHERE id_evento=".$evento_id." AND socio_id=".$socio_id."");
$statement2->execute();

if (!$statement2) {
    echo 'Error al eliminar la delegación';
} else {
    $results2 = $statement2->fetch(PDO::FETCH_ASSOC);
    // http_response_code(200);
    echo "Se ha eliminado la delegación!";
}
$conn = null;
?>