<?php

/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 */

// Set content-type header
header('Content-Type: application/json; charset=utf-8');

// Load required files
require_once '../config.php';

// Response object
$result = [ 'success' => false ];

// Define a new PDO instance
$pdo = new PDO("mysql:host={$servername};dbname={$database};charset={$charset}", $username, $password);

/**
 * Run SQL query
 *
 * @param string $query
 * @param string $type
 * @param array $params
 * 
 * @return PDOStatement
 */
function run($query, $type, $params = [ ]) {
    global $pdo;

    $statement = $pdo->prepare($query);
    foreach ($params as $key => &$value)
        $statement->bindParam(":{$key}", $value);
    
    $statement->execute();
    if (!$statement)
        throw_message("Error al ejecutar la consulta de {$type}");

    return $statement;
}

function send_data($data) {
    global $result;

    $result['success'] = true;
    $result['data'] = $data;

    die(get_result());
}

/**
 * Throw error message
 *
 * @param string $message
 * 
 * @return string
 */
function throw_message($message) {
    global $result;

    $result['message'] = $message;

    die(get_result());
}

/**
 * Get parsed response object
 *
 * @return string
 */
function get_result() {
    global $result;

    return json_encode($result);
}
