<?php
/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 */

require_once './helpers.php';

// Required parameters
$usuario = (isset($_POST['usuario']) ? $_POST['usuario'] : null);
$pregunta = (isset($_POST['pregunta']) ? $_POST['pregunta'] : null);
$respuesta = (isset($_POST['respuesta']) ? $_POST['respuesta'] : null);
if (!$usuario || !$pregunta || (!$respuesta && ($respuesta < 0 || $respuesta > 2)))
    throw_message('No se han recibido los datos correctamente');

// Check if there is any active event and select the first result
$eventos_query = run('SELECT id FROM eventos WHERE activo = 1 LIMIT 1', 'eventos');
$eventos = $eventos_query->fetchAll(PDO::FETCH_NUM);
$evento_id = ($eventos[0][0] ?? null);
if (!$evento_id)
    throw_message('No hay ningún evento activo');

// Check if user has delegated the vote in the current event
$votos_delegados_query = run('SELECT * FROM participantes_delegaciones WHERE id_evento = :evento_id AND socio_id = :usuario_id AND socio_delegado_id IS NOT NULL', 'votos_delegados', [ 'usuario_id' => $usuario, 'evento_id' => $evento_id ]);
$votos_delegados = $votos_delegados_query->rowCount();
if ($votos_delegados > 0)
    throw_message('El usuario ha delegado su voto');

// Get question by id and event id
$preguntas_query = run("SELECT id, pregunta FROM preguntas_votar WHERE id = :pregunta_id AND id_evento = {$evento_id} AND activo = 1", 'preguntas', [ 'pregunta_id' => $pregunta ]);
$pregunta = $preguntas_query->fetch(PDO::FETCH_ASSOC);
if (!$pregunta)
    throw_message('No se ha encontrado la pregunta');

// Check if user already voted
$votos_query = run("SELECT * FROM votos WHERE id_usuario = :usuario_id AND id_pregunta = {$pregunta['id']}", 'votos', [ 'usuario_id' => $usuario ]);
$votos = $votos_query->rowCount();
if ($votos > 0)
    throw_message('El usuario ya ha votado a esta pregunta');

// Register vote
$vote = run('INSERT INTO votos(id_usuario, id_pregunta, id_respuesta) VALUES(:id_usuario, :id_pregunta, :id_respuesta)', 'registrar_voto', [
    'id_usuario' => $usuario,
    'id_pregunta' => $pregunta['id'],
    'id_respuesta' => $respuesta
]);

send_data('La respuesta se ha registrado correctamente');
