<?php
# @author Carlos Colmenarez <carlos.colmenarez@s3w.es>

//datos de conexion
include_once("../config.php");

if (!isset($_GET["evento_id"])) {
    echo "No se han recibido los datos correctamente";
    die();
}
$evento_id = $_GET["evento_id"];
//Conexión a BBDD
$pdo=new PDO("mysql:host=$servername;dbname=$database;charset=$charset", $username, $password);

//devolver pregunta activa del evento
$statement2=$pdo->prepare("SELECT * FROM participantes_delegaciones WHERE id_evento=".$evento_id." AND socio_delegado_id IS NOT NULL");
$statement2->execute();

if (!$statement2) {
    echo "Error al consultar votos delegados";
} else {
    $results2 = $statement2->fetchAll(PDO::FETCH_ASSOC);
    // http_response_code(200);
    echo json_encode($results2);
}
$conn = null;
?>