<?php
/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 */

require_once './helpers.php';

// Get event id parameter
$evento_id = ($_GET['id'] ?? null);
if (!$evento_id)
    throw_message('Evento no definido');

// Check if event exists
$eventos_query = run('SELECT * FROM eventos WHERE id = :evento_id', 'eventos', [ 'evento_id' => $evento_id ]);
$evento = $eventos_query->fetch(PDO::FETCH_ASSOC);
if (!$evento)
    throw_message('No se ha encontrado el evento');

$data = [ ];
// Get current event questions
$sql = "SELECT p.id as id, p.pregunta as pregunta, p.orden as orden 
        FROM preguntas_votar p 
        JOIN (SELECT v.id_pregunta FROM votos v GROUP BY v.id_pregunta) AS v
        ON p.id=v.id_pregunta
        WHERE p.id_evento = :evento_id";

$preguntas_query = run($sql, 'preguntas', [ 'evento_id' => $evento_id ]);
$preguntas = $preguntas_query->fetchAll(PDO::FETCH_ASSOC);

if (!$preguntas) {
    goto send_data;
}
// Get all votes from every event question
foreach ($preguntas as $pregunta):
    $votos_query = run("SELECT id_respuesta, id_usuario FROM votos WHERE id_pregunta = {$pregunta['id']}", 'votos');
    $votos = $votos_query->fetchAll(PDO::FETCH_ASSOC);

    $votos_delegados_query = run("SELECT socio_id, socio_delegado_id, COUNT(*) as valor_voto FROM participantes_delegaciones WHERE id_evento = :evento_id AND socio_delegado_id IS NOT NULL GROUP BY socio_delegado_id HAVING valor_voto > 0 ORDER BY valor_voto DESC", 'votos_delegados', [ 'evento_id' => $evento_id ]);
    $votos_delegados = $votos_delegados_query->fetchAll(PDO::FETCH_ASSOC);

    foreach ($votos_delegados as $voto_delegado):
        $votos_by_user = array_filter($votos, function($voto) use ($voto_delegado) { return $voto['id_usuario'] == $voto_delegado['socio_delegado_id']; });
        $voto_by_user = (array_values($votos_by_user)[0] ?? null);

        if (!$voto_by_user)
            continue;

        for ($i = 0; $i < $voto_delegado['valor_voto']; $i++)
            $votos[] = [
                'id_respuesta' => $voto_by_user['id_respuesta'],
                'id_usuario' => $voto_delegado['socio_delegado_id']
            ];
    endforeach;

    $aprobado = array_filter($votos, function($voto) { return $voto['id_respuesta'] == 0; });
    $desestimado = array_filter($votos, function($voto) { return $voto['id_respuesta'] == 1; });
    $abstencion  = array_filter($votos, function($voto) { return $voto['id_respuesta'] == 2; });
    $total_votos = count($aprobado) + count($desestimado) + count($abstencion);
    
    if($total_votos>0) { 
        $aprobado_porcentaje = count($aprobado) * 100 / $total_votos;
        $desestimado_porcentaje = count($desestimado) * 100 / $total_votos;
        $abstencion_porcentaje = count($abstencion) * 100 / $total_votos;
    } else {
        $aprobado_porcentaje = 0;
        $desestimado_porcentaje = 0;
        $abstencion_porcentaje = 0;
    }
    
    $data[] = [
        'pregunta' => $pregunta['pregunta'],
        'aprobado' => (string)count($aprobado)." (". $aprobado_porcentaje."%)",
        'desestimado' => (string)count($desestimado)." (". $desestimado_porcentaje."%)",
        'abstencion' => (string)count($abstencion)." (". $abstencion_porcentaje."%)",
        'total_votos' => $total_votos
    ];
endforeach;

send_data:
send_data($data);
