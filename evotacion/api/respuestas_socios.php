<?php
/**
 * @author Carlos Colmenarez <carlos.colmenarez@s3w.es>
 * Devuelve JSON con votos de los usuarios de un evento
 */

require_once './helpers.php';

###### VALIDATIONS ######
// Get event id parameter
$evento_id = ($_GET['id'] ?? null);
if (!$evento_id)
    throw_message('Evento no definido');

// Check if event exists
$eventos_query = run('SELECT * FROM eventos WHERE id = :evento_id', 'eventos', [ 'evento_id' => $evento_id ]);
$evento = $eventos_query->fetch(PDO::FETCH_ASSOC);
if (!$evento)
    throw_message('No se ha encontrado el evento');

###### FECTH DATA #######
$data = [];
// Get questions of event
$preguntas_query = run("SELECT id, pregunta, orden FROM preguntas_votar WHERE id_evento = :evento_id ORDER BY orden", 'preguntas', [ 'evento_id' => $evento_id ]);
$preguntas = $preguntas_query->fetchAll(PDO::FETCH_ASSOC);
if (!$preguntas) {
    $data = [["Aún no hay preguntas en el evento"]];
    goto send;
}
$preguntas_ids = implode(", ", array_column($preguntas, 'id'));

// Get socios who have answers
$sql = "SELECT id_usuario FROM votos WHERE id_pregunta IN (".$preguntas_ids.") GROUP BY id_usuario";
$socios_query = run($sql, 'socios', [ 'preguntas_ids' => $preguntas_ids]);
$socios = $socios_query->fetchAll(PDO::FETCH_ASSOC);
if (!$socios) {
    $data = [["No se han encontrado votos en el evento"]];
    goto send;
}

// Get names of participantes
$participantes_sql = "SELECT socio_id, socio_nombre 
                        FROM participantes_delegaciones 
                        WHERE id_evento = ".$evento_id;

$participantes_query = run($participantes_sql, 'participantes');
$participantes = $participantes_query->fetchAll(PDO::FETCH_ASSOC);
if (!$participantes) {
    $data = [["No se han encontrado participantes en el evento"]];
    goto send;
}

/** Get voto del socio de una pregunta en concreto
*/
function getVotoSocio($socio_id, $pregunta_id) {
    $votos_sql = "SELECT *
    FROM votos 
    WHERE id_pregunta IN ($pregunta_id) AND id_usuario = $socio_id";
    $votos_query = run($votos_sql, 'participantes');
    $votos = $votos_query->fetch(PDO::FETCH_ASSOC);
    if ($votos) {
        switch ($votos["id_respuesta"]) {
            case 0:
                return "A favor";
                break;
            case 1:
                return "En contra";
                break;
            case 2:
                return "Abstención";
                break;
            default:
            return "respuesta no definida";
        }
    } else {
        return "";
    }
}
//socios con votos delegados
$votosdelegados_sql = "SELECT socio_id, socio_delegado_id FROM participantes_delegaciones WHERE id_evento=".$evento_id." AND socio_delegado_id IS NOT NULL";
$votosdelegados_query = run($votosdelegados_sql, 'participantes');
$votosdelegados = $votosdelegados_query->fetchAll(PDO::FETCH_ASSOC);
####### RESPONSE #########
foreach($socios as $key=>$socio):
    $socio_id = $socio["id_usuario"];
    
    $socio_nombre="";
    foreach ($participantes as $participante):
        if ($participante["socio_id"] == $socio["id_usuario"]) {
            $socio_nombre = $participante["socio_nombre"];
        }
    endforeach;

    $data [] = [
        'nsocio' => $socio_id,
        'socio_nombre' => $socio_nombre
    ];

    //valor del voto
    $valor_voto = 1;
    foreach($votosdelegados as $votosdelegado):
        if ($votosdelegado["socio_delegado_id"] == $socio["id_usuario"]) {
            $valor_voto++;
        }
    endforeach;
    $data[$key]["valorvoto"] = $valor_voto;

    //votos
    foreach($preguntas as $index=>$pregunta):
        $respuesta = getVotoSocio($socio_id, $pregunta["id"]);
        $tag = "pregunta".$pregunta["orden"];
        $data[$key][$tag] = $respuesta;
    endforeach;
    
endforeach;

send:
send_data($data);