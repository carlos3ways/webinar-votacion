<?php
/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 */

require_once './helpers.php';

// Check if there is any active event and select the first result
$eventos_query = run('SELECT id FROM eventos WHERE activo = 1 LIMIT 1', 'eventos');
$eventos = $eventos_query->fetchAll(PDO::FETCH_NUM);
$evento_id = ($eventos[0][0] ?? null);
if (!$evento_id)
    throw_message('No hay ningún evento activo');


// Get current event question
$preguntas_query = run("SELECT id, pregunta FROM preguntas_votar WHERE id_evento = {$evento_id} AND activo = 1", 'preguntas');
$pregunta = $preguntas_query->fetch(PDO::FETCH_ASSOC);
if (!$pregunta)
    throw_message('No hay ninguna pregunta activa');

$votos_query = run("SELECT id_respuesta, id_usuario FROM votos WHERE id_pregunta = {$pregunta['id']}", 'votos');
$votos = $votos_query->fetchAll(PDO::FETCH_ASSOC);

$votos_delegados_query = run("SELECT socio_delegado_id, COUNT(*) as valor_voto FROM participantes_delegaciones WHERE id_evento = {$evento_id} AND socio_delegado_id IS NOT NULL GROUP BY socio_delegado_id HAVING valor_voto > 0 ORDER BY valor_voto DESC", 'votos_delegados');
$votos_delegados = $votos_delegados_query->fetchAll(PDO::FETCH_ASSOC);

foreach ($votos_delegados as $voto_delegado):
    $votos_by_user = array_filter($votos, function($voto) use ($voto_delegado) { return $voto['id_usuario'] == $voto_delegado['socio_delegado_id']; });
    $voto_by_user = ($votos_by_user[0] ?? null);
    if (!$voto_by_user)
        continue;

    for ($i = 0; $i < $voto_delegado['valor_voto']; $i++)
        $votos[] = [
            'id_respuesta' => $voto_by_user['id_respuesta'],
            'id_usuario' => $voto_delegado['socio_delegado_id']
        ];
endforeach;

send_data([
    'pregunta' => $pregunta,
    'votos' => $votos
]);
