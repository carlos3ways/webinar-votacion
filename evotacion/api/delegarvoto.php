<?php
# @author Carlos Colmenarez <carlos.colmenarez@s3w.es>

//datos de conexion
include_once("../config.php");

//validar datos recibidos
if (!isset($_POST["delegar_de_id"]) || !isset($_POST["delegar_a_id"]) || !isset($_POST["evento_id"])) {
    echo "No se han recibido los datos correctamente";
    die();
}
$evento_id=$_POST["evento_id"];
//datos de socio que delega su voto
$delegar_de_id=$_POST["delegar_de_id"];
$delegar_de_nombre = (isset($_POST["delegar_de_nombre"])) ? $_POST["delegar_de_nombre"] : "";

//datos de socios a quien delega su voto
$delegar_a_id=$_POST["delegar_a_id"];
$delegar_a_nombre = (isset($_POST["delegar_a_nombre"])) ? $_POST["delegar_a_nombre"] : "";

//Conexión a BBDD
$pdo=new PDO("mysql:host=$servername;dbname=$database;charset=$charset", $username, $password);

//verificar si el socio ya tiene el voto delegado a otro socio en el mismo evento
$statement=$pdo->prepare("SELECT * FROM participantes_delegaciones WHERE id_evento=".$evento_id." AND socio_id=".$delegar_de_id." AND socio_delegado_id IS NOT NULL LIMIT 1;");
$statement->execute();
if (!$statement) {  
    echo 'Error al verificar datos del usuario';
    die();
} else {
    $delegaciones = $statement->fetchAll(PDO::FETCH_NUM);
	// existe id y delegado_id es nulo
    if ((isset($delegaciones[0][4]) && $delegaciones[0][4]>0)) {
        echo "Error: el voto del socio ya ha sido delegado";
        die();
    } else {
		$statement2 = $pdo->prepare("INSERT INTO participantes_delegaciones (id_evento, socio_id, socio_nombre, socio_delegado_id, socio_delegado_nombre) VALUES (:id_evento, :socio_id, :socio_nombre, :socio_delegado_id, :socio_delegado_nombre)");
		$statement2->bindParam(':id_evento', $evento_id);
		$statement2->bindParam(':socio_id', $delegar_de_id);
		$statement2->bindParam(':socio_nombre', $delegar_de_nombre);
		$statement2->bindParam(':socio_delegado_id', $delegar_a_id);
		$statement2->bindParam(':socio_delegado_nombre', $delegar_a_nombre);
		if($statement2->execute()) {
			echo "Se ha delegado el voto correctamente!";
			die();
		}
	}
}


//Registrar delegacion
$statement2 = $pdo->prepare("UPDATE participantes_delegaciones SET socio_delegado_id = :socio_delegado_id, socio_delegado_nombre = :socio_delegado_nombre WHERE id_evento = :id_evento AND socio_id = :socio_id AND socio_nombre = :socio_nombre");
$statement2->bindParam(':id_evento', $evento_id);
$statement2->bindParam(':socio_id', $delegar_de_id);
$statement2->bindParam(':socio_nombre', $delegar_de_nombre);
$statement2->bindParam(':socio_delegado_id', $delegar_a_id);
$statement2->bindParam(':socio_delegado_nombre', $delegar_a_nombre);
if($statement2->execute()) {
    echo "Se ha delegado el voto correctamente!";
}

?>