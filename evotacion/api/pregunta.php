<?php
/**
 * @author Alexis Bogado <alexis.bogado@s3w.es>
 */

require_once './helpers.php';

// Required parameters
$uid = ($_GET['uid'] ?? null);
if (!$uid)
    throw_message('No se ha definido ningún usuario');

// Check if there is any active event and select the first result
$eventos_query = run('SELECT id FROM eventos WHERE activo = 1 LIMIT 1', 'eventos');
$eventos = $eventos_query->fetchAll(PDO::FETCH_NUM);
$evento_id = ($eventos[0][0] ?? null);
if (!$evento_id)
    throw_message('No hay ningún evento activo');

// Get current event question
$preguntas_query = run('SELECT id, pregunta FROM preguntas_votar WHERE id_evento = :evento_id AND activo = 1', 'preguntas', [ 'evento_id' => $evento_id ]);
$pregunta = $preguntas_query->fetch(PDO::FETCH_ASSOC);
if (!$pregunta)
    throw_message('No hay ninguna pregunta activa');

// Check if user already vote
$votos_query = run('SELECT * FROM votos WHERE id_usuario = :usuario_id AND id_pregunta = :pregunta_id', 'votos', [ 'usuario_id' => $uid, 'pregunta_id' => $pregunta['id'] ]);
$voto = $votos_query->fetch(PDO::FETCH_ASSOC);

// Check if user has delegated the vote
$votos_delegados_query = run('SELECT * FROM participantes_delegaciones WHERE id_evento = :evento_id AND socio_id = :usuario_id AND socio_delegado_id IS NOT NULL', 'votos_delegados', [ 'usuario_id' => $uid, 'evento_id' => $evento_id ]);
$votos_delegados = $votos_delegados_query->rowCount();

send_data([
    'pregunta' => $pregunta,
    'answered' => ($voto ? $voto['id_respuesta'] : false),
    'delegado' => ($votos_delegados > 0)
]);
