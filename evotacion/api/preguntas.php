<?php
/**
 * @author Carlos Colmenarez <carlos.colmenarez@s3w.es>
 * JSON preguntas de un evento
 */

require_once './helpers.php';

###### VALIDATIONS ######
// Get event id parameter
$evento_id = ($_GET['id'] ?? null);
if (!$evento_id)
    throw_message('Evento no definido');

$data = [];
// Check if event exists
$eventos_query = run('SELECT * FROM eventos WHERE id = :evento_id', 'eventos', [ 'evento_id' => $evento_id ]);
$evento = $eventos_query->fetch(PDO::FETCH_ASSOC);
if (!$evento) {
    $data = [["No se ha encontrado el evento"]];
    goto send;
}
###### FECTH DATA #######
// Get questions of event
$preguntas_query = run("SELECT id, pregunta, orden FROM preguntas_votar WHERE id_evento = :evento_id ORDER BY orden", 'preguntas', [ 'evento_id' => $evento_id ]);
$preguntas = $preguntas_query->fetchAll(PDO::FETCH_ASSOC);
if (!$preguntas) {
    $data = [["Aún no hay preguntas en el evento"]];
    goto send;
}
$preguntas_ids = implode(", ", array_column($preguntas, 'id'));

####### RESPONSE #########
foreach($preguntas as $index=>$pregunta):
    // $data[$index][$pregunta["orden"]] = $pregunta["pregunta"];
    $data[] = [
        'numero' => $pregunta["orden"],
        'pregunta' => html_entity_decode(strip_tags($pregunta["pregunta"]))
    ];
endforeach;

send:
send_data($data);