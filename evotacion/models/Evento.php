<?php

if (!isset($_GET["evento_id"])) {
    echo "No se han recibido los datos correctamente";
    die();
}
$id = $_GET["evento_id"];

function getFecha($id) {
    //Conexión a BBDD
    include_once("../config.php");
    $pdo=new PDO("mysql:host=$servername;dbname=$database;charset=$charset", $username, $password);

    //devolver fecha de inicio del evento
    $statement2=$pdo->prepare("SELECT fecha_inicio FROM eventos WHERE id = :id");
    $statement2->bindParam(':id', $id);
    $statement2->execute();

    if (!$statement2) {
        echo "Error al consultar evento";
    } else {
        $results2 = $statement2->fetch(PDO::FETCH_ASSOC);
        return $results2["fecha_inicio"];
    }
    $conn = null;
}
