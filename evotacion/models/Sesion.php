<?php
# @author Carlos Colmenarez <carlos.colmenarez@s3w.es>

const SESSION_TIME = 28800;
const PASSWORD = 'icp2012';

class Session {
    static function start() {
        if (!isset($_SESSION)) {
            ini_set("session.cookie_lifetime", SESSION_TIME);
            ini_set("session.gc_maxlifetime", SESSION_TIME);
            session_start();
        }
    }
    static function check() {
        $login = (isset($_SESSION['acceso_admin']) && $_SESSION['acceso_admin']==1) ? true : false;
        return $login;    
    }
}