/* añadir campos a evento: anonimo y ranquing automatico */
ALTER TABLE `eventos`
	ADD COLUMN `anonimo` TINYINT(1) NOT NULL DEFAULT '0' AFTER `align_logo_foot`,
	ADD COLUMN `autoranking` TINYINT(1) NOT NULL DEFAULT '0' AFTER `anonimo`;
