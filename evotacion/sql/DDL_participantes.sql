/* Tabla de participantes en una votación (solo nickname) */
CREATE TABLE `participantes` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`nickname` VARCHAR(100) NULL DEFAULT '' COLLATE 'utf16_spanish_ci',
	`id_evento` TINYINT(4) NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`)
)
COLLATE='utf16_spanish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1000000000;

/* Índice único para nickname-evento */
ALTER TABLE `participantes`
ADD UNIQUE INDEX `UNIQUE_NICKNAME` (`id_evento`, `nickname`);