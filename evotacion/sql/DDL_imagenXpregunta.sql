/* añadir campo IMAGEN para CADA pregunta */
ALTER TABLE `preguntas` ADD `imagen` VARCHAR(192) NULL AFTER `musica`;
ALTER TABLE `preguntas` ADD `formato_a_4` TINYINT(1) NULL DEFAULT 0 AFTER `tiempo_responder`;

/* añadir campo IMAGEN para CADA respuesta */
ALTER TABLE `respuestas` ADD `imagen` VARCHAR(192) NULL AFTER `respuesta`;
