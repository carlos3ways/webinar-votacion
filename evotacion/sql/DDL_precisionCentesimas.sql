/* cambiar las fechas a DATETIME(2), en votos y en preguntas_history */
ALTER TABLE `votos` CHANGE `fecha` `fecha` DATETIME(3) DEFAULT CURRENT_TIMESTAMP(3) NOT NULL;
ALTER TABLE `preguntas_history` CHANGE `fecha` `fecha` DATETIME(3) DEFAULT CURRENT_TIMESTAMP(3) NOT NULL;
