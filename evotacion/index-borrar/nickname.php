<?php

    //die('Error 500');
    // pedir nickname si es un login solo con código
    if (!isset($_SESSION)) {
        session_start();
    }
    // si ha hecho login pero no hay nickname
    if (isset($_SESSION['acceso_permitido']) == 1) {
        // comprobar el nickname (id_usuario not set) o que el evento es anónimo
        if ( (isset($_SESSION['id_usuario']) && $_SESSION['id_usuario'] > 0) ||
             ($_SESSION['evento_anonimo'] == 1) ) {
            // ya hay usuario -> votar.php
            header("Location: votar.php");
            exit();
        }
    } else {
        // no hay login -> index.php
        header("Location: index.php");
        exit();
    }

    // Hay acceso pero no hay id_usuario
    // Se debe registrar el nickname desde aquí
    $id_evento = $_SESSION['id_evento'];

?>

<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8">
<title>Vota!!</title>
<meta http-equiv="Cache-control" content="no-cache">
<meta name="robots" content="noindex,nofollow">
<meta name="viewport" content="width=device-width, user-scalable=no" />
<link rel="shortcut icon" type="image/x-icon" href="assets/images/s3w-voto.ico" />
<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css"/>
<?php
    include("admin/ajax/check_theme.php");
?>
<style type="text/css" media="screen">
    .container {
        padding-top: 90px!important;
    }
    
    .outer {
        display: table;
        position: absolute;
        min-height: 500px;
        height: 100%;
        width: 100%;
    }

    .middle {
        display: table-cell;
        vertical-align: middle;
    }

    .inner {
        margin-left: auto;
        margin-right: auto; 
        max-width: 350px;
        width: 85%;
    }

</style>

</head>
<body>

<?php
    // define de upload images (para estilo personalizado)
    include_once "admin/inc/config.php";
    // conexión BD
    include_once "admin/ajax/db_connection.php";
    // funciones de estilo personalizado
    include_once "admin/inc/estilo_evento.php";
    // controlar estilos personalizados
    // recuperar los datos del evento
    $estilo_evento = getDatosEstiloEvento($db, $id_evento);
    $tema = $estilo_evento['theme'];

    // si está personalizado, leemos los datos de estilo
    if ($tema == 1) {
        $htmls = getHTMLSNavs($estilo_evento);
        echo $htmls['navbartop'];
    } else {
        include('admin/inc/navbar.php');
    }
?>

    <div class="outer">
        <div class="middle">
            <div class="inner">

                <div class="loginmodal-container">
                    <h1><?php echo $estilo_evento['nombre_evento']; ?></h1>
                    <hr>
                    <h4>Para participar, entra tu nombre o un apodo.</h4>
                    <h5>¡Debe ser único entre todos los participantes!</h5>
                    <div id="error-nickname" class="row col-md-12 hide">
                        <div id="error-nickname-text" class="bg-danger text-danger form-control-static col-md-6"></div>
                    </div>
                    <input type="text" id="nickname" placeholder="Nombre o apodo...">
                    <input type="submit" id="btn_nickname" name="login" class="login loginmodal-submit" value="Continuar">
                </div>

            </div>
        </div>
    </div>

<?php

    // el pie: si era personalizado
    if (isset($htmls['footer']) && $htmls['footer'] != '') {
        echo $htmls['footer'];
    } else {
        // el por defecto
        include('admin/inc/pre_footer.php') ;
    }
  
?>

<script type="text/javascript" src="assets/js/jquery-1.11.3.min.js"></script>
<script  type="text/javascript">
$(document).ready(function () {
    $('#btn_nickname').on('click', function() {
        if (! $('#error-nickname').hasClass('hide')) {
            $('#error-nickname').addClass('hide');
        }
        var nick = $('#nickname').val();
        $.post("admin/ajax/check_nickname.php", {
            nick:nick
        }, function (data, status) {
            // controlar el resultado
            var json = JSON.parse(data);
            if (json.ok) {
                window.location.href = 'votar.php';
            } else {
                // mostrar el error si no se ha podido guardar el nickname
                $('#error-nickname-text').html(json.error);
                if ($('#error-nickname').hasClass('hide')) {
                    $('#error-nickname').removeClass('hide');
                }
            }
        });
    });
});
</script>

</body>
</html>