<?php 

if(!isset($_SESSION)){
  session_start();
  $oculta = "";
}

$sessionid =  session_id();
$id_evento="";

?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Evotación - Sistema interactivo de votación</title>
    <meta http-equiv="Cache-control" content="no-cache">
    <meta name="robots" content="noindex,nofollow">
    <meta name="viewport" content="width=device-width, user-scalable=no" />
    <link rel="shortcut icon" type="image/x-icon" href="../assets/images/s3w-voto.ico" />
    <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="../assets/css/custom.css"/>
</head>

<body>

    <div class="container"  style="padding-top: 50px;">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-12">
                    <img class="img-responsive center-block"  src="../assets/images/evotacion_logo.png">
                    <h5 class="text-center" style="margin-bottom: 50px;color:#5B656E!important;opacity: 0.6;"><b>Sistema de Votación Interactiva 1.0.</b></h5>
                </div>
                <div id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
                    <div class="col-md-12">
                        <div class="modal-dialog">
                            <div class="loginmodal-container">
                                <h1 style="color:#5B656E!important;">Código de Acceso</h1><br>
                                <input type="text" id="codigo" placeholder="Código de acceso">
                                <input type="submit" name="login" class="login loginmodal-submit" value="Entrar" id="btn_codigo">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<script type="text/javascript" src="../assets/js/jquery-1.11.3.min.js"></script>
<!-- <script type="text/javascript" src="../assets/js/bootstrap.min.js"></script> -->

<script  type="text/javascript">
    $(document).ready(function () {
        $('#btn_codigo').on('click', function() {
            var codigo = $('#codigo').val();
            $.post("../admin/ajax/check.php", {
                codigo:codigo
            }, function (data, status) {
                window.location.href = 'nickname.php';
            });
        });
    });
</script>

</body>
</html>
