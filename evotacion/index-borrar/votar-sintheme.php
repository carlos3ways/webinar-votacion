<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Vota!!</title>
    <meta http-equiv="Cache-control" content="no-cache">
    <meta name="viewport" content="width=device-width, user-scalable=no" />
    <link rel="shortcut icon" type="image/x-icon" href="assets/images/s3w-voto.ico" />
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css"/>

    <?php

session_start();
if(isset($_SESSION['acceso_permitido']) == 1){
    $oculta = "hidden";
    $sessionid =  session_id();
    //echo "  - Acceso permitido - ";
    $id_evento = $_SESSION['id_evento'];
     if($id_evento==27){
echo '<link rel="stylesheet" type="text/css" href="../assets/css/custom_27.css" />';
}else{
echo '<link rel="stylesheet" type="text/css" href="../assets/css/custom.css" />';
}
?>



    <style type="text/css" media="screen">
        .container{padding-top:80px!important;}
        .btn-circle {
          width: 30px;
          height: 30px;
          text-align: center;
          padding: 6px 0;
          font-size: 12px;
          line-height: 1.428571429;
          border-radius: 15px;
        }
        
        .btn-circle.btn-lg {
          width: 50px;
          height: 50px;
          padding: 10px 16px;
          font-size: 18px;
          line-height: 1.33;
          border-radius: 25px;
        }

      .btn-circle.btn-xl {
        width: 70px;
        height: 70px;
        padding: 10px 16px;
        font-size: 24px;
        line-height: 1.33;
        border-radius: 35px;
      }

      .btn:focus {
        outline: none!important;
      }

  </style>

</head>
<body>
<?php 

   // echo $sessionid. " " .$id_evento ."- ";
    $ultima_respuesta="";

      if(isset($_SESSION['ultima_respuesta']))
      {
        $ultima_respuesta = $_SESSION['ultima_respuesta'];
      }
//    $data_hash=  password_hash($sessionid, PASSWORD_BCRYPT);
 //   echo $data_hash;
}
else{
  header("Location: index.php");
}
?>

<?php include('/admin/inc/navbar.php'); ?>
<!-- Content Section -->
<div class="container">
    <div class="row">
        <div class="col-md-12">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-xs-12 text-center">
             <div id="msghasvotado" class="">
<h4>Respuesta: <div style=" font-size: 20px; width: 100px;margin:0 auto;padding-top: 10px;">


<div class="btn btn-default btn-circle btn-xl" id="btn-2" data-id="2" style="
    width: 49px;
    height: 52px; background-color:#ccc;border-radius:10px;
"> <?=$ultima_respuesta?></div></h4>


            </div>
             <div id="msgreturn" class="text-success bg-warning"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="pull-left">
               <div data-ion="<?=$sessionid?>" id="<?=uniqid()?>" class="vote" ></div>
               <div data-evento="<?=$id_evento?>" data-hash="<?=$data_hash?>"  class="v_evento"></div> 
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-xs-12 martoph7020">
            <div class="col-md-4 col-xs-4 text-center">
              <button class="btn btn-default btn-circle btn-xl" id="btn-1" data-id="1" >1</button>
            </div>
            <div class="col-md-4 col-xs-4 text-center">
              <button class="btn btn-default btn-circle btn-xl" id="btn-2" data-id="2">2</button>
            </div>
            <div class="col-md-4 col-xs-4 text-center">
              <button  class="btn btn-default btn-circle btn-xl" id="btn-3" data-id="3">3</button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 martoph7020">
            <div class="col-md-4 col-xs-4 text-center">
              <button class="btn btn-default btn-circle btn-xl" id="btn-4" data-id="4">4</button>
            </div>
            <div class="col-md-4 col-xs-4 text-center">
              <button class="btn btn-default btn-circle btn-xl" id="btn-5" data-id="5">5</button>
            </div>
            <div class="col-md-4 col-xs-4 text-center">
              <button  class="btn btn-default btn-circle btn-xl" id="btn-6" data-id="6">6</button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 martoph7020">
            <div class="col-md-4 col-xs-4 text-center">
              <button class="btn btn-default btn-circle btn-xl" id="btn-7" data-id="7">7</button>
            </div>
            <div class="col-md-4 col-xs-4 text-center">
              <button class="btn btn-default btn-circle btn-xl" id="btn-8" data-id="8">8</button>
            </div>
            <div class="col-md-4 col-xs-4 text-center">
              <button  class="btn btn-default btn-circle btn-xl" id="btn-9" data-id="9">9</button>
            </div>
        </div>        
    </div>
    <div class="row">
        <div class="col-md-12 martoph7020">
            <div class="col-md-4 col-xs-4 text-center">
            </div>
            <div class="col-md-4 col-xs-4 text-center">
              <button class="btn btn-default btn-circle btn-xl" id="btn-0" data-id="10">0</button>
            </div>
            <div class="col-md-4 col-xs-4 text-center">
            </div>
        </div>        
    </div>
</div>

<!-- /Content Section -->
<?php include('/admin/inc/pre_footer.php') ;?>


<script type="text/javascript" src="assets/js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/loadReadyVotos.js"></script>
</body>
</html>