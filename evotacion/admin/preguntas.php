<?php 

include_once('../models/Sesion.php');
if (!isset($_SESSION)) {
    session_start();
}

// variables de entorno (DIR_WEB para lang tinymce)
include 'inc/config.php';

if ( isset($_SESSION['acceso_admin']) && ($_SESSION['acceso_admin'] == 1) ) {

    if(isset($_REQUEST['id'])){
        $id_evento = $_REQUEST['id'];
        include('inc/head.php'); ?>

    <body>
    <?php include('inc/navbar_s3w.php'); ?>
    <!-- Content Section -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div>
                    <a href="index.php" class="btn btn-success">&lt; Volver</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h1>Sistema de Votaciones</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="pull-right">
                <button class="btn btn-success" onclick="addPreEnPre(<?=$id_evento?>)">Añadir Pregunta</button>
                <!-- DESHABILITADOS BOTONES IMPORTAR PREGUNTAS Y REINICIAR VOTOS -->
                <!-- <button class="btn btn-success" onclick="showImportarPreguntas(<?=$id_evento?>)">Importar Preguntas</button>
                <div class="btn btn-danger" alt="Reiniciar votos" onclick="todosVotosCero(<?=$id_evento?>)"> <span class="ftprin">Reiniciar votación evento</span></div> -->
                </div>
            </div>
        </div>
        <div id="console-event"></div>
        <div class="row">
            <div class="col-md-12">
                <!-- Botones de activar o desactivar votación o resultados -->
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <div class="pull-right">
                            <span><strong>Activar/desactivar opciones del directo: </strong></span><br><br>
                            <label class="checkbox-inline">
                            <input id="controlVotacion" type="checkbox" data-toggle="toggle" data-value="poll"> Votación
                            </label>
                            <label class="checkbox-inline">
                            <input id="controlResultados" type="checkbox" data-toggle="toggle" data-value="results"> Resultados
                            </label>           
                        </div>
                    </div>
                </div>
                <h3>Preguntas:</h3>
                <span class="help-block">Activa y ordena preguntas, y/o actualiza el texto de la pregunta. La pregunta a mostrar al momento de habilitar la opción de voto a los socios será la que esté en "On".</span>

                <?php include_once('ajax/readPreguntas.php');?>
            </div>
        </div>
    </div>
    <!-- /Content Section -->
    <?php include('inc/modal_delete_pregunta.php'); ?>
    <?php include('inc/modal_add_pregunta.php'); ?>
    <?php include('inc/modal_votos_cero.php'); ?>
    <?php include('inc/modal_todos_votos_cero.php'); ?>
    <?php include('inc/modal_musica_pregunta.php'); ?>
    <?php include('inc/modal_imagen_pregunta.php'); ?>
    <?php include('inc/modal_subir_musica.php'); ?>
    <?php include('inc/modal_importar_preguntas.php'); ?>
    <?php include('inc/footer.php'); ?>


    <script type="text/javascript" src="../assets/js/script.js"></script>
    <script type="text/javascript" src="../assets/js/bootstrap-toggle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
    <script src="../vendor/kartik-v/bootstrap-fileinput/js/fileinput.min.js"></script>
    <script src="../vendor/kartik-v/bootstrap-fileinput/js/locales/es.js"></script>
    <script>
        var dir_musica = '<?=_DIR_MUSICA_WEB_?>';
        var dir_images_upload = '<?=_DIR_IMAGES_UPLOAD_WEB_?>';
    </script>
    <script type="text/javascript" src="../assets/js/loadReadyPreguntas.js"></script>

    <script>
        // Activar/desactivar votacion/resultados en directo
        function showhideVotacionResultados(opcion, estado) {
            $.post("ajax/showhideVotacionResultados.php", {
                opcion: opcion,
                estado: estado
            },  function (data, status) {
                $.ajax({url: "https://<?php echo $_SERVER['SERVER_NAME']; ?>/actualizar_fuentes", success: function(result){
                    console.log(data);
                    console.log(status);
                }});
            });
        }
        $('#controlVotacion').change(function (event) {
            var opcion = $(this).attr("data-value");
            var estado = $(this).prop('checked');
            showhideVotacionResultados(opcion, estado);
            if(estado) {
                $('#controlResultados').prop('checked', false).change();
            }
        });
        $('#controlResultados').change(function (event) {
            var opcion = $(this).attr("data-value");
            var estado = $(this).prop('checked');
            showhideVotacionResultados(opcion, estado);
            if(estado) {
                $('#controlVotacion').prop('checked', false).change();
            }
        });
        function actualizarToggleVotacionResultados(elemento) {
            console.log(elemento);
            if (elemento.activo == 1) {
                if (elemento.slug=="poll") {
                    $("#controlVotacion").prop('checked', true).change();
                    console.log("checked votacion");
                } else {
                    if(elemento.slug=="results") {
                        $("#controlResultados").prop('checked', true).change();
                        console.log("checked resultados");
                    }
                }
                } else {
                    if (elemento.slug=="poll") {
                        $("#controlVotacion").prop('checked', false).change();
                        console.log("unchecked votacion");
                    } else {
                    if(elemento.slug=="results") {
                        $("#controlResultados").prop('checked', false).change();
                        console.log("unchecked resultados");
                    }
                }
            }
        }
        function checkSource() {
            $.ajax('/storage/fuentes.html?' + Date.now(), {
                success: /* async */ function (data) {
                    source = JSON.parse(data);
                    actualizarToggleVotacionResultados(source.find(el => el.tipo =="voting" && el.slug == "poll"));
                    actualizarToggleVotacionResultados(source.find(el => el.tipo =="voting" && el.slug == "results"));
                }
            });
        };
        checkSource();
    </script>
    <?php include('inc/pre_footer_s3w.php') ;?>
    <?php 
    }//fin request id
}
else{
      header("Location: login.php");
}
?>
</body>
</html>