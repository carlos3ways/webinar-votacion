<?php 

if (!isset($_SESSION)) {
    session_start();
}

// Solo para usuario administración
if (isset($_SESSION['acceso_admin']) && $_SESSION['acceso_admin']==1) {
    
    // recuperar los datos del evento
    if (isset($_REQUEST['id'])) {
        $id_evento = $_REQUEST['id'];
        include("ajax/db_connection.php");
        include('inc/head.php'); 

        // preguntas del evento
        $query = "SELECT * "
                . " FROM preguntas_votar "
                . "WHERE id_evento = " . $id_evento ." "
                . "ORDER BY orden ASC";
        if (!$preguntas = mysqli_query($db, $query)) {
            exit(mysqli_error($db));
        }
        if (!$preguntas2 = mysqli_query($db, $query)) {
            exit(mysqli_error($db));
        }
    ?>

    <body>
    <?php include('inc/navbar_s3w.php'); ?>
    <!-- Content Section -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Sistema de Votaciones</h1>
                <h2>Estadísticas</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="pull-right">
                    <a href="index.php"><button class="btn btn-info"> Ver Eventos</button></a>
                </div>
            </div>
        </div>
        <div id="console-event"></div>
        <div class="row">
            <div class="col-md-12">
                <h3>Estadísticas:</h3>
                <table id="statsAjax" class="table">
                    <thead>
                        <tr>
                            <th>Rank.</th>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th class="dt-center"><span class='glyphicon glyphicon-ok' aria-hidden='true' style='color: green;'></span></th>
                            <th class="dt-center"><span class='glyphicon glyphicon-remove' style='color: red;' aria-hidden='true'></span></th>
                            <th class="dt-center"><span class='glyphicon glyphicon-time' aria-hidden='true'></span></th>

                            <?php
                                $cont=1;
                                while ($row = mysqli_fetch_assoc($preguntas)) { 
                            ?>
                            <th>P.<?=$cont?></th>
                            <?php
                                    $cont++;
                                }
                            ?>
                            <th></th>

                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>

                <h3>Estadísticas Totales:</h3>
                <table class="table" id="totalStatsAjax">
                    <thead>
                        <tr style="border-bottom: none;">
                            <th colspan="3" style="border:0px"></th>
                            <?php
                                $cont=1;
                                $html_columnas = '';
                                while ($row = mysqli_fetch_assoc($preguntas2)) {
                            ?>
                            <th colspan="3" class="dataTable-border text-center">Prg <?=$cont?></th>
                            <?php
                                    $html_columnas .= '<th class="dataTable-border">votos</th><th class="dataTable-border">aciertos</th><th class="dataTable-border">tiempo</th>';
                                    $cont++;
                                }
                            ?>
                            
                        </tr>
                        <tr>
                            <th class="nobortop">Usuarios</th>
                            <th class="nobortop">Preguntas</th>
                            <th class="nobortop">Respuestas</th>

                            <?php
                                echo $html_columnas;
                            ?>

                            <th class="nobortop">%Acierto</th>
                            <th class="nobortop">%Error</th>
                            <th class="nobortop">Tiempo</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
          
       
            </div>
        </div>
    </div>
    <!-- /Content Section -->



    <?php include('inc/footer.php'); ?>

    <script type="text/javascript" src="../assets/js/script.js"></script>
    <script type="text/javascript" src="../assets/js/bootstrap-toggle.min.js"></script>
    <script type="text/javascript" src="../assets/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../assets/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="../assets/js/jquery.sparkline.min.js"></script>

<script>
$(document).ready( function() {
    // recuperar los datos de estadísticas
    $('#statsAjax').DataTable({
        "ajax": {
            'type': 'POST',
            'url': 'ajax/view_stats.php',
            'data': { id_evento: '<?=$id_evento?>'},
            error: function (xhr, error, thrown) {
                alert( 'No hay datos en Estadísticas' );
            },
            complete: function (data){
            }
        },
        "bSort": false,
        "columnDefs": [
            {"className": "left", "targets": [ 1, 2] },
            {"className": "dt-center", "targets": "_all"}
        ],
    });
    // recuperar los totales
    $('#totalStatsAjax').DataTable({
        "ajax": {
            'type': 'POST',
            'url': 'ajax/total_stats.php',
            'data': { id_evento: '<?=$id_evento?>'},
            error: function (xhr, error, thrown) {
                alert( 'No hay datos en Totales' );
            }

        },
        "columnDefs": [
            {"className": "dt-center", "targets": "_all"}
        ],
        searching: false,
        paging: false,
        info: false
    });


});
</script>


    <?php //include_once('ajax/readUsuarios.php');?>
    <?php include('inc/pre_footer_s3w.php') ;?>
    <?php 
    } // si hay id_evento

} else {
    // usuario administración no existente
    header("Location: login.php");
}
?>
</body>
</html>