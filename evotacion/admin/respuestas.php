<?php
if (!isset($_SESSION)) {
    session_start();
}
if ( isset($_SESSION['acceso_admin']) && ($_SESSION['acceso_admin'] == 1) ) {

    // variables de entorno (DIR_WEB para lang tinymce)
    include 'inc/config.php';

    if (isset($_REQUEST['id_pregunta'])) {
        $id_evento = $_REQUEST['id'];
        $id_pregunta= $_REQUEST['id_pregunta'];
?>
<!DOCTYPE html>
<html lang="es">

<?php
        include('inc/head.php');
?>
    <body>
    <?php include('inc/navbar_s3w.php'); ?>
    <!-- Content Section -->
    <div class="container padbot30">
        <div class="row">
            <div class="col-md-12">
                <h1>Sistema de Votaciones</h1>
                <h3>Respuestas</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="pull-right">
                    <a href="index.php"><button class="btn btn-info">Volver a Eventos</button></a>
                    <a href="preguntas.php?id=<?=$id_evento?>"><button class="btn btn-success">Volver a Preguntas</button></a>
                    <button class="btn btn-warning" onclick="addResEnRes(<?=$id_evento?>,<?=$id_pregunta?>)">Añadir Respuestas</button>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?php include_once('ajax/readRespuestas.php');?>
            </div>
        </div>
    </div>
    <!-- /Content Section -->

    <?php include('inc/modal_delete_respuesta.php'); ?>
    <?php include('inc/modal_add_respuesta.php'); ?>
    <?php include('inc/modal_imagen_respuesta.php'); ?>
    <?php include('inc/footer.php'); ?>
    <script type="text/javascript" src="../assets/js/script.js"></script>
    <script>
        var dir_images_upload = '<?=_DIR_IMAGES_UPLOAD_WEB_?>';
    </script>
    <script type="text/javascript" src="../assets/js/loadReadyRespuestas.js"></script>
    <?php include('inc/pre_footer_s3w.php') ;?>
<?php 
    } // fin request id_pregunta
} else {
    // No acceso_admin
    header("Location: login.php");
}

?>
</body>
</html>