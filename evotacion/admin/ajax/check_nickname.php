<?php 

// Control de la entrada del nickname

// arrancar sesion
if (!isset($_SESSION)) {
    session_start(); 
}

// controlar si ya hay usuario (por cualquier motivo)
if (isset($_SESSION['acceso_permitido']) == 1) {
    // comprobar el nickname (id_usuario not set)
    if (isset($_SESSION['id_usuario']) && $_SESSION['id_usuario'] > 0) {
        // ya hay usuario -> votar.php
        header("Location: votar.php");
    }
} else {
    // no hay login -> index.php
    header("Location: index.php");
}

// si hay acceso pero no hay todavía id_usuario, comprobar que llega el nickname
if (isset($_POST['nick']) && !empty($_POST['nick'])) {
    // se inserta en la BD el nickname
    // saltará Duplicate Key si ya existe el nickname
    include("db_connection.php");
    $id_evento = $_SESSION['id_evento'];
    $nickname = $_POST['nick'];
    $query = "INSERT INTO participantes ( nickname, id_evento )"
            . " VALUES ( '".mysqli_real_escape_string($db, $nickname)."', ".$id_evento." )";

    // si el insert va bien o no....
    if (!$result = mysqli_query($db, $query)) {
        if (mysqli_errno($db) == 1062) {
            // nickname duplicado!!
            echo json_encode(array('ok' => false, 'error' => 'Nickname duplicado'));
        } else {
            // error estándar
            echo json_encode(array('ok' => false, 'error' => 'No se pudo guardar el nickname. Prueba de nuevo en unos instantes.'));
//            echo json_encode(array('ok' => false, 'error' => mysqli_error($db)));
        }
    } else {
    
        // perfecto, grabamos en la sesión y volvemos
        $_SESSION['id_usuario'] = mysqli_insert_id($db);
        echo json_encode(array('ok' => true));
    }
    
} else {
    // no ha llegado nada, nickname vacío
    echo json_encode(array('ok' => false, 'error' => 'Nickname vacío!'));
}
