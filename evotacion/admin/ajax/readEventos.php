<?php

// arrancar la sesión, si hace falta
if (!isset($_SESSION)) {
    session_start();
}

// solo para administrador
if ( isset($_SESSION['acceso_admin']) && ($_SESSION['acceso_admin'] == 1) ) {

    // include Database connection file 
    include("db_connection.php");

    // Design initial table header 
    $data = '<table class="table table-bordered table-striped">
                <tr>
                    <th>Id</th>
                    <th>Evento</th>
                    <th>Fecha</th>
                    <th>Activo</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>';

    $query = "SELECT * FROM eventos WHERE deleted_at IS NULL";

    if (!$result = mysqli_query($db, $query)) {
        exit(mysqli_error($db));
    }

    // if query results contains rows then featch those rows 
    if (mysqli_num_rows($result) > 0) {
    	$number = 1;
    	while ($row = mysqli_fetch_assoc($result)) {
            if($row['activo']==1){
                $data .= '<tr class="success">';
            }else{
                $data .= '<tr>';
            }
    		
            $data .= '<td>'.$row['id'].'</td>
                <td>'.$row['evento'].'</td>
                <td>'.$row['fecha_inicio'].'</td>
                <td>'.$row['activo'].'</td>
                <td>
                    <button onclick="getEventoDetalle('.$row['id'].')" class="btn btn-warning">Editar</button>
                </td>
                <td>
                    <button onclick="deleteEvento('.$row['id']. ",'".$row['fecha_inicio']."'".')" class="btn btn-danger">Borrar</button>
                </td>
                <td>
                    <a href="delegacionvotos.php?evento_id='.$row['id'].'"> <button class="btn btn-success">Delegar votos</button></a>
                </td>
                <td>
                    <a href="preguntas.php?id='.$row['id'].'"> <button class="btn btn-info">Preguntas</button></a>
                </td>
                <td>
                    <a href="participantes.php?evento_id='.$row['id'].'"> <button class="btn btn-primary">Participantes</button></a>
                </td>
                <td>
                    <a href="resultados.php?evento_id='.$row['id'].'"> <button class="btn btn-secondary">Resultados</button></a>
                </td>
    		</tr>';
            $number++;
    	}
    } else {
    	// records now found 
    	$data .= '<tr><td colspan="8">Records not found!</td></tr>';
    }

    $data .= '</table>';

    echo $data;

} else {
    // no acceso admin
    echo "Acceso no autorizado";
}
?>