<?php

// arrancar la sesión, si hace falta
if (!isset($_SESSION)) {
    session_start();
}

// solo para administrador
if ( isset($_SESSION['acceso_admin']) && ($_SESSION['acceso_admin'] == 1) ) {

    // recibe un array con la lista de preguntas
    if (!empty($_POST['preguntas'])) {

        include("db_connection.php");
        $preguntas= $_POST['preguntas'];

        // para cada pregunta
        foreach ($preguntas as $preg) {

            $id = $preg["pregunta_id"];

            $activo = 0;
            if( $preg["activo"] == "true"){
                $activo = 1;
            }

            $pregunta = $preg["pregunta"];
            $orden = $preg["orden"];
            $formato = $preg["formato"];

            // actualizar la pregunta en la BD
            $query = "UPDATE preguntas_votar
                         SET pregunta = '$pregunta', 
                             activo = $activo,
                             formato_a_4 = $formato,
                             orden = $orden
                       WHERE id = $id";

            if (!$result = mysqli_query($db, $query)) {
                exit(mysqli_error($db));
            }

            // si la pregunta está activa, hay que insertarla en la historia de preguntas
            // la pregunta también se puede activar desde la toolbar de mostrar pregunta
            $query ="INSERT INTO preguntas_history
                    (id_pregunta) VALUES ($id)";

            mysqli_query($db, $query);
            
        }
    }

} else {
    // no acceso admin
    echo "Acceso no autorizado";
}

?>