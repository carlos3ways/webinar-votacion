<?php

// arrancar la sesión, si hace falta
if (!isset($_SESSION)) {
    session_start();
}

// solo para administrador
if ( isset($_SESSION['acceso_admin']) && ($_SESSION['acceso_admin'] == 1) ) {

    // readRespuestas.php
    // Monta la lista de respuestas de una pregunta
    if (isset($id_pregunta)) {

        include_once "db_connection.php";
        // para el directorio de imagenes
        include_once "inc/config.php";

        $query = "SELECT * 
                    FROM preguntas_votar 
                   WHERE id= $id_pregunta";

        if (!$result = mysqli_query($db, $query)) {
            exit(mysqli_error($db));
        }

        $apreguntas =  mysqli_fetch_assoc($result);
        $str_pregunta = $apreguntas['pregunta'];

        $query = "SELECT * 
                    FROM respuestas 
                   WHERE id_pregunta= $id_pregunta 
                ORDER BY orden";

        if (!$result = mysqli_query($db, $query)) {
            exit(mysqli_error($db));
        }

        if(mysqli_num_rows($result) > 0)
        {
            $number = 1;
?>
<div class="form-group" id="lista_respuestas">
 <div class="form-group">
  <label for="comment">Pregunta:</label>
  <textarea class="form-control hidden" rows="5" id="pregunta" data-id-pregunta="<?=$id_pregunta?>"><?=$str_pregunta?></textarea>
  <?=$str_pregunta?>
</div>

    <label>Respuestas:</label> 
    <span class="help-block">Puedes ordenar las respuestas y marcar la correcta. Selecciona, modifica o elimina la imagen asociada.</span>
    <ul id="respuestas_sortable" class="ui-sortable">
<?php
            while($row = mysqli_fetch_assoc($result)){
?>
      <li id="respuesta_<?=$row['id']?>" data-id="<?=$row['id']?>">
        <div class="input-group col-md-12">
            <span class="input-group-addon">
                <input type="checkbox" <?php if($row['es_correcta']==1){echo "checked";} ?>/> <i class="glyphicon glyphicon-move mar10lyr"></i> 
                <span class="tooltip-inner mar10lyr"> Nº <?=$row['orden']?> </span>
                <div id="imagen-<?=$row['id']?>" class="glyphicon glyphicon-picture btn btn-info btn-activado<?php if ($row['imagen'] != '') echo " active"; ?>" alt="Imagen" onclick="showImagenRespuesta(<?=$row['id']?>)" data-imagen="<?=$row['imagen']?>"> <span class="ftprin">Imagen</span></div>
                <div class="lista-respuestas"><img src="<?=_DIR_IMAGES_UPLOAD_WEB_."/".$row['imagen']?>" onerror="this.src='../assets/images/trans.png'"></div>
                <button type="button" class="glyphicon glyphicon-trash btn btn-danger" onclick="borrarRespuesta(<?=$row['id']?>)"> <span class="ftprin">Eliminar respuesta</span> </button>
            </span>
            <textarea id="txtrespuesta<?=$row['id']?>" class="form-control input input50 width80p txtrespuesta txttinymce"><?=htmlspecialchars($row['respuesta'])?></textarea>
<!--            <input class="form-control input input50 width80p txtrespuesta" type="text" value="<?=htmlspecialchars($row['respuesta'])?>" >-->
        </div>
      </li>
<?php
                $number++;
            }
?>
    </ul>
</div>
 <button onclick="saveRespuestas()" class="btn btn-success">Guardar</button>
<?php
        }
        else
        {
            // records now found 
            echo  '<div class="col-md-12" >Records not found!</div>';
        }
    }

} else {
    // no acceso admin
    echo "Acceso no autorizado";
}
?>