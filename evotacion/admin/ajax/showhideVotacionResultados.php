<?php

// arrancar la sesión, si hace falta
if (!isset($_SESSION)) {
    session_start();
}

// validar acceso
if (!isset($_SESSION['acceso_admin']) && ($_SESSION['acceso_admin'] !== 1) ) {
    echo "Acceso no autorizado";
    die();
}

include("db_connection.php");

if (!isset($_POST["opcion"]) || !isset($_POST["estado"])) {
    echo "No se han recibido los datos correctamente";
    die();
}

$opcion = $_POST["opcion"];
$estado = $_POST["estado"];

// cambiar estado de opcion
$query = "UPDATE controlfuentes
            SET activo = $estado
            WHERE tipo = 'voting'
            AND slug LIKE '$opcion'";
if (!$result = mysqli_query($db, $query)) {
    exit(mysqli_error($db));
    echo "Error al activar/desctivar votacion/resultados";
    die();
}
//Si activo opción debo desactivar la otra
if ($estado == 1) {
    $opcion_desactivar = ($opcion=="results") ? "poll" : "results";
// cambiar estado de opcion
    $query2 = "UPDATE controlfuentes
                SET activo = 0
                WHERE tipo = 'voting'
                AND slug LIKE '$opcion_desactivar'";
    if (!$result2 = mysqli_query($db, $query2)) {
        exit(mysqli_error($db));
        echo "Error al desctivar votacion/resultados";
        die();
    }
}
echo "Se ha actualizado las fuentes correctamente!";