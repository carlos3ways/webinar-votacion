<?php

// arrancar la sesión, si hace falta
if (!isset($_SESSION)) {
    session_start();
}

// solo para administrador
if ( isset($_SESSION['acceso_admin']) && ($_SESSION['acceso_admin'] == 1) ) {

    if (!empty($_POST['id'])) {   

        include("db_connection.php");

        $idpregunta = $_POST['id'];
        $musica = $_POST['musica'];

        $query = "UPDATE preguntas_votar
                    SET musica = '$musica'
                    WHERE id = $idpregunta";

        if (!$result = mysqli_query($db, $query)) {
            exit(mysqli_error($db));
        }
    }

} else {
    // no acceso admin
    echo "Acceso no autorizado";
}

?>