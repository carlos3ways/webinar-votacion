<?php

// arrancar la sesión, si hace falta
if (!isset($_SESSION)) {
    session_start();
}

// solo para administrador
if ( isset($_SESSION['acceso_admin']) && ($_SESSION['acceso_admin'] == 1) ) {

    // alta de un nuevo evento
    if (!empty($_POST['evento'])) {

        include("db_connection.php");
        // directorio de subida de imágenes
        include("../inc/config.php");

        $evento = $_POST['evento'];
        $codigo_acceso = $_POST['codigo_acceso'];
        $fecha_inicio = $_POST['fecha_inicio'];
        $fecha_fin = $_POST['fecha_fin'];
        $activo = $_POST['activo'];
        $anonimo = $_POST['anonimo'];
        $autoranking = $_POST['autoranking'];
        $autosolucion = $_POST['autosolucion'];
        $theme = $_POST['theme'];
        $musica = $_POST['musica'];
        $tiempo ="0";

        if (!empty($_POST['tiempo'])) {
            $tiempo = $_POST['tiempo'];
        }

        // si hay personalización, meter también los datos y subir las imágenes!!
        $extra_campos = '';
        $extra_valores = '';
        if ($_POST['theme'] == 1) {
            // si había colores
            if (isset($_POST['fondo_head']) && $_POST['fondo_head'] != '') {
                $extra_campos .= ",fondo_head";
                $extra_valores .= ",'".$_POST['fondo_head']."'";
            }
            if (isset($_POST['fondo_foot']) && $_POST['fondo_foot'] != '') {
                $extra_campos .= ",fondo_foot";
                $extra_valores .= ",'".$_POST['fondo_foot']."'";
            }
            if (isset($_POST['respuesta_ok']) && $_POST['respuesta_ok'] != '') {
                $extra_campos .= ",fondo_respuesta_ok";
                $extra_valores .= ",'".$_POST['respuesta_ok']."'";
            }
            if (isset($_POST['respuesta_ko']) && $_POST['respuesta_ko'] != '') {
                $extra_campos .= ",fondo_respuesta_ko";
                $extra_valores .= ",'".$_POST['respuesta_ko']."'";
            }
            // si había alineación
            if (isset($_POST['align_logo_head']) && $_POST['align_logo_head'] != '') {
                $extra_campos .= ",align_logo_head";
                $extra_valores .= ",'".$_POST['align_logo_head']."'";
            }
            if (isset($_POST['align_logo_foot']) && $_POST['align_logo_foot'] != '') {
                $extra_campos .= ",align_logo_foot";
                $extra_valores .= ",'".$_POST['align_logo_foot']."'";
            }
        }
        
        //Si se ha creado como activo desactivar los demás
        if ($activo == 1) {
            $query1 = "UPDATE eventos
                SET activo = 0";

            if (!$result1 = mysqli_query($db, $query1)) {
                exit(mysqli_error($db));
            }
        }
        
        // SQL normal con el extra de personalización
        $query = "INSERT INTO eventos (evento, codigo_acceso, fecha_inicio, fecha_fin, activo, anonimo, autoranking, autosolucion, tiempo, theme, musica".$extra_campos.")" 
               . "VALUES('$evento', '$codigo_acceso', '$fecha_inicio', '$fecha_fin', $activo, $anonimo, $autoranking, $autosolucion, '$tiempo', $theme, '$musica'".$extra_valores.")";

        /////////////////////////////////////////////
        // la gestión de imágenes se hace posteriormente al INSERT, ya que el nombre del fichero incluye el ID del evento
        /////////////////////////////////////////////
        if (!$result = mysqli_query($db, $query)) {
            exit(mysqli_error($db));
        } else {
            // si había imagenes, subirlas
            if (isset($_FILES["logo_head"]["name"]) || isset($_FILES["logo_foot"]["name"])) {                
                // recuperar el ID insertado
                $nuevo_id = mysqli_insert_id($db);
                $dir_destino = "../.."._DIR_IMAGES_UPLOAD_;
                $sql_update = '';
                // logo cabecera
                if(isset($_FILES["logo_head"]["name"])) {
                    // chequear que es una imagen y sacar su extension
                    $check = getimagesize($_FILES["logo_head"]["tmp_name"]);
                    $extension = image_type_to_extension($check[2]);
                    if ($check !== false) {
                        $nombre_fichero_head = "head_".$nuevo_id.$extension;
                        $fichero_final_head = $dir_destino . "/". $nombre_fichero_head;
                        if (move_uploaded_file($_FILES['logo_head']['tmp_name'], $fichero_final_head)) {
                            // se ha subido correctamente
                            $sql_update = "logo_head = '$nombre_fichero_head'";
                        } else {
                            echo "No se puedo subir el fichero.";
                        }
                    } else {
                        echo "El fichero no es una imagen...";
                    }
                }
                // logo pie
                if(isset($_FILES["logo_foot"]["name"])) {
                    // chequear que es una imagen y sacar su extension
                    $check = getimagesize($_FILES["logo_foot"]["tmp_name"]);
                    $extension = image_type_to_extension($check[2]);
                    if ($check !== false) {
                        $nombre_fichero_foot = "foot_".$nuevo_id.$extension;
                        $fichero_final_foot = $dir_destino . "/". $nombre_fichero_foot;
                        if (move_uploaded_file($_FILES['logo_foot']['tmp_name'], $fichero_final_foot)) {
                            // se ha subido correctamente
                            // si también había cabecera hay que añadir una coma
                            if ($sql_update != '') {
                                $sql_update .= ',';
                            }
                            $sql_update .= "logo_foot = '$nombre_fichero_foot'";
                        } else {
                            echo "No se puedo subir el fichero.";
                        }
                    } else {
                        echo "El fichero no es una imagen...";
                    }
                }
                // si se ha subido algo, hacer el update
                if ($sql_update != '') {
                    $sql_update = "UPDATE eventos "
                            . "       SET ".$sql_update
                            . "     WHERE id = ".$nuevo_id;
                    if (!$result = mysqli_query($db, $sql_update)) {
                        exit(mysqli_error($db));
                    }
                }
            }
        }
    } else {
        // evento EMPTY
        echo "añadir nombre de evento";
    }
} else {
    // no acceso admin
    echo "Acceso no autorizado";
}

?>