<?php

// arrancar la sesión, si hace falta
if (!isset($_SESSION)) {
    session_start();
}

// solo para administrador
if ( isset($_SESSION['acceso_admin']) && ($_SESSION['acceso_admin'] == 1) ) {

    if (!empty($_POST['id'])) {   

        include("db_connection.php");
        // directorio de subida de imágenes
        include("../inc/config.php");

        $idpregunta = $_POST['id'];
        $eliminar = $_POST['eliminar'];

        $sql_imagen = "";
        // si es ELIMINAR, no hace falta subir nada
        if ($eliminar == 1) {
            $sql_imagen = "imagen = ''";
        } else {
            // Comprobar si hay imagen
            $dir_destino = "../.."._DIR_IMAGES_UPLOAD_;
            if(isset($_FILES["imagen"]["name"])) {
                // chequear que es una imagen y sacar su extension
                $check = getimagesize($_FILES["imagen"]["tmp_name"]);
                $extension = image_type_to_extension($check[2]);
                if ($check !== false) {
                    $nombre_fichero = "pregunta_".$idpregunta.$extension;
                    $fichero_final = $dir_destino . "/". $nombre_fichero;
                    if (move_uploaded_file($_FILES['imagen']['tmp_name'], $fichero_final)) {
                        // se ha subido correctamente
                        $sql_imagen = " imagen = '$nombre_fichero'";
                    } else {
                        echo "Error copiando?¿";
                    }
                } else {
                    echo "El fichero no es una imagen...";
                }
            }
        }

        $query = "UPDATE preguntas_votar
                    SET  ".$sql_imagen."
                    WHERE id = $idpregunta";

        if (!$result = mysqli_query($db, $query)) {
            exit(mysqli_error($db));
        }
    }

} else {
    // no acceso admin
    echo "Acceso no autorizado";
}

?>