<?php

// arrancar la sesión, si hace falta
if (!isset($_SESSION)) {
    session_start();
}

// solo para administrador
if ( isset($_SESSION['acceso_admin']) && ($_SESSION['acceso_admin'] == 1) ) {

    if (!empty($_POST['id'])) {
        include("db_connection.php");
        $id = $_POST['id'];
        $query = "DELETE FROM usuarios_votar WHERE id =". $id;

        if (!$result = mysqli_query($db, $query)) {
            exit(mysqli_error($db));
        }
    } else {
        echo "error";
    }

} else {
    // no acceso admin
    echo "Acceso no autorizado";
}
?>