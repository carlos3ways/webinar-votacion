<?php

// arrancar la sesión, si hace falta
if (!isset($_SESSION)) {
    session_start();
}

// solo para administrador
if ( isset($_SESSION['acceso_admin']) && ($_SESSION['acceso_admin'] == 1) ) {

    // include Database connection file
    include("db_connection.php");

    // check request
    if(isset($_POST['id']) && isset($_POST['id']) != "")
    {

        $id = $_POST['id'];


        $query = "SELECT * FROM usuarios_votar WHERE id = '$id'";
        if (!$result = mysqli_query($db, $query)) {
            exit(mysqli_error($db));
        }

        $response = array();

        if(mysqli_num_rows($result) > 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                $response = $row;
            }
        }
        else
        {
            $response['status'] = 200;
            $response['message'] = "Data not found!";
        }


        // display JSON data
        echo json_encode($response);
    }
    else
    {
        $response['status'] = 200;
        $response['message'] = "Invalid Request!";
    }

} else {
    // no acceso admin
    echo "Acceso no autorizado";
}
