<?php
// Estadísticas y ranking de aciertos
// arrancar la sesión, si hace falta
if (!isset($_SESSION)) {
    session_start();
}

// solo para administrador
if ( isset($_SESSION['acceso_admin']) && ($_SESSION['acceso_admin'] == 1) ) {

    if (isset($_REQUEST['id_evento'])) {
        // evento existente
        $id_evento = $_REQUEST['id_evento'];
        include "db_connection.php";
        // funciones de estadísticas
        include "../inc/ranking.php";

        // recuperar la clasificacion con TODAS las preguntas
        // 0 -> Todas las preguntas
        $clasificacion = clasificacion($db, $id_evento, 0);
        $datosFinales = array_values($clasificacion['clasificacion']);
        $respuesta = array (
            'success' => true,
            'message' => "",
            'data' => $datosFinales
        );
    } else {
        // no había evento
        $respuesta = array (
            'success' => false,
            'message' => 'Error: no hay datos'
        );  
    }

    // escribir la respuesta
    echo json_encode($respuesta);

} else {
    // no acceso admin
    echo "Acceso no autorizado";
}
?>