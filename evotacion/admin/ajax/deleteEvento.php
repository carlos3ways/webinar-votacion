<?php
// Eliminar un evento y todos sus datos asociados
// arrancar la sesión, si hace falta
if (!isset($_SESSION))
    session_start();

// Solo para administrador
if ((!isset($_SESSION['acceso_admin']) || ($_SESSION['acceso_admin'] != 1)) || (!isset($_POST['id']) || isset($_POST['id']) == '')):
    echo 'Acceso no autorizado';
    return;
endif;

// Include Database connection file
include("db_connection.php");

// Get evento id
$id = ($_POST['id'] ?? null);
if (!$id || !is_numeric($id)):
    echo 'Id de evento no definido';
    return;
endif;

// ALTER TABLE `eventos` ADD `deleted_at` DATETIME NULL DEFAULT NULL;
// ALTER TABLE `participantes_delegaciones` ADD `deleted_at` DATETIME NULL DEFAULT NULL;
// ALTER TABLE `participantes` ADD `deleted_at` DATETIME NULL DEFAULT NULL;
// ALTER TABLE `preguntas_history` ADD `deleted_at` DATETIME NULL DEFAULT NULL;
// ALTER TABLE `preguntas_votar` ADD `deleted_at` DATETIME NULL DEFAULT NULL;
// ALTER TABLE `votos` ADD `deleted_at` DATETIME NULL DEFAULT NULL;

$date = date('Y-m-d H:i:s');

execute("UPDATE eventos SET deleted_at = '{$date}' WHERE id = {$id}");
execute("UPDATE participantes SET deleted_at = '{$date}' WHERE id_evento = {$id}");
execute("UPDATE participantes_delegaciones SET deleted_at = '{$date}' WHERE id_evento = {$id}");
execute("UPDATE preguntas_votar SET deleted_at = '{$date}' WHERE id_evento = {$id}");
execute("UPDATE preguntas_history SET deleted_at = '{$date}' WHERE id_pregunta IN (SELECT id FROM preguntas_votar WHERE id_evento = {$id})");
execute("UPDATE votos SET deleted_at = '{$date}' WHERE id_pregunta IN (SELECT id FROM preguntas_votar WHERE id_evento = {$id})");

echo 'Evento borrado correctamente';

/**
 * Execute mysql query
 *
 * @param string $query
 * 
 * @return mixed
 */
function execute($query) {
    global $db;

    $result = mysqli_query($db, $query);
    if (!$result)
        exit(mysqli_error($db));

    return $result;
}
