<?php

// arrancar la sesión, si hace falta
if (!isset($_SESSION)) {
    session_start();
}

// solo para administrador
if ( isset($_SESSION['acceso_admin']) && ($_SESSION['acceso_admin'] == 1) ) {

    include("db_connection.php");
    // directorio de subida de imágenes
    include("../inc/config.php");

    if (isset($_POST)) {
        // recuperar los parámetros
        $id = $_POST['id'];
        $evento = $_POST['evento'];
        $codigo_acceso = $_POST['codigo_acceso'];
        $fecha_inicio = $_POST['fecha_inicio'];
        $fecha_fin = $_POST['fecha_fin'];
        $activo = $_POST['activo'];
        $anonimo = $_POST['anonimo'];
        $autoranking = $_POST['autoranking'];
        $autosolucion = $_POST['autosolucion'];
        $tiempo = $_POST['tiempo'];
        // nombre del fichero de música
        $musica = $_POST['musica'];
        $theme = $_POST['theme'];
        // estilo personalizado
        $fondo_head = $_POST['fondo_head'];
        $fondo_foot = $_POST['fondo_foot'];
        $elim_logo_head = $_POST['elim_logo_head'];
        $elim_logo_foot = $_POST['elim_logo_foot'];
        $align_logo_head = $_POST['align_logo_head'];
        $align_logo_foot = $_POST['align_logo_foot'];
        $fondo_respuesta_ok = $_POST['fondo_respuesta_ok'];
        $fondo_respuesta_ko = $_POST['fondo_respuesta_ko'];

        // controlar los ficheros de las imágenes
        // Si no llega nada -> no se modifica nada
        // Pero, si se elimina -> predomina el eliminar
        $sql_logo_head = "";
        $sql_logo_foot = "";
        $dir_destino = "../.."._DIR_IMAGES_UPLOAD_;
        // Comprobar el logo HEAD
        if(isset($_FILES["logo_head"]["name"])) {
            // chequear que es una imagen y sacar su extension
            $check = getimagesize($_FILES["logo_head"]["tmp_name"]);
            $extension = image_type_to_extension($check[2]);
            if ($check !== false) {
                $nombre_fichero_head = "head_".$id.$extension;
                $fichero_final_head = $dir_destino . "/". $nombre_fichero_head;
                if (move_uploaded_file($_FILES['logo_head']['tmp_name'], $fichero_final_head)) {
                    // se ha subido correctamente
                    $sql_logo_head = " logo_head = '$nombre_fichero_head',";
                } else {
                    echo "Error copiando?¿";
                }
            } else {
                echo "El fichero no es una imagen...";
            }
        }
        // Comprobar si se elimina el logo HEAD
        if ($elim_logo_head == "1") {
            $sql_logo_head = " logo_head = '',";
        }
        // Lo mismo con el logo FOOT
        if(isset($_FILES["logo_foot"]["name"])) {
            // chequear que es una imagen y sacar su extension
            $check = getimagesize($_FILES["logo_foot"]["tmp_name"]);
            $extension = image_type_to_extension($check[2]);
            if ($check !== false) {
                $nombre_fichero_foot = "foot_".$id.$extension;
                $fichero_final_foot = $dir_destino . "/". $nombre_fichero_foot;
                if (move_uploaded_file($_FILES['logo_foot']['tmp_name'], $fichero_final_foot)) {
                    // se ha subido correctamente
                    $sql_logo_foot = " logo_foot = '$nombre_fichero_foot',";
                } else {
                    echo "Error copiando?¿";
                }
            } else {
                echo "El fichero no es una imagen...";
            }
        }
        // Comprobar si se elimina el logo HEAD
        if ($elim_logo_foot == "1") {
            $sql_logo_foot = " logo_foot = '',";
        }

        //Si se ha marcado como activo desactivar los demás
        if ($activo == 1) {
            $query1 = "UPDATE eventos
                SET activo = 0
                WHERE id != $id";

            if (!$result1 = mysqli_query($db, $query1)) {
                exit(mysqli_error($db));
            }
        }

        $query = "UPDATE eventos
                     SET evento = '$evento', 
                         codigo_acceso = '$codigo_acceso', 
                         fecha_inicio = '$fecha_inicio',
                         fecha_fin = '$fecha_fin',
                         activo = $activo,
                         anonimo = $anonimo,
                         autoranking = $autoranking,
                         autosolucion = $autosolucion,
                         tiempo = '$tiempo',
                         theme = $theme,
                         musica = '$musica',
                         fondo_head = '$fondo_head',
                         fondo_foot = '$fondo_foot',
                         ".$sql_logo_head."
                         ".$sql_logo_foot."
                         align_logo_head = '$align_logo_head',
                         align_logo_foot = '$align_logo_foot',
                         fondo_respuesta_ok = '$fondo_respuesta_ok',
                         fondo_respuesta_ko = '$fondo_respuesta_ko'
                   WHERE id = $id";

        if (!$result = mysqli_query($db, $query)) {
            exit(mysqli_error($db));
        }

    }

} else {
    // no acceso admin
    echo "Acceso no autorizado";
}
