<?php

// arrancar la sesión, si hace falta
if (!isset($_SESSION)) {
    session_start();
}

// solo para administrador
if ( isset($_SESSION['acceso_admin']) && ($_SESSION['acceso_admin'] == 1) ) {

    if (!empty($_POST['id_pregunta'])) {
        include("db_connection.php");

        $respuestas = $_POST['respuestas'];
        $id_pregunta = $_POST['id_pregunta'];


        $query= "SELECT count(id) FROM respuestas WHERE id_pregunta = " . $id_pregunta;

        if (!$result = mysqli_query($db, $query)) {
            exit(mysqli_error($db));
        }

        $fila = mysqli_fetch_array($result);
        $conserva_orden = $fila[0];

        foreach ($respuestas as $resp) {

            if(!empty($resp["respuesta"])){
                $respuesta = $resp["respuesta"];
                $orden = $resp["orden"] + $conserva_orden;

                $es_correcta = 0;

                if($resp["correcta"]=="true" ){
                    $es_correcta = 1;
                }

                $query = "INSERT INTO respuestas (id_pregunta, respuesta, orden, es_correcta) 
                VALUES ($id_pregunta, '$respuesta', $orden, $es_correcta)";

                if (!$result = mysqli_query($db, $query)) {
                    exit(mysqli_error($db));
                }
            }
        }
    } else {
        echo "error no existe id pregunta";
    }

} else {
    // no acceso admin
    echo "Acceso no autorizado";
}
?>