<?php

// arrancar la sesión, si hace falta
if (!isset($_SESSION)) {
    session_start();
}

// solo para administrador
if ( isset($_SESSION['acceso_admin']) && ($_SESSION['acceso_admin'] == 1) ) {

    // Totales de estadísticas
    if (isset($_REQUEST['id_evento'])) {

        // evento existente
        $id_evento = $_REQUEST['id_evento'];
        include "db_connection.php";
        // funciones de estadísticas
        include "../inc/ranking.php";

        // recuperar la clasificacion (y otros datos totales) con TODAS las preguntas
        // 0 -> Todas las preguntas
        $clasificacion = clasificacion($db, $id_evento, 0);
        // sacamos de la clasificacion los totales
        // array final de resultados
        $a_final_data = array();

        // Total usuarios
        $total_usuarios = count($clasificacion['clasificacion']);
        array_push($a_final_data, $total_usuarios);
        // Total preguntas
        $preguntas = $clasificacion['preguntas'];
        $total_preguntas = count($preguntas);
        array_push($a_final_data, $total_preguntas);
        // Total votos
        $total_votos = count($clasificacion['votos']);
        array_push($a_final_data, $total_votos);

        // para cada pregunta mostramos el #votos %acierto #tiempomedio
        // resultados tiene [usuario][pregunta] => resultado y tiempo
        // nos quedamos solo con [pregunta] -> resultado y tiempo
        $resultados = array_values($clasificacion['resultados']);

        // variables con los totales
        $votos_totales = 0;
        $tiempo_total = 0;
        $aciertos_totales = 0;
        // recorremos las preguntas
        foreach ($preguntas as $pregunta) {
            // variables contador pregunta
            $aciertos_pregunta = 0;
            $errores_pregunta = 0;
            $tiempo_pregunta = 0;
            $votos_pregunta = 0;
            // recorrer el array de resultados recogiendo votos de esa pregunta
            foreach ($resultados as $respuestas_participante) {
                // comprobar si los datos son de la pregunta
                // recorrer todas las respuestas del participante
                foreach ($respuestas_participante as $id_pregunta => $datos) {
                    if ($pregunta['id_pregunta'] == $id_pregunta) {
                        // contabilizar datos
                        if ($datos['respuesta'] == 1) {
                            // acierto
                            $aciertos_pregunta++;
                            $aciertos_totales++;
                        } else {
                            // error
                            $errores_pregunta++;
                        }
                        $tiempo_pregunta += $datos['segundos'];
                        $votos_pregunta++;
                        $tiempo_total += $datos['segundos'];
                        $votos_totales++;
                    }
                }
            }
            // meter los datos #votos %acierto #tiempomedio
            array_push($a_final_data, $votos_pregunta);
            array_push($a_final_data, (number_format(($aciertos_pregunta / $votos_pregunta), 2, '.', '')*100)."%");
            array_push($a_final_data, number_format(($tiempo_pregunta / $votos_pregunta), 1, '.', '')." seg");
        }

        // al final, el agregado de %acierto y el tiempo medio
        array_push($a_final_data, (number_format(($aciertos_totales / $votos_totales), 2, '.', '')*100)."%");
        array_push($a_final_data, (number_format((($votos_totales - $aciertos_totales) / $votos_totales), 2, '.', '')*100)."%");
        array_push($a_final_data, number_format(($tiempo_total / $votos_totales), 1, '.', '')." seg");

        // montar la respuesta final
        $respuesta = array (
            'success' => true,
            'message' => "",
            'data' => array( "0" => $a_final_data )
        );

    } else {
        // no había evento
        $respuesta = array (
            'success' => false,
            'message' => 'Error: no hay datos'
        );  
    }

    echo json_encode($respuesta);

} else {
    // no acceso admin
    echo "Acceso no autorizado";
}
?>