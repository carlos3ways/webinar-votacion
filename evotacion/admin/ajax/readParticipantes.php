<?php
// Leer los participantes de un evneto
// arrancar la sesión, si hace falta
if (!isset($_SESSION)) {
    session_start();
}

// solo para administrador
if ( isset($_SESSION['acceso_admin']) && ($_SESSION['acceso_admin'] == 1) ) {

    // solo si se le pasa un evento
    if (isset($_REQUEST['id_evento'])) {

        // 
        $id_evento = $_REQUEST['id_evento'];
        include "db_connection.php";
        // getParticipantes
        include "../inc/ranking.php";

        // lista de participantes
        if ($participantes = getParticipantes($db, $id_evento)) {
            $respuesta = array (
                'success' => true,
                'participantes' => $participantes
            );
        } else {
            // error en la consulta
            $respuesta = array (
                'success' => false,
                'status'  => 1,
                'message' => 'Lista vacía'
            );  
        }
    } else {
        // no había evento
        $respuesta = array (
            'success' => false,
            'status'  => 0,
            'message' => 'Error: parámetro evento inexistente'
        );  
    }

    // escribir la respuesta
    echo json_encode($respuesta);

} else {
    // no acceso admin
    echo json_encode("Acceso no autorizado");
}
?>