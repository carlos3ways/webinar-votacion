<?php

// arrancar la sesión, si hace falta
if (!isset($_SESSION)) {
    session_start();
}

// solo para administrador
if ( isset($_SESSION['acceso_admin']) && ($_SESSION['acceso_admin'] == 1) ) {

    if ((!empty($_POST['id_pregunta']) && (!empty($_POST['id_evento'])))) {
        include("db_connection.php");

        $id_pregunta = $_POST['id_pregunta'];
        $id_evento = $_POST['id_evento'];

        // borrar los votos
        $query = "DELETE FROM votos 
                   WHERE id_evento = ". $id_evento . 
                   " AND id_pregunta=" .$id_pregunta;

        if (!$result = mysqli_query($db, $query)) {
            echo "Error borrando los votos";
            exit(mysqli_error($db));
        } else {
            // borrar que la pregunta se efectuó
            $query = "DELETE FROM preguntas_history
                       WHERE id_pregunta=" .$id_pregunta;
            if (!$result = mysqli_query($db, $query)) {
                echo "Error borrando la historia de la pregunta";
                exit(mysqli_error($db));
            } else {
                echo "Datos borrados correctamente!";
            }
        }
    }

} else {
    // no acceso admin
    echo "Acceso no autorizado";
}
?>