<?php

// arrancar la sesión, si hace falta
if (!isset($_SESSION)) {
    session_start();
}

// solo para administrador
if ( isset($_SESSION['acceso_admin']) && ($_SESSION['acceso_admin'] == 1) ) {

    // include Database connection file
    include("db_connection.php");

    // Get User Details
    $query = "SELECT * FROM eventos ORDER BY evento";
    if (!$result = mysqli_query($db, $query)) {
        exit(mysqli_error($db));
    }

    $response = array();

    if(mysqli_num_rows($result) > 0) {

        $response = '<select name="eventos" class="selectpicker">';
        // para convertirlo en selectpicker, hay que crear el select al cargar la página
        // solo se rellena posteriormente las opciones
        $response = '';
        while ($row = mysqli_fetch_assoc($result)) {


            $response.= "<option value='".$row['id']."'>".$row['evento']."</option>";
        }
        $response.= "</select>";
    } else {
        $response['status'] = 200;
        $response['message'] = "Data not found!";
    }
    // display JSON data
    echo $response;

} else {
    // no acceso admin
    echo "Acceso no autorizado";
}
