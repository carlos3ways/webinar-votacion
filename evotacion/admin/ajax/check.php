<?php 
// control de login
// PRIMERO CODIGO ACCESO
// si se ha entrado algo, buscar evento x codigo de acceso
if (!empty($_POST['codigo'])) {
    include("db_connection.php");
    // buscar en la BD por codigo_acceso
    $codigo_acceso = $_POST['codigo'];
    $query = "SELECT id, anonimo"
            . " FROM eventos "
            . "WHERE activo = 1 "
            . "  AND codigo_acceso = '" . $codigo_acceso . "'";

    if (!$result = mysqli_query($db, $query)) {
        exit(mysqli_error($db));
    }

    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

    // si había algo, iniciar sesión
    if(!empty($row['id'])){
        session_start();
        $_SESSION['id_evento'] = $row['id'];
        // usuario o participante
        $_SESSION['tipo_usuario'] = 'participante'; 
        $_SESSION['id_usuario'] = 0; 
        $_SESSION['acceso_permitido'] = 1;
        $_SESSION['evento_anonimo'] = $row['anonimo'];
        $_SESSION['fecha_registro'] = date('Y-m-d H:i:s');
        echo "código correcto";
    } else {
        session_start();
        session_unset();
        session_destroy();
    }
} else {
    session_start();
    session_unset();
    session_destroy();
}