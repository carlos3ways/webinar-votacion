<?php

// arrancar la sesión, si hace falta
if (!isset($_SESSION)) {
    session_start();
}

// solo para administrador
if ( isset($_SESSION['acceso_admin']) && ($_SESSION['acceso_admin'] == 1) ) {
    if (!empty($_POST['pregunta'])) {
        include("db_connection.php");


        $id_evento = $_POST['id_evento'];

        $query= "SELECT count(id) FROM preguntas_votar WHERE id_evento = " . $id_evento;
        if (!$result = mysqli_query($db, $query)) {
            exit(mysqli_error($db));
        }

    //    $orden = mysqli_result($result, 0);
        $fila = mysqli_fetch_array($result);
        $orden = $fila[0];
        $orden++;

        $pregunta = $_POST['pregunta'];
        $musica = $_POST['musica'];

        $activo = $_POST['activo'];

        $respuestas = $_POST['respuestas'];

        $query = "INSERT INTO preguntas_votar (id_evento, pregunta, activo, orden, musica) 
        VALUES($id_evento, '$pregunta', $activo, $orden, '$musica')";

        if (!$result = mysqli_query($db, $query)) {
            exit(mysqli_error($db));
        }

        $id_pregunta =  mysqli_insert_id($db);

        foreach ($respuestas as $resp) {
            if(!empty($resp["respuesta"])){
                $respuesta = $resp["respuesta"];
                $orden = $resp["orden"];
                $es_correcta = $resp["correcta"];

                $es_correcta = 0;
                if( $resp["correcta"] == "true"){
                    $es_correcta = 1;
                }

                $query = "INSERT INTO respuestas (id_pregunta, respuesta, orden, es_correcta) 
                VALUES ($id_pregunta, '$respuesta', $orden, $es_correcta)";

                if (!$result = mysqli_query($db, $query)) {
                    exit(mysqli_error($db));
                }
            }
        }
    }
    else{
        echo "añadir nombre de evento";
    }

} else {
    // no acceso admin
    echo "Acceso no autorizado";
}
?>