<?php

// arrancar la sesión, si hace falta
if (!isset($_SESSION)) {
    session_start();
}

// solo para administrador
if ( isset($_SESSION['acceso_admin']) && ($_SESSION['acceso_admin'] == 1) ) {

    if (isset($id_evento)) {
        // include Database connection file 
        include("db_connection.php");

        $query = "SELECT * 
                  FROM preguntas_votar 
                  WHERE id_evento = $id_evento 
                  ORDER BY orden";

        if (!$result = mysqli_query($db, $query)) {
            exit(mysqli_error($db));
        }

        // si hay preguntas
        if (mysqli_num_rows($result) > 0) {
            $number = 1;
?>
<div class="form-group" id="lista_preguntas">
    <ul id="preguntas_sortable" class="ui-sortable">
<?php 
            while ($row = mysqli_fetch_assoc($result)) {

                $contapreg=0;

                $iquery = "SELECT id FROM respuestas WHERE id_pregunta = " . $row['id'];

                if (!$resulta = mysqli_query($db, $iquery)) {
                    exit(mysqli_error($db));
                }

                $estilofila="";

                if (mysqli_num_rows($resulta) > 0) {
                  $contapreg = mysqli_num_rows($resulta);
                } else {
                    $estilofila = "list-group-item-danger";
                }
?>
        <li id="pregunta_<?=$row['id']?>" data-id="<?=$row['id']?>" class=" ">
            <div class="input-group col-md-12">
                <span class="input-group-addon <?=$estilofila?>">
                    <input id="checkActivo_<?=$row['id']?>" type="checkbox" data-respuestas-count="<?=$contapreg?>" data-id="<?=$row['id']?>" data-onstyle="success" data-toggle="toggle" <?php if($row['activo']==1){echo "checked";} ?>/> 
                    <i class="glyphicon glyphicon-move mar10lyr"></i><span class="tooltip-inner mar10lyr"> Nº <?=$row['orden']?></span>
                    <!-- DESHABILITADO BOTONES DE FUNCIONES PREGUNTA -->
                    <!-- <a href="respuestas.php?id=<?=$id_evento?>&id_pregunta=<?=$row['id']?>" class="glyphicon glyphicon-edit btn btn-primary" alt="Editar respuestas" title="Editar respuestas"> <span class="ftprin">Resp.</span></a> 
                    <div id="formato-<?=$row['id']?>" class="glyphicon glyphicon-th-large btn btn-info btn-activado<?php if ($row['formato_a_4'] == 1) echo " active"; ?>" alt="Formato de respuestas" title="Formato de respuestas" data-toggle="button"></div>
                    <div id="imagen-<?=$row['id']?>" class="glyphicon glyphicon-picture btn btn-info btn-activado<?php if ($row['imagen'] != '') echo " active"; ?>" alt="Imagen" onclick="showImagenPregunta(<?=$row['id']?>)" data-imagen="<?=$row['imagen']?>"> <span class="ftprin">Imagen</span></div>
                    <div class="glyphicon glyphicon-music btn btn-primary" alt="Música" onclick="showMusicaPregunta(<?=$row['id']?>)"> <span class="ftprin">Música</span></div>
                    <div id="musica-<?=$row['id']?>" class="glyphicon glyphicon-play btn btn-primary btn-activado<?php if ($row['musica'] != '') echo " active"; ?>" alt="Escuchar" onclick="playMusica(<?=$row['id']?>)" data-musica="<?=$row['musica']?>" <?php echo ($row['musica'] != '') ? '' : 'disabled'; ?> data-toggle="tooltip" data-placement="top" title="<?=$row['musica']?>"></div>
                    <div class="glyphicon glyphicon-repeat btn btn-warning" alt="Reiniciar votación" onclick="votosCero(<?=$row['id']?>, <?=$id_evento?>)"> <span class="ftprin">Reset votos</span></div> -->
                    <button class="glyphicon glyphicon-trash btn btn-danger" onclick="borrarPregunta(<?=$row['id']?>)"></button>
                </span>
                <textarea id="txtpregunta<?=$row['id']?>" class="form-control input input50 width80p txtpregunta txttinymce"><?=htmlspecialchars($row['pregunta'])?></textarea>
            </div>
        </li>
<?php
                $number++;
            }
?>
    </ul>
</div>
<button onclick="savePreguntas()" class="btn btn-success">Guardar</button>
<?php
        } else {
            // Sin preguntas
            echo  '<div class="col-md-12" >No se han encontrado preguntas.</div>';
        }
    }

} else {
    // no acceso admin
    echo "Acceso no autorizado";
}
?>