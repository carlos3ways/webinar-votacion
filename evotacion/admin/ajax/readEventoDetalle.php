<?php

// Devuelve los datos de un evento.
// Es llamado solo cuando se va a actualizar un evento.
// Por lo tanto recargar la lista de archivos de música es coherente aquí
// Devuelve también, pues, el html de las opciones del select de música


// arrancar la sesión, si hace falta
if (!isset($_SESSION)) {
    session_start();
}

// solo para administrador
if ( isset($_SESSION['acceso_admin']) && ($_SESSION['acceso_admin'] == 1) ) {


    // include Database connection file
    include "db_connection.php";

    // check request
    if(isset($_POST['id']) && isset($_POST['id']) != "")
    {
        // get User ID
        $id = $_POST['id'];

        // Get User Details
        $query = "SELECT * FROM eventos WHERE id = '$id'";
        if (!$result = mysqli_query($db, $query)) {
            exit(mysqli_error($db));
        }

        $response = array();

        if(mysqli_num_rows($result) > 0) {
            while ($row = mysqli_fetch_assoc($result)) {
                $response = $row;
            }
        }
        else
        {
            $response['status'] = 200;
            $response['message'] = "Data not found!";
        }

        // guardar el resultado en un array. Separamos los datos del evento del html de la lista de opciones
        $resultado = array();
        $resultado['evento'] = $response;
        // añadir la lista de ficheros de audio
        // recuperar la lista de archivos de música presentes
        // como opciones de un select
        include_once '../inc/getMusica.php';
        $lista_opciones_musica = getOpcionesHTMLMusica();
        $response['lista_musica'] = $lista_opciones_musica;
        // devolver todos los datos
        $json_final = json_encode($response);
        echo $json_final;

    }
    else
    {
        $response['status'] = 200;
        $response['message'] = "Invalid Request!";
    }

} else {
    // no acceso admin
    echo "Acceso no autorizado";
}
