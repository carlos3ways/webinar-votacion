<?php

// arrancar la sesión, si hace falta
if (!isset($_SESSION)) {
    session_start();
}

// solo para administrador
if ( isset($_SESSION['acceso_admin']) && ($_SESSION['acceso_admin'] == 1) ) {

    if (!empty($_POST['id_evento'])) {   

        include("db_connection.php");

        $id_evento= $_POST['id_evento'];
        $id_pregunta= $_POST['id_pregunta'];
        $activo = 0;

        //pongo todas las preguntas desactivadas
        $query = "UPDATE preguntas_votar 
                     SET activo = 0
                   WHERE id_evento = $id_evento";

        if (!$result = mysqli_query($db, $query)) {
            exit(mysqli_error($db));
        }   

        // si la pregunta está activa, activar la votación
        if (isset($_POST['activo'])) {

            $activo = $_POST['activo'];

            if ($activo) {
                $query = "UPDATE preguntas_votar 
                             SET activo = $activo
                           WHERE id_evento = $id_evento 
                             AND id = $id_pregunta";

                if (!$result = mysqli_query($db, $query)) {
                    exit(mysqli_error($db));
                }   

                // guardar en la historia que se activa la pregunta
                // la pregunta también se puede activar desde la gestión de preguntas!
                $query ="INSERT INTO preguntas_history
                        (id_pregunta) VALUES ($id_pregunta)";

                mysqli_query($db, $query);
            }
        }
    }

} else {
    // no acceso admin
    echo "Acceso no autorizado";
}
?>