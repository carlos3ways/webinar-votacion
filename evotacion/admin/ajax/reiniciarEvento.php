<?php

// arrancar la sesión, si hace falta
if (!isset($_SESSION)) {
    session_start();
}

// solo para administrador
if ( isset($_SESSION['acceso_admin']) && ($_SESSION['acceso_admin'] == 1) ) {

    if (!empty($_POST['id_evento']))
    {   
        include("db_connection.php");

        $id_evento = $_POST['id_evento'];

        // borrar los votos
        $query = "DELETE FROM votos
        WHERE id_evento = ". $id_evento;

        if (!$result = mysqli_query($db, $query)) {
            echo "Error borrando los votos.";
            exit(mysqli_error($db));
        } else {
            // borrar la historia de preguntas
            $query = "DELETE FROM preguntas_history
                       WHERE id_pregunta IN (SELECT id FROM preguntas_votar WHERE id_evento=".$id_evento.")";
            if (!$result = mysqli_query($db, $query)) {
                echo "Error borrando la historia de las preguntas del evento.";
                exit(mysqli_error($db));
            } else {
                // borrar los participantes!
                // Solo si no se marcó el checkbox de "No borrar participantes"
                if ($_POST['participantes'] == 0) {
                    $query = "DELETE FROM participantes
                               WHERE id_evento=".$id_evento;
                    if (!$result = mysqli_query($db, $query)) {
                        echo "Error borrando los participantes del evento.";
                        exit(mysqli_error($db));
                    } else {
                        echo "Datos borrados correctamente!";
                    }
                } else {
                    echo "Datos borrados correctamente!";
                }
            }
        }
    }

} else {
    // no acceso admin
    echo "Acceso no autorizado";
}
?>