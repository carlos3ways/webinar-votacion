<?php

// arrancar la sesión, si hace falta
if (!isset($_SESSION)) {
    session_start();
}

// solo para administrador
if ( isset($_SESSION['acceso_admin']) && ($_SESSION['acceso_admin'] == 1) ) {

    // directorio de musica
    include_once "../inc/config.php";
    if (isset($_FILES["fichero-musica"]["name"])) {
        $fichero_destino = _DIR_MUSICA_PATH_."/".$_FILES["fichero-musica"]["name"];
        // comprobar si el fichero ya existe, no se machaca nunca
        if (file_exists($fichero_destino)) {
            echo json_encode(array("error" => "Ya existe un fichero con este nombre."));
        } else {
            if (move_uploaded_file($_FILES['fichero-musica']['tmp_name'], $fichero_destino)) {
                // se ha subido correctamente
                echo json_encode(array("fichero" => $_FILES["fichero-musica"]["name"]));
            } else {
                echo json_encode(array("error" => "No se ha podido subir el archivo: ".$_FILES['fichero-musica']['error']));
            }
        }
    }

} else {
    // no acceso admin
    echo "Acceso no autorizado";
}
