<?php

// arrancar la sesión, si hace falta
if (!isset($_SESSION)) {
    session_start();
}

// solo para administrador
if ( isset($_SESSION['acceso_admin']) && ($_SESSION['acceso_admin'] == 1) ) {

    // directorio de musica
    include_once "db_connection.php";
    if (isset($_FILES["fichero"]["name"])) {
//        $fichero_destino = _DIR_MUSICA_PATH_."/".$_FILES["fichero-musica"]["name"];
        // comprobar si el fichero ya existe, no se machaca nunca
//        if (file_exists($fichero_destino)) {
//            echo json_encode(array("error" => "Ya existe un fichero con este nombre."));
//        } else {
//            if (move_uploaded_file($_FILES['fichero-musica']['tmp_name'], $fichero_destino)) {
//                // se ha subido correctamente
//                echo json_encode(array("fichero" => $_FILES["fichero-musica"]["name"]));
//            } else {
//                echo json_encode(array("error" => "No se ha podido subir el archivo: ".$_FILES['fichero-musica']['error']));
//            }
//        }

        // evento
        $evento = $_POST['idevento'];
        // leer el fichero temporal directamente
        $file = $_FILES['fichero']['tmp_name'];
        $handle = fopen($file, "r");
        if ($file == NULL) {
          echo 'No se ha recibido ningún fichero';
        } else {
            // saltar la primera línea de cabecera
            $filesop = fgetcsv($handle, 1000);
            // lo vamos a hacer con una transaccion
            mysqli_autocommit($db, FALSE);
            $error = '';
            // empezar la transacción
            mysqli_begin_transaction($db);
            
            while(($filesop = fgetcsv($handle, 0, ';', '"')) !== false) {
                $pregunta = utf8_encode(($filesop[0]));
                $tiempo = $filesop[1];
                $formato = $filesop[2];
                $sql = "INSERT INTO preguntas_votar (id_evento, pregunta, tiempo_responder, formato_a_4, orden) "
                        . "SELECT $evento, '".mysqli_real_escape_string($db, $pregunta)."', $tiempo, $formato, COUNT(*)+1"
                        . "  FROM preguntas_votar"
                        . " WHERE id_evento = $evento";
                // insertar cada fila con la pregunta
                if (!$result = mysqli_query($db, $sql)) {
                    // si hay un fallo se hace un rollback
                    mysqli_rollback($db);
                    $error = mysqli_error($db);
                    break;
                }
                $total_respuestas = $filesop[3];
                $respuesta_correcta = $filesop[4];
                // id de la pregunta insertada
                $id_pregunta = mysqli_insert_id($db);
                // insertar cada respuestas
                for ($i=5 ; $i<$total_respuestas+5 ; $i++) {
                    ( ($i-4) == $respuesta_correcta ) ? $correcta = 1 : $correcta = 0;
                    $sql = "INSERT INTO respuestas (id_pregunta, respuesta, es_correcta, orden) "
                            . "  VALUES ($id_pregunta, '".mysqli_real_escape_string($db, utf8_encode($filesop[$i]))."', $correcta, ($i-4))";
                    // insertar la respuesta
                    if (!$result = mysqli_query($db, $sql)) {
                        // si hay un fallo se hace un rollback
                        mysqli_rollback($db);
                        $error = mysqli_error($db);
                        break;
                    }
                }
            }
            // si no ha habido ROLLBACK se graban los cambios
            if ($error == '') {
                mysqli_commit($db);
            }
        }
    }

} else {
    // no acceso admin
    echo "Acceso no autorizado";
}
