<?php

// arrancar la sesión, si hace falta
if (!isset($_SESSION)) {
    session_start();
}

// solo para administrador
if ( isset($_SESSION['acceso_admin']) && ($_SESSION['acceso_admin'] == 1) ) {

    if (isset($_POST['id_evento'])) {
        $id_evento = $_POST['id_evento'];

        include("db_connection.php");

        $query = "SELECT * FROM usuarios_votar 
        WHERE id_evento= $id_evento 
        ORDER BY id";

        if (!$result = mysqli_query($db, $query)) {
            exit(mysqli_error($db));
        }

        if(mysqli_num_rows($result) > 0){
          $row_count = mysqli_num_rows($result);
          $number = 1;

          while($row = mysqli_fetch_assoc($result)){
              $contapreg=0;
              $estilofila="";

              $datosArray[] = [
                $row['id'],
                $row['nombre'],
                $row['apellidos'],
                $row['email'],
                $row['telefono'],
                $row['direccion'],
                $row['provincia'],
                $row['activo'], 
               '<a onclick="getUsuario('.$row['id'].')" class="glyphicon glyphicon-edit btn btn-primary"></a>',
               '<a onclick="borrarUsuario('.$row['id'].')" class="glyphicon glyphicon-trash btn btn-danger"></a>'
                ];
          }

          $respuesta = array(
            'success' => true,
            'message' => "resultados: ".$row_count,
             'data' => $datosArray 
          );
        }
        else{
          $respuesta = array(
            'success' => false,
            'message' => 'Error no data'
          );  
        }
    } else {
        $respuesta = array(
            'success' => false,
            'message' => 'Error no evento'
        );
    }

    echo json_encode($respuesta);

} else {
    // no acceso admin
    echo "Acceso no autorizado";
}
?>