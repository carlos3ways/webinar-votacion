<?php 
//validar e iniciar session
include_once('../models/Sesion.php');
Session::start();

?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <meta http-equiv="Cache-control" content="no-cache">

    <meta name="viewport" content="width=device-width, user-scalable=no" />
    <link rel="shortcut icon" type="image/x-icon" href="../assets/images/s3w-voto.ico" />
    <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="../assets/css/custom.css"/>
</head>

<body>

<?php include('inc/navbar_s3w.php'); ?>

<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
        <div class="modal-dialog">
            <div class="loginmodal-container">
            <h1>Panel de Control</h1><br>
            <input type="password" id="codigo" placeholder="Código de acceso">
            <input type="submit" name="login" class="login loginmodal-submit" value="Login" id="btn_codigo">
            </div>
        </div>
      </div>
    </div>
  </div>
</div>


<!-- /Content Section -->
<?php include('inc/pre_footer_s3w.php') ;?>

<script type="text/javascript" src="../assets/js/jquery-1.11.3.min.js"></script>
<script  type="text/javascript">
$(document).ready(function () {
    $('#btn_codigo').on('click', function() {
        var codigo = $('#codigo').val();
        $.post("../admin/ajax/login.php", {
            codigo:codigo
        }, function (data, status) {
          if(data == 'codigo correcto') {
            window.location.href = 'index.php';
          } else {
            alert(data);
          }
        });
    });
});
</script>


</body>
</html>