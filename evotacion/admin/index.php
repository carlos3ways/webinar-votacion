<?php 
//validar e iniciar session
include_once('../models/Sesion.php');
Session::start();
if(!Session::check()) {
    header('Location: login.php');
    die();
}

?>

<!DOCTYPE html>
<html lang="es">
<?php include('inc/head.php'); ?>
<body>
<?php include('inc/navbar_s3w.php'); ?>


<!-- Content Section -->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>Sistema de Votaciones</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                <button class="btn btn-success" data-toggle="modal" data-target="#add_new_evento">Añadir Evento</button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h3>Eventos:</h3>
            <div class="contenido_eventos"></div>
        </div>
    </div>
</div>
<!-- /Content Section -->

<?php include('inc/modal_add_evento.php'); ?>
<?php include('inc/modal_update_evento.php'); ?>
<?php include('inc/modal_add_pregunta.php'); ?>
<?php include('inc/modal_subir_musica.php'); ?>
<?php include('inc/pre_footer_s3w.php') ;?>
<?php include('inc/footer.php'); ?>
<script type="text/javascript" src="../assets/js/script.js"></script>
<script src="../vendor/itsjavi/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
<script src="../vendor/kartik-v/bootstrap-fileinput/js/fileinput.min.js"></script>
<script src="../vendor/kartik-v/bootstrap-fileinput/js/locales/es.js"></script>
<script>
    var dir_musica = '<?=_DIR_MUSICA_WEB_?>';
    var dir_images_upload = '<?=_DIR_IMAGES_UPLOAD_WEB_?>';
    $(function () {
        $('#fondo_head, #fondo_foot, #respuesta_ok, #respuesta_ko').colorpicker({fallbackColor: '#FFFFFF', format: 'hex' });
    });

</script>
<script type="text/javascript" src="../assets/js/loadReadyEventos.js"></script>

</body>
</html>