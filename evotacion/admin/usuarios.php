<?php 

// pantalla de gestión de usuarios
if (!isset($_SESSION)) {
    session_start();
}

// control del acceso administrador
if ( isset($_SESSION['acceso_admin']) && ($_SESSION['acceso_admin'] == 1) ) {

    // si hay evento
    if (isset($_REQUEST['id'])) {
        $id_evento = $_REQUEST['id'];
        include('inc/head.php'); 
        include('inc/header_usuarios.php');
    ?>

    <body>
    <?php include('inc/navbar_s3w.php'); ?>
    <!-- Content Section -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Sistema de Votaciones</h1>
                <h2>Gestión usuarios</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="pull-right">
                    <a href="index.php"><button class="btn btn-info"> Ver Eventos</button></a>
                    <button class="btn btn-success" type="button" data-toggle="modal" data-target="#add_new_usuario">Nuevo Usuario</button>
                    <button class="btn btn-" type="button" data-toggle="modal" data-target="#">(prox) importar desde csv</button>
                </div>
            </div>
        </div>
        <div id="console-event"></div>
        <div class="row">
            <div class="col-md-12">
                <h3>Usuarios:</h3>
                <table id="usuariosAjax" class="table">
                    <thead>
                        <tr>
                          <th>id</th>
                          <th>Nombre</th>
                          <th>Apellidos</th>
                          <th>Email</th>
                          <th>Teléfono</th>
                          <th>Direccion</th>
                          <th>Provincia</th>
                          <th>Activo</th>
                          <th></th>
                          <th></th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- /Content Section -->

    <?php include('inc/modal_add_usuario.php'); ?>
    <?php include('inc/modal_edit_usuario.php'); ?>
    <?php include('inc/modal_delete_usuario.php'); ?>
    <?php include('inc/footer.php'); ?>

    <script type="text/javascript" src="../assets/js/script.js"></script>
    <script type="text/javascript" src="../assets/js/bootstrap-toggle.min.js"></script>
    <script type="text/javascript" src="../assets/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../assets/js/dataTables.bootstrap.min.js"></script>

<script>
$(document).ready(function(){
    $('#usuariosAjax').DataTable({
        "ajax": {
            'type': 'POST',
            'url': 'ajax/readUsuarios.php',
            'data': { id_evento: '<?=$id_evento?>'},
            // "order": [[ 5, "desc" ]],
        },
        "deferRender": true
    });
});
</script>
    <?php //include_once('ajax/readUsuarios.php');?>
    <?php include('inc/pre_footer_s3w.php') ;?>
    <?php 
    } //fin hay id_evento

} else {
    // volver al login
    header("Location: login.php");
}
?>
</body>
</html>