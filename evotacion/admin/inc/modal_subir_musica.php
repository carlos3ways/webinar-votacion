<?php
    // recuperar la lista de archivos de música presentes
    // como opciones de un select
//    include_once 'inc/getMusica.php';
//    $lista_opciones_musica = getOpcionesHTMLMusica();
    
?>

<div class="modal fade" id="subir_musica_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Subir ficheros de música</h4>
                <input id="pickerTipo" type="hidden">
                <input id="pickerOrigen" type="hidden">
                <input id="ficheroSubido" type="hidden">
            </div>
            <div class="modal-body">
                <div class="file-loading">
                    <label>Fichero</label>
                    <input id="fichero-musica" name="fichero-musica" type="file" class="file" data-show-preview="false" data-allowed-file-extensions='["mp3", "wav"]'
                           data-language="es" data-el-error-container="#errores-subida-fichero" data-upload-url="ajax/subir_musica.php">
                </div>

                <div id="errores-subida-fichero"></div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" id="btn-seleccionar-fichero" class="btn btn-primary" onclick="seleccionarMusica()" disabled />Seleccionar</button>
            </div>
        </div>
    </div>
</div>
<!-- // Modal -->