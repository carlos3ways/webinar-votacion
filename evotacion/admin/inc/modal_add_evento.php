<?php
    // recuperar la lista de archivos de música presentes
    // como opciones de un select
    include_once 'inc/getMusica.php';
?>

<!-- Modal - Add New Evento -->
<div class="modal fade" id="add_new_evento" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-evento" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Añadir nuevo evento</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-9 col-md-9">
                            <label for="even">Evento</label>
                            <input type="text" id="evento" placeholder="Nombre del evento" class="form-control"/>
                        </div>
                        <div class="oculto col-sm-3 col-md-3 nopad-left">
                            <label for="codigo_acceso">Código acceso</label>
                            <input type="text" id="codigo_acceso" placeholder="Codigo acceso" class="form-control"/>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-9 col-md-9">
                            <label for="fecha_inicio">Fecha inicio</label>
                            <div class='input-group date' id='fecha_inicio'>
                                <input type='text' placeholder="Fecha inicio" class="form-control" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span><br>
                            </div>
                            <small id="emailHelp" class="form-text text-muted">Después de la fecha indicada no se podrá delegar votos o eliminar un evento.</small>
                        </div>
                        <div class="oculto col-sm-6 col-md-6">
                            <label for="fecha_fin">Fecha fin</label>
                            <div class='input-group date' id='fecha_fin'>
                                <input type='text' placeholder="Fecha fin" class="form-control" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="oculto form-group">
                    <div class="row">
                        <div class="col-sm-2 col-md-2">
                            <div class="form-group martop15">
                                <label for="tiempo">Tiempo </label>
                                <input type="text" id="tiempo" placeholder="Tiempo" class="form-control" />
                            </div>
                        </div>
                        <div class="col-sm-10 col-md-10">
                            <div class="form-group martop15">
                                <label for="musica">Música </label>
                                <select name="musica" id="musica" class="selectpicker musica" data-origen="_new">
                                </select>
                                <audio id="audio_new_evento" src="" preload="auto" controls></audio>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="oculto form-group">
                    <div class="row">
                        <div class="col-sm-4 col-md-4">
                            <label for="anonimo">Participantes anónimos </label>
                            <input type="checkbox" id="anonimo" placeholder="Participantes anónimos" />
                        </div>
                        <div class="col-sm-5 col-md-5">
                            <label for="autosolucion">Mostrar respuesta correcta </label>
                            <input type="checkbox" id="autosolucion" placeholder="Mostrar solución automáticamente" checked />
                        </div>
                        <div class="col-sm-3 col-md-3">
                            <label for="autoranking">Mostrar ránking </label>
                            <input type="checkbox" id="autoranking" placeholder="Mostrar ránking automáticamente" />
                        </div>
                    </div>
                </div>

                <div class="oculto form-group">
                    <label for="theme">Personalizar estilo </label>
                    <input type="checkbox" id="theme" placeholder="theme" />
                </div>

                <div class="oculto container-personalizar-estilo" style="display:none;">

                    <div class="oculto row">
                        <div class="col-sm-3 col-md-3">
                            <div class="form-group">
                                <label for="fondo_head">Color cabecera </label>
                                <div id="fondo_head" class="input-group colorpicker-component" >
                                    <input type="text" class="form-control input-md" value="" />
                                    <span class="input-group-addon"><i></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3">
                            <div class="form-group">
                                <label for="fondo_foot">Color pie </label>
                                <div id="fondo_foot" class="input-group colorpicker-component" >
                                    <input type="text" class="form-control input-md" value="" />
                                    <span class="input-group-addon"><i></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3">
                            <div class="form-group">
                                <label for="respuesta_ok">Resp.correcta </label>
                                <div id="respuesta_ok" class="input-group colorpicker-component" >
                                    <input type="text" class="form-control input-md" value="" />
                                    <span class="input-group-addon"><i></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3">
                            <div class="form-group">
                                <label for="respuesta_ko">Resp.errónea </label>
                                <div id="respuesta_ko" class="input-group colorpicker-component" >
                                    <input type="text" class="form-control input-md" value="" />
                                    <span class="input-group-addon"><i></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="oculto row">
                        <div class="col-sm-12 col-md-12">
                            <label>Imagen cabecera</label> <span id="link-vaciar-new-logo-head" class="hide">(limpiar <span id="vaciar-logo-head" class="glyphicon glyphicon-trash text-danger"></span>)</span>
                            <div class="container-personalizar-estilo">
                                <div class="row">
                                    <div class="col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <label for="logo_new_head">Subir/cambiar imagen</label>
                                            <input type="file" id="logo_new_head" class="form-control" onchange="leerImagenURL(this, 'img_new_logo_head');" />
                                        </div>
                                    </div>
                                    <div class="col-sm-3 col-md-3 nopad-left">
                                        <div class="form-group marbot5">
                                            <label>Vista previa</label>
                                            <img id="img_new_logo_head" class="thumbnail" onerror="this.src='../assets/images/trans.png'">
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-md-2">
                                        <div class="form-group">
                                            <label for="align_logo_head">Alinear</label>
                                            <select name="align_logo_head" id="align_logo_head" class="selectpicker" data-width="fit">
                                                <option value="left">Izquierda&nbsp;</option>
                                                <option value="center">Centro</option>
                                                <option value="right">Derecha</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="oculto row">
                        <div class="col-sm-12 col-md-12">
                            <label>Imagen pie</label> <span id="link-vaciar-new-logo-foot" class="hide">(limpiar <span id="vaciar-logo-foot" class="glyphicon glyphicon-trash text-danger"></span>)</span>
                            <div id="container-logo-foot" class="container-personalizar-estilo">
                                <div class="row">
                                    <div class="col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <label for="logo_new_foot">Subir/cambiar imagen</label>
                                            <input type="file" id="logo_new_foot" class="form-control" onchange="leerImagenURL(this, 'img_new_logo_foot');" />
                                        </div>
                                    </div>
                                    <div class="col-sm-3 col-md-3 nopad-left">
                                        <div class="form-group">
                                            <label>Vista previa</label>
                                            <img id="img_new_logo_foot" src="" class="thumbnail" onerror="this.src='../assets/images/trans.png'">
                                        </div>
                                    </div>
                                    <div class="col-sm-2 col-md-2">
                                        <div class="form-group">
                                            <label for="align_logo_foot">Alinear</label>
                                            <select name="align_logo_foot" id="align_logo_foot" class="selectpicker" data-width="fit">
                                                <option value="left">Izquierda&nbsp;</option>
                                                <option value="center">Centro</option>
                                                <option value="right">Derecha</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="activo">Activo </label>
                    <input type="checkbox" id="activo" placeholder="Activo" />
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" onclick="addEvento()">Guardar</button>
            </div>
        </div>
    </div>
</div>
<!-- // Modal -->