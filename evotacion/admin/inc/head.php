<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>eVotacion</title>
    <meta http-equiv="Cache-control" content="no-cache">
    <meta name="robots" content="noindex,nofollow">
    <link rel="shortcut icon" type="image/x-icon" href="../assets/images/s3w-vota.ico" />
    <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.css"/>
    <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap-datetimepicker.min.css"/>
    <link rel="stylesheet" type="text/css" href="../vendor/itsjavi/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.css">
    <link rel="stylesheet" type="text/css" href="../assets/css/custom.css"/>
    <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap-toggle.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <link rel="stylesheet" type="text/css" href="../vendor/kartik-v/bootstrap-fileinput/css/fileinput.min.css" media="all">
    <script src="../assets/js/logout.js"></script>
<?php
    if ( defined('_DIR_WEB_') ) {
?>
    <script src="../vendor/tinymce/tinymce/tinymce.js"></script>
    <script>
        tinymce.init({  selector: '.txttinymce'
                    ,   menubar: false
                    ,   plugins: "textcolor, lists, code"
                    ,   toolbar: 'undo redo | fontsizeselect forecolor | bold italic | numlist bullist | code'
                    ,   toolbar_items_size: "small"
                    ,   fontsize_formats: "8px 10px 12px 14px 18px 24px 36px 48px 60px"
                    ,   language: 'es'
                    ,   language_url: '<?=_DIR_WEB_?>/assets/js/tinymce_lang/es.js'
                    ,   skin_url: '<?=_DIR_WEB_?>/assets/css/tinymce_skin/mini'
                    ,   setup: function (editor) {
                            editor.on('change', function () {
                                editor.save();
                            });
                        }
                    ,   content_style: " body, p { margin-top:5px; } .mce-ico { font-size:14px; }"
                    ,   min_height: 70
                    ,   branding: false
                    ,   statusbar : false
        });
    </script>
<?php
    }
?>
</head>