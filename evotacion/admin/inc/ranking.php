<?php
//////////////////////////////////////////////////////////////////////
// Funciones para la generación de estadísticas y la clasificación
//////////////////////////////////////////////////////////////////////

// Preguntas que entran en la estadística
function getPreguntas($db, $id_evento, $num_pregunta = 0) {
    
    $sql_filtro_pregunta = '';
    if ($num_pregunta > 0) {
        // Hasta la pregunta $num_pregunta
        $sql_filtro_pregunta = " AND p.orden <= ".$num_pregunta;
    }
    $query ="SELECT p.id id_pregunta, p.id_evento, ph.id as id_pregunta_history, ph.fecha, e.tiempo
               FROM preguntas_votar p LEFT JOIN preguntas_history ph ON p.id = ph.id_pregunta
                                JOIN eventos e ON e.id = p.id_evento
              WHERE p.id_evento= ".$id_evento.$sql_filtro_pregunta."
           ORDER BY p.orden, ph.fecha DESC";
    if (!$preguntas = mysqli_query($db, $query)) {
        return false;
    }
    // recuperamos todas las filas
    $res = mysqli_fetch_all($preguntas, MYSQLI_ASSOC);
    return $res;
    
}

// Respuestas correctas de las preguntas que entran en la estadística
function getRespuestasCorrectas ($db, $filtro) {
    
    // sql de la consulta
    $query = "SELECT * FROM respuestas
              WHERE id_pregunta 
              IN (".$filtro.") 
              AND es_correcta = 1";

    if (!$correctas = mysqli_query($db, $query)) {
        return false;
    }
    // recuperamos todas las filas
    $res = mysqli_fetch_all($correctas, MYSQLI_ASSOC);
    return $res;

}

// Lista de participantes de un evento
function getParticipantes ($db, $id_evento) {

    $query = "SELECT id, nickname "
            . " FROM participantes "
            . "WHERE id_evento = ".$id_evento
            ." UNION "
            ."SELECT id, CONCAT(nombre,' ',apellidos) as nickname"
            . " FROM usuarios_votar "
            . "WHERE id_evento = ".$id_evento;
    if (!$participantes = mysqli_query($db, $query)) {
        return false;
    }
    // recuperar todas las filas
    $res = mysqli_fetch_all($participantes, MYSQLI_ASSOC);
    return $res;

}

// Lista de votos emitidos de las preguntas correspondientes
function getVotosEmitidos($db, $id_evento, $filtro) {

    // filtro de preguntas
    $sql_filtro = " AND v.id_pregunta IN (".$filtro.") ";
    $query = "SELECT v.id_pregunta, v.id_respuesta, v.fecha,
                     COALESCE(u.id, p.id) as id,
                     COALESCE(CONCAT(u.nombre,' ',u.apellidos),p.nickname) as nombre
                FROM votos v LEFT JOIN usuarios_votar u ON u.id = v.id_usuario AND u.activo = 1
                             LEFT JOIN participantes p ON p.id = v.id_usuario 
               WHERE v.id_evento = ".$id_evento.$sql_filtro."
            ORDER BY u.id";
    if (!$votos = mysqli_query($db, $query)) {
        return false;
    }
    // recuperar todas las filas
    $res = mysqli_fetch_all($votos, MYSQLI_ASSOC);
    return $res;

}

// Calcular la diferencia entre 2 fechas (strings)
function diferenciaTiempo($inicio, $final) {
    // convertir los strings a DateTime
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
// ATENCION: MYSQL no soporta milisegundos hasta 5.6.4
// La versión actual de BD es 5.1.73
// Por lo tanto el resultado está en segundos
//    $stamp_ini = DateTime::createFromFormat('Y-m-d H:i:s.u', $inicio);
//    $stamp_fin = DateTime::createFromFormat('Y-m-d H:i:s.u', $final);
    $stamp_ini = DateTime::createFromFormat('Y-m-d H:i:s', $inicio);
    $stamp_fin = DateTime::createFromFormat('Y-m-d H:i:s', $final);
    // diferencia
    $intervalo = date_diff($stamp_fin, $stamp_ini);
    // formatear (microsegundos solo a partir de PHP 7.1)
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
// ATENCION: MYSQL no soporta milisegundos hasta 5.6.4
// La versión actual de BD es 5.1.73
// Por lo tanto el resultado está en segundos
//    if (phpversion() >= '7.1') {
//        $resultado = substr($intervalo->format('%s.%f'),0,-4);
//    } else {
//        $resultado = $intervalo->format('%s');
//    }
////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////
// ATENCION: MYSQL no soporta milisegundos hasta 5.6.4
// La versión actual de BD es 5.1.73
// Por lo tanto el resultado está en segundos
    $resultado = $intervalo->format('%s');

    return floatval($resultado);
}

// Procesar todos los datos de participantes/preguntas/votos
// Calcula aciertos/errores/tiempo total
function procesarVotos(&$data_participantes, &$data_preguntas, &$data_respuestas_ok, &$data_votos, &$a_base_data, &$datosArray, &$respuestasxusu) {

    // para cada participante
    foreach ($data_participantes as $dp) {
        // inicializar los datos de respuestas de los participantes
        $respuestasxusu[$dp['id']]['nombre'] = $dp['nickname'];
        $respuestasxusu[$dp['id']]['aciertos'] = 0;
        $respuestasxusu[$dp['id']]['errores'] = 0;
        $respuestasxusu[$dp['id']]['tiempo_total'] = 0;
        // para cada pregunta
        foreach ($data_preguntas as $dtp) {
            // nueva pregunta
            $pregunta_votada = 0;
            // para cada voto emitido
            foreach ($data_votos as $du) {
                // guardar el id de pregunta, si no estaba puesto ya
                if (!isset($a_base_data[$dtp['id_pregunta']])) {
                    // crear la entrada para el orden de las columnas DataTable
                    $a_base_data[$dtp['id_pregunta']]=0;
                }
                // si el voto es de esta pregunta y de este usuario:
                if ($dtp['id_pregunta'] == $du['id_pregunta'] && $du['id'] == $dp['id']) {

                    // calcular el tiempo (MILISEGUNDOS) que tardó en contestar
                    $segundos = diferenciaTiempo($dtp['fecha'], $du['fecha']);
// solo segundos                    $segundos = strtotime($du['fecha']) - strtotime($dtp['fecha']);
                    // recorrer la lista de respuestas correctas para ver si es correcta
                    $respuesta = 0;
                    foreach ($data_respuestas_ok as $re) {
                        // si la respuesta es correcta, guarda acierto y tiempo
                        if ($re['id_pregunta'] == $du['id_pregunta']) {
                            if ($re['orden'] == $du['id_respuesta']) {
                                $respuestasxusu[$du['id']]['aciertos']++;
                                $respuesta = 1;
                                break; // rompo porque puede tener más de alguna contestación correcta
                            }
                        }
                    }
                    // si la respuesta NO ha sido correcta, un error mas
                    if ($respuesta == 0) {
                        $respuestasxusu[$du['id']]['errores']++;
                    }
                    // registramos el tiempo que se tardó en contestar
                    $respuestasxusu[$du['id']]['tiempo_total'] += $segundos;
                    // guardar los datos estadísticos
                    $data =  array('id_usuario' => $du['id'], 
                                   'nombre' => $du['nombre'],
                                   'respuesta' => $respuesta,
                                   'segundos' => $segundos);
                    $datosArray[$du['id']][$dtp['id_pregunta']]= $data;
                    // marcamos la pregunta como que se votó
                    $pregunta_votada = 1;
                }
            }
            // si al final de todos los votos, la pregunta no está votada
            // entonces es que es errónea con el tiempo total de la pregunta
            if ($pregunta_votada == 0) {
                $respuestasxusu[$dp['id']]['errores']++;
                // registramos el tiempo que se tardó en contestar
                $respuestasxusu[$dp['id']]['tiempo_total'] += $dtp['tiempo'];
            }
        } // foreach pregunta
    } // foreach participante
    
}

// Clasificación hasta la pregunta concreta (0 = TODAS)
function clasificacion($db, $id_evento, $num_pregunta = 0) {

    // inicializar los array necesarios
    // ids de las preguntas del evento
    $ids_preguntas=[];
    // datos de las preguntas del evento
    $data_preguntas=[];
    // datos de las votaciones de los usuarios
    $data_votos=[];
    // datos de todos los participantes (y usuarios)
    $data_participantes=[];
    // respuestas correctas de cada pregunta del evento
    $data_respuestas_ok=[];
    // datos de estadísticas de los votos: dos dimensiones: [usuario][pregunta]
    $datosArray=[];
    // array ordenado con los campos según la tabla de clasificación en pantalla
    $a_base_data=[];
    // array con la tabla final ordenada de los participantes
    $dataFinal= [];
    // datos de las respuestas de usuario
    $respuestasxusu=[];

    // los campos en $a_base_data se crean en orden según la tabla HTML a mostrar (DataTable)
    $a_base_data['puesto_ranking'] = 0;
    $a_base_data['id_usuario']=0;
    $a_base_data['nombre']=0;
    // iniciar los totales
    $a_base_data['preguntas_acertadas'] = 0;
    $a_base_data['preguntas_erroneas'] = 0;
    $a_base_data['tiempo_medioxpregunta'] = 0;

    // recuperamos las preguntas y el momento en que se lanzaron
    $preguntas = getPreguntas($db, $id_evento, $num_pregunta);
    if (! $preguntas) {
        exit("Error recuperando las preguntas");
    }

    // guardar los datos de las preguntas ( y los ids a parte)
    $data_preguntas = $preguntas;
    foreach ($preguntas as $pregunta) {
        array_push($ids_preguntas, $pregunta['id_pregunta']);
    }
    // los ids de las preguntas, para recuperar las respuestas correctas
    $preg_in = implode(",", $ids_preguntas);

    $data_respuestas_ok = getRespuestasCorrectas($db, $preg_in);
    if (! $data_respuestas_ok) {
        exit("Error recuperando las respuestas");
    }

    // lista de participantes
    $data_participantes = getParticipantes($db, $id_evento);
    //////////////////////////////////////////////////////////////////////////////////////////////
    // ATENCION!! SE añade una "a" al ID porque array_multisort mantiene los índices tipo string
    // y en cambio reindexa los enteros (ordenar, para el ranking)
    //////////////////////////////////////////////////////////////////////////////////////////////
    foreach ($data_participantes as $indice => $row) {
        $data_participantes[$indice]['id'] = "a".$data_participantes[$indice]['id'];
    }
    
    // recuperar todos los votos emitidos (de las preguntas en juego)
    $data_votos = getVotosEmitidos($db, $id_evento, $preg_in);
    if (! $data_votos) {
        exit("Error recuperando los votos");
    }
    //////////////////////////////////////////////////////////////////////////////////////////////
    // ATENCION!! SE añade una "a" al ID porque array_multisort mantiene los índices tipo string
    // y en cambio reindexa los enteros (ordenar, para el ranking)
    //////////////////////////////////////////////////////////////////////////////////////////////
    foreach ($data_votos as $indice => $row) {
        $data_votos[$indice]['id'] = "a".$data_votos[$indice]['id'];
    }    

    // procesar los datos: pregunta/respuestas/votos/aciertos/tiempo
    // arrays pasados por referencia
    procesarVotos($data_participantes, $data_preguntas, $data_respuestas_ok, $data_votos, $a_base_data, $datosArray, $respuestasxusu);

    // para ordenar el ranking (por aciertos y por total de tiempo)
    array_multisort(array_column($respuestasxusu,'aciertos'), SORT_DESC, array_column($respuestasxusu,'tiempo_total'), SORT_ASC , $respuestasxusu);
    // el contador será el puesto en el ranking
    $contrank=1;

    // total de usuarios y preguntas
    $total_usuarios = count($respuestasxusu);
    $total_preguntas = count($ids_preguntas);
    
    // Tenemos los datos de los usuarios en $respuestasxusu
    // y los datos de las respuestas en $datosArray
    // Recorremos el array con todos los usuarios ordenados por ranking y mostramos todos sus datos
    foreach ($respuestasxusu as $id_usu => $resumen_usu) {
        // vamos rellenando los datos del usuario
        //////////////////////////////////////////////////////////////////////////////////////////////
        // ATENCION!! Quitar la "a" al ID añadido antes por array_multisort
        //////////////////////////////////////////////////////////////////////////////////////////////
        $data_user= $a_base_data;
        $data_user['id_usuario'] = substr($id_usu, 1);
        $data_user['nombre'] = $resumen_usu['nombre'];
        $data_user['preguntas_acertadas'] = $respuestasxusu[$id_usu]['aciertos'];
        $data_user['preguntas_erroneas']  = $respuestasxusu[$id_usu]['errores'];
        $data_user['puesto_ranking'] = '#'.$contrank++;
        $data_user['tiempo_medioxpregunta'] = number_format(($respuestasxusu[$id_usu]['tiempo_total'] / $total_preguntas), 1, '.', ' ') . " seg";
        $data_user['blanco'] = '';
        // para cada pregunta comprobar si respondió bien o mal
        foreach ($data_preguntas as $pregunta) {
            
            // un DIV para cada pregunta
            $respuesta = "<div id='".$id_usu."_".$pregunta['id_pregunta']."'>";
            // comprobar si se respondió (hay registro en $datosArray
            // si se respondió -> mostrar tiempo
            // si no -> mostrar solo la X
            if ( isset($datosArray[$id_usu][$pregunta['id_pregunta']])) {
                // se respondió
                // poner el icono de correcta/incorrecta
                if ($datosArray[$id_usu][$pregunta['id_pregunta']]["respuesta"] == 1) {
                    $respuesta .= "<span class='glyphicon glyphicon-ok' aria-hidden='true' style='color: green;'></span>";
                } else {
                    $respuesta .= "<span class='glyphicon glyphicon-remove' style='color: red;' aria-hidden='true'></span>";
                }
                // y después los datos del tiempo
                $respuesta .= "&nbsp;<span class='glyphicon glyphicon-time' aria-hidden='true'></span> ";
                $respuesta .= $datosArray[$id_usu][$pregunta['id_pregunta']]['segundos'] . "\" ";
            } else {
                // no se respondió
                $respuesta .= "<span class='glyphicon glyphicon-remove' style='color: red;' aria-hidden='true'></span>";
            }
            $respuesta .= "</div>";
            $data_user[$pregunta['id_pregunta']] = $respuesta;  

        }
        // y guardar sus datos en el array final (dataFinal)
        array_push($dataFinal,array_values($data_user));
        unset($data_user);
    }
    
    // devolvemos un array con los datos de clasificación, y varios datos para totales
    $clas = array ( 'clasificacion' => $dataFinal
                ,   'preguntas' => $data_preguntas
                ,   'votos' => $data_votos
                ,   'resultados' => $datosArray);
    return $clas;

}
