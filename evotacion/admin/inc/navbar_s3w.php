<div class="header" style="background-color:#f4f4f4;padding:1rem">
    <div class="row" style="width:100%;margin:0;-ms-flex-align:center;align-items:center;display:-ms-flexbox;display:flex;-ms-flex-wrap:wrap;flex-wrap:wrap">
        <div class="col-xs-12 col-sm-3">
            <img src="../../assets/images/logo-seimc.png" alt="Header" style="max-width:100%;height:80px;margin-left:auto;margin-right:auto;display:block;margin-top:1rem" />
        </div>

        <div class="col-xs-12 col-sm-6 text-center" style="margin-top:.5rem;margin-bottom:.5rem">
            <h2 style="font-weight:700">Gestor de Asamblea Virtual</h2>
        </div>

        <?php if (Session::check()): ?>
        <div class="col-xs-12 col-sm-3 text-center text-sm-right" style="-ms-flex-item-align:start;align-self:flex-start">
          <button class="btn btn-danger" onclick="logout()">Desconectar</button>
        </div>
        <?php endif; ?>
    </div>
</div>