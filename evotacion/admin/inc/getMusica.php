<?php

if (is_file('inc/config.php')) {
    include_once 'inc/config.php';
} else {
    include_once '../inc/config.php';
}

// devuelve el HTML de las <option> del select de música
function getOpcionesHTMLMusica() {
    $html_final = '<option value=""> Selecciona un archivo</option>';
    $html_final .= '<option value="-"> Sube un archivo...</option>';
    // recuperar la lista de ficheros
    $lista_ficheros = scandir(_DIR_MUSICA_PATH_);
    foreach($lista_ficheros as $nombre_fichero) {
       // controlar que no es "." o ".."
       if ($nombre_fichero != "." && $nombre_fichero != "..") {
           $html_final .= '<option value="'.$nombre_fichero.'">'.$nombre_fichero.'</option>';
        }
    }
   
    return $html_final;

}
