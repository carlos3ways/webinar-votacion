<?php
    // PreguntasPlayer_Control (Gestor de la evolución de preguntas)

    // recuperar TODAS las preguntas (¿DISTINCT + join con respuestas?!!!)
    $query = "SELECT DISTINCT p.* 
                FROM preguntas_votar p JOIN respuestas r ON p.id = r.id_pregunta
               WHERE p.id_evento= $id_evento 
            ORDER BY p.orden";

    if (!$result = mysqli_query($db, $query)) {
        exit(mysqli_error($db));
    }

    // gestión de anterior y siguiente
    $id_panterior = "";
    $id_psiguiente = "";

    $i=1;

    $a_evenpa=[];
    // recorrer las preguntas ordenadas y guardar los IDS en $a_evenpa
    while ($i <= mysqli_num_rows($result)) {
        $row = mysqli_fetch_assoc($result);
        array_push($a_evenpa, $row['id']);
        $i++;
    }

    // total de preguntas
    $p_count= mysqli_num_rows($result);
    // clave -> índice del array de preguntas (empezando por 0)
    // $id_pregunta es GET['id_pregunta'] (resultado/index.php)
    $clave = array_search($id_pregunta, $a_evenpa);

    // si la clave no es la última -> hay posterior
    if ( $clave < $p_count-1) {
        $id_psiguiente = $a_evenpa[$clave+1];
    }
    // si la clave no es la primera -> hay previo
    if ( $clave > 0) {
        $id_panterior = $a_evenpa[$clave-1];
    }

    $simple_url = "index.php?id=$id_evento&id_pregunta=";

?>
<div class="envoltorio">
    <div id="menu-wrapper">
        <div class="checkbox" title="W">
            <input id="toggle-resultado" data-toggle="toggle" type="checkbox">
        </div>
        <div class="paginador">
        <?php if (!empty($id_panterior)){ ?>
            <a href="<?=$simple_url . $id_panterior?>" title="Flecha anterior" id="pa_anterior"> <i class="glyphicon glyphicon-menu-left fs30"></i></a>
        <?php  } ?>

        <?php if(!empty($id_psiguiente)){ ?>
        <a href="<?=$simple_url . $id_psiguiente?>" title="Flecha siguiente " id="pa_siguiente"> <i class="glyphicon glyphicon-menu-right fs30"></i></a>
        <?php  } else { ?>
        <a href="final.php?id=<?=$id_evento?>" title="F - Finalizar" id="final_evento"> <i class="glyphicon glyphicon-flag fs30"></i></a>
        <?php  } ?>
        </div>
        <?php if ( $tiempo != 0){ ?>
        <div class="mando_cuenta bs-callout">
            <div class="reset glyphicon glyphicon-refresh" title="R - Reiniciar"> </div>
            <div class="start glyphicon glyphicon-play" title="Space - Iniciar"> </div>
            <div class="pause glyphicon glyphicon-pause" title="P - Pausa"> </div>
            <div class="solucion glyphicon glyphicon-ok" title="S - Solución"> </div>
            <div class="ranking glyphicon glyphicon-equalizer" title="C - Clasificación"> </div>
            <div class="flr">   
                <input type="text" value="00" id="tiempo_menu"> <span > <i class="glyphicon glyphicon-time"> </i> </span>
            </div>
        </div>
        <?php } 
        else{ ?>
        <div class="checkbox" title="V">
            <input id="toggle-vote-onoff" data-toggle="toggle" data-style="ios" data-onstyle="success" type="checkbox" data-on="Voto ON" data-off="Voto OFF">
        </div>
        <div class="mando_cuenta bs-callout" style="padding-left: 40px;">
        <div class="solucion glyphicon glyphicon-ok" title="S"> </div>
        <div class="repeat glyphicon glyphicon glyphicon-repeat" title="U"> </div>
        </div>

        
        <?php } ?>
        <!-- <a id="demo_mode">dm</a> -->
        <div class="textura" title="cerrar"> </div>
    </div>
    <div id="lado-wrapper"> </div>
</div>