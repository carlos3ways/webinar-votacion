<div class="modal fade" id="confirm_poner_cero" tabindex="-1" role="dialog" aria-labelledby="myModalLabelo">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabelo">Vas a reiniciar la votación de la pregunta</h4>
            </div>
            <div class="modal-body">
                Vas a borrar los datos de todas las votaciones previas de esta pregunta, ¿estás seguro?
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-primary" id="votacion_cero">Reiniciar votación</button>
                <button type="button" data-dismiss="modal" class="btn">Cancelar</button>
            </div>
        </div>
    </div>
</div>