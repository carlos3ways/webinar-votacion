<?php

// Gestión de los estilos personalizados de un evento
// Se utiliza desde la parte de admin y la parte de votación

// recupera los datos de un evento.
// Devuelve un array
function getDatosEstiloEvento($db, $id_evento) {

    // query del evento
    $query = "SELECT evento as nombre_evento, autoranking, autosolucion, anonimo, tiempo, musica"
            . "    , theme, fondo_head, fondo_foot, logo_head, logo_foot, align_logo_head, align_logo_foot "
            . "    , fondo_respuesta_ok, fondo_respuesta_ko "
            . " FROM eventos WHERE id = " . $id_evento;

    if (!$result = mysqli_query($db, $query)) {
        exit(mysqli_error($db));
    } else {
        return mysqli_fetch_array($result);
    }

}

// genera los htmls correspondientes a la cabecera y el pie
// Devuelve array con los códigos HTML
function getHTMLSNavs($row) {
    $res = array();
    $fondo_head = $row['fondo_head'];
    $fondo_foot = $row['fondo_foot'];
    // para evitar cache, añadimos timestamp
    $logo_head = $row['logo_head'].'?t='.date('His');
    $logo_foot = $row['logo_foot'].'?t='.date('His');
    $align_logo_head = $row['align_logo_head'];
    $align_logo_foot = $row['align_logo_foot'];
    // montar la navbar personalizada: HEAD
    $estilos_head = "height: 90px;";
    if ($fondo_head != '') {
        $estilos_head .= "background-color: ".$fondo_head.";";
    }
    if ($logo_head != '') {
        $estilos_head .= "background-image: url('"._DIR_IMAGES_UPLOAD_WEB_."/".$logo_head."'); background-repeat: no-repeat; background-size: contain;";
        if ($align_logo_head != '') {
            $estilos_head .= "background-position: ".$align_logo_head." top;";
        }
    }
    $html_navbartop = '<nav class="navbar-fixed-top" style="'.$estilos_head.'"><div class="container"><div class="navbar-header">';
    $html_navbartop .= '</div><div id="navbar" class="collapse navbar-collapse"></div></div></nav>';
    // montar la navbar personalizada: FOOT
    $estilos_foot = "height: 78px;";
    if ($fondo_foot != '') {
        $estilos_foot .= "background-color: ".$fondo_foot.";";
    }
    if ($logo_foot != '') {
        $estilos_foot .= "background-image: url('"._DIR_IMAGES_UPLOAD_WEB_."/".$logo_foot."'); background-repeat: no-repeat; background-size: contain;";
        if ($align_logo_foot != '') {
            $estilos_foot .= "background-position: ".$align_logo_foot." top;";
        }
    }
    $html_footer = '<footer class="footer" style="'.$estilos_foot.'"><div class="contefooter"><p class="text-muted"></p></div></footer>';

    // devolver el resultado
    $res['navbartop'] = $html_navbartop;
    $res['footer'] = $html_footer;
    
    return $res;
}

// Altera el color en $steps puntos
// Sube los valores de los tres colores RGB
function alter_brightness($colourstr, $steps) {
  $colourstr = str_replace('#','',$colourstr);
  $rhex = substr($colourstr,0,2);
  $ghex = substr($colourstr,2,2);
  $bhex = substr($colourstr,4,2);

  $r = hexdec($rhex);
  $g = hexdec($ghex);
  $b = hexdec($bhex);

  $r = max(0,min(255,$r + $steps));
  $g = max(0,min(255,$g + $steps));  
  $b = max(0,min(255,$b + $steps));

  return '#'.dechex($r).dechex($g).dechex($b);
}