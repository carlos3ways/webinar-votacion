<!-- Modal - Update User details -->
<div class="modal fade" id="add_new_pregunta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Añadir pregunta</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="evento">Eventos</label>
                    <div id="eventos">
                        <select name="eventos" class="selectpicker" data-width="100%">
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="pregunta">Pregunta</label>
                    <input type="text" id="pregunta" placeholder="Pregunta" class="form-control"/>
                </div>
                <!-- DESHABILITADA OPCIONES DE MÚSICA Y RESPUESTAS -->
                <!-- <div class="form-group">
                    <label for="musica">Música </label>
                    <select name="musica" id="musica" class="selectpicker musica" data-origen="_new" data-tipo="pregunta">
                    </select>
                    <audio id="audio_new_pregunta" src="" preload="auto" controls></audio>
                </div> -->

                <!-- <div class="form-group" id="widget_answers">
                <label>Respuestas</label>
                <span class="help-block">Haz un check en las respuestas que sean correctas, ordena las preguntas si es necesario. Los campos con texto vacío son ignorados.</span>


                <ul id="sortable" class="ui-sortable">
                  <li id="respuesta_1">
                    <div class="input-group">
                      <span class="input-group-addon"><input type="checkbox" /> <i class="glyphicon glyphicon-move"></i></span>
                      <input class="form-control" name="option[]" type="text" value="">
                    </div>                  
                  </li>
                  <li id="respuesta_2">
                    <div class="input-group">
                      <span class="input-group-addon"><input type="checkbox" /> <i class="glyphicon glyphicon-move"></i></span>
                      <input class="form-control" name="option[]" type="text" value="">
                    </div>                  
                  </li>
                  <li id="respuesta_3">
                    <div class="input-group">
                      <span class="input-group-addon"><input type="checkbox" /> <i class="glyphicon glyphicon-move"></i></span>
                      <input class="form-control" name="option[]" type="text" value="">
                    </div>                  
                  </li>                                
                </ul>
               <button type="button" class="btn btn-default" onclick="addRespuestaAfter()">Añadir respuesta</button>
               
              </div> -->
<!--
                <div class="form-group">
                    <label for="tiempo_responder">Tiempo para responder</label>
                    <input type="text" id="tiempo_responder" placeholder="Tiempo para responder" class="form-control"/>
                </div>
-->
                 <div class="form-group hide">
                    <label for="activo">Activo </label>
                    <input type="checkbox" id="activo" placeholder="Activo" />
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" onclick="savePreguntayRespuestas()" >Guardar</button>
                <input type="hidden" id="hidden_user_id">
            </div>
        </div>
    </div>
</div>
<!-- // Modal -->