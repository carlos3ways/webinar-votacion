<div class="modal fade" id="confirm_poner_todos_cero" tabindex="-1" role="dialog" aria-labelledby="myModalLabelo">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabelo"> Reiniciar la votación del evento</h4>
            </div>
            <div class="modal-body">
                <p>Se borrarán los <strong>participantes</strong> y <strong>todos los votos emitidos</strong> de todas las preguntas.</p> 
                <div class="form-group">
                    <input type="checkbox" id="participantes" placeholder="">
                    <label for="participantes"> No eliminar participantes </label>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-primary" id="votacion_todos_cero">Reiniciar todo</button>
                <button type="button" data-dismiss="modal" class="btn">Cancelar</button>
            </div>
        </div>
    </div>
</div>