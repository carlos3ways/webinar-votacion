<?php
    // recuperar la lista de archivos de música presentes
    // como opciones de un select
    include_once 'inc/getMusica.php';
?>

<div class="modal fade" id="musica_pregunta_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Música de la Pregunta</h4>
                <input type="hidden" id="id" />
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Pregunta</label>
                    <div id="texto-pregunta"></div>
                </div>

                <div class="form-group">
                    <label for="musica">Música: </label>
                    <select name="musica" id="musica" class="selectpicker musica" data-tipo="pregunta">
                    </select>
                    <audio id="audio_pregunta" src="" preload="auto" controls></audio>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" onclick="updateMusicaPregunta()" >Actualizar</button>
            </div>
        </div>
    </div>
</div>
<!-- // Modal -->