<!-- Modal - Add New Evento -->
<div class="modal fade" id="edit_usuario" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Editar usuario</h4>
            </div>
           
            <div class="modal-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-5 col-md-5">
                            <label>Nombre</label>
                            <input type="text" id="nombre" placeholder="Nombre" class="form-control"/>
                        </div>
                        <div class="col-sm-7 col-md-7">
                            <label>Apellidos</label>
                            <input type="text" id="apellidos" placeholder="Apellidos" class="form-control"/>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-9 col-md-9">
                            <label>Email </label>
                            <input type="text" id="email" placeholder="Email" class="form-control" />
                        </div>
                        <div class="col-sm-3 col-md-3">
                            <label>Password </label>
                            <input type="password" id="password" placeholder="Password" class="form-control" />
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3 col-md-3">
                            <label>Teléfono </label>
                            <input type="text" id="telefono" placeholder="Telefono" class="form-control" />
                        </div>
                        <div class="col-sm-9 col-md-9">
                            <label>Dirección </label>
                            <input type="text" id="direccion" placeholder="Dirección" class="form-control" />
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label>Provincia </label>
                    <input type="text" id="provincia" placeholder="Provincia" class="form-control" />
                </div>

                <div class="form-group">
                    <label for="activo">Activo</label>
                    <input type="checkbox" id="activo" placeholder="activo" class="form-control" />
                </div>

                <input type="hidden" id="id_evento" value="<?php if (isset($id_evento)){echo $id_evento;}?>" />
                <input type="hidden" id="id_usuario" value="" />

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="editarUsuario" >Guardar</button>
            </div>
            
        </div>
    </div>
</div>
<!-- // Modal -->