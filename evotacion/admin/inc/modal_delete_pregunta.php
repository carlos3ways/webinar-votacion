<div class="modal fade" id="confirm_delete_pregunta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
     <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"> Eliminar</h4>
            </div>


    <div class="modal-body">
   Vas a eliminar la pregunta, ¿estás seguro?
    </div>
    <div class="modal-footer">
      <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete_pregunta">Borrar</button>
      <button type="button" data-dismiss="modal" class="btn" id="delete_cancel">Cancelar</button>
    </div>
  </div>
  </div>
</div>