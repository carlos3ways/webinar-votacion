<div class="modal fade" id="imagen_pregunta_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Imagen de la Pregunta</h4>
                <input type="hidden" id="id" />
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Pregunta</label>
                    <div id="texto-pregunta"></div>
                </div>

                <div class="form-group">
                    <div class="form-group">
                        <label for="imagen_pregunta">Subir/cambiar imagen</label>
                        <span id="link-eliminar-imagen-pregunta" class="hide">(eliminar <span id="eliminar-imagen-pregunta" class="glyphicon glyphicon-trash text-danger"></span>)</span>
                        <input type="file" id="imagen_pregunta" class="form-control" onchange="leerImagenURL(this, 'img_pregunta', false);" />
                    </div>
                    <div class="form-group marbot5">
                        <label>Vista previa</label>
                        <img id="img_pregunta" src="" class="thumbnail" onerror="this.src='../assets/images/trans.png'">
                        <input id="elim_imagen_pregunta" name="elim_imagen_pregunta" type="hidden">
                        <input id="hay_imagen_pregunta" name="hay_imagen_pregunta" type="hidden" class="danger">
                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" onclick="updateImagen('pregunta')" >Actualizar</button>
            </div>
        </div>
    </div>
</div>
<!-- // Modal -->