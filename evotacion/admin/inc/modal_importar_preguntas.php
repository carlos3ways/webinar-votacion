<?php
    // recuperar la lista de archivos de música presentes
    // como opciones de un select
//    include_once 'inc/getMusica.php';
//    $lista_opciones_musica = getOpcionesHTMLMusica();
    
?>

<div class="modal fade" id="importar_preguntas_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Importar preguntas</h4>
                <input id="idevento" type="hidden">
            </div>
            <div class="modal-body">
                <div>
                    <p>El fichero a importar debe tener formato .CSV (campos entrecomillados y separados por punto y coma), con las siguientes características:</p>
                    <ul>
                        <li>La primera fila es obviada (nombres de los campos).</li>
                        <li>Columnas:
                            <ul>
                                <li>Texto de la pregunta</li>
                                <li>Tiempo de respuesta</li>
                                <li>Formato lista (0) o cuadrícula (1)</li>
                                <li>Número de respuestas (p.ej: 4)</li>
                                <li>Número de la respuesta correcta (p.ej; 2)</li>
                                <li>Para cada respuesta:
                                    <ul>
                                        <li>Texto de la respuesta</li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <p>Se puede encontrar un fichero de muestra <a href="preguntas_ejemplo.csv">aquí</a></p>
                    <label>Fichero</label>
                    <input id="fichero-preguntas" name="fichero-preguntas" type="file">
<!--                    <input id="fichero-preguntas" name="fichero-preguntas" type="file" class="file" data-show-preview="false" data-allowed-file-extensions='["xls", "wav"]'
                           data-language="es" data-el-error-container="#errores-subida-fichero" data-upload-url="ajax/importar_preguntas.php">-->
                </div>

                <div id="errores-subida-fichero"></div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button type="button" id="btn-seleccionar-fichero" class="btn btn-primary" onclick="importarPreguntas()" />Importar</button>
            </div>
        </div>
    </div>
</div>
<!-- // Modal -->