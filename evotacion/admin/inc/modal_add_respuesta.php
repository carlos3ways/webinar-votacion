<!-- Modal - Update User details -->
<div class="modal fade" id="add_new_respuesta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Añadir Respuestas</h4>
            </div>
            <div class="modal-body">

                <div class="form-group">
                </div>

                <div class="form-group" id="widget_answers">
                <label>Respuestas</label>
                <span class="help-block">Haz un check en las respuestas que sean correctas, ordena las respuestas si es necesario. Los campos con texto vacío son ignorados.</span>


                <ul id="sortable" class="ui-sortable">
                  <li id="respuesta_1">
                    <div class="input-group">
                      <span class="input-group-addon"><input type="checkbox" /> <i class="glyphicon glyphicon-move"></i></span>
                      <input class="form-control" name="option[]" type="text" value="">
                    </div>                  
                  </li>
                  <li id="respuesta_2">
                    <div class="input-group">
                      <span class="input-group-addon"><input type="checkbox" /> <i class="glyphicon glyphicon-move"></i></span>
                      <input class="form-control" name="option[]" type="text" value="">
                    </div>                  
                  </li>
                  <li id="respuesta_3">
                    <div class="input-group">
                      <span class="input-group-addon"><input type="checkbox" /> <i class="glyphicon glyphicon-move"></i></span>
                      <input class="form-control" name="option[]" type="text" value="">
                    </div>                  
                  </li>                                
                </ul>
               <button type="button" class="btn btn-default" onclick="addRespuestaAfterRes()">Añadir respuesta</button>
               
              </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" onclick="saveResRespuestas(<?=$id_pregunta?>)" >Guardar</button>
                <input type="hidden" id="hidden_user_id">
            </div>
        </div>
    </div>
</div>
<!-- // Modal -->