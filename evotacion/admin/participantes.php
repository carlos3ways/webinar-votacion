<?php 
# @author Carlos Colmenarez <carlos.colmenarez@s3w.es>

//validar e iniciar session
include_once('../models/Sesion.php');
Session::start();
if(!Session::check()) {
    header('Location: login.php');
    die();
}

if (!isset($_GET["evento_id"])) {
    echo "No se han recibido los datos correctamente <a href=\"index.php\">volver <\a>";
    die();
}
$evento_id = $_GET["evento_id"];
?>

<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8">
    <title>Listado participantes a asamblea</title>
    <meta http-equiv="Cache-control" content="no-cache">
    <meta name="robots" content="noindex,nofollow">
    <link rel="shortcut icon" type="image/x-icon" href="../assets/images/s3w-vota.ico" />
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.21/datatables.min.css"/>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.21/datatables.min.js"></script>
    <!-- Select2 -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <!-- Botones Select2 -->
    <link href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css" rel="stylesheet" />
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>
    <!-- CSS Original eVotacion -->
    <link href="../assets/css/custom.css" rel="stylesheet" />
    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" media="all">
</head>
<body>
<?php include('inc/navbar_s3w.php'); ?>

<!-- Content Section -->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div>
                <a href="index.php" class="btn btn-success">&lt; Volver</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <p></p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h1>Asistencia a Asamblea:</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <p id="demo"></p>
            <img id="loading-image" src="loading.gif">
            <!-- Tabla de socios -->
            <table id="example" class="display order-column" width="100%"></table>
        </div>
    </div>
</div>
<!-- /Content Section -->
<?php include('inc/pre_footer_s3w.php'); ?>
<script src="..\assets\js\logout.js"></script>

<!-- Scripts delegar votos -->
<script>
function renderTable() {
    $('#example').DataTable( {
        data: listado,
        dataSrc: "",
        columns: [
            {title: "Nº Socio", data: 'socio_id'},
            { title: "Nombre", data: 'socio_nombre'}
            // { title: "email", data: 'email' },
        ],
        "pageLength": 20,
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excel',
                text: '<i class="fa fa-download" aria-hidden="true"></i> Detalle Participantes',
                className: 'boton-descargar'
            }
            // 'copy',
            // 'csv',
            // 'pdf',
            // 'print'
        ],
        language: {
            "decimal": "",
            "emptyTable": "No hay información",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ Entradas",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar:",
            "zeroRecords": "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        }
    });
}
function ocultarLoading() {
    var loading = document.getElementById("loading-image");
    loading.classList.add("d-none");
}

$(document).ready(function() {
    //Consultar listado socios (api SEIMC)
        var url = "https://<?php echo $_SERVER['SERVER_NAME']; ?>/evotacion/api/participantes.php?evento_id="+<?= $evento_id ?>;
        jQuery.getJSON(url)
            .done(function(data) {
                listado = data;
                ocultarLoading();
                // console.log(listado);
                renderTable();
        });
});//Fin document ready

</script>
</body>
</html>