<?php 
# @author Carlos Colmenarez <carlos.colmenarez@s3w.es>

//validar e iniciar session
require_once('../models/Sesion.php');
require_once('../models/Evento.php');

Session::start();
if(!Session::check()) {
    header('Location: login.php');
    die();
}

if (!isset($_GET["evento_id"])) {
    echo "No se han recibido los datos correctamente <a href=\"index.php\">volver <\a>";
    die();
}
$evento_id = $_GET["evento_id"];
$fecha_evento = getFecha($evento_id);
?>

<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8">
    <title>Delegación de votos | Gestor de Asambleas SEIMC</title>
    <meta http-equiv="Cache-control" content="no-cache">
    <meta name="robots" content="noindex,nofollow">
    <link rel="shortcut icon" type="image/x-icon" href="../assets/images/s3w-vota.ico" />
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.21/datatables.min.css"/>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.21/datatables.min.js"></script>
    <!-- Select2 -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <!-- Botones Select2 -->
    <link href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css" rel="stylesheet" />
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>
    <!-- CSS Original eVotacion -->
    <link href="../assets/css/custom.css" rel="stylesheet" />
    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" media="all">
</head>
<body>
<?php include('inc/navbar_s3w.php'); ?>

<!-- Content Section -->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div>
                <a href="index.php" class="btn btn-success">&lt; Volver</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <p></p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h1>Delegación de votos</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-right">
            <input type="checkbox" id="solo_delegados"> <label for="solo_delegados">Mostrar solo votos delegados</label>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <p id="demo"></p>
            <img id="loading-image" src="loading.gif">
            <!-- Tabla de socios -->
            <table id="example" class="display order-column" width="100%"></table>

            <!-- Modal -->            
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Delegar voto</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form>
                      <div class="form-group">
                        <label for="recipient-name" class="col-form-label">De:</label>
                        <span id="delegar-de" class="col-form-label">Socio</span>
                        <input type="hidden" class="form-control" id="delegar_de_id" value="">
                        <input type="hidden" class="form-control" id="delegar_de_nombre" value="">
                        <input type="hidden" class="form-control" id="evento_id" value="<?= $evento_id ?>">
                      </div>
                      <div class="form-group">
                        <label for="message-text" class="col-form-label">A:</label>
                        <!-- select2 socio -->
                        <select class="js-delegar-a" name="delegar_a" style="width: 90%">
                            <option></option>
                        </select>
                        <!-- <textarea class="form-control" id="message-text"></textarea> -->
                        <input type="hidden" class="form-control" id="delegar-para-id" value="">
                      </div>
                    </form>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" id="btnCancel" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary" id="btnSave">Guardar</button>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
<!-- /Content Section -->
<?php include('inc/pre_footer_s3w.php'); ?>
<script src="..\assets\js\logout.js"></script>

<!-- Scripts delegar votos -->
<script>
// Funciones javascript
function paraSelect2(array, select) {
  string = '[';
  for(i=0; i<array.length; i++){
    string=string+"{";
    for(y=0; y<3; y++) {
      if(y===0) {
        string = string+'"id":"'+array[i][y]+'", ';
      }
      if(y==1) {
        if (select)
            string = string+'"text":"' + array[i][0] + ' - '+array[i][y]+'", ';
        else
            string = string+'"text":"'+array[i][y]+'", ';
      }
      if(y==2) {
        string = string+'"email":"'+array[i][y]+'"';
      }
    }
    string=string+"}";
    if(i<array.length-1){
      string=string+", ";
    }
  }
  string=string+"]";
  json = JSON.parse(string);
  return json;
}

function tojson() {
    listadojson = paraSelect2(listado, false);
    listadoselect = paraSelect2(listado, true);
}

function renderTable(listado = null) {
    if (listado) console.log(listado)
    $('#example').DataTable( {
        data: (listado || listadojson),
        dataSrc: "",
        columns: [
            {title: "Nº Socio", data: 'id'},
            { title: "Nombre", data: 'text'},
            // { title: "email", data: 'email' },
            { title: "Delegado a:"},
            { title: "Acción" }
        ],
        "columnDefs": [ 
            {
                "targets": -2,
                "data": 'id',
                "render": function ( data, type, row, meta ) {
                    if(yadelegados.find( socio => socio.socio_id == data)) {
                        var delegacion = yadelegados.find( socio => socio.socio_id == data);
                        return delegacion.socio_delegado_nombre
                    } else {
                        return ""
                    }
                }
            },
            {
                "targets": -1,
                "data": 'id',
                "render": function ( data, type, row, meta ) {
                    if(yadelegados.find( socio => socio.socio_id == data)) {
                        var delegacion = yadelegados.find( socio => socio.socio_id == data);
                        return "<button data-socio_id='"+delegacion.socio_id+"' class='btnDelete btn btn-danger'>Borrar delegación</button>"
                    } else {
                        return "<button class='btnDelegar btn btn-primary'>Delegar voto</button>"
                    }
                }
            } 
        ],
        "pageLength": 20,
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excelHtml5',
                text: '<i class="fa fa-download" aria-hidden="true"></i> Detalle Delegaciones',
                exportOptions: {
                    columns: [0,1,2]
                },
                className: 'boton-descargar'
            }
        ],
        language: {
            "decimal": "",
            "emptyTable": "No hay información",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ Entradas",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar:",
            "zeroRecords": "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        }
    });
}
function ocultarLoading() {
    var loading = document.getElementById("loading-image");
    loading.classList.add("d-none");
}

$(document).ready(function() {
    var table = null;

    $('#solo_delegados').on('change', function() {
        var loading = document.getElementById("loading-image");
        loading.classList.remove("d-none");
        $('#example').html('');
        table.destroy();

        if (this.checked) {
            renderTable(paraSelect2(yadelegados.map(delegado => [ delegado.socio_id, delegado.socio_nombre, null ]), false))
        } else {
            renderTable();
        }

        ocultarLoading();
        table = $('#example').DataTable();
    });

    //Consultar listado socios (api SEIMC)
    var url = "https://www.seimc.org/aasitio_webservice/ws-datos-seimc-v1.php?servicio=Asamblea_Listado"; 
    jQuery.getJSON(url)
        .done(function(data) {
            listado = data;
            var url2 = "https://<?php echo $_SERVER['SERVER_NAME']; ?>/evotacion/api/votosdelegados.php?evento_id="+<?= $evento_id ?>;
            jQuery.getJSON(url2)
                .done(function(data2) {
                yadelegados = data2;
                ocultarLoading();
                // console.log(listado);
                tojson();
                renderTable();
                // console.log(listadojson);
                $(".js-delegar-a").select2({
                    data: listadoselect,
                    placeholder: "Seleccione un socio",
                    allowClear: true
                });
                $(".js-delegar-a-select").select2({
                    data: listadoselect
                });
                
                table = $('#example').DataTable();
                //Accion botón "delegar voto"
                var modal = $('exampleModal');
                $(document).on( 'click', '#example tbody .btnDelegar', function () {
                    fecha = '<?php echo $fecha_evento ?>';
                    fecha_evento = new Date(fecha);
                    dia_evento = new Date(fecha_evento.getFullYear(), fecha_evento.getMonth(), fecha_evento.getDate(), 0, 0, 0);
                    now = new Date();
                    if (dia_evento<now) {
                        alert("No puedes delegar voto por que ya ha iniciado el evento o ya ha sucedido");
                    } else {
                        var data = table.row( $(this).parents('tr') ).data();
                        $('#delegar-de').text( data['text']);
                        $('#delegar_de_nombre').val(data['text']);
                        $('#delegar_de_id').val(data['id']);
                        $('#exampleModal').modal('toggle');
                    }
                });
                //Eliminar delegación
                $(document).on( 'click', '#example tbody .btnDelete', function () {
                    fecha = '<?php echo $fecha_evento ?>';
                    fecha_evento = new Date(fecha);
                    dia_evento = new Date(fecha_evento.getFullYear(), fecha_evento.getMonth(), fecha_evento.getDate(), 0, 0, 0);
                    now = new Date();
                    if (dia_evento<now) {
                        alert("No puedes borrar la delegación por que ya ha iniciado el evento o ya ha sucedido");
                    } else {
                        var r = confirm("¿Seguro que deseas borrar?");
                        if (r == true) {
                            var evento_id=$('#evento_id').val();
                            var data = table.row( $(this).parents('tr') ).data();
                            $.post("../api/borrardelegacion.php", {
                                evento_id:evento_id,
                                socio_id:data["id"],
                            }, function (data, status) {
                                alert(data);
                                console.log(data);
                                console.log(status);
                                window.location.href = 'delegacionvotos.php?evento_id=<?php echo $evento_id ?>';
                            });
                        }
                        event.preventDefault();
                }
                });
            });
        });

    $('#btnCancel').on('click', function (event) {
        $('.js-delegar-a').val(null).trigger('change');
    });
    //Guardar delegación de voto
    $('#btnSave').on('click', function (event) {
        var seleccion = $('.js-delegar-a').select2('data');
        if(seleccion[0]["id"]=="" || seleccion[0]["id"]==null) {
            alert("debes seleccionar un socio");
        } else {
            var evento_id=$('#evento_id').val();
            var delegar_de_id=$('#delegar_de_id').val();
            if(delegar_de_id=="" || delegar_de_id==null) {
                alert("error al guardar delegación");
            }
            var delegar_de_nombre=$('#delegar_de_nombre').val();
            var delegar_a_id=seleccion[0]["id"];
            var delegar_a_nombre=seleccion[0]["text"];

            //enviar datos
            $.post("../api/delegarvoto.php", {
                evento_id:evento_id,
                delegar_de_id:delegar_de_id,
                delegar_de_nombre:delegar_de_nombre,
                delegar_a_id:delegar_a_id,
                delegar_a_nombre:delegar_a_nombre
            }, function (data, status) {
                $('#exampleModal').modal('hide');
                alert(data);
                console.log(data);
                console.log(status);
                window.location.href = 'delegacionvotos.php?evento_id=<?php echo $evento_id ?>';
                // $('.js-delegar-a').val(null).trigger('change');
            });
        }
        event.preventDefault();
    });
});//Fin document ready

</script>
</body>
</html>