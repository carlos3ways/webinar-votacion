<?php 

// Recopilar info de los votos para mostrar los resultados en porcentaje %
// Solo para admin users

// arrancar la sesión, si hace falta
if (!isset($_SESSION)) {
    session_start();
}

// solo para administrador
if ( isset($_SESSION['acceso_admin']) && ($_SESSION['acceso_admin'] == 1) ) {

    // recuperar parámetros
    $id_pregunta = $_REQUEST['id_pregunta'];
    $id_evento = $_REQUEST['id_evento'];

    include("../admin/ajax/db_connection.php");

    // consulta con los totales de votos emitidos
    $query = "SELECT r.id, r.es_correcta, r.orden, count(v.id) as numvotos
                FROM respuestas r LEFT JOIN votos v ON v.id_pregunta = r.id_pregunta AND v.id_respuesta = r.orden
               WHERE r.id_pregunta = " . $id_pregunta
        . " GROUP BY r.id";
//    $query = "SELECT r.id, r.es_correcta, r.orden, count(v.id) as numvotos
//                FROM respuestas r LEFT JOIN votos v ON v.id_respuesta = r.id
//               WHERE r.id_pregunta = " . $id_pregunta
//        . " GROUP BY r.id";

    if (!$result = mysqli_query($db, $query)) {
        exit(mysqli_error($db));
    }

    // array final de resultados
    $a_respuestas = array();
    $total_votos = 0;

    // Recorrer los resultados. Hay que guardar el total de votos, guardamos los votos en un array
    while($row = mysqli_fetch_assoc($result))
    {
        $total_votos += $row['numvotos'];
        array_push($a_respuestas, array('id_respuesta' => $row['id'], 'es_correcta' => $row['es_correcta'], 'votos' => $row['numvotos'], 'porcentaje' => 0));
    }
    // Solo queda calcular los porcentajes (solo si hay algun voto!!! Si no, sería división x 0)
    if ($total_votos > 0) {
        foreach ($a_respuestas as $key => $respuesta) {
            $a_respuestas[$key]['porcentaje'] = round(($respuesta['votos']/$total_votos)*100);
        }
    }
    
    $json_final = json_encode($a_respuestas);

    echo $json_final;


}

?>