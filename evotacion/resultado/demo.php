<?php 
$id_pregunta = $_POST['id_pregunta'];
$id_evento = $_POST['id_evento'];

include("../admin/ajax/db_connection.php");

    //conocer las posibles respuestas
    $consulta = "SELECT  r.id as id_respuesta
    FROM preguntas_votar p 
    JOIN respuestas r
    ON p.id=r.id_pregunta
    WHERE p.id_evento = " . $id_evento . " AND p.id=" .$id_pregunta;

    if (!$resultado = mysqli_query($db, $consulta)) {
        exit(mysqli_error($db));
    }

    $a_respu=[];

    while($fil = mysqli_fetch_assoc($resultado))
    {
       array_push($a_respu, $fil['id_respuesta']);
    }



    for ($i=0; $i < 10; $i++) { 

      $valrnd = array_rand($a_respu) +1;
 //insertar votos aleatorios
        $consulta = "INSERT INTO votos (id_evento, id_pregunta, id_respuesta) 
        VALUES ('" . $id_evento . "', '" .$id_pregunta."', '".$valrnd."')";
  
        if (!$reinser = mysqli_query($db, $consulta)) {
            exit(mysqli_error($db));
        }

    }



    $query = "SELECT count(id) FROM votos WHERE id_pregunta = " . $id_pregunta;

    if (!$result = mysqli_query($db, $query)) {
        exit(mysqli_error($db));
    }

    $row = mysqli_fetch_array($result);
    $votos_total = $row[0];

    //exisen votos 
    if($votos_total >= 0) {

    $query = "SELECT p.id as pregunta_id, p.pregunta, r.es_correcta, r.respuesta, r.id as id_respuesta, r.orden 
    FROM preguntas_votar p 
    JOIN respuestas r
    ON p.id=r.id_pregunta
    where p.id_evento = " . $id_evento . " AND p.id=" .$id_pregunta;

    $a_respuestas = array();
    //$id_pregunta;
    $pregunta;
    $id_respuesta;
    $respuesta;
    $es_correcta;
    $orden;

    if (!$result = mysqli_query($db, $query)) {
        exit(mysqli_error($db));
    }


    if(mysqli_num_rows($result) >= 0)
    {
        $number = 1;
        while($row = mysqli_fetch_assoc($result))
        {
            $pregunta = $row['pregunta'];
            $id_respuesta = $row['id_respuesta'];
            $respuesta = $row['respuesta'];
            $es_correcta = $row['es_correcta'];
            $orden= $row['orden'];
            $porcentaje = 0;
           
            $queryv = "SELECT count(id) FROM votos WHERE id_pregunta = " . $id_pregunta . " AND id_respuesta = " . $orden;
            if (!$resu = mysqli_query($db, $queryv)) {
                exit(mysqli_error($db));
            }

            $rowv = mysqli_fetch_array($resu);
            $votos_respuesta = $rowv[0];
            
            if($votos_total){
                $porcentaje = (($votos_respuesta * 100) / $votos_total);
                $porcentaje = round($porcentaje); 
            }

            array_push($a_respuestas, array('id_respuesta' => $id_respuesta, 'respuesta' => $respuesta, 'es_correcta' => $es_correcta, 'porcentaje' => $porcentaje));
        }
    }
?>

<div class="container micontenedor">
    <h1><?=$pregunta;?></h1>
    <br>
    <?php 
    foreach ($a_respuestas as $res) {
        $textcolor="";

        if(($res['porcentaje'] == '0') && ($res['es_correcta'])){
            $textcolor="txt0ok";
        }
        elseif ($res['porcentaje'] == '0') {
            $textcolor="txt0gris";
        }
    ?>
     <h3><?=$res['respuesta']?></h3>
      <div class="progress">
        <div class="progress-bar <?php if($res['es_correcta']){echo "progress-bar-ok active";}else{echo "progress-bar-gris";} ?> " role="progressbar" aria-valuenow="<?=$res['porcentaje']?>" aria-valuemin="0" aria-valuemax="100" id="<?=$res['id_respuesta']?>" >
         <span class="<?=$textcolor?>"> <?=$res['porcentaje']?>% </span>
        </div>
      </div>
    <?php 
    }
    ?>
</div>

<?php 
 } // fin exisen votos 
 ?>