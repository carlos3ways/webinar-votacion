<?php 
if (!isset($_SESSION)) {
    ini_set("session.cookie_lifetime","21600");
    ini_set("session.gc_maxlifetime","21600");
    session_start();
}

// solo para admin
if ( isset($_SESSION['acceso_admin']) && ($_SESSION['acceso_admin'] == 1) ) {
    $id_pregunta = "";
    $id_evento = "";

    if (isset($_GET['id_pregunta'])){
        $id_pregunta = $_GET['id_pregunta'];
    }

    if (isset($_GET['id'])){
        $id_evento = $_GET['id'];
    }
    // si hay evento, recuperar datos de evento y estilos (si hay)
    if ( (!empty($id_evento)) ) {

        // define de upload images
        include_once "../admin/inc/config.php";
        // conexión BD
        include_once "../admin/ajax/db_connection.php";
        // funciones de estilo personalizado
        include_once "../admin/inc/estilo_evento.php";

        // iniciar variables
        $tiempo = "";
        $musica = "";
        $tema = "";
        $fondo_head = "";
        $fondo_foot = "";
        $fondo_respuesta_ok = "";
        $fondo_respuesta_ko = "";
        $logo_head = "";
        $logo_foot = "";
        $align_logo_head = "";
        $align_logo_foot = "";

        // recuperar todos los datos del evento para las preguntas y estilo
        $row = getDatosEstiloEvento($db, $id_evento);

        $tiempo = $row['tiempo'];
        $musica = $row['musica'];
        $tema = $row['theme'];
        $autoranking = $row['autoranking'];
        $autosolucion = $row['autosolucion'];
        $anonimo = $row['anonimo'];
        $fondo_respuesta_ok = $row['fondo_respuesta_ok'];
        $fondo_respuesta_ko = $row['fondo_respuesta_ko'];

    }


?>

<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/x-icon" href="../assets/images/s3w-vota.ico" />
        <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.css" />

        <?php
        // resultado = 1 -> modificar ruta de CSS
        $resultado = 1;
        include_once("../admin/ajax/check_theme.php");
        // generar la parte personalizada de las respuestas
        // si hay algo, metemos un style directamente
        if ($fondo_respuesta_ok != '' || $fondo_respuesta_ko != '') {
            echo "<style>";
            // controlar el OK
            if ($fondo_respuesta_ok != '') {
                
                echo ".progress-bar-ok.mostrar { background-color: ".$fondo_respuesta_ok." !important; }";
                echo ".progress-ok.mostrar { background-color: ".alter_brightness($fondo_respuesta_ok, 25)." !important; border: 1px solid ".$fondo_respuesta_ok." !important; }";
            }
            if ($fondo_respuesta_ko != '') {
                echo ".progress-bar-ok { background-color: ".$fondo_respuesta_ko." !important; }";
                echo ".progress-bar-gris, .progress-bar-ok { background-color:".$fondo_respuesta_ko." !important; }";
            }
            echo "</style>";
        }
        
        ?>
        
        <link rel="stylesheet" type="text/css" href="../assets/css/custom-resultado.css" />
        <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap-toggle.min.css" />
        <link rel="stylesheet" type="text/css" href="../assets/css/flipclock.css" />
        <title>eVotacion </title>
    </head>
<body>

<?php

    // si hay evento, recuperar datos de evento y estilos (si hay)
    if ( (!empty($id_evento)) ) {

        // si está personalizado, leemos los datos de estilo
        if ($tema == 1) {
            $htmls = getHTMLSNavs($row);
            echo $htmls['navbartop'];
        } else {
            include('../admin/inc/navbar.php');
        }

    }

    // si además hay pregunta, recuperar las respuestas
    if (!empty($id_pregunta)) {

        // votos emitidos
        $query = "SELECT count(id) FROM votos WHERE id_pregunta = " . $id_pregunta;

        if (!$result = mysqli_query($db, $query)) {
            exit(mysqli_error($db));
        }

        $row = mysqli_fetch_array($result);
        $votos_total = $row[0];

        // existen votos ¿?
        if ($votos_total >= 0) {

            $query = "SELECT  p.id as pregunta_id
                            , p.pregunta
                            , p.orden as orden_pregunta
                            , p.imagen as imagen_p
                            , p.formato_a_4
                            , r.es_correcta
                            , r.respuesta
                            , r.id as id_respuesta
                            , r.orden
                            , r.imagen as imagen_r
                            , COALESCE(IF(p.musica='',NULL,p.musica), '".$musica."') as musica
                      FROM  preguntas_votar p JOIN respuestas r ON p.id=r.id_pregunta
                      WHERE p.id_evento = " . $id_evento . "
                        AND p.id=" .$id_pregunta;

            $a_respuestas = array();
            $pregunta;
            $id_respuesta;
            $respuesta;
            $es_correcta;
            $orden;

            if (!$result = mysqli_query($db, $query)) {
                exit(mysqli_error($db));
            }

            // recuperar pregunta y respuestas
            if(mysqli_num_rows($result) > 0)
            {
                $number = 1;
                while($row = mysqli_fetch_assoc($result))
                {
                    $pregunta = $row['pregunta'];
                    $imagen_p = $row['imagen_p'];
                    $formato_a_4 = $row['formato_a_4'];
                    $id_respuesta = $row['id_respuesta'];
                    $respuesta = $row['respuesta'];
                    $imagen_r = $row['imagen_r'];
                    $es_correcta = $row['es_correcta'];
                    $orden= $row['orden'];
                    $musica = $row['musica'];

                    // para cada respuesta, recuperar los votos
                    $queryv = "SELECT count(id) FROM votos WHERE id_pregunta = " . $id_pregunta . " AND id_respuesta = " . $orden;
                    if (!$resu = mysqli_query($db, $queryv)) {
                        exit(mysqli_error($db));
                    }

                    $rowv = mysqli_fetch_array($resu);

                  /*
                    $votos_respuesta = $rowv[0];
                    $porcentaje = (($votos_respuesta * 100) / $votos_total);
                    $porcentaje = round($porcentaje); 

        , 'es_correcta' => $es_correcta, 'porcentaje' => $porcentaje
                    */
                    // guardar el orden de la pregunta (para la clasificación posterior)
                    $orden_pregunta = $row['orden_pregunta'];

                    array_push($a_respuestas, array('id_respuesta' => $id_respuesta, 'respuesta' => $respuesta, 'imagen' => $imagen_r));
                }

                // para el ranking, hay que recuperar la lista de preguntas hasta la actual
                $query = "SELECT * "
                        . " FROM preguntas_votar "
                        . "WHERE id_evento = " . $id_evento ." "
                        . "ORDER BY orden ASC";
                if (!$preguntas = mysqli_query($db, $query)) {
                    exit(mysqli_error($db));
                }
                // también hay que saber el total de preguntas
                $num_total_preguntas = mysqli_num_rows($preguntas);
                
                // clase para el formato a 4 de las respuestas
                $clase_formato4 = '';
                if ($formato_a_4 == 1) {
                    $clase_formato4 = ' class="respuesta-a-4"';
                }

?>

<div class="wrapp-contador">

    <?php
        // mostrar el contador si está controlado por el tiempo
                if ( $tiempo != 0) {
    ?>
    <div class="inwrapp-contador">
        <div class="clock mar0" ></div>
    </div>
    <?php 
                }
    ?>

    <?php
        // montar el audio, si hay música para la pregunta
                if ( $musica != '') {
                    // cargar los datos de configuración DIR_MUSICA y DIR_IMAGES
                    include_once '../admin/inc/config.php';
    ?>
    <div class="hide">
        <audio id="audio_pregunta" src="<?=_DIR_MUSICA_WEB_.'/'.$musica?>" preload="auto" controls></audio>
    </div>
    <?php 
                }
    ?>


    <div class="container micontenedor padtop0">
        <div id='container-pregunta'>
            <?php
                // PREGUNTA Y RESPUESTAS
                // Hay que controlar el tipo de organización visual
                // si la pregunta tiene imagen, se muestra a la izquierda, y las respuestas a la derecha
                // si no tiene, las respuestas ocupan todo el espacio
                // LAS RESPUESTAS pueden ser en lista o formato_a_4

                // PREGUNTA
                // Texto de la pregunta, si lo hay
                if ($pregunta != '') {
                    echo '<h1>'.$pregunta.'</h1>';
                }
                // Imagen de la pregunta, si la hay
                // ( guardar si hay o no imagen, para establecer el WIDTH de la parte de respuestas )
                $clase_respuestas = "";
                // $imagen_p contiene el último valor (que ya vale, porque la imagen es la misma para todas las respuestas)
                if ($imagen_p != '') {
                    echo '<div class="div-img-pregunta"><img src="'._DIR_IMAGES_UPLOAD_WEB_.'/'.$imagen_p.'"></div>';
                    $clase_respuestas = 'class="a-4"';
                }
            ?>
        </div>
        <div id="container-respuestas"<?=$clase_respuestas?>>
       
            <ol>
        <?php
                // MOSTRAR LAS RESPUESTAS
                // vienen dentro de una lista ordenada numerada
                foreach ($a_respuestas as $res) {
        ?>
                <li<?=$clase_formato4?> id="li-resp-<?=$res['id_respuesta']?>">
                <?php if ($res['respuesta'] != '') { echo $res['respuesta']; } ?>
                <?php if ($res['imagen'] != '') { echo '<img src="'._DIR_IMAGES_UPLOAD_WEB_.'/'.$res['imagen'].'">'; } ?>
                </li>
       
        <?php 
                }
        ?>

            </ol>
        </div>
    </div>

    <div class="container miranquing martop20" style="display: none">
        <div id="resultado-final" style="display: none">
            <h1>RESULTADO FINAL</h1>
        </div>
        <table id="rankingActual" class="table">
            <thead>
                <tr>
                    <th>Rank.</th>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th><span class='glyphicon glyphicon-ok' aria-hidden='true' style='color: green;'></span></th>
                    <th><span class='glyphicon glyphicon-remove' style='color: red;' aria-hidden='true'></span></th>
                    <th><span class='glyphicon glyphicon-time' aria-hidden='true'></span></th>

                    <?php
                        $cont=1;
                        $classfirst=' primera';
                        // en $preguntas estan todas. Iteramos solo hasta $orden_pregunta
                        while ($row = mysqli_fetch_assoc($preguntas)) {
                            // controlar si hemos pasado la actual
                            if ($row['orden'] > $orden_pregunta) {
                                break;
                            }
                            // controlar primera columna de pregunta
                            $classfirst = '';
                    ?>
                    <th>P.<?=$cont?></th>
                    <?php
                            $cont++;
                        }
                    ?>

                    <th id="detalle-preguntas"><span class='glyphicon glyphicon-eye-open small' aria-hidden='true' ></span></th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>

    <?php
    
            } // end si hay respuestas

    ?>

    <div class="animate-puedevotar" style="margin:0 auto;"><div> <i class="glyphicon glyphicon-refresh fa-spin-custom"></i>  Puede votar</div></div>
</div>

    <?php include('../admin/inc/menu_wrapper.php') ;?>
    

    <?php 
        } // fin existen votos 
    ?>

<script type="text/javascript" src="../assets/js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="../assets/js/jquery-ui.js"></script>
<script type="text/javascript" src="../assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../assets/js/bootstrap-toggle.min.js"></script>
<script type="text/javascript" src="../assets/js/flipclock.min.js"></script>
<script type="text/javascript" src="../assets/js/script.js"></script>
<script type="text/javascript" src="../assets/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../assets/js/dataTables.bootstrap.min.js"></script>

<script type="text/javascript">
var id_evento = <?=$id_evento?>;
var id_pregunta = <?=$id_pregunta?>;
var orden_pregunta = <?=$orden_pregunta?>;
var num_total_preguntas = <?=$num_total_preguntas?>;
var tiempo = <?php if ($tiempo<>'') { echo $tiempo; } else { echo "0"; } ?>;
var anonimo = <?=$anonimo?>;
// mostrar u ocultar los resultados de la pregunta
$('#toggle-resultado').change(function() {

    // clase o color de las barras (según el estilo del evento)
    var respOk = '<?=$fondo_respuesta_ok?>', respKo = '<?=$fondo_respuesta_ko?>';
    // si está activo
    if ($(this).prop('checked') == true) {
        // recuperar los % de respuestas en un JSON
        $.post("respuestas.php",{
            id_pregunta: id_pregunta,
            id_evento: id_evento
        }, function (data, status) {
            // cargar y mostrar los datos de resultados
            var divprogreso = '';
            var claseprog = '';
            var clasebar = '';
            var clasespan = '';
            for(var k in data) {
                // data contiene el JSON con las respuestas y sus %
                // montar el DIV correspondiente según los valores 
                if (data[k].es_correcta == 1) {
                    claseprog = ' progress-ok';
                    clasebar = 'progress-bar-ok';
                    clasespan = 'txt0ok';
                } else {
                    claseprog = '';
                    clasebar = 'progress-bar-gris';
                    clasespan = 'txt0gris';
                }
                
                divprogreso = '<div class="progress'+claseprog+'" style="display:none"><div class="progress-bar '+clasebar+'" role="progressbar" aria-valuenow="'+data[k].porcentaje+'" aria-valuemin="0" aria-valuemax="100" id="'+data[k].id_respuesta+'" >';
                divprogreso += '<span class="'+clasespan+'"> '+data[k].porcentaje+'% </span></div></div>';
                // quitar el progress que pudiera haber
                $('#li-resp-'+data[k].id_respuesta+" .progress").remove();
                $('#li-resp-'+data[k].id_respuesta).append(divprogreso);
            }
            
//            $('.micontenedor').html(data);
//            // mostrar las barras de % de votos de cada pregunta
            $('.progress').show(2000);
//            // la longitud de la barra se determina por el porcentaje de votos
            setTimeout( function() {
                $('.progress .progress-bar').css("width",function() {
                    return $(this).attr("aria-valuenow") + "%";
                });
            }, 1000);
            // mostrar la solucion si es "autosolucion"
<?php
        // si hay autosolucion, mostrar cuál es la respuesta correcta
        if ($autosolucion == 1) {
?>
            setTimeout( function() {
                $('.solucion').click();
            }, 1000);
<?php
        } // si no, se hará a mano
?>
            // después de mostrar los resultados, mostrar el ránking si hay autoranking
<?php
        // si hay autoranking se muestra directamente
        if ($autoranking == 1 && $anonimo == 0) {
?>
            setTimeout( function() {
                ranking(id_evento,orden_pregunta,num_total_preguntas);
            }, 6000);
<?php
        } // si no, no se muestra el ranking
?>
        }, "json");
    } else {
        // si se desactiva, ocultar los resultados
        $('.progress').hide(2000);
    }
});


$('#demo_mode').on('click', function() {
    $.post( "demo.php",
            {
                id_pregunta: id_pregunta,
                id_evento: id_evento
            }, function (data, status) {
                $('.micontenedor').html(data);
                    $('.progress').show(2000);
                    setTimeout(function(){
                        $('.progress .progress-bar').css("width",function() {
                            return $(this).attr("aria-valuenow") + "%";
                        });
                    }, 1000);
            }
    );
});

// votación manual (si no hay tiempo)
$('#toggle-vote-onoff').change(function() {

    // iniciar o finalizar la votación
    if ($(this).prop('checked') == true) {
        processVotacion('inicio');
        $('.animate-puedevotar').show();
    } else {
        processVotacion('fin');
        $('.animate-puedevotar').hide();
    }
});


$('.repeat').on('click', function(){

    if ($('#toggle-resultado:checkbox:checked').length){
        $.post("updateRespuestas.php",{
            id_pregunta: id_pregunta,
            id_evento: id_evento
        }, function (datajson, status) {

            $.each(datajson, function (key, data) {
                console.log(data.porcentaje);
                if(data.porcentaje>0){
                    $('#' + data.id_respuesta + ' span').removeClass();
                }

                $('#' + data.id_respuesta).css('width', data.porcentaje+'%');
                $('#' + data.id_respuesta + ' span').text(data.porcentaje+'%');
                /*$.each(data, function (index, data) {
                    console.log('', data)
                });*/
            });


           
        });
    }
});

function processVotacion(estado){
    var activo=0;

    if(estado !='fin') {
        activo =1;
        gestionMusica('audio_pregunta', 'play');
    } else {
        gestionMusica('audio_pregunta', 'stop');
    }

    $.post("../admin/ajax/updateVotacion.php",{
            id_pregunta: id_pregunta,
            id_evento: id_evento,
            activo:activo
        }, function (data, status) {
            
            console.log(data);
            var tecla_res = 'W';
            // mostrar los resultados cuando acaba el tiempo para votar
            if (activo == 0) {
                setTimeout(function(){
                    $('#toggle-resultado').bootstrapToggle('toggle');
                }, 3000);
            }
    });
}

$( document ).ready(function() {
    $('#lado-wrapper').show();
//    $('#menu-wrapper').hide();
//    $('.progress').hide();

    cuentaAtras(tiempo);
});

// muestra/oculta el ranking y las preguntas
function toggleRanking() {
    // si se ve el contenerdor, mostrar ranking
    if ( $('.micontenedor').is(":visible") ) {
        // mostrar ranking / ocultar stats
        $('.inwrapp-contador').hide(1000);
        $('.micontenedor').hide(1000);
        $('.miranquing').show(1000);
    } else {
        // ocultar ranking / mostrar stats
        $('.miranquing').hide(1000);
        $('.inwrapp-contador').show(1000);
        $('.micontenedor').show(1000);
    }
}
// recupera y muestra el ranking de participantes hasta una pregunta concreta
// si es la última pregunta -> muestra el título de resultado final
function ranking(evento, pregunta, total_preguntas) {

    // solo si el evento no es anónimo
    if (anonimo == 0) {
        if (pregunta == total_preguntas) {
            // mostrar el título de RESULTADO FINAL
            $('#resultado-final').show(1000);
        }
        // comprobar si la DateTable ha sido creada o no
        if ( ! $.fn.DataTable.isDataTable( '#rankingActual' ) ) {
            // recuperar los datos
            $('#rankingActual').DataTable({
                "ajax": {
                    'type': 'POST',
                    'url': '../admin/ajax/getRanking.php',
                    'data': { id_evento: evento, orden_pregunta: pregunta},
                    error: function (xhr, error, thrown) {
                        alert( 'No hay datos en Estadísticas: error:' + error );
                    },
                    complete: function (data) {
                        toggleRanking();
                    }
                },
                "columnDefs": [
                    // ID no se muestra
                    {"visible":false, "targets": [1] },
                    // Nombre alineado a la izquierda
                    {"className": "left", "targets": [ 2 ] },
                    // Lo demás, centrado, incluyendo el link de mostrar (orden_pregunta+6)
                    {"className": "dt-center", "targets": [ 0, 3, 4, 5 , orden_pregunta+6 ] },
                    // Primera pregunta tiene borde izquierdo
                    {"className": "dt-center prg primera", "targets": [ 6 ] },
                    {"className": "dt-center prg", "targets": "_all" }
                ],
                "bSort": false,
                searching: false,
                paging: false,
                info: false

            });
        } else {
            // ya existe la Datatable, solo mostrar/ocultar
            toggleRanking();
        }

    }

}
    
</script>
<script type="text/javascript" src="../assets/js/custom-resultado.js"></script>

<?php
} elseif(!empty($id_evento)) {
    
    //ver listado preguntas
    include("../admin/ajax/db_connection.php");
    $query = "SELECT DISTINCT p.id, p.pregunta
              FROM preguntas_votar p JOIN respuestas r ON p.id = r.id_pregunta
              WHERE p.id_evento = " .$id_evento. "
              ORDER BY p.orden";

    if (!$result = mysqli_query($db, $query)) {
        exit(mysqli_error($db));
    }

    // botón para iniciar con la lista de participantes o con la primera pregunta
    echo '<div class="container">';
    echo '<div class="row">';
    echo '<div class="col-md-12">';
    echo '<div class="pull-right">';
    echo '<button class="btn btn-primary mar10lyr" onclick="arrancarVotacion(0,'.$id_evento.')">Lista de participantes</button>';
    echo '<button class="btn btn-success" onclick="arrancarVotacion(1,'.$id_evento.')">Primera Pregunta</button>';
    echo '</div>';
    echo '</div>';
    echo '</div>';

    // Lista de preguntas: se muestran en una lista ordenada
    echo '<div class="micontenedor">';
    echo '<h1>Preguntas y resultados</h1>';
    echo '<ol class="preguntas-resultado">';
    // mostrar cada pregunta
    // debemos guardar el ID de la primera pregunta (para arrancar la votación). Crearé un input hidden al final de la lista
    $primera_pregunta = 0;
    while ($row = mysqli_fetch_assoc($result)) {
        // solo se ejecutará la primera iteración. A partir de la segunda ya tendrá un valor
        if ($primera_pregunta == 0) {
            $primera_pregunta = $row['id'];
        }
        echo "<li><h3><a href='index.php?id=".$id_evento."&id_pregunta=".$row['id']."'><i class='glyphicon glyphicon-tasks'></i></a> <div class=\"pregunta-resultado\" >".$row["pregunta"]."</div></h3></li>";
    }
    echo "</ol>";
    // creamos el input hidden
    echo '<input type="hidden" id="primera_pregunta" value="'.$primera_pregunta.'">';
    echo "</div>";
    
    // cargar los javascripts
    echo '<script type="text/javascript" src="../assets/js/jquery-1.11.3.min.js"></script>';
    echo '<script type="text/javascript" src="../assets/js/jquery-ui.js"></script>';
    echo '<script type="text/javascript" src="../assets/js/bootstrap.min.js"></script>';
    echo '<script type="text/javascript" src="../assets/js/script.js"></script>';

}
?>


<?php
    // final del container
    echo "</div>";

    // el pie: si era personalizado
    if (isset($htmls['footer']) && $htmls['footer'] != '') {
        echo $htmls['footer'];
    } else {
        // el por defecto
        include('../admin/inc/pre_footer.php') ;
    }

} else {
    // no existe session[acceso_admin]
    header("Location: ../admin/login.php");
}


?>
    </body>
</html>
