<?php 
    $id_evento = "";

    if (isset($_GET['id'])){
        $id_evento = $_GET['id'];
    }
    // si hay evento, recuperar datos de evento y estilos (si hay)
    if ( (!empty($id_evento)) ) {

        // define de upload images
        include_once "../admin/inc/config.php";
        // conexión BD
        include_once "../admin/ajax/db_connection.php";
        // funciones de estilo personalizado
        include_once "../admin/inc/estilo_evento.php";

        // iniciar variables
        $tiempo = "";
        $musica = "";
        $tema = "";
        $fondo_head = "";
        $fondo_foot = "";
        $fondo_respuesta_ok = "";
        $fondo_respuesta_ko = "";
        $logo_head = "";
        $logo_foot = "";
        $align_logo_head = "";
        $align_logo_foot = "";

        // recuperar todos los datos del evento para las preguntas y estilo
        $row = getDatosEstiloEvento($db, $id_evento);

        $tiempo = $row['tiempo'];
        $musica = $row['musica'];
        $tema = $row['theme'];
        $autoranking = $row['autoranking'];
        $autosolucion = $row['autosolucion'];
        $anonimo = $row['anonimo'];
        $fondo_respuesta_ok = $row['fondo_respuesta_ok'];
        $fondo_respuesta_ko = $row['fondo_respuesta_ko'];

    }


?>

<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/x-icon" href="../assets/images/s3w-vota.ico" />
        <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.css" />
        <style>
            #pie {
                position: fixed;
                bottom: 0px;
                height: 78px;
                width: 100%;
                padding:0; margin:0;
            }
            
            img.logo {
                width: 500px !important;
            }
        </style>

        <?php
        // resultado = 1 -> modificar ruta de CSS
        include_once("../admin/ajax/check_theme.php");
      
        ?>
        
        <link rel="stylesheet" type="text/css" href="../assets/css/custom-resultado.css" />
        <title>eVotacion </title>
    </head>
<body>

<?php

    // si hay evento, recuperar datos de evento y estilos (si hay)
    if ( (!empty($id_evento)) ) {

        // si está personalizado, leemos los datos de estilo
        if ($tema == 1) {
            $htmls = getHTMLSNavs($row);
            echo $htmls['navbartop'];
        } else {
            include('../admin/inc/navbar.php');
        }

    }


?>

    <div class="container text-center" style="margin:200px auto;">


        <div id='container-pregunta'>
            <h1>Gracias!</h1>
            <img class="logo" src="../assets/images/logo_amir.png" />
        </div>

    </div>

    <div id="pie">
    <?php 
        // el pie: si era personalizado
        if (isset($htmls['footer']) && $htmls['footer'] != '') {
            echo $htmls['footer'];
        } else {
            // el por defecto
            include('../admin/inc/pre_footer.php') ;
        }

    ?>
    </div>
</body>
</html>