<?php 
if (!isset($_SESSION)) {
    ini_set("session.cookie_lifetime","21600");
    ini_set("session.gc_maxlifetime","21600");
    session_start();
}

// solo para admin
if ( isset($_SESSION['acceso_admin']) && ($_SESSION['acceso_admin'] == 1) ) {

    $id_evento = "";

    if (isset($_GET['id'])){
        $id_evento = $_GET['id'];
        $id_pregunta =  $_GET['pr'];
    }
    // si hay evento, recuperar datos de evento y estilos (si hay)
    if ( (!empty($id_evento)) ) {

        // define de upload images
        include_once "../admin/inc/config.php";
        // conexión BD
        include_once "../admin/ajax/db_connection.php";
        // funciones de estilo personalizado
        include_once "../admin/inc/estilo_evento.php";

        // iniciar variables
        $musica = "";
        $tema = "";

        // recuperar todos los datos del evento para las preguntas y estilo
        $row = getDatosEstiloEvento($db, $id_evento);

        $musica = $row['musica'];
        $tema = $row['theme'];


    }


?>

<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/x-icon" href="../assets/images/s3w-vota.ico" />
        <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.css" />

        <?php
        // resultado = 1 -> modificar ruta de CSS
        $resultado = 1;
        include_once("../admin/ajax/check_theme.php");
        
        ?>
        
        <link rel="stylesheet" type="text/css" href="../assets/css/custom-resultado.css" />
        <link rel="stylesheet" type="text/css" href="../assets/css/bootstrap-toggle.min.css" />
        <link rel="stylesheet" type="text/css" href="../assets/css/flipclock.css" />
        <title>eVotacion </title>
    </head>
<body>

<?php

    // si hay evento, recuperar datos de evento y estilos (si hay)
    if ( (!empty($id_evento)) ) {

        // si está personalizado, leemos los datos de estilo
        if ($tema == 1) {
            $htmls = getHTMLSNavs($row);
            echo $htmls['navbartop'];
        } else {
            include('../admin/inc/navbar.php');
        }

    }

    // montar el audio, si hay música para la pregunta
    if ( $musica != '') {
?>
    <div class="hide">
        <audio id="audio_participantes" src="<?=_DIR_MUSICA_WEB_.'/'.$musica?>" preload="auto" autoplay loop></audio>
    </div>
<?php 
    }
?>

    <div class="container micontenedor padtop0">
        <div class="row">
            <div class="col-md-12">
                <div class="pull-right">
                    <button class="btn btn-success" onclick="arrancarVotacion(1,<?=$id_evento?>)">Primera pregunta</button>
                    <?='<input type="hidden" id="primera_pregunta" value="'.$id_pregunta.'">'?>

                </div>
            </div>
        </div>

        <h1>Participantes</h1>
        <div id="lista-participantes">
       
        </div>
    </div>

    <script type="text/javascript" src="../assets/js/jquery-1.11.3.min.js"></script>
    <script type="text/javascript" src="../assets/js/jquery-ui.js"></script>
    <script type="text/javascript" src="../assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../assets/js/script.js"></script>
    <script>

        var timerParticipantes;

        $(document).ready(function() {

            var evento = <?=$id_evento?>;

            function mostrarParticipantes() {
                // leer participantes (recuperarlos de la BD)
                // pintarParticipantes refresca la lista en pantalla o muestra el mensaje de error
                // ruta (mover a admin)
                var listaParticipantes = leerParticipantes(evento, '../admin/', pintarParticipantes);
            }

            timerParticipantes = setInterval(mostrarParticipantes, 3000);

            // lanzar la primera consulta a la BD
            mostrarParticipantes();
        });

    </script>

<?php

    // el pie: si era personalizado
    if (isset($htmls['footer']) && $htmls['footer'] != '') {
        echo $htmls['footer'];
    } else {
        // el por defecto
        include('../admin/inc/pre_footer.php') ;
    }

} else {
    // no existe session[acceso_admin]
    header("Location: ../admin/login.php");
}


?>
</body>
</html>
