$(document).ready(function () {
    readEventos(); 
    $('#add_new_evento #fecha_inicio').datetimepicker({
        locale: 'es',
        format: 'YYYY/MM/DD HH:mm:ss',
        minDate: moment(),
    });

    $('#add_new_evento #fecha_fin').datetimepicker({
        locale: 'es',
        format: 'YYYY/MM/DD HH:mm:ss',
        minDate: moment(),
    });

    $('#update_evento_modal #fecha_inicio').datetimepicker({
        locale: 'es',
        format: 'YYYY/MM/DD HH:mm:ss',  
    });

    $('#update_evento_modal #fecha_fin').datetimepicker({
        locale: 'es',
        format: 'YYYY/MM/DD HH:mm:ss',
    });
    
    // programar el Onchange de los SELECT de música para cargar el audio
    // Como se utiliza selectpicker, se debe controlar si el evento es del select o del "picker"
    // hay audios en el evento, pero también en nueva pregunta!!
    // SUBIR AUDIO: como hay una opción nueva (subir fichero) hay que controlar si se selecciona
    $('.musica').on('change', function() {
        var valor = $(this).val();
        if (valor === "-") {
            // volver a "ningún audio seleccionado"
            $(this).selectpicker('val','');
            // llamar a la modal de seleccionar fichero
            // guardando el nombre del picker para refrescar y seleccionar la opción si corresponde
            $('#subir_musica_modal #pickerTipo').val($(this).data('tipo'));
            $('#subir_musica_modal #pickerOrigen').val($(this).data('origen'));
            // y limpiando botón, ficheroSubido, desactivar botón, mensajes de error o lo que sea de ejecuciones anteriores
            $('#subir_musica_modal #ficheroSubido').val('');
            $('#subir_musica_modal #btn-seleccionar-fichero').html('Seleccionar');
            $('#subir_musica_modal #btn-seleccionar-fichero').prop('disabled', true);
            $('#fichero-musica').fileinput('clear');
            // mostrar la modal
            $('#subir_musica_modal').modal('show');
        } else {
            // proceso normal de cargarAudio
            var nuevo = $(this).data("origen");
            if (nuevo === undefined) nuevo = '';
            // para ver si es de pregunta o evento
            var tipo = $(this).data("tipo");
            if (tipo === undefined) tipo = 'evento';
            // cargar el audio si el evento es del select
            var objetoid = $(this).attr('id');
            if ( objetoid == "musica" ) {
                cargarAudio('audio'+nuevo+'_'+tipo, $(this).val());
            }
        }
    });

    $('#update_evento_modal').on('hide.bs.modal', function () {
        gestionMusica('audio_evento', 'stop');
        // ocultar colorpicker (por si se cierra con ESC)
        $('.colorpicker-visible').removeClass('colorpicker-visible');
    });

    // NUEVO EVENTO -> limpiar la música al llamarlo
    $('#add_new_evento').on('show.bs.modal', function () {
        // vaciar player y recargar el select de la música
        cargarAudio('audio_new_evento', '');
        cargarListaMusica('add_new_evento');
        $("#add_new_evento #musica").selectpicker('val','');
        // ocultar y vaciar estilo personalizado
        $('#add_new_evento #theme').prop('checked', false);
        $('.container-personalizar-estilo').hide(0);
        $("#add_new_evento #fondo_head").colorpicker('setValue', '#FFFFFF');
        $("#add_new_evento #fondo_foot").colorpicker('setValue', '#FFFFFF');
        $("#add_new_evento #respuesta_ok").colorpicker('setValue', dfltRepuestaOk);
        $("#add_new_evento #respuesta_ko").colorpicker('setValue', dfltRepuestaKo);
        if ( ! $('#link-vaciar-new-logo-head').hasClass('hide')) {
            $('#link-vaciar-new-logo-head').addClass('hide');
        }
        $('#logo_new_head').val('');
        eliminarVistaPrevia('img_new_logo_head');
        $('#add_new_evento #align_logo_head').selectpicker('val','left');
        if ( ! $('#link-vaciar-new-logo-foot').hasClass('hide')) {
            $('#link-vaciar-new-logo-foot').addClass('hide');
        }
        $('#logo_new_foot').val('');
        eliminarVistaPrevia('img_new_logo_foot');
        $('#add_new_evento #align_logo_foot').selectpicker('val','left');
    });
    // parar la musica al cerrar la modal
    $('#add_new_evento').on('hide.bs.modal', function () {
        gestionMusica('audio_new_evento', 'stop');
        // deseleccionar los logos de estilo (por si hubiera)
        $("#add_new_evento #logo_head").val('');
        $("#add_new_evento #logo_foot").val('');
        // ocultar colorpicker (por si se cierra con ESC)
        $('.colorpicker-visible').removeClass('colorpicker-visible');
    });
    // Lo mismo con NUEVA PREGUNTA
    $('#add_new_pregunta').on('show.bs.modal', function () {
        // vaciar player y select de la música
        cargarAudio('audio_new_pregunta', '');
        $("#add_new_pregunta #musica").val('');
    });

    $('#add_new_pregunta').on('hide.bs.modal', function () {
        gestionMusica('audio_new_pregunta', 'stop');
    });

    $('#add_new_pregunta').on('hide.bs.modal', function () {
        gestionMusica('audio_new_pregunta', 'stop');
    });

    // Mostrar/ocultar estilo personalizado (add evento)
    $('#add_new_evento #theme').on('click', function () {
        // Tema personalizado activa/desactiva estilo personalizado
        if ( ! $(this).is(':checked') ) {
            if ( $('.container-personalizar-estilo').is(":visible") ) {
                $('.container-personalizar-estilo').hide(500);
            }
        } else {
            if ( ! $('.container-personalizar-estilo').is(":visible") ) {
                $('.container-personalizar-estilo').show(500);
            }
        }
    });
    // Mostrar/ocultar estilo personalizado (update evento)
    $('#update_evento_modal #theme').on('click', function () {
        // Tema personalizado activa/desactiva estilo personalizado
        if ( ! $(this).is(':checked') ) {
            if ( $('.container-personalizar-estilo').is(":visible") ) {
                $('.container-personalizar-estilo').hide(500);
            }
        } else {
            if ( ! $('.container-personalizar-estilo').is(":visible") ) {
                $('.container-personalizar-estilo').show(500);
            }
        }
    });
    
    // eliminar/limpiar logo head (lo mismo para el foot)
    // marcar como para eliminar!! y mostrar mensaje, o limpiar los campos de entrada
    $('#link-eliminar-logo-head').on('click', function () {
        // según si es para activar o desactivar
        if ( $('#elim_logo_head').val() == 1 ) {
            // activar
            $('#elim_logo_head').val('0');
            // cambiar el texto del mensaje
            $('#link-eliminar-logo-head').html('(eliminar <span id="eliminar-logo-head" class="glyphicon glyphicon-trash text-danger"></span>)');
        } else {
            // desactivar
            $('#elim_logo_head').val('1');
            $('#link-eliminar-logo-head').html('(se eliminará la imagen al actualizar, pulsa <span id="eliminar-logo-head" class="glyphicon glyphicon-repeat text-success"></span> para deshacer)');
        }
    });
    $('#link-eliminar-logo-foot').on('click', function () {
        // según si es para activar o desactivar
        if ( $('#elim_logo_foot').val() == 1 ) {
            // activar
            $('#elim_logo_foot').val('0');
            // cambiar el texto del mensaje
            $('#link-eliminar-logo-foot').html('(eliminar <span id="eliminar-logo-foot" class="glyphicon glyphicon-trash text-danger"></span>)');
        } else {
            // desactivar
            $('#elim_logo_foot').val('1');
            $('#link-eliminar-logo-foot').html('(se eliminará la imagen al actualizar, pulsa <span id="eliminar-logo-foot" class="glyphicon glyphicon-repeat text-success"></span> para deshacer)');
        }
    });
    // vaciar los campos de logo head! (luego lo mismo para el foot)
    $('#link-vaciar-logo-head').on('click', function () {
        // borrar fichero e imagen si había
        $('#logo_head').val('');
        eliminarVistaPrevia('img_logo_head');
        // y ocultar el link
        if ( !$('#link-vaciar-logo-head').hasClass('hide')) {
            $('#link-vaciar-logo-head').addClass('hide');
        }
    });
    $('#link-vaciar-new-logo-head').on('click', function () {
        // borrar fichero e imagen si había
        $('#logo_new_head').val('');
        eliminarVistaPrevia('img_new_logo_head');
        // y ocultar el link
        if ( !$('#link-vaciar-new-logo-head').hasClass('hide')) {
            $('#link-vaciar-new-logo-head').addClass('hide');
        }
    });
    $('#link-vaciar-logo-foot').on('click', function () {
        // borrar fichero e imagen si había
        $('#logo_foot').val('');
        eliminarVistaPrevia('img_logo_foot');
        // y ocultar el link
        if ( !$('#link-vaciar-logo-foot').hasClass('hide')) {
            $('#link-vaciar-logo-foot').addClass('hide');
        }
    });
    $('#link-vaciar-new-logo-foot').on('click', function () {
        // borrar fichero e imagen si había
        $('#logo_new_foot').val('');
        eliminarVistaPrevia('img_new_logo_foot');
        // y ocultar el link
        if ( !$('#link-vaciar-new-logo-foot').hasClass('hide')) {
            $('#link-vaciar-new-logo-foot').addClass('hide');
        }
    });

    // fichero de música subido correctamente
    // Guardamos el nombre y actualizar el botón
    $('#fichero-musica').on('fileuploaded', function(event, data, previewId, index) {
        var response = data.response;
        var nombre = data.response.fichero;
        // guardar nombre
        $('#ficheroSubido').val(nombre);
        // Cambiar el texto del botón
        // Si el nombre del fichero es mayor de 20 caracteres, meter "..."
        if (nombre.length > 20) {
            nombre = nombre.substr(0,15)+'...';
        }
        $('#btn-seleccionar-fichero').html('Seleccionar "'+nombre+'"');
        // activar boton
        $('#btn-seleccionar-fichero').prop('disabled', false);
    });

});