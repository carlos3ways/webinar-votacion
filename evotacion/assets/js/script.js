// variables globales: colores por defecto
var dfltRepuestaOk = '#c3d600';
var dfltRepuestaKo = '#CCCCCC';
var msgSinParticipantes = 'No se ha encontrado ningún participante.';

// Añade evento
function addEvento() {
    var evento = $("#add_new_evento #evento").val();
    var codigo_acceso = $("#add_new_evento #codigo_acceso").val();
    var fecha_inicio = $("#add_new_evento #fecha_inicio input").val();
    var fecha_fin = $("#add_new_evento #fecha_fin input").val();
    var tiempo = $("#add_new_evento #tiempo").val();
    var musica = $("#add_new_evento #musica").val();
    var activo = 0;
    if ( $('#add_new_evento #activo').is(':checked')) {  activo = 1; }
    var anonimo = 0;
    if ( $('#add_new_evento #anonimo').is(':checked')) {  anonimo = 1; }
    var autoranking = 0;
    if ( $('#add_new_evento #autoranking').is(':checked')) {  autoranking = 1; }
    var autosolucion = 0;
    if ( $('#add_new_evento #autosolucion').is(':checked')) {  autosolucion = 1; }

    // Como ahora hay ficheros, hay que pasar parametros con formData
    var fd = new FormData();
    fd.append("evento", evento);
    fd.append("codigo_acceso", codigo_acceso);
    fd.append("fecha_inicio", fecha_inicio);
    fd.append("fecha_fin", fecha_fin);
    fd.append("activo", activo);
    fd.append("anonimo", anonimo);
    fd.append("autoranking", autoranking);
    fd.append("autosolucion", autosolucion);
    fd.append("tiempo", tiempo);
    fd.append("musica", musica);
    var theme = 0;
    // si hay tema personalizado, enviar los datos extra
    if ( $('#add_new_evento #theme').is(':checked')) {
        theme = 1; 
        // Colores cabecera y pie
        var fondo_head = $("#add_new_evento #fondo_head").colorpicker('getValue');
        var fondo_foot = $("#add_new_evento #fondo_foot").colorpicker('getValue');
        var respuesta_ok = $("#add_new_evento #respuesta_ok").colorpicker('getValue');
        var respuesta_ko = $("#add_new_evento #respuesta_ko").colorpicker('getValue');
        var align_logo_head = $("#add_new_evento #align_logo_head").val();
        var align_logo_foot = $("#add_new_evento #align_logo_foot").val();
        fd.append("fondo_head", fondo_head);
        fd.append("fondo_foot", fondo_foot);
        fd.append("respuesta_ok", respuesta_ok);
        fd.append("respuesta_ko", respuesta_ko);
        fd.append("align_logo_head", align_logo_head);
        fd.append("align_logo_foot", align_logo_foot);
        // y los ficheros, solo si hay algo
        var logo_head = $("#add_new_evento #logo_new_head");
        var logo_foot = $("#add_new_evento #logo_new_foot");
        if (logo_head[0].files[0] !== undefined) {
            fd.append( 'logo_head', logo_head[0].files[0] );
        }
        if (logo_foot[0].files[0] !== undefined) {
            fd.append( 'logo_foot', logo_foot[0].files[0] );
        }
    }
    fd.append("theme", theme);

    $.ajax({
        url: 'ajax/addEventos.php',
        data: fd,
        processData: false,
        contentType: false,
        type: 'POST',
        success: function(data) {
                    $("#add_new_evento").modal("hide");
                    readEventos();
                    $("#add_new_evento #evento").val("");
                    $("#add_new_evento #codigo_acceso").val("");
                    $("#add_new_evento #fecha_inicio input").val("");
                    $("#add_new_evento #fecha_fin input").val("");
                    $("#add_new_evento #activo").val("");
                    $("#add_new_evento #anonimo").prop('checked',false);
                    $("#add_new_evento #autoranking").prop('checked',false);
                    $("#add_new_evento #autosolucion").prop('checked',true);
                    $("#add_new_evento #tiempo").val("");
                    $("#add_new_evento #theme").prop('checked',false);
                    $("#add_new_evento #fondo_head").colorpicker('setValue', '#FFFFFF');
                    $("#add_new_evento #fondo_foot").colorpicker('setValue', '#FFFFFF');
                    $("#add_new_evento #respuesta_ok").colorpicker('setValue', dfltRepuestaOk);
                    $("#add_new_evento #respuesta_ko").colorpicker('setValue', dfltRepuestaKo);
                 },
        error: function() {
                    alert('Falló la entrada de datos.');
               }
    });

}

// Lee registros
function readEventos() {
	$.get("ajax/readEventos.php", {}, function (data, status) {
		$(".contenido_eventos").html(data);
	});
}

// Eliminar un evento y todas sus preguntas/respuestas/votos....
function deleteEvento(id, fecha = null) {
    fecha_evento = new Date(fecha);
    dia_evento = new Date(fecha_evento.getFullYear(), fecha_evento.getMonth(), fecha_evento.getDate(), 0, 0, 0);
    now = new Date();
    if (dia_evento<now) {
        alert("No puedes borrar el evento por que ya ha iniciado o ya ha sucedido");
    } else {
        var conf = confirm("Vas a borrar un evento por completo. ¿Estás seguro?");
        if (conf == true) {
            $.post("ajax/deleteEvento.php", {
                            id: id
                    },
                    function (data, status) {
                            // reload Users by using readEventos();
                            readEventos();
                    }
            );
        }
    }
}


// Abrir el detalle de un evento para actualizar
// La lista de canciones del directorio de música debe recargarse cada vez
// Esa lista se recupera también en readEventoDetalle.php
function getEventoDetalle(id) {

    $.post("ajax/readEventoDetalle.php", {
                id: id
            },
            function (data, status) {
                    // PARSE json data
                    var a_event = JSON.parse(data);
                    $("#update_evento_modal #id").val(a_event.id);
                    $("#update_evento_modal #evento").val(a_event.evento);
                    $("#update_evento_modal #codigo_acceso").val(a_event.codigo_acceso);
                    $("#update_evento_modal #fecha_inicio input").val(a_event.fecha_inicio);
                    $("#update_evento_modal #fecha_fin input").val(a_event.fecha_fin);
                    $("#update_evento_modal #tiempo").val(a_event.tiempo);
                    
                    // hay que rellenar la lista de opciones de música
                    $("#update_evento_modal #musica").html(a_event.lista_musica);
                    $("#update_evento_modal #musica").selectpicker('refresh');
                    $("#update_evento_modal #musica").selectpicker('val',a_event.musica);
                    // cargar el color picker
                    // fondo del color que toca, o blanco (colorpicker no deja vaciarlo)
                    if (a_event.fondo_head !== null && a_event.fondo_head !== "") {
                        $("#update_evento_modal #fondo_head").colorpicker('setValue', a_event.fondo_head);
                    } else {
                        $("#update_evento_modal #fondo_head").colorpicker('setValue', '#FFFFFF');
                    }
                    if (a_event.fondo_foot !== null && a_event.fondo_foot !== "") {
                        $("#update_evento_modal #fondo_foot").colorpicker('setValue', a_event.fondo_foot);
                    } else {
                        $("#update_evento_modal #fondo_foot").colorpicker('setValue', '#FFFFFF');
                    }
                    if (a_event.fondo_respuesta_ok !== null && a_event.fondo_respuesta_ok !== "") {
                        $("#update_evento_modal #respuesta_ok").colorpicker('setValue', a_event.fondo_respuesta_ok);
                    } else {
                        $("#update_evento_modal #respuesta_ok").colorpicker('setValue', dfltRepuestaOk);
                    }
                    if (a_event.fondo_respuesta_ko !== null && a_event.fondo_respuesta_ko !== "") {
                        $("#update_evento_modal #respuesta_ko").colorpicker('setValue', a_event.fondo_respuesta_ko);
                    } else {
                        $("#update_evento_modal #respuesta_ko").colorpicker('setValue', dfltRepuestaKo);
                    }
                    // imagen HEAD que toca, o ninguna (sin clase "zoomable")
                    if (a_event.logo_head !== null && a_event.logo_head !== "") {
                        $("#update_evento_modal #img_logo_head").attr('src', dir_images_upload + "/" + a_event.logo_head + "?" + Date.now());
                        if ( ! $("#update_evento_modal #img_logo_head").hasClass('zoomable') ) {
                            $("#update_evento_modal #img_logo_head").addClass('zoomable');
                        }
                        // mostrar el link a eliminar
                        if ( $("#update_evento_modal #link-eliminar-logo-head").hasClass('hide') ) {
                            $("#update_evento_modal #link-eliminar-logo-head").removeClass('hide');
                        }
                        // marcar como que hay logo (eliminar/deshacer)
                        linkEliminar('head');
                        $("#update_evento_modal #hay_logo_head").val('1');
                    } else {
                        $("#update_evento_modal #img_logo_head").attr('src', '');
                        if ( $("#update_evento_modal #img_logo_head").hasClass('zoomable') ) {
                            $("#update_evento_modal #img_logo_head").removeClass('zoomable');
                        }
                        // ocultar el link a eliminar
                        if ( ! $("#update_evento_modal #link-eliminar-logo-head").hasClass('hide') ) {
                            $("#update_evento_modal #link-eliminar-logo-head").addClass('hide');
                        }
                        // marcar como que no hay logo (limpiar)
                        linkLimpiar('head');
                        $("#update_evento_modal #hay_logo_head").val('0');
                    }
                    // imagen FOOT que toca, o ninguna (sin clase "zoomable")
                    if (a_event.logo_foot !== null && a_event.logo_foot !== "") {
                        $("#update_evento_modal #img_logo_foot").attr('src', dir_images_upload + "/" + a_event.logo_foot + "?" + Date.now());
                        if ( ! $("#update_evento_modal #img_logo_foot").hasClass('zoomable') ) {
                            $("#update_evento_modal #img_logo_foot").addClass('zoomable');
                        }
                        // mostrar el link a eliminar
                        if ( $("#update_evento_modal #link-eliminar-logo-foot").hasClass('hide') ) {
                            $("#update_evento_modal #link-eliminar-logo-foot").removeClass('hide');
                        }
                        // marcar como que hay logo (eliminar/deshacer)
                        linkEliminar('foot');
                        $("#update_evento_modal #hay_logo_foot").val('1');
                    } else {
                        $("#update_evento_modal #img_logo_foot").attr('src', '');
                        if ( $("#update_evento_modal #img_logo_foot").hasClass('zoomable') ) {
                            $("#update_evento_modal #img_logo_foot").removeClass('zoomable');
                        }
                        // ocultar el link a eliminar
                        if ( ! $("#update_evento_modal #link-eliminar-logo-foot").hasClass('hide') ) {
                            $("#update_evento_modal #link-eliminar-logo-foot").addClass('hide');
                        }
                        // marcar como que no hay logo (limpiar)
                        linkLimpiar('foot');
                        $("#update_evento_modal #hay_logo_foot").val('0');
                    }
                    // y quitar el fichero seleccionado anterior (si lo hubiera)
                    $("#update_evento_modal #logo_head").val('');
                    $("#update_evento_modal #logo_foot").val('');
                    // y poner los eliminar a 0
                    $("#update_evento_modal #elim_logo_head").val(0);
                    $("#update_evento_modal #elim_logo_foot").val(0);
                    // la alineación de los logos
                    if (a_event.align_logo_head !== null && a_event.align_logo_head !== "") {
                        $("#update_evento_modal #align_logo_head").selectpicker('val', a_event.align_logo_head);
                    } else {
                        $("#update_evento_modal #align_logo_head").selectpicker('val', 'left');
                    }
                    if (a_event.align_logo_foot !== null && a_event.align_logo_foot !== "") {
                        $("#update_evento_modal #align_logo_foot").selectpicker('val', a_event.align_logo_foot);
                    } else {
                        $("#update_evento_modal #align_logo_foot").selectpicker('val', 'left');
                    }

                    if (a_event.theme == 0) { 
                        $("#update_evento_modal #theme").prop("checked",false);
                        $("#update_evento_modal #theme").val(a_event.theme);
                        // ocultar Personalizar estilos
                        $('.container-personalizar-estilo').hide();
                    } else {
                        $("#update_evento_modal #theme").prop("checked",true);
                        // mostrar Personalizar estilos
                        $('.container-personalizar-estilo').show();
                    }

                    if (a_event.anonimo == 1) { 
                        $("#update_evento_modal #anonimo").prop("checked",true);
                    } else {
                        $("#update_evento_modal #anonimo").prop("checked",false);
                    }

                    if (a_event.autoranking == 1) { 
                        $("#update_evento_modal #autoranking").prop("checked",true);
                    } else {
                        $("#update_evento_modal #autoranking").prop("checked",false);
                    }

                    if (a_event.autosolucion == 1) { 
                        $("#update_evento_modal #autosolucion").prop("checked",true);
                    } else {
                        $("#update_evento_modal #autosolucion").prop("checked",false);
                    }

                    if (a_event.activo == 1) { 
                        $("#update_evento_modal #activo").prop("checked",true);
                    } else {
                        $("#update_evento_modal #activo").prop("checked",false);
                    }

                    // cargar el audio (tanto si hay algo como si no, se limpia!)
                    cargarAudio('audio_evento', a_event.musica);
            }
    );

    $("#update_evento_modal").modal("show");
}

// mostrar la modal para actualizar la música de la pregunta
function showMusicaPregunta(id) {

    // recuperar los datos de la música
    $("#musica_pregunta_modal #id").val(id);
    $("#musica_pregunta_modal #texto-pregunta").html( $('textarea#txtpregunta'+id).text() );
    // recargar la lista de opciones y seleccionar la de la pregunta
    cargarListaMusica('musica_pregunta_modal', $('#musica-'+id).attr('data-musica') );

    // si hay musica tocando
    if (botonPlaying != '') {
        // imagen de play
        $(botonPlaying).removeClass('glyphicon-pause');
        $(botonPlaying).addClass('glyphicon-play');
        // parar la música
        gestionMusica('audio_pregunta', 'stop', 0);
    }

    // mostrar la modal
    $("#musica_pregunta_modal").modal("show");
    cargarAudio('audio_pregunta',$('#musica-'+id).attr('data-musica'));
}

// mostrar la modal para actualizar la imagen de la pregunta
function showImagenPregunta(id) {

    // recuperar los datos de la pregunta
    $("#imagen_pregunta_modal #id").val(id);
    $("#imagen_pregunta_modal #texto-pregunta").html( $('textarea#txtpregunta'+id).text() );
    // borrar si había alguna imagen
    $("#imagen_pregunta_modal #imagen_pregunta").val('');
    // desmarcar si se había eliminado
    $("#imagen_pregunta_modal #elim_imagen_pregunta").val('0');
    eliminarVistaPrevia('img_pregunta');
    // reiniciar link a eliminar (visible = false, contenido = 'eliminar')
    reiniciarEliminarVaciar('imagen-pregunta', false, 'eliminar');

    // Poner el estado actual si hay
    // si hay imagen:   - link eliminar
    //                  - cargar imagen
    //                  - marcar como que tiene imagen
    if ($('#imagen-'+id).attr('data-imagen') != '') {
        // link a eliminar
        reiniciarEliminarVaciar('imagen-pregunta', true, 'eliminar');
        // cargar imagen
        $("#imagen_pregunta_modal #img_pregunta").attr('src', dir_images_upload + "/" + $('#imagen-'+id).attr('data-imagen') + "?" + Date.now());
        // marcar como que tiene imagen
        $("#imagen_pregunta_modal #hay_imagen_pregunta").val('1');
    }
    // mostrar la modal
    $("#imagen_pregunta_modal").modal("show");
}

// mostrar la modal para actualizar/añadir la imagen de la respuesta
function showImagenRespuesta(id) {

    // recuperar los datos de la respuesta
    $("#imagen_respuesta_modal #id").val(id);
    $("#imagen_respuesta_modal #texto-respuesta").html( $('textarea#txtrespuesta'+id).text() );
    // borrar si había alguna imagen
    $("#imagen_respuesta_modal #imagen_respuesta").val('');
    // desmarcar si se había eliminado
    $("#imagen_respuesta_modal #elim_imagen_respuesta").val('0');
    eliminarVistaPrevia('img_respuesta');
    // reiniciar link a eliminar (visible = false, contenido = 'eliminar')
    reiniciarEliminarVaciar('imagen-respuesta', false, 'eliminar');

    // Poner el estado actual si hay
    // si hay imagen:   - link eliminar
    //                  - cargar imagen
    //                  - marcar como que tiene imagen
    if ($('#imagen-'+id).attr('data-imagen') != '') {
        // link a eliminar
        reiniciarEliminarVaciar('imagen-respuesta', true, 'eliminar');
        // cargar imagen
        $("#imagen_respuesta_modal #img_respuesta").attr('src', dir_images_upload + "/" + $('#imagen-'+id).attr('data-imagen') + "?" + Date.now());
        // marcar como que tiene imagen
        $("#imagen_respuesta_modal #hay_imagen_respuesta").val('1');
    }
    // mostrar la modal
    $("#imagen_respuesta_modal").modal("show");
}

// mostrar la modal para importar preguntas/respuestas
// id: evento
function showImportarPreguntas(id) {

    // guardar el evento
    $("#importar_preguntas_modal #idevento").val(id);
    // mostrar la modal
    $("#importar_preguntas_modal").modal("show");
}

// cargar un audio en el player, controlando caracteres HTML
function cargarAudio(idplayer, cancion) {

    var origen;
    if (cancion == '') {
        origen = '';
    } else {
        origen = dir_musica+"/"+cancion;
    }
    $('#'+idplayer).attr("src", origen);
    // subirle el volumen
    var audiop = $('#'+idplayer);
    audiop[0].volume = 1;
}

// arrancar/parar música en la lista de preguntas
// cambia los iconos de reproduccion/pausa de todas las preguntas
function playMusica(id) {

    var botonPulsado = '#musica-'+id;
    // solo si el boton no está disabled (si hay algo en data-musica
    if ($(botonPulsado).attr('data-musica') != '') {
        
        // dos escenarios:  arrancar música -> implica parar cualquier otro audio que sonara
        //                  parar música -> se para la que suena y punto
        // así pues, comprobamos qué icono tiene la pregunta correspondiente
        if ( $(botonPulsado).hasClass('glyphicon-play') ) {
            // arrancar la música
            $('#audio_pregunta').attr("src", dir_musica+"/" + $(botonPulsado).attr('data-musica'));
            gestionMusica('audio_pregunta', 'play');
            // quitar el play del boton actual (si hay)
            if (botonPlaying) {
                $(botonPlaying).removeClass('glyphicon-pause');
                $(botonPlaying).addClass('glyphicon-play');
            }
            $(botonPulsado).removeClass('glyphicon-play');
            $(botonPulsado).addClass('glyphicon-pause');
            botonPlaying = botonPulsado;
        } else {
            // estaba en play, solo lo paramos
            gestionMusica('audio_pregunta', 'stop');
            $(botonPulsado).removeClass('glyphicon-pause');
            $(botonPulsado).addClass('glyphicon-play');
        }

    }
}

// controla el arrancar o para la música si existe la canción
function gestionMusica(idplayer, accion, delay = 500) {

    var audiop = $('#'+idplayer);
    
    // si el elemento existe:
//    if (audiop !== undefined) {
    if (audiop.length) {
        switch(accion) {
            case 'play': // fade in
                         if (audiop[0].volume != 0) {
                             audiop[0].volume = 0;
                         }
                         audiop.animate({volume: 1}, delay);
                         audiop[0].play();
                         break;
            case 'stop': audiop.animate({volume: 0}, delay);
                         setTimeout(function(){
                             audiop[0].pause();
                             audiop[0].currentTime = 0;
                         },delay);
                         botonPlaying = '';
                         break;
            case 'pause': audiop.animate({volume: 0}, delay);
                          setTimeout(function(){
                             audiop[0].pause();
                             audiop.currentTime = 0;
                          },delay);
                          botonPlaying = '';
                          break;
        }
    }

}

// actualizar en la BD la música de la pregunta
function updateMusicaPregunta() {

    // recuperar los parámetros
    var idpregunta = $("#musica_pregunta_modal #id").val();
    var musica = $("#musica_pregunta_modal #musica").val();
    
    // Actualizar la BD
    var peticion = $.ajax({
                        method: "POST"
                      , url:    "ajax/updateMusicaPregunta.php"
                      , data:   { id: idpregunta, musica: musica }
    });
    peticion.done ( function (data, status) {
        $("#musica_pregunta_modal").modal("hide");
        // refrescar la pantalla
        window.location.reload();
    });
    peticion.fail( function () {
        alert('Se ha producido algún error!');
    });

}

// actualizar en la BD la música de la pregunta
function importarPreguntas() {

    // comprobar si hay fichero para subir
    var fichero = $("#importar_preguntas_modal #fichero-preguntas");
    if (fichero[0].files[0] !== undefined) {
    // post con el fichero
        var idevento = $("#importar_preguntas_modal #idevento").val();
        var fd = new FormData();
        fd.append("idevento", idevento);
        fd.append('fichero', fichero[0].files[0] );
        
        $.ajax({
            url: 'ajax/importarPreguntas.php',
            data: fd,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function(data) {
                        $("#importar_preguntas_modal").modal("hide");
                        window.location.reload();
                     },
            error: function() {
                        alert('Falló la subida o algo...');
                   }
        });
    } else {
        alert('Seleccciona algún fichero.');
    }

}

// actualizar en la BD la imagen de una pregunta o de una respuesta
// @input: 'pregunta'/'respuesta'
function updateImagen(tipo) {

    // recuperar los parametros
    var id = $("#imagen_"+tipo+"_modal #id").val();
    var imagen = $("#imagen_"+tipo+"_modal #imagen_"+tipo);
    var eliminar = $("#imagen_"+tipo+"_modal #elim_imagen_"+tipo).val();
    
    // meter los parámetros en FormatData, ya que hay un posible upload de fichero
    var fd = new FormData();
    fd.append("id", id);
    fd.append("eliminar", eliminar);
    if (imagen[0].files[0] !== undefined) {
        fd.append( 'imagen', imagen[0].files[0] );
    }

    // Actualizar la BD con la llamada correspondiente
    $.ajax({
        url: "ajax/updateImagen"+tipo[0].toUpperCase() + tipo.slice(1)+".php",
        data: fd,
        processData: false,
        contentType: false,
        type: 'POST',
        success: function(data) {
                    $("#imagen_"+tipo+"_modal").modal("hide");
                    // refrescar la pantalla
                    window.location.reload();
                 },
        error: function() {
                    alert('Falló la subida o algo...');
               }
    });

}

// actualizar datos de evento, incluyendo imagenes de estilo si las hay
function updateEvento() {
    var id = $("#update_evento_modal #id").val();
    var evento = $("#update_evento_modal #evento").val();
    var codigo_acceso = $("#update_evento_modal #codigo_acceso").val();
    var fecha_inicio= $("#update_evento_modal #fecha_inicio input").val();
    var fecha_fin = $("#update_evento_modal #fecha_fin input").val();
    var tiempo = $("#update_evento_modal #tiempo").val();
    var musica = $("#update_evento_modal #musica").val();
    var fondo_head = $("#update_evento_modal #fondo_head").colorpicker('getValue');
    var fondo_foot = $("#update_evento_modal #fondo_foot").colorpicker('getValue');
    var fondo_respuesta_ok = $("#update_evento_modal #respuesta_ok").colorpicker('getValue');
    var fondo_respuesta_ko = $("#update_evento_modal #respuesta_ko").colorpicker('getValue');
    var logo_head = $("#update_evento_modal #logo_head");
    var logo_foot = $("#update_evento_modal #logo_foot");
//    var elem = $("#update_evento_modal #align_logo_head");
//    var align_logo_head = elem.val();
    var elim_logo_head = $("#update_evento_modal #elim_logo_head").val();
    var elim_logo_foot = $("#update_evento_modal #elim_logo_foot").val();
    var align_logo_head = $("#update_evento_modal #align_logo_head").val();
    var align_logo_foot = $("#update_evento_modal #align_logo_foot").val();
    var activo = 0;
    var anonimo = 0;
    var autoranking = 0;
    var autosolucion = 0;
    var theme=0;

    if ($("#update_evento_modal #theme").is(':checked') ) {
        theme = 1;
    }
    if ($("#update_evento_modal #activo").is(':checked') ) {
        activo = 1;
    }
    if ($("#update_evento_modal #anonimo").is(':checked') ) {
        anonimo = 1;
    }
    if ($("#update_evento_modal #autoranking").is(':checked') ) {
        autoranking = 1;
    }
    if ($("#update_evento_modal #autosolucion").is(':checked') ) {
        autosolucion = 1;
    }

    // Actualizar
    // Como ahora hay ficheros, hay que pasar parametros con formData
    var fd = new FormData();
    fd.append("id", id);
    fd.append("evento", evento);
    fd.append("codigo_acceso", codigo_acceso);
    fd.append("fecha_inicio", fecha_inicio);
    fd.append("fecha_fin", fecha_fin);
    fd.append("activo", activo);
    fd.append("anonimo", anonimo);
    fd.append("autoranking", autoranking);
    fd.append("autosolucion", autosolucion);
    fd.append("tiempo", tiempo);
    fd.append("musica", musica);
    fd.append("theme", theme);
    fd.append("fondo_head", fondo_head);
    fd.append("fondo_foot", fondo_foot);
    fd.append("fondo_respuesta_ok", fondo_respuesta_ok);
    fd.append("fondo_respuesta_ko", fondo_respuesta_ko);
    fd.append("elim_logo_head", elim_logo_head);
    fd.append("elim_logo_foot", elim_logo_foot);
    fd.append("align_logo_head", align_logo_head);
    fd.append("align_logo_foot", align_logo_foot);
    // y los ficheros, solo si hay algo
    if (logo_head[0].files[0] !== undefined) {
        fd.append( 'logo_head', logo_head[0].files[0] );
    }
    if (logo_foot[0].files[0] !== undefined) {
        fd.append( 'logo_foot', logo_foot[0].files[0] );
    }

    $.ajax({
        url: 'ajax/updateEvento.php',
        data: fd,
        processData: false,
        contentType: false,
        type: 'POST',
        success: function(data) {
                    $("#update_evento_modal").modal("hide");
                    readEventos();
                 },
        error: function() {
                    alert('Falló la subida o algo...');
               }
    });

}


function addPregunta() {
    
    // recargar la lista de música
    cargarListaMusica('add_new_pregunta', '');
    // recuperar la lista de eventos
    $.post("ajax/readAllEventos.php", 
        function (data, status) {
            $("#add_new_pregunta").modal("show");
            camposSortable();
            $("#add_new_pregunta #eventos select").html(data);
            // como se ha convertido en select picker, hay que hacer un refresh
            $("#add_new_pregunta #eventos select").selectpicker('refresh');
            // eliminar si había algún audio
            cargarAudio('audio_new_pregunta', '');
        }
    );
}


// añadir pregunta y su lista de respuestas
function savePreguntayRespuestas()
{
    // array para las respuestas (ordenadas)
    var respuestas=[];
    var v_orden=1;

    // recuperar cada respuesta
    $('#add_new_pregunta #sortable li').each(function (i){
		var id = $(this).attr("id");
		var th = $(this).find('input').is(':checked');
		var respuesta =  $(this).find('.form-control').val();
                if (respuesta != '') {
                    var objrespuesta = {respuesta:respuesta, orden: (v_orden), correcta:th}; 
                    respuestas.push(objrespuesta);
                    v_orden++;
                }
    });

    // y también la pregunta
    var pregunta = $("#add_new_pregunta #pregunta").val();
    var id_evento = $("#add_new_pregunta #eventos option:selected" ).val();
    var musica =  $("#add_new_pregunta #musica").val();

    var activo = 0;
    if ( $('#add_new_pregunta #activo').is(':checked')) {  activo = 1; } 

    // añadir la música!!
    $.post("ajax/addPreguntasyRespuestas.php", {
            pregunta: pregunta,
            id_evento: id_evento,
            //tiempo_responder: tiempo_responder,
            activo: activo,
            respuestas: respuestas,
            musica: musica
    }, function (data, status) {
            location.reload(true);
    });
}


function saveResRespuestas(id_pregunta)
{
    var respuestas=[];
    var v_orden=1;
    $('#add_new_respuesta #sortable li').each(function (i){
            var id = $(this).attr("id");
            var th = $(this).find('input').is(':checked');
            var respuesta =  $(this).find('.form-control').val();
    if (respuesta != ''){
            var objrespuesta = {respuesta:respuesta, orden: (v_orden), correcta:th}; 
            respuestas.push(objrespuesta);
        v_orden++;
    }
    //	alert(id + " respuesta:" + respuesta + " posicion:" + (x) + " checkeado:" + th);
    });

    $.post("ajax/addRespuestas.php", {
            id_pregunta: id_pregunta,
            respuestas: respuestas
    }, function (data, status) {
            location.reload(true);
    });	
}

function addRespuestaAfter()
{	
	var nid = $("#add_new_pregunta #widget_answers li").length;
	nid = parseInt(nid)+1;

	$('<li id="respuesta_' + nid +'" class="ui-sortable-handle"><div class="input-group"><span class="input-group-addon"><input type="checkbox"> <i class="glyphicon glyphicon-move"></i></span><input class="form-control" name="option[]"  type="text"></div></li>')
	.insertAfter($('[id^="respuesta_"]').last());
}

function addRespuestaAfterRes()
{	
	var nid = $("#add_new_respuesta #widget_answers li").length;
	nid = parseInt(nid)+1;

	$('<li id="respuesta_' + nid +'" class="ui-sortable-handle"><div class="input-group"><span class="input-group-addon"><input type="checkbox"> <i class="glyphicon glyphicon-move"></i></span><input class="form-control" name="option[]"  type="text"></div></li>')
	.insertAfter($('[id^="respuesta_"]').last());
}

function camposSortable()
{
	$("#sortable").sortable({
	    /*stop: function(event, ui) {
	        alert("New position: " + ui.item.index());
	    }*/
	    start: function(e, ui) {
	        // creates a temporary attribute on the element with the old index
	        $(this).attr('data-previndex', ui.item.index());
	    },
	    update: function(e, ui) {
	        // gets the new and old index then removes the temporary attribute
	        var newIndex = ui.item.index();
	        var oldIndex = $(this).attr('data-previndex');
	        var element_id = ui.item.attr('id');
	        //alert('id of Item moved = '+element_id+' old position = '+oldIndex+' new position = '+newIndex);
	        $(this).removeAttr('data-previndex');
	    }
	});
//$("#sortable").disableSelection();
}

// guardar cambios en las preguntas
function savePreguntas() {

    // lista de preguntas activas
    var checkeados = $("[id^=pregunta_] input:checked").length;
    // si hay más de uno, es imposible :)
    if (checkeados<=1) {
            
            // guardar en un array la lista de preguntas y grabarlas en la BD.
            var preguntas=[];
            $('#lista_preguntas #preguntas_sortable li').each(function (i){
                    var id = $(this).attr("data-id");
                    var activo = $(this).find('input').is(':checked');
                    var formato = $(this).find('#formato-'+id).hasClass('active');
                    var pregunta =  $(this).find('.txtpregunta').val();
                    var objpregunta = {pregunta_id:id, pregunta:pregunta,  orden: (i+1), activo:activo, formato:formato}; 

                    // añadir la gestión del formato de respuestas
                    preguntas.push(objpregunta);
            });
                    $.post("ajax/updatePreguntas.php", {
                    preguntas: preguntas
            },  function (data, status) {
                    location.reload(true);
            });
    }
}

function saveRespuestas(){
    var respuestas=[];
    var id_pregunta = $('#pregunta').attr("data-id-pregunta");
    var txt_pregunta = $('#pregunta').val();
	$('#lista_respuestas #respuestas_sortable li').each(function (i){
		var id = $(this).attr("data-id");
		var es_correcta = $(this).find('input').is(':checked');
		var respuesta =  $(this).find('.txtrespuesta').val();
		var objrespuesta = {respuesta_id:id, respuesta:respuesta, orden: (i+1), es_correcta:es_correcta}; 
		respuestas.push(objrespuesta);
	});

	$.post("ajax/updateRespuestas.php", {
                respuestas: respuestas,
                id_pregunta: id_pregunta,
                txt_pregunta: txt_pregunta
            }, function (data, status) {
		location.reload(true);
	});
}


//exclusivas preguntas.php
function addPreEnPre(id) {
    // recargar la lista de música
    cargarListaMusica('add_new_pregunta', '');
    $("#add_new_pregunta").modal("show");
    camposSortable();
    var edata = '<select name="eventos"><option value="'+id+'"></option></select>';
    $("#add_new_pregunta #eventos").html(edata);
    $("#add_new_pregunta .modal-body .form-group").eq(0).hide();
}


function addResEnRes(id, id_pregunta) {
    $("#add_new_respuesta").modal("show");
    camposSortable();
    var edata = '<select name="eventos"><option value="'+id+'"></option></select>';
    $("#add_new_pregunta #eventos").html(edata);
    $("#add_new_pregunta .modal-body .form-group").eq(0).hide();
}

function preguntas_camposSortable()
{
    $("#preguntas_sortable").sortable({
        start: function(e, ui) {
            // creates a temporary attribute on the element with the old index
            $(this).attr('data-previndex', ui.item.index());
            // tener cuidado con los TINYMCE!!
            // se pierde la instancia al ordenar y el campo queda en blanco
            tinyMCE.triggerSave();
        },
        update: function(e, ui) {
            // gets the new and old index then removes the temporary attribute
            var newIndex = ui.item.index();
            var oldIndex = $(this).attr('data-previndex');
            var element_id = ui.item.attr('id');
           // alert('Xid of Item moved = '+element_id+' old position = '+oldIndex+' new position = '+newIndex);
            $(this).removeAttr('data-previndex');
            savePreguntas().click();
        },
        stop: function(e,ui) {
            // volver a refrescar los tinyMCE
            $(this).find('.txttinymce').each(function(){
                tinyMCE.execCommand( 'mceRemoveEditor', false, $(this).attr('id') );
                tinyMCE.execCommand( 'mceAddEditor', false, $(this).attr('id') );//                tinyMCE.get($(this).attr('id')).destroy();
            });            
        }
    });
}


function respuestas_camposSortable()
{
	$("#respuestas_sortable").sortable({
        start: function(e, ui) {
            // creates a temporary attribute on the element with the old index
            $(this).attr('data-previndex', ui.item.index());
            // tener cuidado con los TINYMCE!!
            // se pierde la instancia al ordenar y el campo queda en blanco
            tinyMCE.triggerSave();
        },
        update: function(e, ui) {
            // gets the new and old index then removes the temporary attribute
            var newIndex = ui.item.index();
            var oldIndex = $(this).attr('data-previndex');
            var element_id = ui.item.attr('id');
           // alert('id of Item moved = '+element_id+' old position = '+oldIndex+' new position = '+newIndex);
            $(this).removeAttr('data-previndex');

        },
        stop: function(e,ui) {
            // volver a refrescar los tinyMCE
            $(this).find('.txttinymce').each(function(){
                tinyMCE.execCommand( 'mceRemoveEditor', false, $(this).attr('id') );
                tinyMCE.execCommand( 'mceAddEditor', false, $(this).attr('id') );//                tinyMCE.get($(this).attr('id')).destroy();
            });            
        }
    });
}

function borrarPregunta(id){
	$("#confirm_delete_pregunta").modal("show");

	$('#delete_pregunta').on('click', function() {
		$.post("ajax/deletePreguntasyRespuestas.php", {
			id: id
		}, function (data, status) {
                        updateOrdenPreguntas(id);
			//location.reload(true);
		});
	});
}


function borrarRespuesta(id){
	$("#confirm_delete_respuesta").modal("show");

	$('#delete_respuesta').on('click', function() {
		$.post("ajax/deleteRespuesta.php", {
			id: id
		}, function (data, status) {
            updateOrdenRespuestas(id);
			
		});
	});
}



function updateOrdenRespuestas(id_borrada){
    var respuestas=[];
    var id_pregunta = $('#pregunta').attr("data-id-pregunta");
    var txt_pregunta = $('#pregunta').val();
    var v_orden=1;
    $('#lista_respuestas #respuestas_sortable li').each(function (i){

        var id = $(this).attr("data-id");
        if(id_borrada != id){
            var es_correcta = $(this).find('input').is(':checked');
            var respuesta =  $(this).find('.txtrespuesta').val();
            var objrespuesta = {respuesta_id:id, respuesta:respuesta, orden: v_orden, es_correcta:es_correcta}; 
            respuestas.push(objrespuesta);
            v_orden++;
        }
    });

    $.post("ajax/updateRespuestas.php", {
        respuestas: respuestas,
        id_pregunta: id_pregunta,
        txt_pregunta: txt_pregunta
    }, function (data, status) {
       location.reload(true);
    });
}



function updateOrdenPreguntas(id_borrada){
    var checkeados = $("[id^=pregunta_] input:checked").length;
    if(checkeados<=1){
        var preguntas=[];
        var v_orden=1;
        $('#lista_preguntas #preguntas_sortable li').each(function (i){
            var id = $(this).attr("data-id");
            if(id_borrada != id){
                var activo = $(this).find('input').is(':checked');
                var pregunta =  $(this).find('.txtpregunta').val();
                var objpregunta = {pregunta_id:id, pregunta:pregunta,  orden:  v_orden, activo:activo}; 
                preguntas.push(objpregunta);
                v_orden++;
            }
        });
            $.post("ajax/updatePreguntas.php", {
            preguntas: preguntas
        },  function (data, status) {
            location.reload(true);
        });
    }
}

function votosCero(id_pregunta,id_evento){
	$("#confirm_poner_cero").modal("show");

	$('#votacion_cero').on('click', function() {
		$.post("ajax/votosacero.php", {
			id_pregunta: id_pregunta,
			id_evento: id_evento
		}, function (data, status) {
			alert(data);
			location.reload(true);
		});
	});
}


function todosVotosCero(id_evento){
    $("#confirm_poner_todos_cero").modal("show");

    $('#votacion_todos_cero').on('click', function() {
        
        // borrar o no los participantes
        var participantes = 0;
        if($('#confirm_poner_todos_cero #participantes').prop("checked")){
            participantes = 1;
        }
        // realizar el borrado
        $.post("ajax/reiniciarEvento.php", {
            id_evento: id_evento,
            participantes: participantes
        }, function (data, status) {
            alert(data);
            location.reload(true);
        });
    });
}


function bootToggleChangeAndSave(){ 
	$('[id^=checkActivo]').change(function(e) {

        var esteid = $(this).attr('data-id');
        var cnt = $(this).attr('data-respuestas-count');

        var total_lista = $( "#preguntas_sortable li" ).length;

        if( $(this).prop('checked')){
            var total_lista = $( "#preguntas_sortable li" ).length;
            total_lista= total_lista -1;
            $( "#preguntas_sortable li" ).each(function( indexli ) {
               // console.log(indexli);
                var esteotro = $(this).attr('data-id');
                if( esteotro != esteid){
                  $("#checkActivo_"+esteotro).bootstrapToggle('off');
                }
                savePreguntas();

                // if (total_lista == indexli){
                //     if (cnt >0){
                //        savePreguntas();
                //     }
                //     else{
                //         alert('No se puede activar esta pregunta, ya que no tiene ninguna respuesta');
                //         location.reload(true);
                //     }
                // }
            });
        }

        if(!$(this).prop('checked')){
            var todassincheck=0;
             $( "#preguntas_sortable li" ).each(function( indexli ) {
                if(total_lista == (indexli+1)){
                   savePreguntas();
                }
             });
        }
    });
}


function saveUsuario (){
    var nombre= $("#add_new_usuario #nombre").val();
    var apellidos= $("#add_new_usuario #apellidos").val();
    var email= $("#add_new_usuario #email").val();
    var password= $("#add_new_usuario #password").val();
    var telefono= $("#add_new_usuario #telefono").val();
    var direccion= $("#add_new_usuario #direccion").val();
    var provincia= $("#add_new_usuario #provincia").val();

    var activo= 0;
    if($('#add_new_usuario #activo').prop("checked")){
        activo= 1;
    }
    
    var id_evento= $("#add_new_usuario #id_evento").val();

    var data = {
        nombre: nombre,
        apellidos: apellidos,
        email: email,
        password: password,
        telefono: telefono,
        direccion: direccion,
        provincia: provincia,
        activo: activo,
        id_evento: id_evento
    }

   $.ajax({
        url: 'ajax/addUsuario.php',
        method: 'POST',
        data: data
    })
   .done(function( msg ) {
        $("#add_new_usuario #nombre").val('');
        $("#add_new_usuario #apellidos").val('');
        $("#add_new_usuario #email").val('');
        $("#add_new_usuario #password").val('');
        $("#add_new_usuario #telefono").val('');
        $("#add_new_usuario #direccion").val('');
        $("#add_new_usuario #provincia").val('');
        $("#add_new_usuario #activo").val('');
 

        location.reload()
    });
}


function getUsuario(id) {
    $.post("ajax/readUsuarioDetalle.php", {
            id: id
        },
        function (data, status) {
            // PARSE json data
            var a_event = JSON.parse(data);

            if (!a_event.status) {
                $("#edit_usuario #nombre").val(a_event.nombre);
                $("#edit_usuario #apellidos").val(a_event.apellidos);
                $("#edit_usuario #email").val(a_event.email);
                $("#edit_usuario #password").val(a_event.password);
                $("#edit_usuario #telefono").val(a_event.telefono);
                $("#edit_usuario #direccion").val(a_event.direccion);
                $("#edit_usuario #provincia").val(a_event.provincia);
                $("#edit_usuario #id_evento").val(a_event.id_evento);
                $("#edit_usuario #id_usuario").val(a_event.id);

                if (a_event.activo == 1) {
                    $("#edit_usuario #activo").prop("checked",true);
                    $("#edit_usuario #activo").val(a_event.activo);
                } else {
                    $("#edit_usuario #activo").prop("checked",false);
                }
            }
        }
    );

    $("#edit_usuario").modal("show");
}



$('#editarUsuario').on('click', function() {
        var nombre = $("#edit_usuario #nombre").val();
        var apellidos = $("#edit_usuario #apellidos").val();
        var email = $("#edit_usuario #email").val();
        var password = $("#edit_usuario #password").val();
        var telefono = $("#edit_usuario #telefono").val();
        var direccion = $("#edit_usuario #direccion").val();
        var provincia = $("#edit_usuario #provincia").val();
        var id_evento = $("#edit_usuario #id_evento").val();
        var id_usuario = $("#edit_usuario #id_usuario").val();

        var activo= 0;
        if($('#edit_usuario #activo').prop("checked")){
            activo= 1;
        }

        var data = {
            nombre: nombre,
            apellidos: apellidos,
            email: email,
            password: password,
            telefono: telefono,
            direccion: direccion,
            provincia: provincia,
            activo: activo,
            id_evento: id_evento,
            id_usuario: id_usuario
        }

       $.ajax({
            url: 'ajax/updateUsuario.php',
            method: 'POST',
            data: data
        })
       .done(function( msg ) {
            $("#edit_usuario #nombre").val('');
            $("#edit_usuario #apellidos").val('');
            $("#edit_usuario #email").val('');
            $("#edit_usuario #password").val('');
            $("#edit_usuario #telefono").val('');
            $("#edit_usuario #direccion").val('');
            $("#edit_usuario #provincia").val('');
            $("#edit_usuario #activo").val('');
            $("#edit_usuario #id_evento").val('');
            $("#edit_usuario #id_usuario").val('');

            location.reload()
        });
 });


function borrarUsuario(id){
    $("#confirm_delete_usuario #delusu_id").val(id);
    $("#confirm_delete_usuario").modal("show");

    $('#delete_usuario').on('click', function(e) {       
        $.post("ajax/deleteUsuario.php", {
            id:  $("#confirm_delete_usuario #delusu_id").val()
        }, function (data, status) {
           location.reload(true);
        });
    });
}

// leer la imagen seleccionada en el <input type="file">
// y mostrar la imagen previa
function leerImagenURL(input, destino, zoomable = true) {
    var elDestino = $('#'+destino);
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            elDestino.attr('src', e.target.result);
            // si hay que hacerla zoomable
            if (zoomable) {
                // si no tiene el zoomable, se lo metemos
                if ( ! elDestino.hasClass('zoomable') ) {
                    elDestino.addClass('zoomable');
                }
            }
        };
        reader.readAsDataURL(input.files[0]);
    }
    // mostrar si corresponde, el link a limpiar
    switch (destino) {
        case 'img_logo_head':       if ($('#hay_logo_head').val() == 0) {
                                        if ( $("#update_evento_modal #link-vaciar-logo-head").hasClass('hide') ) {
                                            $("#update_evento_modal #link-vaciar-logo-head").removeClass('hide');
                                        }
                                    }
                                    break;
        case 'img_new_logo_head':   if ( $("#add_new_evento #link-vaciar-new-logo-head").hasClass('hide') ) {
                                        $("#add_new_evento #link-vaciar-new-logo-head").removeClass('hide');
                                    }
                                    break;
        case 'img_logo_foot':       if ($('#hay_logo_foot').val() == 0) {
                                        if ( $("#update_evento_modal #link-vaciar-logo-foot").hasClass('hide') ) {
                                            $("#update_evento_modal #link-vaciar-logo-foot").removeClass('hide');
                                        }
                                    }
                                    break;
        case 'img_new_logo_foot':   if ( $("#add_new_evento #link-vaciar-new-logo-foot").hasClass('hide') ) {
                                        $("#add_new_evento #link-vaciar-new-logo-foot").removeClass('hide');
                                    }
                                    break;
        case 'img_pregunta':        if ($('#hay_imagen_pregunta').val() == 1) {
                                        reiniciarEliminarVaciar('imagen-pregunta', true, 'deshacer');
                                        // reseteamos el bit de eliminar....
                                        $('#elim_imagen_pregunta').val('0');
                                    }
        case 'img_respuesta':       if ($('#hay_imagen_respuesta').val() == 1) {
                                        reiniciarEliminarVaciar('imagen-respuesta', true, 'deshacer');
                                        // reseteamos el bit de eliminar....
                                        $('#elim_imagen_respuesta').val('0');
                                    }
    }
}

// borrar la imagen (si la tuviera) del elemento de vista previa
function eliminarVistaPrevia(elemento) {
    var elElemento = $('#'+elemento);
    elElemento.attr('src', '');
    // si tiene el zoomable, quitarlo
    if ( elElemento.hasClass('zoomable') ) {
        elElemento.removeClass('zoomable');
    }
}

// oculta los links de eliminar (subida de logos para estilo personalizado)
function linkLimpiar(elemento) {
    switch (elemento) {
        case 'head':    if ( ! $("#update_evento_modal #link-eliminar-logo-head").hasClass('hide') ) {
                            $("#update_evento_modal #link-eliminar-logo-head").addClass('hide');
                        }
                        break;
        case 'foot':    if ( ! $("#update_evento_modal #link-eliminar-logo-foot").hasClass('hide') ) {
                            $("#update_evento_modal #link-eliminar-logo-foot").addClass('hide');
                        }
                        break;
    }
}

// muestra los links de eliminar (subida de logos para estilo personalizado)
// y oculta los de limpiar
function linkEliminar(elemento) {
    switch (elemento) {
        case 'head':    if ( ! $("#update_evento_modal #link-vaciar-logo-head").hasClass('hide') ) {
                            $("#update_evento_modal #link-vaciar-logo-head").addClass('hide');
                        }
                        if ( $("#update_evento_modal #link-eliminar-logo-head").hasClass('hide') ) {
                            $("#update_evento_modal #link-eliminar-logo-head").removeClass('hide');
                        }
                        break;
        case 'foot':    if ( ! $("#update_evento_modal #link-vaciar-logo-foot").hasClass('hide') ) {
                            $("#update_evento_modal #link-vaciar-logo-foot").addClass('hide');
                        }
                        if ( $("#update_evento_modal #link-eliminar-logo-foot").hasClass('hide') ) {
                            $("#update_evento_modal #link-eliminar-logo-foot").removeClass('hide');
                        }
                        break;
    }
}

// función para recargar la lista de ficheros de música del select
// recibe un parámetro para saber de donde se llamó
function cargarListaMusica(origen, opcion) {
    // primero recuperar la lista de opciones
    var pickerSel = $('#'+origen+' #musica');
    $.ajax({
        url: 'ajax/listaMusica.php',
        async: false, 
        success: function (data) {
            // cargar y refrescar la lista de opciones
            pickerSel.html(JSON.parse(data));
            pickerSel.selectpicker('refresh');
            pickerSel.selectpicker('val',opcion);
        }
    });
}

// función para ordenar
// Debido a la subida de ficheros, hay que reordenar el contenido del picker
function sortAlpha(a,b) { 
    if(a.name > b.name) {
        return 1;
    } else if(a.name < b.name) {
        return -1;
    }
    return 0;
}

// Selecciona el fichero subido (modal_subir_musica) como música del evento/pregunta
// Es necesario refrescar el picker, claro
function seleccionarMusica() {
    // recuperar el elemento al que ser refiere
    // pickerTipo y pickerOrigen tienen los valores para identificar el elemento, ya que todos se llaman "musica"
    var tipo = $('#subir_musica_modal #pickerTipo').val();
    var selectorTipo = '';
    if (tipo != '') {
        selectorTipo = '[data-tipo="'+tipo+'"]';
    } else {
        // necesario para cargar el audio al final
        tipo = 'evento';
        selectorTipo = ':not([data-tipo])';
    }
    var origen = $('#subir_musica_modal #pickerOrigen').val();
    var selectorOrigen = '';
    if (origen != '') {
        selectorOrigen = '[data-origen="'+origen+'"]';
    } else {
        selectorOrigen = ':not([data-origen])';
    }
    // buscamos el elemento filtrando con los selectores
    var pickerMusica = $('select.musica'+selectorTipo+selectorOrigen);
    // Ahora se debe añadir la opción del fichero nuevo
    // recuperar el valor
    var nuevoFichero = $('#ficheroSubido').val();
    // añadirlo al select
    pickerMusica.append($("<option></option>")
                    .attr("value",nuevoFichero)
                    .text(nuevoFichero));
    // reordenar el select: en 5 pasos
    // 1. guardar las opciones en un array
    var listaOpciones = $.map( pickerMusica.find('option'), function(n) {
                                    return { id:n.value, name:n.innerHTML }; 
                        });
    // 2. ordenar las opciones con la función para ordenar creada
    listaOpciones.sort(sortAlpha);
    
    // 3. Limpiar el picker
    pickerMusica.empty();
    
    // 4. Rellenarlo con las opciones ordenadas
    $.each(listaOpciones, function() {
        pickerMusica.append(
            $('<option></option>').val(this.id).html(this.name)
        );
    });

    // 5. Refrescar el picker
    pickerMusica.selectpicker('refresh');
    
    // seleccionar el nuevo valor y cargar el audio
    pickerMusica.selectpicker('val', nuevoFichero);
    cargarAudio('audio'+origen+'_'+tipo, nuevoFichero);

    // Finalmente, ocultar la modal.
    $('#subir_musica_modal').modal('hide');
    
}

// reiniciar el elemento correspondiente
function reiniciarEliminarVaciar(elementotxt, visibilidad, contenido) {
    
    var elemento = $("#link-eliminar-"+elementotxt);
    // visibilidad
    if (visibilidad) {
        if ( elemento.hasClass('hide') ) {
            elemento.removeClass('hide');
        }
    } else {
        if ( ! elemento.hasClass('hide') ) {
            elemento.addClass('hide');
        }
    }
    // contenido
    mensajeEliminarVaciar(elementotxt, contenido);
}

// actualizar el contenido del link de eliminar según el caso
function mensajeEliminarVaciar(elementotxt, contenido) {

    var elemento = $("#link-eliminar-"+elementotxt);
    // contenido
    switch (contenido) {
        case 'eliminar':    elemento.html('(eliminar <span id="eliminar-'+elementotxt+'" class="glyphicon glyphicon-trash text-danger"></span>)');
                            break;
        case 'deshacer':    elemento.html('(deshacer <span id="deshacer-'+elementotxt+'" class="glyphicon glyphicon-repeat text-success"></span>, o pulsa Cancelar)');
                            break;
        case 'vaciar':      elemento.html('(se eliminará la imagen al Actualizar, o pulsa Cancelar)');
                            break;
    }

}

// recuperar de la BD la lista de participantes
function leerParticipantes(evento, ruta, callback) {
    // llamada para recuperar el JSON de participantes
    $.ajax({
        url: ruta+'ajax/readParticipantes.php',
        data: { id_evento: evento },
        dataType: "json", 
        success: function (data) {
            // comprobar si la llamada fue exitosa
            if (data.success) {
                callback(data.participantes);
            } else {
                // si hay un error será porque la lista de participantes está vacía
                if (data.status == 1) {
                    callback(msgSinParticipantes);
                } else {
                    callback("Error recuperando participantes");
                }
            }
        },
        error: function () {
            callback("Error conectando al servidor");
        }
    });
}

// arrancar la votación
// pantalla de participantes (0) o primera pregunta (1)
function arrancarVotacion(destino, evento) {

    var idpregunta = $('#primera_pregunta').val();
    // controlar qué pantalla abrir
    if (destino == 0) {
        // pantalla de participantes
        window.location = 'participantes.php?id='+evento+'&pr='+idpregunta;
    } else {
        // primera pregunta (recuperar el ID de la pregunta)
        window.location = 'index.php?id='+evento+'&id_pregunta='+idpregunta;
    }
}

// lista de participantes en pantalla
// recibe un array de participantes o un string con un mensaje de error
function pintarParticipantes(listaP) {
    
    // DIV de la lista de participantes
    var divlista = $('#lista-participantes');
    var divSinParticipantes = $('#msg-sin-participantes');
    
    if (typeof listaP === 'string' || listaP instanceof String) {
        // es un string -> error
        // pero controlar si es SinParticipantes
        if (listaP == msgSinParticipantes) {
            var htmlSinParticipantes = '<div id="msg-sin-participantes">'+listaP+'</div>';
            divlista.html(htmlSinParticipantes);
        } else {
            // mostrar la alerta y matar el temporizador
            alert(listaP + '\nRecarga la página para recargar la lista de participantes.');
            clearInterval(timerParticipantes);
        }
    } else {
        // proceso de actualizar la lista de participantes
        // se muestra una lista de elementos float: left
        // por si existía, se borra el mensaje sin participantes
        if (divSinParticipantes.length) {
            divSinParticipantes.hide('slow', function () { divSinParticipantes.remove() });
        }

        // 1. Marcar todos los elementos como "no verificados"
        $('.participante').each( function () {
            if (! $(this).hasClass('noverificado')) {
                $(this).addClass('noverificado');
            }
        });
        // 2. Leer la lista de participantes y para cada uno:
        //      - si ya existía se desmarca de "no verificado"
        //      - si no existía, se añade
        for (var key in listaP) {
            // elemento
            elemento = $('#participante-'+listaP[key].id);
            // comprobar si existía
            if ( elemento.length ) {
                // desmarcar como noverificado
                elemento.removeClass('noverificado');
            } else {
                // añadir el elemento
                divlista.append('<div id="participante-'+listaP[key].id+'" class="participante" style="display:none">'+listaP[key].nickname+'</div>');
                $('#participante-'+listaP[key].id).show('slow');
            }
        }

        // 3. Eliminar todos los no verificados
        $('.participante.noverificado').each( function () {
            elemento = $(this);
            elemento.hide('slow', function () { elemento.remove() });
        });
    }
}
        

