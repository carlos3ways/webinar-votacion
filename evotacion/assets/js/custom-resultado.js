document.addEventListener ("keydown", function (zEvent) {
    //if (zEvent.ctrlKey  &&  zEvent.altKey  &&  zEvent.code === "KeyS") {

    //Eventos del mando a distancia
    if (zEvent.code === "PageUp") {
        console.log('boton izquierda');
    }

    if (zEvent.code === "PageDown") {
        console.log('boton derecha');
    }

    if (zEvent.code === "Period") {
        console.log('boton abajo');
    }
        
    if (zEvent.code === "Space") {
        $('.start').click();
        $("#menu-wrapper").click();
    }
    if (zEvent.code === "KeyP") {
        $('.pause').click();
        $("#menu-wrapper").click();
    }

    if (zEvent.code === "KeyR") {
        $('.reset').click();
        $("#menu-wrapper").click();
    }

    if (zEvent.code === "KeyC") {
        $('.ranking').click();
    }

    if (zEvent.code === "KeyS") {
        $('.solucion').click();
    }

    if (zEvent.code === "ArrowLeft") {
        if($('#pa_anterior').length){
            window.location =$('#pa_anterior').attr('href');
        }
    }

    if (zEvent.code === "ArrowRight") {
        if($('#pa_siguiente').length){
            window.location = $('#pa_siguiente').attr('href');
        }
    }

    if (zEvent.code === "KeyF") {
        if($('#final_evento').length){
            window.location = "final.php?id="+id_evento;
        }
    }

    if (zEvent.code === "KeyW") {
        $('#toggle-resultado').bootstrapToggle('toggle');
    }

    if (zEvent.code === "KeyV") {
        $('#toggle-vote-onoff').bootstrapToggle('toggle');
    }

     if (zEvent.code === "KeyU") {
        $('.repeat').click();
        $("#menu-wrapper").click();
    }

    if (typeof(zEvent.code) == "undefined"){
        //space
        if (zEvent.keyCode==32){
            $('.start').click();
            $("#menu-wrapper").click();
        }

        if (zEvent.keyCode == 80) {
            $('.pause').click();
            $("#menu-wrapper").click();
        }

        if (zEvent.keyCode == 82) {
            $('.reset').click();
            $("#menu-wrapper").click();
        }

        if (zEvent.keyCode == 67) {
            $('.ranking').click();
        }

        if (zEvent.keyCode == 83) {
            $('.solucion').click();
        }

        if (zEvent.keyCode == 37) {
            if($('#pa_anterior').length){
                window.location =$('#pa_anterior').attr('href');
            }
        }

        if (zEvent.keyCode == 39) {
            if($('#pa_siguiente').length){
                window.location = $('#pa_siguiente').attr('href');
            }
        }

        if (zEvent.keyCode == 70) {
            if($('#final_evento').length){
                window.location = "final.php?id="+id_evento;
            }
        }

        if (zEvent.keyCode == 87) {
            $('#toggle-resultado').bootstrapToggle('toggle');
        }

        if (zEvent.keyCode == 86) {
            $('#toggle-vote-onoff').bootstrapToggle('toggle');
        }

        if (zEvent.keyCode == 85) {
            $('.repeat').click();
            $("#menu-wrapper").click();
        }
    }
    
} );


$(".textura").on("click",function () {
    var effect = 'slide';
    var options = { direction: 'left' };
    var duration = 400;
    $('#menu-wrapper').toggle(effect, options, duration);
    $('#lado-wrapper').fadeIn(2000);
});

$("#detalle-preguntas").on("click",function () {
    var ojo = $('#detalle-preguntas span');
    if (ojo.hasClass('glyphicon-eye-open')) {
        $('.prg').each(function () {
            $(this).show('slide','',300);
        });
        ojo.removeClass('glyphicon-eye-open');
        ojo.addClass('glyphicon-eye-close');
    } else {
        //justo lo contrario
        $('.prg').each(function () {
            $(this).hide('slide', { direction: 'left'}, 300);
        });
        ojo.removeClass('glyphicon-eye-close');
        ojo.addClass('glyphicon-eye-open');
    }
    
});


$("#lado-wrapper").on("click",function () {
    var effect = 'slide';
    var options = { direction: 'left' };
    var duration = 400;
    $('#menu-wrapper').toggle(effect, options, duration);
    $(this).hide();
});


// Revelar cuál es la(s) solución(es) correcta(s)
$('.solucion').click( function () {
    // elementos con progress-ok, progress-bar-ok y txt0ok
    $('.progress-ok').each( function () {
       if ( ! $(this).hasClass('mostrar') ) {
           $(this).addClass('mostrar');
       } else {
           $(this).removeClass('mostrar');
       }
    });
    $('.progress-bar-ok').each( function () {
       if ( ! $(this).hasClass('mostrar') ) {
           $(this).addClass('mostrar');
       } else {
           $(this).removeClass('mostrar');
       }
    });
    $('.txt0ok').each( function () {
       if ( ! $(this).hasClass('mostrar') ) {
           $(this).addClass('mostrar');
       } else {
           $(this).removeClass('mostrar');
       }
    });
});


function cuentaAtras(segundos) {
    if (segundos == "") {
        segundos = 30;
    }
    var clock;

    clock = new FlipClock($('.clock'), segundos, {
        clockFace: 'Counter',
        autoStart: false,
        countdown: true,  
    });

    $('.pause').click(function() {
        timer.stop();
        gestionMusica('audio_pregunta', 'pause');
    });

    $('.start').on("click", function() {

        textsegundos = $('#tiempo_menu').val();

        if (textsegundos != '00') {
            $('#tiempo_menu').val('00');
            segundos = textsegundos;
            clock.setTime(segundos);
        }

        var c_se = clock.timer.factory.time.time;
        if ( c_se === segundos ) {
            console.log('====votacion activa====');
            processVotacion();
        } else {
            gestionMusica('audio_pregunta', 'play');
        }
        timer._destroyTimer();
        timer.start();

    });

    $('.reset').click(function() {
        timer._destroyTimer();
        clock.reset();
    });

    $('.ranking').click(function() {
        // si el timer está corriendo no se puede mostrar el ranking
        if (! timer.timer > 0) {
            // mostrar/ocultar el ranking
            ranking(id_evento,orden_pregunta,num_total_preguntas);
        }
    });
    
    
    // Use this code if you want to autoincrement the counter.
    var timer = new FlipClock.Timer(clock, {
        callbacks: {
            interval: function() {
                clock.decrement();
                if (clock.timer.factory.time.time == 0) {
                     this._destroyTimer();
                     this.stop();
                     console.log('====votacion finalizada====');
                     processVotacion('fin');
                }
            }
        }
    });
}