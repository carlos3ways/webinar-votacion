
$(function() {
    preguntas_camposSortable();
    bootToggleChangeAndSave();
});

$(document).ready(function(){
    
    // tooltip para los audios (nombre del mp3)
    $('[data-toggle="tooltip"]').tooltip(); 
    
    // programar el change del select musica
    $('.musica').on('change', function() {

        var valor = $(this).val();
        if (valor === "-") {
            // Subir un archivo!!
            // volver a "ningún audio seleccionado"
            $(this).selectpicker('val','');
            // llamar a la modal de seleccionar fichero
            // guardando el nombre del picker para refrescar y seleccionar la opción si corresponde
            $('#subir_musica_modal #pickerTipo').val($(this).data('tipo'));
            $('#subir_musica_modal #pickerOrigen').val($(this).data('origen'));
            // y limpiando botón, ficheroSubido, desactivar botón, mensajes de error o lo que sea de ejecuciones anteriores
            $('#subir_musica_modal #ficheroSubido').val('');
            $('#subir_musica_modal #btn-seleccionar-fichero').html('Seleccionar');
            $('#subir_musica_modal #btn-seleccionar-fichero').prop('disabled', true);
            $('#fichero-musica').fileinput('clear');
            // mostrar la modal
            $('#subir_musica_modal').modal('show');
        } else {
            // proceso normal de cargarAudio
            var nuevo = $(this).data("origen");
            if (nuevo === undefined) nuevo = '';
            // cargar el audio si el evento es del select
            var objetoid = $(this).attr('id');
            if ( objetoid == "musica" ) {
                cargarAudio('audio'+nuevo+'_pregunta', $(this).val());
            }
        }

    });

    $('#musica_pregunta_modal').on('hidden.bs.modal', function () {
        gestionMusica('audio_pregunta', 'stop');
    });

    $('#add_new_pregunta').on('hidden.bs.modal', function () {
        gestionMusica('audio_new_pregunta', 'stop');
    });

    // fichero de música subido correctamente
    // Guardamos el nombre y actualizar el botón
    $('#fichero-musica').on('fileuploaded', function(event, data, previewId, index) {
        var response = data.response;
        var nombre = data.response.fichero;
        // guardar nombre
        $('#ficheroSubido').val(nombre);
        // Cambiar el texto del botón
        // Si el nombre del fichero es mayor de 20 caracteres, meter "..."
        if (nombre.length > 20) {
            nombre = nombre.substr(0,15)+'...';
        }
        $('#btn-seleccionar-fichero').html('Seleccionar "'+nombre+'"');
        // activar boton
        $('#btn-seleccionar-fichero').prop('disabled', false);
    });
    
    // gestión de eliminar imagen-pregunta
    $('#link-eliminar-imagen-pregunta').on('click', function () {
        // según si es para activar o desactivar
        var idpregunta = $("#imagen_pregunta_modal #id").val();
        if ( $('#elim_imagen_pregunta').val() == 1 ) {
            // es un deshacer -> vamos al estado inicial
            showImagenPregunta( idpregunta );
        } else {
            // eliminar estaba a 0
            // controlar si fue un "eliminar" o un "deshacer"
            if ($('#deshacer-imagen-pregunta').length) {
                // es un deshacer -> vamos al estado inicial
                showImagenPregunta( idpregunta );
            } else {
                // era eliminar -> marcar eliminar y mostrar mensaje
                $('#elim_imagen_pregunta').val('1');
                mensajeEliminarVaciar('imagen-pregunta', 'vaciar');
            }
        }
    });

});

var botonPlaying='';