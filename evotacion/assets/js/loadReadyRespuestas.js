$(document).ready(function () {
    respuestas_camposSortable();
    
    // gestión de eliminar imagen-respuesta
    $('#link-eliminar-imagen-respuesta').on('click', function () {
        // según si es para activar o desactivar
        var idrespuesta = $("#imagen_respuesta_modal #id").val();
        if ( $('#elim_imagen_respuesta').val() == 1 ) {
            // es un deshacer -> vamos al estado inicial
            showImagenRespuesta( idrespuesta );
        } else {
            // eliminar estaba a 0
            // controlar si fue un "eliminar" o un "deshacer"
            if ($('#deshacer-imagen-respuesta').length) {
                // es un deshacer -> vamos al estado inicial
                showImagenRespuesta( idrespuesta );
            } else {
                // era eliminar -> marcar eliminar y mostrar mensaje
                $('#elim_imagen_respuesta').val('1');
                mensajeEliminarVaciar('imagen-respuesta', 'vaciar');
            }
        }
    });

});